/*
 * renders a OccupancyMap
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <gtk/gtk.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include "er_renderers.h"
#include <bot_core/bot_core.h>
#include <bot_vis/texture.h>
#include <bot_param/param_client.h>
#include <bot_frames/bot_frames.h>

#include <interfaces/map3d_interface.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/su_lcmtypes.h>
#include <er_lcmtypes/lcm_channel_names.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>


#define PARAM_SHOW_GMAPPER_MAP "Show Gmapper Map"
#define PARAM_MAP_MODE "Map Mode"
#define PARAM_DOWNSAMPLE "DownSample Large Maps"
#define PARAM_NAME_OPACITY "Opacity"

#define RENDERER_NAME "OccupancyMap"

typedef struct _RendererOccupancyMap RendererOccupancyMap;

typedef enum _map_mode_t {
    GMAPPER_MAP, FRONTIER_UTILITY, CAM_FRONTIER_UTILITY, NAVIGATOR_UTILITY, NAVIGATOR_COST, GRIDMAP
} map_mode_t;

struct _RendererOccupancyMap {
    BotRenderer renderer;
    BotEventHandler ehandler;
    lcm_t *lcm;
    BotGtkParamWidget *pw;
    gboolean param_draw_gmapper_map;
    map_mode_t param_map_mode;
    int downsample;
    GtkWidget *label;
    BotViewer *viewer;
    BotFrames *frames;

    erlcm_map_t* multi_map;
    erlcm_map_t map;
    erlcm_map_t frontier_utility_map;
    erlcm_map_t cam_frontier_utility_map;
    erlcm_map_t nav_utility_map;
    erlcm_map_t nav_cost_map;
    erlcm_map_t * draw_map; //pointer to the map that should be being drawn
    BotGlTexture *map2dtexture;
    int texture_count;
    BotGlTexture **map2dtexture_all;
    erlcm_tagged_node_list_t *places;
};

void draw_place(float x,float y,float radius)
{
    glColor4f(1,0,0,.6);
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(x, y);
    for (int i=0; i < 360; i++){
        float degInRad = i*M_PI/180;
        glVertex2f(x+sin(degInRad)*radius,y+cos(degInRad)*radius);
    } 
    glEnd();
}

void request_occupancy_map(lcm_t *lcm)
{
    /* subscripe to map, and wait for it to come in... */
    erlcm_map_request_msg_t msg;
    msg.utime =  bot_timestamp_now();
    msg.requesting_prog = "VIEWER";

    sulcm_map_request_msg_t_publish(lcm,"MAP_REQUEST_CHANNEL", &msg);
}

static void map3d_place_handler(const lcm_recv_buf_t *rbuf, const char *channel, const erlcm_tagged_node_list_t *msg, void *user)
{
    RendererOccupancyMap *self = (RendererOccupancyMap*) user;
    if(self->places !=NULL){
        erlcm_tagged_node_list_t_destroy(self->places);
    }
    self->places = erlcm_tagged_node_list_t_copy(msg);
    bot_viewer_request_redraw(self->viewer);
}

static void upload_map_texture(RendererOccupancyMap *self);


static void on_obstacle_map(const lcm_recv_buf_t *rbuf, const char *channel, 
                            const erlcm_gridmap_tile_t *msg, void *user)
{
    RendererOccupancyMap *self = (RendererOccupancyMap *) user;

    bot_viewer_request_redraw(self->viewer);
}

static void gridmap_handler(const lcm_recv_buf_t *rbuf, const char *channel, 
                            const erlcm_gridmap_t *msg, void *user)
{
    static erlcm_gridmap_t* staticmsg = NULL;
    if (staticmsg != NULL) {
        erlcm_gridmap_t_destroy(staticmsg);
    }

    staticmsg = erlcm_gridmap_t_copy(msg);

    RendererOccupancyMap *self = (RendererOccupancyMap*) user;
    erlcm_map_t *map = NULL;

    if ((strcmp(channel, GMAPPER_GRIDMAP_CHANNEL) == 0) || 
        (strcmp(channel, "MAP_SERVER")==0)) {
        fprintf(stdout,"New map received\n");
        map = &self->map;
    }
    else if (strcmp(channel, FRONTIER_UTILITY_MAP_CHANNEL) == 0) {
        map = &self->frontier_utility_map;
    }
    else if (strcmp(channel, CAM_FRONTIER_UTILITY_MAP_CHANNEL) == 0) {
        map = &self->cam_frontier_utility_map;
    }
    else if (strcmp(channel, NAVIGATOR_UTILITY_MAP_CHANNEL) == 0) {
        map = &self->nav_utility_map;
    }
    else if (strcmp(channel, "NAVIGATOR_COST_MAP") == 0) {
        map = &self->nav_cost_map;
    }
    else{
        //fprintf(stderr,"ERROR: UNHANDLED MAP channel\n");
        exit(1);
    }

    if (map->map != NULL)
        free(map->map);
    if (map->complete_map != NULL)
        free(map->complete_map);

    carmen3d_map_uncompress_lcm_map(map, staticmsg);

    //invert the map values
    for (int i = 0; i < map->config.x_size; i++) {
        for (int j = 0; j < map->config.y_size; j++) {
            map->map[i][j] = 1 - map->map[i][j];
            //shift the unexplored
            if (fabs(map->map[i][j] - .5) < .1)
                map->map[i][j] = .85;
        }
    }
    upload_map_texture(self);
    bot_viewer_request_redraw(self->viewer);
}



static void upload_map_texture(RendererOccupancyMap *self)
{
    int multi_map_ind = -1;
    //fprintf(stderr,"Uploading texture\n");

    switch (self->param_map_mode) {
        case GMAPPER_MAP:
            self->draw_map = &self->map;
            break;
        case FRONTIER_UTILITY:
            self->draw_map = &self->frontier_utility_map;
            break;
        case CAM_FRONTIER_UTILITY:
            self->draw_map = &self->cam_frontier_utility_map;
            break;
        case NAVIGATOR_UTILITY:
            self->draw_map = &self->nav_utility_map;
            break;
        case NAVIGATOR_COST:
            self->draw_map = &self->nav_cost_map;
            break;
    }

    float * draw_complete_map = NULL;
    static CvMat * draw_map_im = NULL;
    if (self->draw_map->map && self->param_draw_gmapper_map) {

        int maxDrawDim = 1024;
        static int old_x_size = 0;
        static int old_y_size = 0;
        int x_size = 0;
        int y_size = 0;
        if (self->downsample && (self->draw_map->config.x_size > maxDrawDim || self->draw_map->config.y_size > maxDrawDim)) {
            //HUGE map... need to downsample
            double AR = (double) self->draw_map->config.y_size / (double) self->draw_map->config.x_size;
            if (AR <= 1) {
                x_size = maxDrawDim;
                y_size = (int) (((double) maxDrawDim) * AR);
            }
            else {
                x_size = (int) (((double) maxDrawDim) * 1 / AR);
                y_size = maxDrawDim;
            }
            //      fprintf(stderr,"map is (%d,%d), downsampling to (%d,%d)\n",self->draw_map->config.x_size,self->draw_map->config.y_size,x_size,y_size);
            if (draw_map_im == NULL) {
                draw_map_im = cvCreateMat(x_size, y_size, CV_32FC1);
            }
            else {
                CvSize s = cvGetSize(draw_map_im);
                if (s.height != x_size || s.width != y_size) {
                    //need to resize matrix
                    cvReleaseMat(&draw_map_im);
                    draw_map_im = cvCreateMat(x_size, y_size, CV_32FC1);
                }
            }
            
            CvMat map_im = cvMat(self->draw_map->config.x_size, self->draw_map->config.y_size, CV_32FC1,
                                 self->draw_map->complete_map);
            cvResize(&map_im, draw_map_im, CV_INTER_NN);
            draw_complete_map = (float*) cvPtr2D(draw_map_im, 0, 0, NULL);
        }
        else {
            x_size = self->draw_map->config.x_size;
            y_size = self->draw_map->config.y_size;
            draw_complete_map = self->draw_map->complete_map;
        }
        
        // create the texture object if necessary
        if (self->map2dtexture == NULL || (x_size != old_x_size || y_size != old_y_size)) {
            if (self->map2dtexture != NULL)
                bot_gl_texture_free(self->map2dtexture);
            int data_size = x_size * y_size * sizeof(float);
            self->map2dtexture = bot_gl_texture_new(y_size, x_size, data_size);
            old_x_size = x_size;
            old_y_size = y_size;
        }
        
        bot_gl_texture_upload(self->map2dtexture, GL_LUMINANCE, GL_FLOAT, y_size * sizeof(float), draw_complete_map);
    }
    else {
        if (self->map2dtexture != NULL) {
            bot_gl_texture_free(self->map2dtexture);
            self->map2dtexture = NULL;
        }
    }    
}

static void OccupancyMap_draw(BotViewer *viewer, BotRenderer *renderer)
{
    RendererOccupancyMap *self = (RendererOccupancyMap*) renderer;

    glPushAttrib (GL_DEPTH_BUFFER_BIT | GL_ENABLE_BIT);
    glEnable (GL_DEPTH_TEST);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable (GL_COLOR_MATERIAL);


    if (self->map2dtexture && self->param_draw_gmapper_map) {
        glColor4d(1.0, 1.0, 1.0, 
                  bot_gtk_param_widget_get_double (self->pw, PARAM_NAME_OPACITY));

        double global_tl[3], global_br[3];
        global_tl[2] = 0;
        global_br[2] = 0;
        carmen3d_map3d_map_index_to_global_coordinates(&global_tl[0], &global_tl[1], self->draw_map->midpt,
                                                       self->draw_map->map_zero, self->draw_map->config.resolution, 0, 0);
        carmen3d_map3d_map_index_to_global_coordinates(&global_br[0], &global_br[1], self->draw_map->midpt,
                                                       self->draw_map->map_zero, self->draw_map->config.resolution, self->draw_map->config.x_size,
                                                       self->draw_map->config.y_size);

        double global_bl[] = {global_br[0], global_tl[1], 0};
        double global_tr[] = {global_tl[0], global_br[1], 0};


        // Transform the global coordinates into the local frame
        double local_tl[3], local_bl[3], local_tr[3], local_br[3];
        bot_frames_transform_vec (self->frames, "global", "local", global_tl, local_tl);
        bot_frames_transform_vec (self->frames, "global", "local", global_bl, local_bl);
        bot_frames_transform_vec (self->frames, "global", "local", global_tr, local_tr);
        bot_frames_transform_vec (self->frames, "global", "local", global_br, local_br);
 
        bot_gl_texture_draw_coords(self->map2dtexture, 
                                   local_tl[0], local_tl[1], 0, 
                                   local_bl[0], local_bl[1], 0,
                                   local_br[0], local_br[1], 0, 
                                   local_tr[0], local_tr[1], 0);
    }

    glPopAttrib ();
}

static void upload_map_texture_single(RendererOccupancyMap *self)
{
    int multi_map_ind = -1;
    //fprintf(stderr,"Uploading texture\n");
    switch (self->param_map_mode) {
        case GMAPPER_MAP:
            self->draw_map = &self->map;
            break;
        case FRONTIER_UTILITY:
            self->draw_map = &self->frontier_utility_map;
            break;
        case CAM_FRONTIER_UTILITY:
            self->draw_map = &self->cam_frontier_utility_map;
            break;
        case NAVIGATOR_UTILITY:
            self->draw_map = &self->nav_cost_map;
            break;
    }

    float * draw_complete_map = NULL;
    static CvMat * draw_map_im = NULL;
    if (self->draw_map->map && self->param_draw_gmapper_map) {
        int maxDrawDim = 1024;
        static int old_x_size = 0;
        static int old_y_size = 0;
        int x_size = 0;
        int y_size = 0;
        if (self->downsample && (self->draw_map->config.x_size > maxDrawDim || self->draw_map->config.y_size > maxDrawDim)) {
            //HUGE map... need to downsample
            double AR = (double) self->draw_map->config.y_size / (double) self->draw_map->config.x_size;
            if (AR <= 1) {
                x_size = maxDrawDim;
                y_size = (int) (((double) maxDrawDim) * AR);
            }
            else {
                x_size = (int) (((double) maxDrawDim) * 1 / AR);
                y_size = maxDrawDim;
            }
            //      fprintf(stderr,"map is (%d,%d), downsampling to (%d,%d)\n",self->draw_map->config.x_size,self->draw_map->config.y_size,x_size,y_size);
            if (draw_map_im == NULL) {
                draw_map_im = cvCreateMat(x_size, y_size, CV_32FC1);
            }
            else {
                CvSize s = cvGetSize(draw_map_im);
                if (s.height != x_size || s.width != y_size) {
                    //need to resize matrix
                    cvReleaseMat(&draw_map_im);
                    draw_map_im = cvCreateMat(x_size, y_size, CV_32FC1);
                }
            }

            CvMat map_im = cvMat(self->draw_map->config.x_size, self->draw_map->config.y_size, CV_32FC1,
                                 self->draw_map->complete_map);
            cvResize(&map_im, draw_map_im, CV_INTER_NN);
            draw_complete_map = (float*) cvPtr2D(draw_map_im, 0, 0, NULL);
        }
        else {
            x_size = self->draw_map->config.x_size;
            y_size = self->draw_map->config.y_size;
            draw_complete_map = self->draw_map->complete_map;
        }

        // create the texture object if necessary
        if (self->map2dtexture == NULL || (x_size != old_x_size || y_size != old_y_size)) {
            if (self->map2dtexture != NULL)
                bot_gl_texture_free(self->map2dtexture);
            int data_size = x_size * y_size * sizeof(float);
            self->map2dtexture = bot_gl_texture_new(y_size, x_size, data_size);
            old_x_size = x_size;
            old_y_size = y_size;
        }

        bot_gl_texture_upload(self->map2dtexture, GL_LUMINANCE, GL_FLOAT, y_size * sizeof(float), draw_complete_map);  
    }
    else {
        if (self->map2dtexture != NULL) {
            bot_gl_texture_free(self->map2dtexture);
            self->map2dtexture = NULL;
        }
    }
  
}

static void OccupancyMap_free(BotRenderer *renderer)
{
    free(renderer);
}

static int mouse_press(BotViewer *viewer, BotEventHandler *ehandler, const double ray_start[3], const double ray_dir[3],
                       const GdkEventButton *event)
{
    //  RendererOccupancyMap *self = (RendererOccupancyMap*) ehandler->user;
    return 0;
}

static void on_param_widget_changed(BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererOccupancyMap *self = (RendererOccupancyMap*) user;

    self->param_draw_gmapper_map = bot_gtk_param_widget_get_bool(pw, PARAM_SHOW_GMAPPER_MAP);
    self->param_map_mode = bot_gtk_param_widget_get_enum(self->pw, PARAM_MAP_MODE);
    self->downsample = bot_gtk_param_widget_get_bool(self->pw,PARAM_DOWNSAMPLE);
    upload_map_texture(self);
    bot_viewer_request_redraw(self->viewer);
}

static BotRenderer*
renderer_occupancy_map_new(BotViewer *viewer, int render_priority, BotParam * _param)
{
    RendererOccupancyMap *self = (RendererOccupancyMap*) calloc(1, sizeof(RendererOccupancyMap));
    BotRenderer *renderer = &self->renderer;
    self->viewer = viewer;
    self->lcm = bot_lcm_get_global (NULL);
    self->map.map = NULL;
    self->map.complete_map = NULL;
    self->map2dtexture = NULL;
    self->param_map_mode = GMAPPER_MAP;

    BotParam *param = _param;
    self->frames = bot_frames_get_global (self->lcm, param);

    renderer->draw = OccupancyMap_draw;
    renderer->destroy = OccupancyMap_free;
    renderer->widget = gtk_vbox_new(FALSE, 0);
    renderer->name = RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;

    BotEventHandler *ehandler = &self->ehandler;
    ehandler->name = RENDERER_NAME;
    ehandler->enabled = 1;
    ehandler->pick_query = NULL;
    ehandler->key_press = NULL;
    ehandler->hover_query = NULL;
    ehandler->mouse_press = mouse_press;
    ehandler->mouse_release = NULL;
    ehandler->mouse_motion = NULL;
    ehandler->user = self;

    bot_viewer_add_event_handler(viewer, &self->ehandler, render_priority);

    // --- SETUP SIDE BOX WIDGET
    self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());
    gtk_box_pack_start(GTK_BOX(renderer->widget), GTK_WIDGET(self->pw), TRUE, TRUE, 0);
    bot_gtk_param_widget_add_booleans(self->pw, 0, PARAM_SHOW_GMAPPER_MAP, 1, NULL);
    bot_gtk_param_widget_add_booleans(self->pw, 0, PARAM_DOWNSAMPLE, 1, NULL);
    bot_gtk_param_widget_add_double (self->pw, PARAM_NAME_OPACITY, 
                                     BOT_GTK_PARAM_WIDGET_SLIDER, 0, 1, 0.05, 0.5);


    self->param_draw_gmapper_map = bot_gtk_param_widget_get_bool(self->pw, PARAM_SHOW_GMAPPER_MAP);
    bot_gtk_param_widget_add_enum(self->pw, PARAM_MAP_MODE, BOT_GTK_PARAM_WIDGET_MENU, self->param_map_mode, "GMapping",
                                  GMAPPER_MAP, "Navigator Utility", NAVIGATOR_UTILITY, "Navigator Cost", NAVIGATOR_COST, "Frontier Utility", FRONTIER_UTILITY, "CAM Utility",
                                  CAM_FRONTIER_UTILITY, NULL);

    gtk_widget_show_all(renderer->widget);
    g_signal_connect(G_OBJECT(self->pw), "changed", G_CALLBACK(on_param_widget_changed), self);
    on_param_widget_changed(self->pw, "", self);


    erlcm_gridmap_t_subscribe(self->lcm, GMAPPER_GRIDMAP_CHANNEL, gridmap_handler, self);
    erlcm_gridmap_t_subscribe(self->lcm, "MAP_SERVER", gridmap_handler, self);
    erlcm_gridmap_t_subscribe(self->lcm, FRONTIER_UTILITY_MAP_CHANNEL, gridmap_handler, self);
    erlcm_gridmap_t_subscribe(self->lcm, NAVIGATOR_UTILITY_MAP_CHANNEL, gridmap_handler, self);
    erlcm_gridmap_t_subscribe(self->lcm, "NAVIGATOR_COST_MAP", gridmap_handler, self);
    erlcm_gridmap_t_subscribe(self->lcm, CAM_FRONTIER_UTILITY_MAP_CHANNEL, gridmap_handler, self);
    erlcm_tagged_node_list_t_subscribe(self->lcm, "TAGGED_NODES", map3d_place_handler, self);

    // Subscribe to the obstacle map for the sake of re-rendering the occupancy map with the most recent global-to-local
    erlcm_gridmap_tile_t_subscribe (self->lcm, "OBSTACLE_MAP", on_obstacle_map, self);

    request_occupancy_map(self->lcm);
    return &self->renderer;
}

void setup_renderer_occupancy_map (BotViewer *viewer, int priority, BotParam * param)
{
    BotRenderer* renderer = renderer_occupancy_map_new(viewer, priority, param);
    bot_viewer_add_renderer(viewer, renderer, priority);
}
