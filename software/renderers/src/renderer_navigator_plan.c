/**
 * Navigator Plan Renderer
 *
 * Subscribes to NAV_PLAN_CHANNEL point_list message.
 *
 * Draw a line showing the trajectory.
 *
 * Widget enables turning on/off trajectory drawing.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <gdk/gdkkeysyms.h>

#include <lcm/lcm.h>
#include <geom_utils/geometry.h>
#include <er_carmen/global.h>
#include "er_renderers.h"
#include "er_gl_utils.h"
#include <bot_core/bot_core.h>
#include <bot_vis/texture.h>
#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/su_lcmtypes.h>
#include <er_lcmtypes/lcm_channel_names.h>

#define RENDERER_NAME "Navigator Plan"

#define PARAM_SHOW_NAVIGATOR_PLAN "Show Navigator Plan"
#define PLACE_NAV_GOAL "Place Goal"
#define START_ROBOT "Go to Goal"
#define STOP_ROBOT "Stop Robot"
#define ADD_NEW_PLACE "Add New Place"
#define QUERY_PLACE "QUERY_PLACE"

typedef struct _RendererNavigatorPlan {
    BotRenderer renderer;
    BotEventHandler ehandler;

    lcm_t *lc;

    BotViewer *viewer;
    BotGtkParamWidget *pw;

    erlcm_point_list_t_subscription_t *navigator_plan_subscription;
    erlcm_point_list_t *navigator_plan;

    gboolean param_draw_navigator_plan;
    gboolean param_goal_placement;
    gboolean param_have_goal;
    gboolean param_add_loc;

    int portal_type; //0 for door , 1 for elevator
    float goal[2];

    int dragging;
    point2d_t drag_start_local;
    point2d_t drag_finish_local;
  
    int have_portal; 
    point2d_t portal[2];

    point2d_t goal_mean;
    double goal_theta;
    double goal_std;

    int sent_topo_request;

    GtkWidget *place_name_entry;
    GtkWidget *placename_dialog;
    GtkWidget *portal_name_entry;
    GtkWidget *portal_dialog;

    erlcm_goal_feasibility_query_t *goal_query_result; 
    erlcm_topology_t *topology; 

    GSList *portal_list;

} RendererNavigatorPlan;

void add_portal_to_list(RendererNavigatorPlan *self, erlcm_portal_node_t *portal){
    /*self->portal_list.list = (erlcm_portal_node_t *) realloc(self->portal_list.list, 
                                                             self->portal_list.no_portals + 1);
    
    self->portal_list.list + self->portal_list.no_portals = 
    self->portal_list.no_portals++;     */
    self->portal_list = g_slist_append(self->portal_list, erlcm_portal_node_t_copy(portal));
    
}

static void
recompute_particle_distribution(RendererNavigatorPlan *self)
{
    self->goal_mean = self->drag_start_local;
    double dx = self->drag_finish_local.x - self->drag_start_local.x;
    double dy = self->drag_finish_local.y - self->drag_start_local.y;

    double theta = atan2(dy,dx);
    self->goal_theta = theta;

    self->goal_std = sqrt(dx*dx + dy*dy);

}


static
void add_place_button(GtkWidget *button __attribute__ ((unused)),
                      gpointer user_data __attribute__ ((unused)))
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) user_data;
    char *errs;
    char name[100];
    strcpy(name, gtk_entry_get_text(GTK_ENTRY(self->place_name_entry)));
  
    fprintf(stderr,"\nPlace Name : %s (%f,%f)\n",name,self->goal_mean.x,self->goal_mean.y);

    
    sulcm_place_node_t msg;
    msg.name = strdup(name);
    msg.type = " ";
    msg.x = self->goal_mean.x;//x;
    msg.y = self->goal_mean.y;//y;
    msg.theta = self->goal_theta;//0;
    msg.std = self->goal_std; 
    sulcm_place_node_t_publish(self->lc, "ADD_PLACE_NODE", &msg);

    free(msg.name);

    self->param_add_loc = 0; 
    gtk_widget_destroy(self->placename_dialog);
}

void start_add_placename(double x, double y, double theta, double std, 
                         RendererNavigatorPlan *self)
{  
    static GtkWidget *name_label, *x_label, *y_label, *theta_label, *x_entry, *y_entry;
    static GtkWidget *theta_entry, *x_std_entry, 
        *y_std_entry, *theta_std_entry;
    int edit_place_id;

    GtkWidget *hbox, *label, *button;
    char buffer[10];

    self->placename_dialog = gtk_dialog_new();
    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (self->placename_dialog)->vbox),
                        hbox, TRUE, TRUE, 0);
    name_label = gtk_label_new("Place name: ");
    gtk_box_pack_start (GTK_BOX (hbox), name_label, TRUE, TRUE, 0);
    self->place_name_entry = gtk_entry_new_with_max_length(21);
    gtk_widget_set_usize(self->place_name_entry, 90, 20);
    gtk_box_pack_start (GTK_BOX(hbox), self->place_name_entry, TRUE, TRUE, 0);
  
    hbox = gtk_hbox_new(FALSE, 3);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (self->placename_dialog)->vbox),
                        hbox, TRUE, TRUE, 0);
  
    x_label = gtk_label_new("X: ");
    gtk_box_pack_start (GTK_BOX (hbox), x_label, TRUE, TRUE, 0);
    x_entry = gtk_entry_new_with_max_length(5);
    gtk_widget_set_usize(x_entry, 45, 20);
    gtk_box_pack_start (GTK_BOX (hbox), x_entry, TRUE, TRUE, 0);
    sprintf(buffer, "%.2f", x);
    gtk_entry_set_text(GTK_ENTRY(x_entry), buffer);

    y_label = gtk_label_new("Y: ");
    gtk_box_pack_start (GTK_BOX (hbox), y_label, TRUE, TRUE, 0);
    y_entry = gtk_entry_new_with_max_length(5);
    gtk_widget_set_usize(y_entry, 45, 20);
    gtk_box_pack_start (GTK_BOX (hbox), y_entry, TRUE, TRUE, 0);
    sprintf(buffer, "%.2f", y);
    gtk_entry_set_text(GTK_ENTRY(y_entry), buffer);
  
    theta_label = gtk_label_new("Theta (deg): ");
    gtk_box_pack_start (GTK_BOX (hbox), theta_label, TRUE, TRUE, 0);
    theta_entry = gtk_entry_new_with_max_length(5);
    gtk_widget_set_usize(theta_entry, 45, 20);
    gtk_box_pack_start (GTK_BOX (hbox), theta_entry, TRUE, TRUE, 0);

    sprintf(buffer, "%.2f", theta);
    gtk_entry_set_text(GTK_ENTRY(theta_entry), buffer);
  
    hbox = gtk_hbox_new(FALSE, 3);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (self->placename_dialog)->vbox),
                        hbox, TRUE, TRUE, 0);
  
    label = gtk_label_new("Tollerance : ");
    gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
    x_std_entry = gtk_entry_new_with_max_length(5);
    gtk_widget_set_usize(x_std_entry, 45, 20);
    gtk_box_pack_start (GTK_BOX (hbox), x_std_entry, TRUE, TRUE, 0);

    sprintf(buffer, "%.2f", std);
    gtk_entry_set_text(GTK_ENTRY(x_std_entry), buffer);
  
    hbox = GTK_DIALOG(self->placename_dialog)->action_area;
  
    button = gtk_button_new_with_label("OK");
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 5);
  
    gtk_signal_connect(GTK_OBJECT(button), "clicked", 
                       (GtkSignalFunc)add_place_button, self);
  
    button = gtk_button_new_with_label("Cancel");
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 5);		
  
    gtk_signal_connect_object(GTK_OBJECT(button),
                              "clicked", (GtkSignalFunc)gtk_widget_destroy, 
                              (gpointer)self->placename_dialog);	

    edit_place_id = -1;
    gtk_widget_show_all(self->placename_dialog);
}



static void navigator_plan_handler(const lcm_recv_buf_t *rbuf, const char *channel, const erlcm_point_list_t *msg,
                                   void *user)
{
    fprintf(stderr, "N");

    RendererNavigatorPlan *self = (RendererNavigatorPlan*) user;
    if (self->navigator_plan != NULL) {
        erlcm_point_list_t_destroy(self->navigator_plan);
    }
    self->navigator_plan = erlcm_point_list_t_copy(msg);

    bot_viewer_request_redraw(self->viewer);
}

static void  goal_query_result_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
                                        const erlcm_goal_feasibility_query_t * msg, void * user  __attribute__((unused)))
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) user;
    if(self->goal_query_result != NULL){
        erlcm_goal_feasibility_query_t_destroy(self->goal_query_result);
    }
    self->goal_query_result = erlcm_goal_feasibility_query_t_copy(msg); 

    bot_viewer_request_redraw(self->viewer);
}

static void topology_handler(const lcm_recv_buf_t *rbuf, const char *channel, const erlcm_topology_t *msg,
                             void *user)
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) user;

    if(self->topology){
        erlcm_topology_t_destroy(self->topology);
    }
    self->topology = erlcm_topology_t_copy(msg);

}

void activate_nav(RendererNavigatorPlan *self,int type)
{
    //self->active = 1;
    if(type==1){
        bot_viewer_set_status_bar_message(self->viewer, 
                                          "Click to Place the goal");
    }else if (type==2){//Start robot
        bot_viewer_set_status_bar_message(self->viewer, 
                                          "Started Robot");    
    }else if (type==3){//Start robot
        bot_viewer_set_status_bar_message(self->viewer, 
                                          "Stopped Robot");    
    }
    if(type==4){
        bot_viewer_set_status_bar_message(self->viewer, 
                                          "Click to Place new location");
    }
    if(type==5){
        bot_viewer_set_status_bar_message(self->viewer, 
                                          "Click to Place Portal");
    }
}

static void navigator_plan_renderer_free(BotRenderer *super)
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) super->user;
    if (self->navigator_plan) {
        erlcm_point_list_t_destroy(self->navigator_plan);
    }
    free(self);
}

void draw_filled_circle(float x,float y,float radius)
{
    glColor4f(1,0,0,.6);
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(x, y);
    for (int i=0; i < 360; i++){
        float degInRad = i*M_PI/180;
        glVertex2f(x+sin(degInRad)*radius,y+cos(degInRad)*radius);
    } 
    glEnd();
}

void draw_circle(float x,float y,float radius)
{
    glColor4f(0,0,0,.6);
    glBegin(GL_LINE_LOOP);
  
    for (int i=0; i < 360; i++)
        {
            float degInRad = i*M_PI/180;
            glVertex2f(x+cos(degInRad)*radius,y+sin(degInRad)*radius);
        } 
    glEnd();
}




static void navigator_plan_renderer_draw(BotViewer *viewer, BotRenderer *super)
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) super->user;

    if(!self->sent_topo_request){
        //send topo request
        erlcm_map_request_msg_t msg;
        msg.utime = bot_timestamp_now();
        msg.requesting_prog = "TOPO_NAV";
        erlcm_map_request_msg_t_publish(self->lc, "TOPOLOGY_REQUEST", &msg);
        self->sent_topo_request = 1;
    }

    glEnable (GL_LIGHTING);
    glDisable (GL_DEPTH_TEST);
    glEnable (GL_LINE_SMOOTH);
    glShadeModel (GL_SMOOTH);
  
    glLineWidth (2.0);
    float color_curr[3];

    color_curr[0] = 0.0;
    color_curr[1] = 0.0;
    color_curr[2] = 1.0;

    if(self->goal_query_result){
        //go through the list and draw the portals and the goal -- what to do about the 

        for(int i=0; i < self->goal_query_result->portal_list.no_of_portals; i++){
            glPushMatrix();                      
            
            double x0 = self->goal_query_result->portal_list.portals[i].xy0[0];
            double y0 = self->goal_query_result->portal_list.portals[i].xy0[1];

            glTranslatef(x0, y0, 0);
            bot_gl_draw_circle(0.5);
            glPopMatrix();
            //glVertex2f(x0,y0);

            glPushMatrix();             


            double x1 = self->goal_query_result->portal_list.portals[i].xy1[0];
            double y1 = self->goal_query_result->portal_list.portals[i].xy1[1];

            glTranslatef(x1, y1, 0);
            bot_gl_draw_circle(0.5);
            glPopMatrix();
            //            glVertex2f(x1,y1);
        }

        //glEnd();

        for(int i=0; i < self->goal_query_result->portal_list.no_of_portals; i++){
            double x0 = self->goal_query_result->portal_list.portals[i].xy0[0];
            double y0 = self->goal_query_result->portal_list.portals[i].xy0[1];

            double x1 = self->goal_query_result->portal_list.portals[i].xy1[0];
            double y1 = self->goal_query_result->portal_list.portals[i].xy1[1];

            double pos[3] = {(x0+x1)/2, (y0+y1)/2, 0};
            char buf[20];
            sprintf(buf, "Node Order : %d",i);
            bot_gl_draw_text(pos, NULL, buf, 0);
        }
    }

    if(self->topology){
        for(int i=0; i < self->topology->portal_list.no_of_portals; i++){
            erlcm_portal_node_t *c_portal = &self->topology->portal_list.portals[i];

            if(c_portal->type ==  ERLCM_PORTAL_NODE_T_PORTAL_DOOR){
                color_curr[0] = 0.0;
                color_curr[1] = 0.0;
                color_curr[2] = 1.0;
            }            
            else if(c_portal->type ==  ERLCM_PORTAL_NODE_T_PORTAL_ELEVATOR){
                color_curr[0] = 1.0;
                color_curr[1] = 0.0;
                color_curr[2] = 0.0;
            }
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_curr);
            glBegin(GL_LINE_STRIP);  
            glVertex2f(c_portal->xy0[0],c_portal->xy0[1]);
            glVertex2f(c_portal->xy1[0],c_portal->xy1[1]);
            glEnd();

            double pos[3] = {(c_portal->xy0[0] + c_portal->xy1[0])/2 ,(c_portal->xy0[1] + c_portal->xy1[1])/2,0};
            if(c_portal->type == 0){
                bot_gl_draw_text(pos, NULL, "Door", 0);
            }
            else if(c_portal->type == 1){
                bot_gl_draw_text(pos, NULL, "Elevator", 0);
            }
            glLineWidth (3.0);
            glPushMatrix();
            glTranslatef(c_portal->xy0[0],c_portal->xy0[1],0);
            bot_gl_draw_circle(0.3);         
            glPopMatrix();

            
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_curr);

            glPushMatrix();
            glTranslatef(c_portal->xy1[0],c_portal->xy1[1],0);
            bot_gl_draw_circle(0.3);         
            glPopMatrix();
            glLineWidth (2.0);
        }

        for(int i=0; i < self->topology->place_list.place_count; i++){
            erlcm_place_node_t *place = &self->topology->place_list.trajectory[i]; 
            
            color_curr[0] = 0.0;
            color_curr[1] = 1.0;
            color_curr[2] = 0.0;

            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_curr);

            glLineWidth (3.0);
            glPushMatrix();
            glTranslatef(place->x,place->y,0);
            bot_gl_draw_circle(0.3);

            color_curr[0] = 0.0;
            color_curr[1] = 0.0;
            color_curr[2] = 0.0;
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_curr);
            double pos[3] = {0,0,0};
            bot_gl_draw_text(pos, NULL, place->name, 0);
            color_curr[0] = 1.0;
            color_curr[1] = 0.0;
            color_curr[2] = 0.0;
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_curr);
            
            glBegin(GL_LINE_STRIP);  
            glVertex2f(0.0,0.0);
            glVertex2f(place->std*cos(place->theta), place->std*sin(place->theta));
            glEnd();

            glPopMatrix();            
           
            glLineWidth (2.0);
        }        
    }

    if(self->portal_list){
        GSList *curr_portal = self->portal_list;
        while(curr_portal){
            erlcm_portal_node_t *c_portal = (erlcm_portal_node_t *) curr_portal->data;
            if(c_portal->type ==  ERLCM_PORTAL_NODE_T_PORTAL_DOOR){
                color_curr[0] = 0.0;
                color_curr[1] = 0.0;
                color_curr[2] = 1.0;
            }            
            else if(c_portal->type ==  ERLCM_PORTAL_NODE_T_PORTAL_ELEVATOR){
                color_curr[0] = 1.0;
                color_curr[1] = 0.0;
                color_curr[2] = 0.0;
            }
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_curr);
            glBegin(GL_LINE_STRIP);  
            glVertex2f(c_portal->xy0[0],c_portal->xy0[1]);
            glVertex2f(c_portal->xy1[0],c_portal->xy1[1]);
            glEnd();

            glLineWidth (3.0);
            glPushMatrix();
            glTranslatef(c_portal->xy0[0],c_portal->xy0[1],0);
            bot_gl_draw_circle(0.3);         
            glPopMatrix();

            
            glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_curr);

            glPushMatrix();
            glTranslatef(c_portal->xy1[0],c_portal->xy1[1],0);
            bot_gl_draw_circle(0.3);         
            glPopMatrix();
            glLineWidth (2.0);
            curr_portal = g_slist_next(curr_portal);
        }
        
    }
    
    else{
    
        if(self->param_add_loc){
            color_curr[0] = 1.0;
            color_curr[1] = 0.0;
            color_curr[2] = 0.0;
        }
        else{
            color_curr[0] = 0.0;
            color_curr[1] = 1.0;
            color_curr[2] = 0.0;
        }
        glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_curr);

        //draw the new goal region
        glTranslatef(self->goal_mean.x, self->goal_mean.y, 0);
        bot_gl_draw_circle(self->goal_std);

        glBegin(GL_LINE_STRIP);  
        glVertex2f(0.0,0.0);
    
        glVertex2f(self->goal_std*cos(self->goal_theta),self->goal_std*sin(self->goal_theta));

        glEnd();

        glTranslatef(-self->goal_mean.x, -self->goal_mean.y, 0);
    }
    if (self->navigator_plan && self->param_draw_navigator_plan) {

        glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_POINT_BIT | GL_CURRENT_BIT);
        glLineWidth(2);
        glBegin(GL_LINE_STRIP);
        glColor3d(0.8, 0.2, 0.2);
        for (int i = 0; i < self->navigator_plan->num_points; i++) {
            glVertex3dv(point3d_as_array(&self->navigator_plan->points[i]));
        }
        glEnd();
        glPopAttrib();
    }

  
  
}

void publish_mission_control(lcm_t *lc,int type){//erlcm_mission_control_type_t type){
    erlcm_mission_control_msg_t msg;
    msg.utime = bot_timestamp_now();
    msg.type = type;
    erlcm_mission_control_msg_t_publish(lc, MISSION_CONTROL_CHANNEL, &msg);
}

static void on_param_widget_changed(BotGtkParamWidget *pw, const char *name, void *user)
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) user;

    self->param_draw_navigator_plan = bot_gtk_param_widget_get_bool(pw, PARAM_SHOW_NAVIGATOR_PLAN);

    if(!strcmp(name, PLACE_NAV_GOAL)) {
        activate_nav(self,1);
        self->param_goal_placement = 1;
        bot_viewer_request_pick (self->viewer, &(self->ehandler));
        //fprintf(stderr,"Nav Goal\n");
    }
    else if(!strcmp(name, START_ROBOT)){
        activate_nav(self,2);
        //start navigator to issue commands to the robot
        publish_mission_control(self->lc, ERLCM_MISSION_CONTROL_MSG_T_NAVIGATOR_GO);
        //publish speech command go message
        erlcm_speech_cmd_t msg;
        msg.cmd_type = "FOLLOWER";
        msg.cmd_property = "GO";
        erlcm_speech_cmd_t_publish(self->lc, "WAYPOINT_NAVIGATOR", &msg);
    }
    else if(!strcmp(name, STOP_ROBOT)) {
        activate_nav(self,3);//pause the navigator
        publish_mission_control(self->lc, ERLCM_MISSION_CONTROL_MSG_T_NAVIGATOR_PAUSE);
        erlcm_speech_cmd_t msg;
        msg.cmd_type = "FOLLOWER";
        msg.cmd_property = "STOP";
        erlcm_speech_cmd_t_publish(self->lc, "WAYPOINT_NAVIGATOR", &msg);
    }
    else if(!strcmp(name, ADD_NEW_PLACE)) {
        self->param_add_loc = 1;
        activate_nav(self,4);
        fprintf(stderr,"Adding New Place : %d\n", self->param_add_loc);
    }
    else if(!strcmp(name, QUERY_PLACE)){
        //send query msg - fixed query for now 
        sulcm_place_node_t msg;
        msg.name = "lounge";
        msg.type = " ";
        msg.x = 0;
        msg.y = 0;
        msg.theta = 0;
        msg.std = 0;
        sulcm_place_node_t_publish(self->lc, "GOAL_NODE", &msg);
    }

    bot_viewer_request_redraw(self->viewer);
}


static int key_press (BotViewer *viewer, BotEventHandler *ehandler, 
                      const GdkEventKey *event)
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) ehandler->user;
    
    if (self->param_goal_placement && event->keyval == GDK_Escape) {
        ehandler->picking = 0;
        self->param_goal_placement = 0;

        bot_gtk_param_widget_set_enabled (self->pw, PLACE_NAV_GOAL, TRUE);

        return 1;
    }

    return 0;
}


static int mouse_motion (BotViewer *viewer, BotEventHandler *ehandler,
                         const double ray_start[3], const double ray_dir[3], 
                         const GdkEventMotion *event)
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) ehandler->user;

    // Is this mouse_motion meant for us
    if(!self->dragging || (self->param_goal_placement == 0 && self->param_add_loc ==0) ) 
        return 0;

    point2d_t drag_pt_local;
    if (0 != geom_ray_z_plane_intersect_3d(POINT3D(ray_start), 
                                           POINT3D(ray_dir), 0, &drag_pt_local)) {
        return 0;
    }

    self->drag_finish_local = drag_pt_local;
    recompute_particle_distribution(self);

    bot_viewer_request_redraw(self->viewer);
    return 1;
}

static int mouse_release(BotViewer *viewer, BotEventHandler *ehandler, const double ray_start[3], 
                         const double ray_dir[3], const GdkEventButton *event)
{
    
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) ehandler->user;
  
    // Was the mouse_release event meant for us.
    if(self->param_goal_placement == 1){
      
        if (self->dragging) {
            self->dragging = 0;    
        }
      
        self->param_goal_placement = 0;
      
        self->param_have_goal = 1;
        self->goal[0] = self->goal_mean.x; 
        self->goal[1] = self->goal_mean.y; 
      
        erlcm_point_t goal;
        memset(&goal, 0, sizeof(carmen_point_t));
        goal.x = self->goal_mean.x; 
        goal.y = self->goal_mean.y; 
        //goal.z = 0.5; //doesnt matter
        goal.yaw = self->goal_theta;

        erlcm_navigator_goal_msg_t msg;
        msg.goal = goal;
        msg.use_theta = 1;//0;
        msg.utime = bot_timestamp_now();
        msg.nonce = random();
        msg.sender = ERLCM_NAVIGATOR_GOAL_MSG_T_SENDER_WAYPOINT_TOOL;
        erlcm_navigator_goal_msg_t_publish(self->lc, "NAV_GOAL_LOCAL", &msg);

        fprintf(stderr, "Goal: x %f y %f z %f yaw %f\n", goal.x, goal.y, goal.z, goal.yaw);
      
        bot_viewer_set_status_bar_message(self->viewer,"Current Goal is at (%f,%f)",self->goal[0],self->goal[1]);
        bot_viewer_request_redraw (self->viewer);

        ehandler->picking = 0;
        return 1;
    }

    // Was the mouse_release event meant for us.
    if(self->param_add_loc== 1){
      
        if (self->dragging) {
            self->dragging = 0;    
        }
      
        self->param_add_loc = 0;
      
        self->param_have_goal = 1;
        self->goal[0] = self->goal_mean.x; 
        self->goal[1] = self->goal_mean.y; 
      

        start_add_placename(self->goal_mean.x, self->goal_mean.y, self->goal_theta,
                            self->goal_std, self);
      
           
        bot_viewer_set_status_bar_message(self->viewer,"New Place is at (%f,%f)",self->goal[0],self->goal[1]);
        bot_viewer_request_redraw (self->viewer);

        ehandler->picking = 0;
        return 1;
    }

    ehandler->picking = 0;

    // If we've gotten here, the event wasn't meant for us.
    return 0;
}
static int mouse_press(BotViewer *viewer, BotEventHandler *ehandler, const double ray_start[3], 
                       const double ray_dir[3], const GdkEventButton *event)
{
    
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) ehandler->user;

    self->dragging = 0;

    // Is this event meant for us?
    if (event->button==1 && self->param_goal_placement==1 && ehandler->picking) {

        point2d_t click_pt_local;
      
        if (0 != geom_ray_z_plane_intersect_3d(POINT3D(ray_start), 
                                               POINT3D(ray_dir), 0, &click_pt_local)) {
            bot_viewer_request_redraw(self->viewer);
            self->param_goal_placement = 0;
            return 0;
        }
      
        self->dragging = 1;
      
        self->drag_start_local = click_pt_local;
        self->drag_finish_local = click_pt_local;
      
        recompute_particle_distribution(self);
      
        bot_viewer_request_redraw(self->viewer);
        return 1;
    }
    else if (self->param_add_loc==1){// && ehandler->picking) {

        point2d_t click_pt_local;
      
        if (0 != geom_ray_z_plane_intersect_3d(POINT3D(ray_start), 
                                               POINT3D(ray_dir), 0, &click_pt_local)) {
            bot_viewer_request_redraw(self->viewer);
            self->param_goal_placement = 0;
            return 0;
        }
      
        self->dragging = 1;
      
        self->drag_start_local = click_pt_local;
        self->drag_finish_local = click_pt_local;
      
        recompute_particle_distribution(self);
      
        bot_viewer_request_redraw(self->viewer);
        return 1;
    }
    
    return 0;
}


static void on_load_preferences(BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererNavigatorPlan *self = user_data;
    bot_gtk_param_widget_load_from_key_file(self->pw, keyfile, RENDERER_NAME);
}

static void on_save_preferences(BotViewer *viewer, GKeyFile *keyfile, void *user_data)
{
    RendererNavigatorPlan *self = user_data;
    bot_gtk_param_widget_save_to_key_file(self->pw, keyfile, RENDERER_NAME);
}

void navigator_plan_renderer_to_viewer(BotViewer *viewer, int render_priority, lcm_t *lcm)
{
    RendererNavigatorPlan *self = (RendererNavigatorPlan*) calloc(1, sizeof(RendererNavigatorPlan));
    BotRenderer *renderer = &self->renderer;

    renderer->draw = navigator_plan_renderer_draw;
    renderer->destroy = navigator_plan_renderer_free;
    renderer->widget = gtk_vbox_new(FALSE, 0);
    renderer->name = RENDERER_NAME;
    renderer->user = self;
    renderer->enabled = 1;


    BotEventHandler *ehandler = &self->ehandler;
    ehandler->name = RENDERER_NAME;
    ehandler->enabled = 1;
    ehandler->pick_query = NULL;
    ehandler->key_press = key_press;
    ehandler->hover_query = NULL;
    ehandler->mouse_press = mouse_press;
    ehandler->mouse_release = mouse_release;
    ehandler->mouse_motion = mouse_motion;
    ehandler->user = self;

    self->viewer = viewer;
    self->lc = lcm;//globals_get_lcm_full(NULL,1);

    // message subscriptions
    self->navigator_plan_subscription = erlcm_point_list_t_subscribe(self->lc, NAV_PLAN_CHANNEL,
                                                                     navigator_plan_handler, self);

    erlcm_goal_feasibility_query_t_subscribe(self->lc,"GOAL_FEASIBILITY_RESULT", goal_query_result_handler, 
                                       self);

    erlcm_topology_t_subscribe(self->lc,"MAP_SERVER_TOPOLOGY", topology_handler, 
                                       self);

    erlcm_topology_t_subscribe(self->lc,"TOPOLOGY", topology_handler, 
                                       self);

    // --- SETUP SIDE BOX WIDGET
    self->pw = BOT_GTK_PARAM_WIDGET(bot_gtk_param_widget_new());
    gtk_box_pack_start(GTK_BOX(renderer->widget), GTK_WIDGET(self->pw), TRUE, TRUE, 0);
    bot_gtk_param_widget_add_booleans(self->pw, 0, PARAM_SHOW_NAVIGATOR_PLAN, 1, NULL);

    self->param_goal_placement = 0;
    self->param_have_goal = 0;
    self->goal[0] = 0.0;
    self->goal[1] = 0.0;
    self->param_add_loc = 0;

    self->portal_list =  NULL;
    self->goal_query_result = NULL;
    self->topology = NULL;
    //add three buttons
    bot_gtk_param_widget_add_buttons(self->pw, PLACE_NAV_GOAL, NULL);
    bot_gtk_param_widget_add_buttons(self->pw, START_ROBOT, NULL);
    bot_gtk_param_widget_add_buttons(self->pw, STOP_ROBOT, NULL);
    bot_gtk_param_widget_add_buttons(self->pw,ADD_NEW_PLACE, NULL);
    bot_gtk_param_widget_add_buttons(self->pw, QUERY_PLACE , NULL);
    
  
    self->param_draw_navigator_plan = bot_gtk_param_widget_get_bool(self->pw, PARAM_SHOW_NAVIGATOR_PLAN);
    gtk_widget_show_all(renderer->widget);
    g_signal_connect(G_OBJECT(self->pw), "changed", G_CALLBACK(on_param_widget_changed), self);
    on_param_widget_changed(self->pw, "", self);

    bot_viewer_add_renderer(viewer, &self->renderer, render_priority);
    bot_viewer_add_event_handler(viewer, &self->ehandler, render_priority);

    g_signal_connect(G_OBJECT(viewer), "load-preferences", G_CALLBACK(on_load_preferences), self);
    g_signal_connect(G_OBJECT(viewer), "save-preferences", G_CALLBACK(on_save_preferences), self);
}
