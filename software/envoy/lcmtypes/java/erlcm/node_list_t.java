/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class node_list_t implements lcm.lcm.LCMEncodable
{
    public long utime;
    public int trajectory_length;
    public erlcm.node_t trajectory[];
 
    public node_list_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x1e02ece24d8cbfb2L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.node_list_t.class))
            return 0L;
 
        classes.add(erlcm.node_list_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + erlcm.node_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeLong(this.utime); 
 
        outs.writeInt(this.trajectory_length); 
 
        for (int a = 0; a < this.trajectory_length; a++) {
            this.trajectory[a]._encodeRecursive(outs); 
        }
 
    }
 
    public node_list_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public node_list_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.node_list_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.node_list_t o = new erlcm.node_list_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.utime = ins.readLong();
 
        this.trajectory_length = ins.readInt();
 
        this.trajectory = new erlcm.node_t[(int) trajectory_length];
        for (int a = 0; a < this.trajectory_length; a++) {
            this.trajectory[a] = erlcm.node_t._decodeRecursiveFactory(ins);
        }
 
    }
 
    public erlcm.node_list_t copy()
    {
        erlcm.node_list_t outobj = new erlcm.node_list_t();
        outobj.utime = this.utime;
 
        outobj.trajectory_length = this.trajectory_length;
 
        outobj.trajectory = new erlcm.node_t[(int) trajectory_length];
        for (int a = 0; a < this.trajectory_length; a++) {
            outobj.trajectory[a] = this.trajectory[a].copy();
        }
 
        return outobj;
    }
 
}

