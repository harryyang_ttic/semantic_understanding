"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class raw_odometry_msg_t(object):
    __slots__ = ["utime", "x", "y", "theta", "tv", "rv", "acceleration"]

    def __init__(self):
        self.utime = 0
        self.x = 0.0
        self.y = 0.0
        self.theta = 0.0
        self.tv = 0.0
        self.rv = 0.0
        self.acceleration = 0.0

    def encode(self):
        buf = BytesIO()
        buf.write(raw_odometry_msg_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qdddddd", self.utime, self.x, self.y, self.theta, self.tv, self.rv, self.acceleration))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != raw_odometry_msg_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return raw_odometry_msg_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = raw_odometry_msg_t()
        self.utime, self.x, self.y, self.theta, self.tv, self.rv, self.acceleration = struct.unpack(">qdddddd", buf.read(56))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if raw_odometry_msg_t in parents: return 0
        tmphash = (0x3fd9d0b52a42b35b) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if raw_odometry_msg_t._packed_fingerprint is None:
            raw_odometry_msg_t._packed_fingerprint = struct.pack(">Q", raw_odometry_msg_t._get_hash_recursive([]))
        return raw_odometry_msg_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

