import sys
from lcm import EventLog
from rrg.rrg_gridmap import rrg_gridmap
from scipy import transpose, zeros
from erlcm.graph_t import graph_t

fname_in = sys.argv[1]

map_log_new_test = EventLog(fname_in,"r")

for e in map_log_new_test:
  if e.channel == "GRAPH":
    graph = graph_t.decode(e.data)
    print graph.no_nodes
    print graph.node_ids
    print graph.x
    print graph.y
  
