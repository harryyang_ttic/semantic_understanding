import sys
import os
from lcm import EventLog
from rrg.rrg_gridmap import rrg_gridmap
from scipy import transpose, zeros, isnan, nan
from erlcm.graph_t import graph_t
from erlcm.graph_node_t import graph_node_t

fname_in = sys.argv[1]

fname_out = os.path.splitext(fname_in)[0]+"_with_topology.lcmlog"

map_log_original = EventLog(fname_in,"r")
map_log_new = EventLog(fname_out,"w",overwrite=True)

for e in map_log_original:
  if e.channel == "GRIDMAP":
    map_log_new.write_event(e.timestamp,"GRIDMAP",e.data)
    
myrrg = rrg_gridmap(sys.argv[1],None,True)
myrrg.create(1200)

nodes_pts = myrrg.nodes_pts
rrg_graph = myrrg.graph
means, pts, labels = myrrg._cluster_regions_spectral(alpha=0.01)

graph = graph_t()
graph.point_count=len(nodes_pts)

graph_nodes=[]

print len(labels)
for i in range(len(nodes_pts)):
  graph_node = graph_node_t()
  graph_node.x = nodes_pts[i][0]
  graph_node.y = nodes_pts[i][1]
  graph_node.edge_count = len(rrg_graph[i])
  edges=[]
  for j in rrg_graph[i]:
    edges.append(j)
  graph_node.edges = edges
  c = labels[i]
  if(isnan(c)):
    graph_node.cluster_id = -1
  else:
    graph_node.cluster_id = int(c)
  graph_nodes.append(graph_node)
  
graph.graph_nodes=graph_nodes
map_log_new.write_event(e.timestamp,"GRAPH",graph.encode())        

        


