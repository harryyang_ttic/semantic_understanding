from rrg_gridmap import rrg_gridmap
from tag_util import tag_file
from scipy import zeros, sqrt, log, exp
from pyTklib import tklib_euclidean_distance
from spectral_clustering_utils import get_median_matrix
from spectral_clustering import *

def get_min_matrix(Dist):
    return [min(Dist[i]) for i in range(len(Dist))]

def dists2weights_semantic(Dist, vtags, alpha=1.0):
    sigma = get_median_matrix(Dist)
    mymin = get_min_matrix(Dist)
    W = zeros([len(Dist),len(Dist)])*1.0
    
    for i in range(len(Dist)):
        #print indices
        for j in range(len(Dist[0])):
            #distance here

            if(i == j):
                W[i,j] = 1.0
                W[j,i] = 1.0
            elif(vtags[i] != vtags[j] and vtags[i] != None):
                pass
            elif(Dist[i,j] >= 0):
                val = exp(-(1.0/(1.0*sigma[j]*sigma[i]))*alpha*Dist[i,j])
                W[i,j] = val
                W[j,i] = val
    return W


class rrg_gridmap_semantic2(rrg_gridmap):

    def __init__(self, map_filename, tag_filename, init_loc=None, places=None):
        rrg_gridmap.__init__(self, map_filename)
        self.places = places
        self.tf = tag_file(tag_filename, map_filename)

    def get_basic_connections(self, inds):
        
        connections = []

        for i in inds:
            if(self.graph.has_key(i)): 
                #full_len = 0
                #if(self.graph.has_key(i)):
                #    full_len = len(self.graph[i])
                #print "Length " , len(self.basic_graph[i]), " - " , full_len

                conn = set(self.graph[i]).intersection(set(inds))

                #conn1 = set(self.graph[i]).intersection(set(inds))
                
                #print "Len " , len(conn) , " - " , len(conn1)
                ind_conn = []
                for j in conn:
                    ind_conn.append(inds.index(j))
                connections.append(ind_conn)
            else:
                connections.append([])

        return connections
        
    def get_nearest_tag_dict(self):
        vtags = {}
        D = {}
        for i, u in enumerate(self.graph.keys()):
            vt, it = self.tf.get_visible_tags(self.nodes_pts[u], max_dist=10e100)
            vt = list(vt)
            #if its greater than 0
            if(len(vt) > 0):
                min_i = None; min_d = 10e100;
                for j in range(len(vt)):
                    X, Y = self.tf.get_tag_locations(vt[j])
                    d = tklib_euclidean_distance([X[0], Y[0]], self.nodes_pts[u])
                    if(d < min_d):
                        min_i = j
                        min_d = d
                
                vtags[u] = vt[min_i]
                D[u] = [self.nodes_pts[u][0], self.nodes_pts[u][1]]
                print "vtags", vtags[u], X[0], Y[0]
            else:
                #otherwise its none
                vtags[u] = None
                D[u] = None
        return vtags, D


    def _cluster_regions_spectral(self, alpha=0.5, t=0.185, max_dist=0.2):
        print "get distances"
        dists = self.get_distance_matrix()
        vtags, myD = self.get_nearest_tag_dict()        

        print "do spectral clustering"
        W = dists2weights_semantic(dists, vtags, alpha)
        labels, k = spectral_clustering_W(W,None,numkmeans=1,seed_number=0,max_dist=max_dist,t=t)
        labels = labels[0]
        
        print "postprocess data and return"
        class_to_I = {}
        for i, c in enumerate(labels):
            if(isnan(c)):
                #print "was nan"
                #raw_input()
                continue
            if(class_to_I.has_key(int(c))):
                class_to_I[int(c)].append(i)
            else:
                class_to_I[int(c)] = [i]

        #get the centroid points and return these as a representative of the 
        # region
        means = []; Pts = [];
        for c in class_to_I:
            pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
            Pts.append(pts)
            mpt = mean(pts, axis=0)
            
            #get the nearest points to each of the mean points
            i, = kNN_index(mpt, transpose(pts), 1)
            means.append(pts[int(i)])
        means = transpose(means)
        
        return means, Pts

    def is_visible(self, pt1, pt2):
        '''checks and returns the true distance if the two points are visible, else -1'''
        mp = self.get_map()
        theta = arctan2(pt1[1] - pt2[1],
                        pt1[0] - pt2[0])
        d, = mp.ray_trace(pt2[0], pt2[1], [theta])
        d_true = tklib_euclidean_distance(pt1, pt2)

        theta1 = arctan2(pt2[1] - pt1[1],
                         pt2[0] - pt1[0])
        d1, = mp.ray_trace(pt1[0], pt1[1], [theta1])

        if(d >= d_true or d1 >= d_true):
            return d_true
        else:
            return -1

    def get_label(self, labels, point):
        #should include the label in the class
        curr_label = None
        indicies = kNN_index(point,
                             transpose(self.nodes_pts),
                             min(100,len(self.nodes_pts)))

        
        for i in indicies:
            
            if(self.is_visible(point, self.nodes_pts[int(i)]) >=0):                
                if(not isnan(labels[int(i)])):
                    curr_label = labels[int(i)]                
                    break

        if(curr_label == None):
            for i in indicies:
                if(not isnan(labels[int(i)])):
                    curr_label = labels[int(i)]  

        return curr_label

    def _cluster_regions_spectral_new(self, alpha=0.5, t=0.185, max_dist=0.2):
        print "get distances"
        dists = self.get_distance_matrix()
        vtags, myD = self.get_nearest_tag_dict()        

        print "do spectral clustering"
        W = dists2weights_semantic(dists, vtags, alpha)
        labels, k = spectral_clustering_W(W,None,numkmeans=1,seed_number=0,max_dist=max_dist,t=t)
        labels = labels[0]
        
        print "postprocess data and return"

        max_cluster_id = -1

        class_to_I = {}
        for i, c in enumerate(labels):
            if(isnan(c)):
                #print "was nan"
                #raw_input()
                continue
            if(class_to_I.has_key(int(c))):
                class_to_I[int(c)].append(i)
            else:
                class_to_I[int(c)] = [i]

            if(c > max_cluster_id): 
                max_cluster_id = c

        final_clusters = []

        means = []
        Pts = [];
        for c in class_to_I:
            
            pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
            Pts.append(pts)
            #conn = self.get_connections(class_to_I[c])
            conn = self.get_basic_connections(class_to_I[c])
            #self.check_cluster(pts)            
            #do something funcky to detect if there are wierd clusters
            
            mpt = mean(pts, axis=0)
            curr_cluster = {'cluster_id':c, 'no_points':len(pts), 'points':pts, 'mean':[mpt[0], mpt[1]], 
                            'conn': conn}
            final_clusters.append(curr_cluster)
        
            #get the nearest points to each of the mean points
            i, = kNN_index(mpt, transpose(pts), 1)
            means.append(pts[int(i)])

        means = transpose(means)

        place_labels = []

        if(self.places !=None):

            
            for l in range(len(self.places)):
                print self.places[l]['name'], self.places[l]['id']
                regions = []
                for k in self.places[l]['loc']:
                    closest_label = self.get_label(labels, k)
                    regions.append(closest_label)
                    print regions
                
                place_labels.append({'id':self.places[l]['id'], 'region':regions[0]})
        print place_labels
        #get the centroid points and return these as a representative of the 
        # region
        '''means = []; Pts = [];
        for c in class_to_I:
            pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
            Pts.append(pts)
            mpt = mean(pts, axis=0)
            
            #get the nearest points to each of the mean points
            i, = kNN_index(mpt, transpose(pts), 1)
            means.append(pts[int(i)])
        means = transpose(means)
        
        return means, Pts'''

        return means, final_clusters, Pts, place_labels#self.places
