from rrg.rrg_gridmap_semantic_lcm import rrg_gridmap_semantic
from lcm import EventLog
from erlcm.gridmap_t import gridmap_t
import tklib.carmen_maptools as carmen_maptools
from pylab import *
from sys import argv
from scipy import transpose, zeros
from scipy.cluster.hierarchy import fclusterdata
from pyTklib.pyTklib import tklib_euclidean_distance
from tklib.spectral_clustering import spectral_clustering_W
from tklib.spectral_clustering_utils import dists2weights_perona
from cPickle import *
import lcm
from carmen3d.seg_point_list_t import seg_point_list_t
from carmen3d.seg_point_t import seg_point_t
from carmen3d.seg_point_list_array_t import seg_point_list_array_t
from carmen3d.cluster_list_t import cluster_list_t
from carmen3d.cluster_t import cluster_t
from carmen3d.place_t import place_t
from carmen3d.place_list_t import place_list_t
from carmen3d.seg_sem_vec_list_t import seg_sem_vec_list_t
from carmen3d.seg_sem_vec_t import seg_sem_vec_t
from erlcm.goal_t import goal_t
#from carmen3d.cluster_conn_t import cluster_conn_t

#load locations from tag file

def publish_places(places): 
    if(places == None):
        return
    #publish places as lcm message
    lc = lcm.LCM()
    msg = place_list_t()
    msg.no_places = len(places)
    place_array = []
    
    for i in places:
        line = "{'name':'"+i['name']+"','id':"+ str(i['id'])+",'loc':["
        for j in i['loc']:
            line += "["+str(j[0])+","+ str(j[1])+"],"
        line += "]}\n"
        print line
        f.write(line)
        pl = place_t()
        pl.label = i['name']
        pl.no_locs = len(i['loc'])
        pl.region_id = i['id']
        if(i.has_key('cluster_id')):
            pl.assigned_cluster = i['cluster_id']
        else:
            pl.assigned_cluster = -1
        locs = []
        for j in i['loc']:
          loc = seg_point_t()  
          loc.pos = [j[0], j[1], .0]
          locs.append(loc)
        pl.locs = locs
        place_array.append(pl)
    f.close()
    msg.places = place_array

    lc.publish("PLACE_LOCATIONS", msg.encode())

def publish_sem_vec(sem_vectors, places, nodes_pts):
    if(places == None):
        return
    lc = lcm.LCM()
    print "Publishing sem vectors"
    
    msg = seg_sem_vec_list_t()

    msg.no_locs = len(places)
    msg.loc_ids = []
    msg.loc_xyz = []
    
    for i in places:
        msg.loc_ids.append(i['id'])
        loc = seg_point_t()
        loc.pos = [0,0,0]
        loc.pos[2] = 0
        for j in i['loc']:
            loc.pos[0] += j[0]
            loc.pos[1] += j[1]
         
        loc.pos[0] = loc.pos[0] / float(len(i['loc']))
        loc.pos[1] = loc.pos[1] / float(len(i['loc']))
        msg.loc_xyz.append(loc)
    msg.no_points = len(sem_vectors)
    msg.vecs = []
    for i in range(len(sem_vectors)):
        #print sem_vectors[i]
        sem_vec = seg_sem_vec_t()
        sem_vec.size = len(sem_vectors[i])
        sem_vec.vec = sem_vectors[i]
        xyz = seg_point_t()
        xyz.pos = [nodes_pts[i][0],nodes_pts[i][1], .0] 
        sem_vec.xyz = xyz
        msg.vecs.append(sem_vec)

    lc.publish("SEMANTIC_VECS", msg.encode())
        

# def publish_clusters(final_clusters):
#     print "------Publishing Clusters------"
#     msg = cluster_list_t()
#     clusters = []

#     means = []

#     for c in final_clusters:
#         c_msg = cluster_t()
#         c_msg.cluster_id = c['cluster_id']
#         c_msg.no_points = c['no_points']
#         points = []
#         conns = []
        
#         for i in range(c['no_points']):
#             pt = seg_point_t()
#             con = cluster_conn_t()
#             pt.pos = [c['points'][i][0], c['points'][i][1], .0]
#             points.append(pt)
#             con.no_conn = len(c['conn'][i])
#             con.conn_ind = c['conn'][i]
#             conns.append(con)

#         c_msg.mean = c['mean']
#         c_msg.points = points
#         c_msg.connections = conns    
        
#         clusters.append(c_msg)            

#     msg.no_clusters = len(final_clusters)
        
#     msg.clusters = clusters

#     lc = lcm.LCM()
#     lc.publish("CLUSTERS", msg.encode())


def publish_graph(graph_pts):

    lc = lcm.LCM()
        
    msg = seg_point_list_array_t()
    #msg.no_pts = len(graph_pts)
    msg.no_pts = len(graph_pts)
    print "msg.no_pts = %d" % (len(graph_pts))
    points = []
    #for i in graph_pts:
    for count in range(msg.no_pts):
        i = graph_pts[count]
        seg_list = seg_point_list_t()
        seg_list.curr_point = seg_point_t()
        seg_list.curr_point.pos = i['pos']
        seg_list.no_conn_pts = len(i['conn_pts'])
        seg_list.sem_dist = i['sem_dist']
        seg_list.metric_dist = i['metric_dist']
        seg_list.comb_dist = i['comb_dist']
        conn_pts = []
        
        for j in i['conn_pts']:
            pt = seg_point_t()
            pt.pos = j
            conn_pts.append(pt)

    
        seg_list.conn_pts = conn_pts
        points.append(seg_list)

    #publish lcm message
    msg.points = points
    print "Publishing Graph"
    lc.publish("SEG_WEIGHTS_ARRAY", msg.encode())

def save_results(places, sem_vec, nodes_pts, clusters,graph, out_file): #add semantic vector
    print "Saving Clusters" 
    print "Location : " , out_file
    results = {'places':places,'clusters':clusters, 'graph':graph, 'sem_vec': sem_vec, 'nodes_pts':nodes_pts}
    
    f = open(out_file,'w')
    dump(results,f)
    f.close()

def load_results(in_file):
    f = open(in_file,'r')
    results = load(f)
    f.close()
    
        
    if(results['places'] != None):
        publish_places(results['places'])
        if('sem_vec' in results.keys() and 'nodes_pts'  in results.keys()):
            publish_sem_vec(results['sem_vec'],results['places'], results['nodes_pts'])
    #publish_clusters(results['clusters'])
    publish_graph(results['graph'])

    return results

def load_locs(placelist_filename, floor_id):
    f = open(placelist_filename, 'r')
    places = []
    line  = f.readline()
    while(line != ""):
        a = eval(line)
        print a
        if(a['floor']==int(floor_id)):
            places.append(a)
        line  = f.readline()
    print "\tDone loading places"

    return places

def load_rand_locs(placelist_filename, floor_id, ind):
    f = open(placelist_filename, 'r')
    places = []
    line  = f.readline()
    while(line != ""):
        a = eval(line)
        print a
        act_ind = min(ind, len(a['points']))
        curr_loc = {'name':a['label'],'id':a['id'], 'loc':[array(a['points'][act_ind])]}        
        line  = f.readline()
    print "\tDone loading places"

    return places

def load_processed_locs(placelist_filename):
    f = open(placelist_filename, 'r')
    places = []
    line  = f.readline()
    while(line != ""):
        a = eval(line)
        print a
        curr_loc = {'name':a['name'],'id':a['id'], 'loc':a['loc']}    
        places.append(curr_loc)
        line  = f.readline()
    print "\tDone loading places"
    print places
    return places

def consolidate_locs(loc):

    dist_loc = []

    #Combining multiple locations
    count = 0
    for i in range(len(loc)):
        loc1 = loc[i]
        print "\t" , loc1
        if loc1['type'] == 'place' and loc1['pos']=='at': #we treat the near ones as objects
            if len(dist_loc) ==0:
                curr_loc = {'name':loc1['name'],'id':count, 'loc':[array([loc1['x'],loc1['y']])]}
                dist_loc.insert(0,curr_loc)
                count+=1
            else:
                match = 0
                print "Current Loc \n\t" , loc1
                for j in dist_loc:
                    if loc1['name'] == j['name'] and loc1['pos']=='at':
                        print "\tConsidered Loc \n\t", j
                        for k in j['loc']:
                            temp_loc = array([loc1['x'],loc1['y']])
                            dist = sqrt(pow(loc1['x']-k[0],2) + pow(loc1['y']-k[1],2)) 
                            
                            print "\t\t" , k

                            if (dist < 5.0):
                                print "\t\t=== Multiple locs found for the same label"
                                j['loc'].insert(0,array([loc1['x'],loc1['y']]))  
                                match = 1
                            break
                if (match ==0):
                    #add new tagged location 
                    curr_loc = {'name':loc1['name'],'id':count, 'loc':[array([loc1['x'],loc1['y']])]}
                    dist_loc.insert(0,curr_loc)
                    count+=1
                    
    return dist_loc

def test1():
    ion()

    final_locs = None
    semantic = True

    #there is a tag file in the argument - load the file 
    if(len(argv) >=4):
        print "Location : " , argv[2] , " Floor : " , argv[3]
        final_locs = load_processed_locs(argv[2])
        #places = load_locs(argv[2], argv[3])
        #final_locs = consolidate_locs(places)
        #publish the places 
        publish_places(final_locs)

    if(len(argv)==5):
        if(argv[4] == '0'):
            semantic = False
        elif(argv[4]=='1'):
            semantic = True
        else:
            print "Unrecognized argument"
        


    map_log_original = EventLog(argv[1], "r")
    gridmap = None
    
    for e in map_log_original:

        if e.channel == "GRIDMAP":
            gridmap = gridmap_t.decode (e.data)
            break

    myrrg = rrg_gridmap_semantic(argv[1], None, final_locs,use_semantic=semantic)
    print "Processed the log"
        
    # #show the rrg points
    # print argv[1]
    # if("direction_floor_8_full" in argv[1]):
    #     myrrg = rrg_gridmap_semantic(argv[1], [30.0,18.0])
    # elif("direction_floor_1" in argv[1]):
    #     myrrg = rrg_gridmap_semantic(argv[1], [86.0,58.0])

    # elif("don_" in argv[1]):
    #     myrrg = rrg_gridmap_semantic(argv[1], [63.266557, 101.616793], final_locs, use_semantic=semantic)
    # elif("tbh_" in argv[1]):
    #     myrrg = rrg_gridmap_semantic(argv[1], [67.066969999999998, 43.402434], final_locs,use_semantic=semantic)
        
    # elif('auto__fl_0' in argv[1]):        
    #     myrrg = rrg_gridmap_semantic(argv[1], [39.604680999999999, 56.481360000000002], final_locs,use_semantic=semantic)        
    # elif('auto__fl_1' in argv[1]):       
    #     myrrg = rrg_gridmap_semantic(argv[1], [ 28.449444, 72.080875], final_locs,use_semantic=semantic)
    # elif('test' in argv[1]):
    #     myrrg = rrg_gridmap_semantic(argv[1], [ 9.8, 10.6], final_locs,use_semantic=semantic)
    # else:
    #     myrrg = rrg_gridmap_semantic(argv[1], [39.604680999999999, 56.481360000000002], final_locs,use_semantic=semantic)
    mymap = myrrg.get_map()
    print "Got the map"
    
    myrrg.create(num_expansions=3000,max_size=100)
    print "Ran create"
    
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    print "Ran plot_tklib_log_gridmap"
    
    #plotting the map with the rrg nodes 
    pts_XY = transpose(myrrg.nodes_pts)
    plot(pts_XY[0], pts_XY[1], 'ro')


    '''print "plotting connections"
    for i in myrrg.graph.keys():
        for j in myrrg.graph[i]:
            p1 = pts_XY[:,i]
            p2 = pts_XY[:,j]
            plot([p1[0], p2[0]], [p1[1], p2[1]], 'k-')
    '''
    #show the paths generated
    figure()
    #carmen_maptools.plot_tklib_log_gridmap(mymap)
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    #print a path 
    if("direction_floor_8_full" in argv[1]):
        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path([30.0,18.0], transpose([[26.0,56.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')
    elif("direction_floor_1" in argv[1]):
        #d, mypath = myrrg.get_path([118.0,11.8], [45.0, 63.9])
        #d, mypath = myrrg.get_path([86.4, 124.0], [45.0, 63.9])
        D, Paths = myrrg.get_path([118.0, 11.8], transpose([[86.4, 124]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')

    elif("don" in argv[1]):
        D, Paths = myrrg.get_path([63.266557, 101.616793], transpose([[14.268274, 48.709615]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')

    #show the clustering
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    #Do the clustering     
    means, final_clusters,  graph_pts, seg_places = myrrg.cluster_regions_spectral_semantic(alpha=0.5)#, places=final_locs)
    publish_places(seg_places)
    #publish_clusters(final_clusters)

    publish_graph(graph_pts)
    publish_sem_vec(myrrg.semantic_vec, final_locs, myrrg.nodes_pts)
    
    #save_results(final_locs, myrrg.semantic_vec, myrrg.nodes_pts, final_clusters, graph_pts, "/home/sachih/software/tklib/pytools/rrt/results.log")
    
    title("clustered regions")
    plot(means[0], means[1], 'g^')
    
    show()
    

if __name__ == "__main__":
    test1()
