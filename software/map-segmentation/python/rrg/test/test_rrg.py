from rrg.rrg_gridmap import rrg_gridmap
import tklib.carmen_maptools as carmen_maptools
from pylab import *
from sys import argv
from scipy import transpose, zeros
from scipy.cluster.hierarchy import fclusterdata
from pyTklib.pyTklib import tklib_euclidean_distance
from tklib.spectral_clustering import spectral_clustering_W
from tklib.spectral_clustering_utils import dists2weights_perona

def test1():
    #ion()
    
    #show the rrg points
    print argv[1]
    if("direction_floor_8_full" in argv[1]):
        myrrg = rrg_gridmap(argv[1], [30.0,18.0])
    elif("direction_floor_1" in argv[1]):
        myrrg = rrg_gridmap(argv[1], [86.0,58.0])
    else:
        myrrg = rrg_gridmap(argv[1], None, True)
    
    mymap = myrrg.get_map()
    
    myrrg.create(1200)
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    
    pts_XY = transpose(myrrg.nodes_pts)
    plot(pts_XY[0], pts_XY[1], 'ro')

    #print "plotting"
    #for i in myrrg.graph.keys():
    #    for j in myrrg.graph[i]:
    #        p1 = pts_XY[:,i]
    #        p2 = pts_XY[:,j]
    #        plot([p1[0], p2[0]], [p1[1], p2[1]], 'k-')

    #show the paths generated
    figure()
    #carmen_maptools.plot_tklib_log_gridmap(mymap)
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    if("direction_floor_8_full" in argv[1]):
        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path([30.0,18.0], transpose([[26.0,56.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')
    elif("direction_floor_1" in argv[1]):
        #d, mypath = myrrg.get_path([118.0,11.8], [45.0, 63.9])
        #d, mypath = myrrg.get_path([86.4, 124.0], [45.0, 63.9])
        D, Paths = myrrg.get_path([118.0, 11.8], transpose([[86.4, 124]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')


    #show a path with poses
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
        
    if("direction_floor_8_full" in argv[1]):
        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path_xyth([30.0,18.0,0.0], transpose([[26.0,56.0,0.0]]))
        mypath = Paths[0]
        d=D[0]
        
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko')

        for i, th in enumerate(mypath[2]):
            plot([mypath[0][i], mypath[0][i]+1.0*cos(th)], [mypath[1][i], mypath[1][i]+1.0*sin(th)], 'k-')
            
    elif("direction_floor_1" in argv[1]):
        #d, mypath = myrrg.get_path([118.0,11.8], [45.0, 63.9])
        #d, mypath = myrrg.get_path([86.4, 124.0], [45.0, 63.9])
        D, Paths = myrrg.get_path_xyth([118.0, 11.8,0.0], transpose([[86.4, 124,0.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko')

        for i, th in enumerate(mypath[2]):
            plot([mypath[0][i], mypath[0][i]+1.0*cos(th)], [mypath[1][i], mypath[1][i]+1.0*sin(th)], 'k-')


    #show the paths generated
    figure()
    #carmen_maptools.plot_tklib_log_gridmap(mymap)
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    if("direction_floor_8_full" in argv[1]):
        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path([30.0,18.0], transpose([[26.0,56.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')
    elif("direction_floor_1" in argv[1]):
        #d, mypath = myrrg.get_path([118.0,11.8], [45.0, 63.9])
        #d, mypath = myrrg.get_path([86.4, 124.0], [45.0, 63.9])
        D, Paths = myrrg.get_path([118.0, 11.8], transpose([[86.4, 124]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')


    #show a path with poses
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
        
    if("direction_floor_8_full" in argv[1]):
        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path_xyth_interpolate([30.0,18.0,0.0], transpose([[26.0,56.0,0.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path interpolated", mypath
        mypath = array(mypath)
        I=range(0, len(mypath[0]), 30)
        mypath = mypath.take(I,axis=1)
        
        plot(mypath[0], mypath[1], 'ko')

        print "plotting orientations"
        for i, th in enumerate(mypath[2]):
            plot([mypath[0][i], mypath[0][i]+1.0*cos(th)], [mypath[1][i], mypath[1][i]+1.0*sin(th)], 'k-')
            
    elif("direction_floor_1" in argv[1]):
        #d, mypath = myrrg.get_path([118.0,11.8], [45.0, 63.9])
        #d, mypath = myrrg.get_path([86.4, 124.0], [45.0, 63.9])
        D, Paths = myrrg.get_path_xyth_interpolate([118.0, 11.8,0.0], transpose([[86.4, 124,0.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path interpolated", mypath
        plot(mypath[0], mypath[1], 'ko')

        for i, th in enumerate(mypath[2]):
            plot([mypath[0][i], mypath[0][i]+1.0*cos(th)], [mypath[1][i], mypath[1][i]+1.0*sin(th)], 'k-')


    #show the clustering
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    

    means, pts = myrrg._cluster_regions_spectral(alpha=0.01)
    title("clustered regions")
    plot(means[0], means[1], 'g^')
    
    shapes = ['o','x','<', '>', '^','p','d']
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

    i=0; j=0;
    for k, mypts in enumerate(pts):
        if(j >= len(colors)):
            j=0
            i+=1
        if(i >= len(shapes)):
            i = 0;
        mypts = transpose(mypts)
        #print mypts
        #print len(colors), j
        #print len(shapes), i
        plot(mypts[0], mypts[1], colors[j]+shapes[i])
        
        for m in range(len(mypts[0])):
            mean_x = means[0][k]; mean_y = means[1][k];
            plot([mean_x, mypts[0][m]], [mean_y, mypts[1][m]], colors[j]+'-')

        j+=1
    show()
    

if __name__ == "__main__":
    test1()
