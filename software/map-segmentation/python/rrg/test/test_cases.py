from rrt.rrg_gridmap import rrg_gridmap
from scipy import transpose, array, pi
import unittest
from environ_vars import TKLIB_HOME

class TestRRGTestCase(unittest.TestCase):
    def test_rrg(self):

        #show the rrg points
        myrrg = rrg_gridmap(TKLIB_HOME+"/data/directions/direction_floor_8_full/direction_floor_8_full.cmf.gz", [30.0,18.0])

        myrrg.create(1200)


        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path([30.0,18.0], transpose([[26.0,56.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath

        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path_xyth([30.0,18.0,0.0], transpose([[26.0,56.0,0.0]]))
        mypath = Paths[0]
        d=D[0]

        print "distance", d
        print "path", mypath


        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path([30.0,18.0], transpose([[26.0,56.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath


        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path_xyth_interpolate([30.0,18.0,0.0], transpose([[26.0,56.0,0.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path interpolated", mypath

        
        D, Paths = myrrg.get_path_xyth_interpolate([28.5000004247, 17.1000002548, pi/2.0], 
                                                   transpose([[17.6000002623,14.8000002205,pi/2.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path interpolated", mypath[-1]
        
        mypath = array(mypath)
        I=range(0, len(mypath[0]), 30)
        mypath = mypath.take(I,axis=1)
        
        means, pts = myrrg._cluster_regions_spectral(alpha=0.01)

