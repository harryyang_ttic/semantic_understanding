from rrg.rrt_gridmap import rrt_gridmap        
import tklib.carmen_maptools
from pylab import *
from sys import argv
from scipy import transpose

def test1():
    ion()
    myrrt = rrt_gridmap(argv[1], [30.0,18.0])
    mymap = myrrt.get_map()
    
    '''for i in range(100):
        for i in range(5):
            myrrt.expand_graph()
        carmen_maptools.plot_tklib_log_gridmap(mymap)

        pts_XY = transpose(myrrt.nodes_pts)
        plot(pts_XY[0], pts_XY[1], 'ro')
    
        print "plotting"
        for i in myrrt.children_I.keys():
            for j in myrrt.children_I[i]:
                p1 = pts_XY[:,i]
                p2 = pts_XY[:,j]
                plot([p1[0], p2[0]], [p1[1], p2[1]], 'k-')
        
        draw()
        raw_input()
        clf()'''

    myrrt.create(1700)
    carmen_maptools.plot_tklib_log_gridmap(mymap)
    
    pts_XY = transpose(myrrt.nodes_pts)
    #plot(pts_XY[0], pts_XY[1], 'ro')
    
    leaves = []
    num_leaves = 0
    for i in range(len(pts_XY[0])):
        if(not myrrt.children_I.has_key(i)):
            num_leaves+=1
            leaves.append(pts_XY[:,i])
    
    leaves=transpose(leaves)
    plot(leaves[0], leaves[1], 'gx')
    

    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap)
    to_loc = [23.0, 83.4]
    #to_loc = [26.0, 56.0]
    path = myrrt.get_path_from_root(to_loc)
    plot(path[0], path[1], 'ko-')
    
    print "num_pts", len(pts_XY[0])
    print "num_leaves", num_leaves
    show()


def test2():
    ion()
    myrrt = rrt_gridmap(argv[1], [30.0,18.0])
    mymap = myrrt.get_map()
    
    myrrt.create(150)
    carmen_maptools.plot_tklib_log_gridmap(mymap)
    
    pts_XY = transpose(myrrt.nodes_pts)
    #plot(pts_XY[0], pts_XY[1], 'ro')
    
    leaves = []
    num_leaves = 0
    for i in range(len(pts_XY[0])):
        if(not myrrt.children_I.has_key(i)):
            num_leaves+=1
            leaves.append(pts_XY[:,i])
    
    leaves=transpose(leaves)
    print "num_pts", len(pts_XY[0])
    print "num_leaves", num_leaves
    plot(leaves[0], leaves[1], 'gx')

    #print "plotting"
    for i in myrrt.children_I.keys():
        for j in myrrt.children_I[i]:
            p1 = pts_XY[:,i]
            p2 = pts_XY[:,j]
            plot([p1[0], p2[0]], [p1[1], p2[1]], 'k-')
        
    show()


if __name__ == "__main__":
    test1()
