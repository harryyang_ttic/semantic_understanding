from rrg.rrg_gridmap_semantic2 import rrg_gridmap_semantic2
import tklib.carmen_maptools as carmen_maptools
from pylab import *
from sys import argv
from scipy import transpose, zeros
from scipy.cluster.hierarchy import fclusterdata
from pyTklib.pyTklib import tklib_euclidean_distance
from tklib.spectral_clustering import spectral_clustering_W
from tklib.spectral_clustering_utils import dists2weights_perona

def test1():
    #show the rrg points
    print argv[1]
    if("direction_floor_8_full" in argv[1]):
        myrrg = rrg_gridmap_semantic2(argv[1], argv[2], [30.0,18.0])
    elif("direction_floor_1" in argv[1]):
        myrrg = rrg_gridmap_semantic2(argv[1], argv[2], [86.0,58.0])
    else:
        myrrg = rrg_gridmap_semantic2(argv[1], argv[2])
    
    mymap = myrrg.get_map()
    myrrg.create(3500)

    #show the clustering
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    vtags, myD = myrrg.get_nearest_tag_dict()
    
    
    plt_vals = {}
    for u in vtags.keys():
        if(myD[u] == None):
            continue
        if(plt_vals.has_key(vtags[u])):
            plt_vals[vtags[u]].append(myD[u])
        else:
            plt_vals[vtags[u]] = [myD[u]]

    
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w', 'b', 'g', 'r']
    for i, key in enumerate(plt_vals.keys()):
        print plt_vals[key]
        #print transpose(plt_vals[key])
        XY = transpose(plt_vals[key])
        plot(XY[0], XY[1], colors[i]+'o')
    title("nearest clusters")

    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    means, pts = myrrg._cluster_regions_spectral(alpha=1.0, t=0.185, max_dist=0.2)
    title("clustered regions")
    plot(means[0], means[1], 'g^')
    
    shapes = ['o','x','<', '>', '^','p','d']
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

    i=0; j=0;
    for k, mypts in enumerate(pts):
        if(j >= len(colors)):
            j=0
            i+=1
        if(i >= len(shapes)):
            i = 0;
        mypts = transpose(mypts)
        #print mypts
        #print len(colors), j
        #print len(shapes), i
        plot(mypts[0], mypts[1], colors[j]+shapes[i])
        
        for m in range(len(mypts[0])):
            mean_x = means[0][k]; mean_y = means[1][k];
            plot([mean_x, mypts[0][m]], [mean_y, mypts[1][m]], colors[j]+'-')

        j+=1
    show()

    

if __name__ == "__main__":
    test1()
