from pyTklib.pyTklib import *
from erlcm.gridmap_t import gridmap_t
from scipy import arctan2, transpose, cos, sin, array, zeros, argmin, mean, arange, ones, size, nan
from tklib.spectral_clustering import spectral_clustering_W
from tklib.spectral_clustering_utils import dists2weights_perona
from math import *
from rrg_gridmap import rrg_graph 
import copy

class rrg_graph_semantic(rrg_graph):
    def __init__(self, init_loc, places=None):
        tklib_init_rng(0)
        
        rrg_graph.__init__(self, self.init_loc, max_nn = 100)
        self.places = places
        self.place_points = []

        if(self.places !=None):
            for i in self.places:
                self.place_points.append([])            

        self.semantic_vec = []
        #add semantic vector if places are loaded
        if(self.places !=None):
            self.semantic_vec = [self.get_sem_vector(init_loc)]

    #this adds a node to the RRT
    #There is a slight difference in this graph - this checks the mutual visibility of points 
    #Basic one chacks visibility in one direction (which for some reason is not always the same 
    #when ray tracing in the other direction
    def expand_graph(self):
        map = self.get_map()
        
        sloc = map.get_random_open_location(0.0)
        
        #get nearest neighbors (from_loc)
        I_nn = kNN_index(sloc, transpose(self.nodes_pts), 
                         min(self.max_nn, len(self.nodes_pts)));

        is_visible = False; from_loc = None; theta = None;
        print "len(I_nn) = %d" % (len(I_nn))
        for j in range(len(I_nn)):
            i = I_nn[j]
            from_loc = self.nodes_pts[int(i)]

            #get the orientation from the nearest neighbor and 
            #    ray trace to see if the path is free
            theta = arctan2(sloc[1]-from_loc[1], sloc[0]-from_loc[0])
            d, = map.ray_trace(from_loc[0], from_loc[1], [theta])
            d_true = tklib_euclidean_distance(from_loc, sloc);

            #if the path is free then extend by that length
            print "d = %.2f, d_true = %.2f" % (d, d_true)
            if(d > d_true):
                d=d_true
                is_visible = True
                break;
        
        if(not is_visible):
            return
        
        #get the new node and add it to the set of nodes
        new_node_xy = [from_loc[0]+d*cos(theta), from_loc[1]+d*sin(theta)]
        self.nodes_pts.append(new_node_xy)

        #now add the relevant connections to the neighbors
        for i in I_nn:
            theta = arctan2(self.nodes_pts[int(i)][1]-new_node_xy[1], 
                            self.nodes_pts[int(i)][0]-new_node_xy[0])
            
            d, = map.ray_trace(new_node_xy[0], new_node_xy[1], [theta])
            d_true = tklib_euclidean_distance(new_node_xy, self.nodes_pts[int(i)]);

            theta1 = arctan2(new_node_xy[1] - self.nodes_pts[int(i)][1],
                            new_node_xy[0] - self.nodes_pts[int(i)][0])
            
            d1, = map.ray_trace(self.nodes_pts[int(i)][0], self.nodes_pts[int(i)][1], [theta1])

            if(d_true > 20.0):
                break

            ###Changed part
            if(d > d_true and d1 > d_true):
                if(self.graph.has_key(int(i))):
                    self.graph[int(i)].add((len(self.nodes_pts)-1))
                else:
                    self.graph[int(i)] = set([len(self.nodes_pts)-1])
                
                if(self.graph.has_key(len(self.nodes_pts)-1)):
                    self.graph[len(self.nodes_pts)-1].add(int(i))
                else:
                    self.graph[len(self.nodes_pts)-1] = set([int(i)])
           

    #this prunes the graph and removes poorly connected points - not used
    def prune_graph(self, threshold=0):
        mp = self.get_map()
        new_nodes_pts = []
        new_graph = {}

        for i in range(len(self.nodes_pts)):
            if(self.graph.has_key(int(i))):
                if(len(self.graph[int(i)]) > threshold):
                    new_nodes_pts.append(self.nodes_pts[i])

        print "Old Size : " , len(self.nodes_pts) , " => New Size : " , len(new_nodes_pts)
                    
        for j in range(len(new_nodes_pts)):
            sloc = new_nodes_pts[j]
            I_nn = kNN_index(sloc, transpose(new_nodes_pts), 
                         min(self.max_nn, len(new_nodes_pts)));
        
            #now add the relevant connections to the neighbors
            for i in I_nn:
                if(i == j):
                    continue
                
                theta = arctan2(new_nodes_pts[int(i)][1]-sloc[1], 
                                new_nodes_pts[int(i)][0]-sloc[0])

                d, = mp.ray_trace(sloc[0], sloc[1], [theta])
                d_true = tklib_euclidean_distance(sloc, new_nodes_pts[int(i)]);

                if(d > d_true):
                    if(new_graph.has_key(int(i))):
                        new_graph[int(i)].add(int(j))
                    else:
                        new_graph[int(i)] = set([int(j)])

                    if(new_graph.has_key(j)):
                        new_graph[j].add(int(i))
                    else:
                        new_graph[j] = set([int(i)])

                

        self.nodes_pts = new_nodes_pts
        self.graph = new_graph

    def get_sem_vector_with_dist(self, node_loc, node_ind):       
        mp = self.get_map()
        visibility = []
        dist = []

        min_dist = 10000
        min_ind = -1
        
        temp_label_dist = 1000*ones(len(self.places))

        for l in range(len(self.places)):
            found = 0
            #search through all the tagged positions for each location
            #to see if they are visible and within the perimeter
            curr_min_dist = 1000
            
            for k in self.places[l]['loc']:
                d_true = self.is_visible(k,node_loc)
                
                #Semantic visibility considered up-to only 20m 
                if d_true >=0 and d_true < 20.0:
                    if(d_true < temp_label_dist[l]):
                        temp_label_dist[l] = d_true 
                    found = 1
                
                    if min_dist > d_true:
                        min_ind = l
                        min_dist = d_true

            if found == 0:
                visibility.append(0)
            else:
                visibility.append(1)
                #self.place_points[l].append(node_ind)
        
        #Only using the closest visible landmark (incase it sees more than one)
        closest_visibility = zeros(len(self.places))
                
        closest_dist = 1000*ones(len(self.places))
        
        if(min_ind >=0):
            
            closest_visibility[min_ind] = 1     
            closest_dist[min_ind] = temp_label_dist[min_ind]
            
            self.place_points[min_ind].append(node_ind)
        
        #returning closest points
        return closest_visibility, closest_dist


    def is_visible(self, pt1, pt2):
        '''checks and returns the true distance if the two points are visible, else -1'''
        mp = self.get_map()
        theta = arctan2(pt1[1] - pt2[1],
                        pt1[0] - pt2[0])
        d, = mp.ray_trace(pt2[0], pt2[1], [theta])
        d_true = tklib_euclidean_distance(pt1, pt2)

        theta1 = arctan2(pt2[1] - pt1[1],
                         pt2[0] - pt1[0])
        d1, = mp.ray_trace(pt1[0], pt1[1], [theta1])

        if(d >= d_true or d1 >= d_true):
            return d_true
        else:
            return -1

    def is_co_visible(self, vec_1, d_vec_1, vec_2, d_vec_2):
        #check if the two points see the same landmark - and if they do - connect them 
        #return the distance through the land mark if so - else return -1 
        
        if(sum(vec_1) ==0 and sum(vec_2) == 0):
            return 10000

        min_dist = 10000

        for i in range(len(vec_1)):
            if(vec_1[i] and vec_2[i]):
                print vec_1, vec_2
                dist = d_vec_1[i] + d_vec_2[i] # the min dist from each landmark
                if(dist < min_dist):
                    min_dist = dist

        return min_dist
    
    def get_sem_vector(self, node_loc):
       
        mp = self.get_map()
        visibility = []

        min_dist = 10000
        min_ind = -1

        temp_label_dist = 10000*ones(len(self.places))

        for l in range(len(self.places)):
            found = 0
            #search through all the tagged positions for each location
            #to see if they are visible and within the perimeter
            for k in self.places[l]['loc']:

                theta = arctan2(k[1]-node_loc[1], k[0]-node_loc[0])
                d, = mp.ray_trace(node_loc[0], node_loc[1], [theta])
                d_true = tklib_euclidean_distance(node_loc, k);

                if d >= d_true and d_true < 10.0:  
                    temp_label_dist[l] = d_true 
                    found = 1
                
                    if min_dist > d_true:
                        min_ind = l
                        min_dist = d_true

            if found == 0:
                visibility.append(0)
            else:
                visibility.append(1)

        return visibility

    def fill_semantic_vectors(self):
        ##Might be easier to use the kNN from the points 
        '''Generate the semantic vectors for the graph nodes'''
        print "------Getting semantic vectors-------"
        self.semantic_vec = []
        if(self.places !=None):
            for i in self.nodes_pts:
                self.semantic_vec.append(self.get_sem_vector(i))

    def fill_semantic_vectors_no_conn(self):
        '''Generate the semantic vectors for the graph nodes'''
        print "------Getting semantic vectors-------"
        self.semantic_vec = []
        self.sem_dist_vec = []
        if(self.places !=None):
            for i in range(len(self.nodes_pts)):
                sem_vec, dist = self.get_sem_vector_with_dist(self.nodes_pts[i],i)
                self.semantic_vec.append(sem_vec)
                self.sem_dist_vec.append(dist)

            for i in range(len(self.places)):
                print "Place => " , i ," : " , len(self.place_points[i])
            

    def fill_semantic_vectors_new(self):
        '''Generate the semantic vectors for the graph nodes'''
        print "------Getting semantic vectors-------"
        self.semantic_vec = []
        self.sem_dist_vec = []
        self.place_points = []
        if(self.places !=None):
            for i in self.places:
                self.place_points.append([])       
        
        new_points_added = 0

        if(self.places !=None):
            for i in range(len(self.nodes_pts)):
                sem_vec, dist = self.get_sem_vector_with_dist(self.nodes_pts[i],i)
                self.semantic_vec.append(sem_vec)
                self.sem_dist_vec.append(dist)

            for i in range(len(self.places)):
                print "Place => " , i ," : " , len(self.place_points[i])

            for i in range(len(self.places)):                
                if( len(self.place_points[i]) <= 1):
                    continue
                for j in self.place_points[i]:
                    neighbours = []
                    inds = []
                    ref_point = self.nodes_pts[j]
                    for k in self.place_points[i]:
                        if k !=j:
                            neighbours.append(self.nodes_pts[k])
                            inds.append(k)
                    
                    #For each point that sees a landmark, connect it to up-to 30 neighbours
                    I_nn = kNN_index(ref_point, transpose(neighbours), 
                                     min(30, len(neighbours)));
                    
                    for nn_ind in I_nn:
                        k = int(inds[int(nn_ind)])
                        
                        #print self.graph[j]
                        #print self.basic_graph[j]
                        
                        #create the new connections in the graph - based on the semantic visibility
                        if(self.graph.has_key(j)):
                            if(k not in self.graph[j]): 
                                new_points_added+=1
                                #print "\t",j, k
                                self.graph[j].add(k)
                                if(self.graph.has_key(k)):
                                    self.graph[k].add(j)
                                else:
                                    self.graph[k] = set([j])
                        else: 
                           self.graph[j] = set([k])
                           #print "\t",j, k
                           new_points_added+=1
                           if(self.graph.has_key(k)):
                               self.graph[k].add(j)
                           else:
                               self.graph[k] = set([j])

                        #print "After"

                        #print self.graph[j]
                        #print self.basic_graph[j]

        print "No of Nodes added " , new_points_added

class rrg_gridmap_semantic(rrg_graph_semantic):
    def __init__(self, lcm_logfile, init_loc=None, places=None, use_semantic=True):
        tklib_init_rng(0);
        
        self.lcm_logfile = lcm_logfile
        self.use_semantic = False
        if(places !=None and use_semantic):
            print "Using Semantic Segmentation"
            self.use_semantic = True
        else:
            print "Using Basic Segmentation"
        self.semantic_vec = None
        
        self.map = None
        self.map = self.get_map()
        
        if(init_loc == None):
            self.init_loc = self.map.get_random_open_location(0.5)
        else:
            self.init_loc = init_loc

        rrg_graph_semantic.__init__(self, self.init_loc, places)
        
    def get_map(self):
        if(self.map == None):
            self.map = tklib_log_gridmap()
            self.map.load_carmen_map_from_lcm_log(self.lcm_logfile)

        return self.map

    def create(self, num_expansions=100, max_size=100):
        #changed to sample up to a max number
        print "Expanding graph"
        while(len(self.nodes_pts) < max_size):
            print "Expanding: len(self.nodes_pts) = %d" % (len(self.nodes_pts))
            self.expand_graph()

        print "Done Expanding graph"
        #do a deep copy of the basic reachability graph
        self.basic_graph = copy.deepcopy(self.graph)
        print "Graph Coppied"
    
    def get_distance_matrix(self):
        print "Size of Dist matrix " , len(self.graph.keys()), len(self.nodes_pts)

        dists = zeros([len(self.nodes_pts), len(self.nodes_pts)])-1.0
        
        for i, u in enumerate(self.graph.keys()):
            for j, v in enumerate(self.graph[u]):
                #distance is caluclated only for the visible nodes
                dists[u,v] = tklib_euclidean_distance(self.nodes_pts[u], self.nodes_pts[v])
                dists[v,u] = dists[u,v]

        return dists


    def get_distance_matrix_semantic(self, places=None):
        if(self.use_semantic):
            self.fill_semantic_vectors()
        
        print "Size of Dist matrix " , len(self.graph.keys()), len(self.nodes_pts)

        graph_arrays = []

        dists = zeros([len(self.nodes_pts), len(self.nodes_pts)])-1.0
        
        points = []

        for i, u in enumerate(self.graph.keys()):
            curr_node = {'pos':[self.nodes_pts[u][0], self.nodes_pts[u][1],.0]}
            conn_pts = []
            sem_dist_array = []
            metric_dist_array = []
            comb_dist_array = []

            for j, v in enumerate(self.graph[u]):
                conn_pts.append([self.nodes_pts[v][0], self.nodes_pts[v][1],.0])

                #distance is caluclated only for the visible nodes
                met_dist = tklib_euclidean_distance(self.nodes_pts[u], self.nodes_pts[v])

                sem_dist = 0
                
                if(self.use_semantic):
                    sem_dist = tklib_euclidean_distance(self.semantic_vec[u], self.semantic_vec[v])
                    beta = 2.0/3.0
                    
                    if(sum(self.semantic_vec[u]) == 0 and sum(self.semantic_vec[v]) == 0):
                        beta = 0.5

                    comb_dist = beta * sem_dist + (1- beta) * met_dist
                else:
                    comb_dist = met_dist

                #add to lcm message
                sem_dist_array.append(sem_dist)
                metric_dist_array.append(met_dist)
                comb_dist_array.append(comb_dist)

                dists[u,v] = comb_dist
                dists[v,u] = dists[u,v]
                
            curr_node['conn_pts'] = conn_pts
            curr_node['sem_dist'] = sem_dist_array
            curr_node['metric_dist'] = metric_dist_array
            curr_node['comb_dist'] = comb_dist_array
            points.append(curr_node)
            
        return dists, points


    #Checks consistancy in the cluster and splits them if there is seperate non-connected graphs
    def check_cluster(self, n_inds, labels, curr_no_clusters):
        #check cluster based on the reachability of these points  
        #if points are not reaachable - then resegment the cluster
        #this will fix the clusters if there is wierdnesss
        #seems to do what we need 
        clusters = []        
        
        #print "Size of Cluster : ", len(n_inds) 
        #print n_inds

        for i_ind in n_inds:
            connected = False
            neigh = []
            if(self.basic_graph.has_key(i_ind)):
                neigh = self.basic_graph[i_ind]
               
            if(len(clusters)>0 and len(neigh)>0):
                connected_clusters = []
                for ind_cluster, clus in enumerate(clusters):
                    if(set(neigh).intersection(set(clus))):
                        #print "Found cluster"
                        if(not connected):
                            #add this point to the cluster
                            clus.append(i_ind)
                            
                        connected = True
                        connected_clusters.append(ind_cluster)

                #merge clusters
                if(len(connected_clusters) > 1):
                    new_clusters = []
                    merged_cluster = []
                    for ind_cluster, clus in enumerate(clusters):
                        if(set([ind_cluster]).isdisjoint(set(connected_clusters))):
                            new_clusters.append(clus)
                        else:
                            merged_cluster.extend(clus)
                    new_clusters.append(merged_cluster)
                    clusters = new_clusters                                       
                        
            if (not connected):
                curr_cluster = [i_ind]
                clusters.append(curr_cluster)
               

        #print "The number of sub clusters : " , len(clusters)
        max_cluster_size = 0
        max_sub_cluster_ind = -1
        for ind, i in enumerate(clusters):
            #print len(i)
            if(max_cluster_size < len(i)):
                max_cluster_size = len(i)
                max_sub_cluster_ind = ind
                
        if(len(clusters) ==1):
            return 0
        
        no_new_clusters = 0

        new_cluster_no = curr_no_clusters

        for ind, i in enumerate(clusters):
            if(ind != max_sub_cluster_ind):
                #check and reassign to a new cluster
                if(len(i) > 1):
                    #increase the number of clusters                    
                    for k in i:
                        labels[k] = new_cluster_no

                    no_new_clusters += 1
                    new_cluster_no += 1
                else:
                    for k in i:
                        labels[k] = nan
                
        return no_new_clusters

        
    def get_label(self, labels, point):
        #should include the label in the class
        curr_label = None
        indicies = kNN_index(point,
                             transpose(self.nodes_pts),
                             min(100,len(self.nodes_pts)))
        for i in indicies:
            if(self.is_visible(point, self.nodes_pts[int(i)]) >=0):
                if(not isnan(labels[int(i)])):
                    curr_label = labels[int(i)]                
                    break

        if(curr_label == None):
            for i in indicies:
                if(not isnan(labels[int(i)])):
                    curr_label = labels[int(i)]  

        #this can still return None - need to return -1
        #but make sure that it is properly handled at the viewer

        return curr_label

    def get_label_mean(self, labels, point):       
        #should include the label in the class
        curr_label = {}
        count = 0
        assign_label = None
        
        indicies = kNN_index(point,
                             transpose(self.nodes_pts),
                             min(100,len(self.nodes_pts)))
        
        closest_dist = 1000
        for i in indicies:
            dist = self.is_visible(point, self.nodes_pts[int(i)])

            if( (not isnan(labels[int(i)])) and dist >=0):
                if(closest_dist > dist):
                    closest_dist = dist

                if((dist - closest_dist > 2.0) or (count >= 10)):    
                    break
                
                if(curr_label.has_key(labels[int(i)])):
                    curr_label[labels[int(i)]] += 1
                else:
                    curr_label[labels[int(i)]] = 1
                    
                count+=1
                
                
        if(len(curr_label.keys()) == 0):
            for i in indicies:
                if(not isnan(labels[int(i)])):
                    curr_label[labels[int(i)]] = 1
                    assign_label =  labels[int(i)]
        else:
            max_count = 0
            assign_label = -1

            for i in curr_label.keys():
                if(max_count < curr_label[i]):
                    max_count = curr_label[i]
                    assign_label = i
        return assign_label, curr_label

    #Not really used 
    def show_explosion(self, labels):
        print "Showing Explosions"
        mp = self.get_map()
        #get the free locations
        free_locs = array(mp.get_free_locations());
        free_indexes = array(mp.get_free_inds());
        occupied_indexes = array(mp.get_occupied_inds());
        free_label_regions = []

        cluster_pts = {}
 
        for i in range(len(free_locs[0])):
            curr_label = self.get_label(labels, free_locs[:,i])
            free_label_regions.append(curr_label)
            if(cluster_pts.has_key(curr_label)):
                cluster_pts[curr_label].append(free_locs[:,i])
            else:
                cluster_pts[curr_label] = []
                cluster_pts[curr_label].append(free_locs[:,i])            
            
        full_clusters = []

        
        
        for i in cluster_pts.keys():
            mpt = mean(array(cluster_pts[i]), axis=0)
            curr_cluster = {'cluster_id':i,
                            'no_points':len(cluster_pts[i]), 'points':cluster_pts[i], 
                            'mean':[mpt[0], mpt[1]]}
            full_clusters.append(curr_cluster)

        print "Done .."
            
        return full_clusters

    def get_distance_matrix_semantic_new(self, places=None):
        mp = self.get_map()

        #this function will create the semantic vectors but will not create additonal connections in the graph
        #self.fill_semantic_vectors_no_conn() 

        #this function creates the additional connections as well as create semantic vectors
        if(self.use_semantic):
            self.fill_semantic_vectors_new()
        
        print "Size of Dist matrix " , len(self.graph.keys()), len(self.nodes_pts)

        graph_arrays = []

        dists = zeros([len(self.nodes_pts), len(self.nodes_pts)])-1.0
        
        points = []

        count = 0 

        for i, u in enumerate(self.graph.keys()):

            curr_node = {'pos':[self.nodes_pts[u][0], self.nodes_pts[u][1],.0]}
            conn_pts = []
            sem_dist_array = []
            metric_dist_array = []
            comb_dist_array = []

            for j, v in enumerate(self.graph[u]):
                conn_pts.append([self.nodes_pts[v][0], self.nodes_pts[v][1],.0])

                #distance is caluclated only for the visible nodes
                
                theta = arctan2(self.nodes_pts[v][1] - self.nodes_pts[u][1],
                                self.nodes_pts[v][0] - self.nodes_pts[u][0])
                d, = mp.ray_trace(self.nodes_pts[u][0], self.nodes_pts[u][1], [theta])
                d_true = tklib_euclidean_distance(self.nodes_pts[u], self.nodes_pts[v])

                theta1 = arctan2(self.nodes_pts[u][1] - self.nodes_pts[v][1],
                                self.nodes_pts[u][0] - self.nodes_pts[v][0])
                d1, = mp.ray_trace(self.nodes_pts[v][0], self.nodes_pts[v][1], [theta1])

                if(d >= d_true): 
                    met_dist = d_true

                #This gets called only when I make additional connections between points that 
                #see the same landmark but do not see eachother
                elif(self.use_semantic and len(self.sem_dist_vec) > 0):
                    min_dist = 1000
                    for i in range(len(self.sem_dist_vec[u])):
                        temp_dist = self.sem_dist_vec[u][i] + self.sem_dist_vec[v][i] 
                        if(temp_dist < min_dist):
                            min_dist = temp_dist
                    
                    met_dist = min_dist #uses the combined distance through the landmark
                    #met_dist = d_true #tried using metric distance as well 

                    if met_dist == 1000:
                        print "Error"
                        count +=1

                sem_dist = 0
                
                if(self.use_semantic):
                    sem_dist = tklib_euclidean_distance(self.semantic_vec[u], self.semantic_vec[v])
                    #beta = 2.0/3.0

                    #if(sum(self.semantic_vec[u]) == 0 and sum(self.semantic_vec[v]) == 0):
                    #    beta = 0.5
                    
                    #Used mixing weight
                    beta = 0.3
                    comb_dist = beta * sem_dist + (1- beta) * met_dist

                else:
                    comb_dist = met_dist

                #add to lcm message
                sem_dist_array.append(sem_dist)
                metric_dist_array.append(met_dist)
                comb_dist_array.append(comb_dist)

                dists[u,v] = comb_dist
                dists[v,u] = dists[u,v]
            

            curr_node['conn_pts'] = conn_pts
            curr_node['sem_dist'] = sem_dist_array
            curr_node['metric_dist'] = metric_dist_array
            curr_node['comb_dist'] = comb_dist_array
            points.append(curr_node)
                
        return dists, points

    #cluster the regions and return the centroid and labels
    def cluster_regions_spectral(self, alpha=0.5):
        print "get distances"
        dists  = self.get_distance_matrix()
        
        print "do spectral clustering"
        W = dists2weights_perona(dists, alpha)
        labels, k = spectral_clustering_W(W,None,numkmeans=1,seed_number=0)
        labels = labels[0]
        
        print "postprocess data and return"
        class_to_I = {}
        for i, c in enumerate(labels):
            if(class_to_I.has_key(int(c))):
                class_to_I[int(c)].append(i)
            else:
                class_to_I[int(c)] = [i]

        #get the centroid points and return these as a representative of the 
        # region
        
        means = []
        for c in class_to_I:
            pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
            
            mpt = mean(pts, axis=0)

            #get the nearest points to each of the mean points
            i, = kNN_index(mpt, transpose(pts), 1)
            means.append(pts[int(i)])
        means = transpose(means)
        
        return means

    def get_connections(self, inds):
        
        connections = []

        for i in inds:
            if(self.graph.has_key(i)): 
                #print "Graph " 
                #print "Ind : " , i 
                #print self.graph[i]
                conn = set(self.graph[i]).intersection(set(inds))
                ind_conn = []
                for j in conn:
                    ind_conn.append(inds.index(j))
                connections.append(ind_conn)
            else:
                connections.append([])

        return connections

    def get_basic_connections(self, inds):
        
        connections = []

        for i in inds:
            if(self.basic_graph.has_key(i)): 
                #full_len = 0
                #if(self.graph.has_key(i)):
                #    full_len = len(self.graph[i])
                #print "Length " , len(self.basic_graph[i]), " - " , full_len

                conn = set(self.basic_graph[i]).intersection(set(inds))

                #conn1 = set(self.graph[i]).intersection(set(inds))
                
                #print "Len " , len(conn) , " - " , len(conn1)
                ind_conn = []
                for j in conn:
                    ind_conn.append(inds.index(j))
                connections.append(ind_conn)
            else:
                connections.append([])

        return connections

    def split_region(self, mp1, mp2, pl_id1, pl_id2,  region_ids, curr_no_regions, labels, contested_region_id):
        print "-------------Splitting Region----------" 
        #print "Region ID's"
        #print region_ids
        inter_rp1 = set(self.place_points[pl_id1]).intersection(set(region_ids))
        inter_rp2 = set(self.place_points[pl_id2]).intersection(set(region_ids))
        
        print "Full Region :" , len(region_ids)
        
        print "Region : " , pl_id1 , len(inter_rp1), len(self.place_points[pl_id1])
        #print self.place_points[pl_id1]
        print "Region : " , pl_id2 , len(inter_rp2), len(self.place_points[pl_id2])
        #print self.place_points[pl_id2]
        
        reassinged_pts = []
        #reassign points for the region with the least intersection 
        new_mp = []

        if(len(inter_rp1) < len(inter_rp2)):
            reassinged_pts = inter_rp1
            new_mp = mp1
            old_mp = mp2
        else:
            reassinged_pts = inter_rp2
            new_mp = mp2
            old_mp = mp1
        
        if(len(reassinged_pts)==0):
            print "No points seen by one of the landmarks - Returning"
            return contested_region_id, contested_region_id          

        #do a simple vernoi split here by assigning the points closest to the mean to a new region
        new_region = curr_no_regions

        ##Not really important now - but check for visibility also - before reassigning 

        for i in reassinged_pts:
            #print "Point : " , self.nodes_pts[i]
            min_d1 = 1000
            min_d2 = 1000
            for j in new_mp:
                d1 = tklib_euclidean_distance(self.nodes_pts[i], j);
                #d1 = self.is_visible(self.nodes_pts[i], j);
                if(min_d1 > d1):# and d1 >=0):
                    min_d1 = d1
            for j in old_mp:
                d2 = tklib_euclidean_distance(self.nodes_pts[i], j);
                #d2 = self.is_visible(self.nodes_pts[i], j);
                if(min_d2 > d2):# and d2 >=0):
                    min_d2 = d2
            
            if(min_d1 < min_d2): #closest to the mp1
                labels[i] = new_region
                #print "Reassigning Label"
                #print min_d1, min_d2
            #else: 
            #    print min_d1, min_d2
            #    print "Keeping the same label" 
                #reassign to new region 
        if(len(inter_rp1) < len(inter_rp2)):
            return new_region, contested_region_id            
        else:
            return contested_region_id, new_region

    
    def merge_region(self, to_merge_region_ids, new_id):
        print "Merging Region" 
        
        for i in to_merge_region_ids:
            labels[i] = new_id
            

        #cluster the regions and return the centroid and labels
    def cluster_regions_spectral_semantic(self, alpha=0.5):
        print "++++Get distances++++"
            
        if(not self.use_semantic):
            print "Basic Distances"
            
        else:
            print "Semantic Distance"

        #self.prune_graph(threshold=1)
        #dists, graph_pts = self.get_distance_matrix_semantic()
        dists, graph_pts = self.get_distance_matrix_semantic_new()
        
        print "do spectral clustering"
        W = dists2weights_perona(dists, alpha)
        labels, k = spectral_clustering_W(W,None,numkmeans=1,seed_number=0, max_dist=0.4)#max_dist=0.2)
        #0.2 will supress all the wierd points
        labels = labels[0]
        
        max_cluster_id = -1

        print "postprocess data and return"
        class_to_I = {}
        for i, c in enumerate(labels):
            if(isnan(c)):
                continue
            if(class_to_I.has_key(int(c))):
                class_to_I[int(c)].append(i)
            else:
                class_to_I[int(c)] = [i]

            if(c > max_cluster_id): 
                max_cluster_id = c

        #get the centroid points and return these as a representative of the region
        final_clusters = []

        means = []

        print "Extracting Clusters"
        
        for c in class_to_I:
            no_additions = self.check_cluster(class_to_I[c], labels, max_cluster_id+1)
            max_cluster_id += no_additions

        max_cluster_id = -1
        class_to_I = {}
        for i, c in enumerate(labels):
            if(isnan(c)):
                continue
            if(class_to_I.has_key(int(c))):
                class_to_I[int(c)].append(i)
            else:
                class_to_I[int(c)] = [i]

            if(c > max_cluster_id): 
                max_cluster_id = c

        for c in class_to_I:
            
            pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
            #conn = self.get_connections(class_to_I[c])
            conn = self.get_basic_connections(class_to_I[c])
            #self.check_cluster(pts)            
            #do something funcky to detect if there are wierd clusters
            
            mpt = mean(pts, axis=0)
            curr_cluster = {'cluster_id':c, 'no_points':len(pts), 'points':pts, 'mean':[mpt[0], mpt[1]], 
                            'conn': conn}
            final_clusters.append(curr_cluster)
        
            #get the nearest points to each of the mean points
            i, = kNN_index(mpt, transpose(pts), 1)
            means.append(pts[int(i)])

        means = transpose(means)

        reprocess_clusters = False #this will be set if there are splits in places

        place_labels = []        
        
        if(self.places !=None):
            #check assignment of same label to multiple places
            #and assignment of multiple labels to same place (at different locations)
            
            for l in range(len(self.places)):
                print self.places[l]['name'], self.places[l]['id']
                regions = []
                for k in self.places[l]['loc']:
                    closest_label = self.get_label(labels, k)
                    regions.append(closest_label)

                same_label = True
                for k in regions:
                    for j in regions:
                        if(k != j):
                            same_label = False
                            break
                    if(not same_label):
                        break

                if(not same_label):
                    print "Err :: Same area has two lables"
                    reg_count = []
                    max_reg_count = 0
                    max_region = -1
                    for j in regions:
                        inter_set = set(self.place_points[l]).intersection(set(class_to_I[j]))
                        print "Region " , j , " Size ", len(class_to_I[j]), "Intersection " ,  len(inter_set)
                        reg_count.append(len(inter_set))
                        if(max_reg_count < len(inter_set)):
                            max_reg_count = len(inter_set)
                            max_region = j
                    #print len(inter_set)
                    print max_reg_count
                    print "Best Region : " , max_region
                    
                    curr_pl_label = max_region
                    place_labels.append(curr_pl_label)
                        
                    #can use the merge_region but prob not a good idea
                    #check the indices of the points that are visible 
                    #only merge labels if we have to - otherwise makes things more complicated

                    #will check what is the most visible region of these two 
                    
                #currently taking the first label as the label for the region
                else:
                    curr_pl_label = regions[0]
                    place_labels.append(curr_pl_label)

            print "Place Labels"
            print place_labels           
            
        if(self.use_semantic):
            ######Convert the indexes to sets and check - for intersections etc 

            fixed_ind = []
            
            for i_ind, i in enumerate(place_labels):
                unique_label = True
                matching_ind = None

                for j_ind , j in enumerate(place_labels):
                    if(i_ind == j_ind):
                        continue
                    if(i_ind in set(fixed_ind) or j_ind in set(fixed_ind)):
                        continue
                    if(i==j):
                        print "Err :: Non Unique Label"
                        reprocess_clusters = True
                        unique_label = False
                        matching_ind = j_ind
                        break

                if(not unique_label):  #does not handle more than two places in the same region 
                    print "Doing something to split the region"                    
                    #check if we can reuse spectral clustering (or some other from of clustering to 
                    #split the points                    
                    fixed_ind.append(i_ind)
                    fixed_ind.append(matching_ind)
                    pts = array(self.nodes_pts).take(class_to_I[i], axis=0)
                    print "No of Points in the region : " , size(pts, axis=0)
                    #use the split region function 
                    
                    reg_1, reg_2 = self.split_region(self.places[i_ind]['loc'], self.places[matching_ind]['loc'], i_ind, 
                                      matching_ind, class_to_I[i], max_cluster_id +1, labels, i)
                    #if(new_region >=0):
                    #    max_cluster_id +=1
                    place_labels[i_ind] = reg_1
                    place_labels[j_ind] = reg_2

                    if(reg_1 != reg_2):                        
                        max_cluster_id +=1

        
        if(reprocess_clusters):
            #reprocess the cluster values - as the labels have changed 
            print "Reprocessing data and return"
            class_to_I = {}
            for i, c in enumerate(labels):
                if(isnan(c)):
                    continue

                if(class_to_I.has_key(int(c))):
                    class_to_I[int(c)].append(i)
                else:
                    class_to_I[int(c)] = [i]

            #get the centroid points and return these as a representative of the region
            final_clusters = []

            means = []

            print "Extracting Clusters"
            for c in class_to_I:
                pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
                #conn = self.get_connections(class_to_I[c])
                conn = self.get_basic_connections(class_to_I[c])

                mpt = mean(pts, axis=0)
                curr_cluster = {'cluster_id':c, 'no_points':len(pts), 'points':pts, 
                                'mean':[mpt[0], mpt[1]], 'conn': conn}
                final_clusters.append(curr_cluster)

                #get the nearest points to each of the mean points
                i, = kNN_index(mpt, transpose(pts), 1)
                means.append(pts[int(i)])

            means = transpose(means)

            #also need to output the cluster id's of the tagged places

        
        #reassign the cluster ids to the places - 
        #this is done if we have place list - irrespective of doing sem or not

        if(self.places !=None):
            #assign the selected cluster id to the place

            for l in range(len(self.places)):
                print self.places[l]['name'], self.places[l]['id']
                self.places[l]['cluster_id'] = place_labels[l]
                
        #full_cluster = self.show_explosion(labels)
        #return means, full_cluster, graph_pts, self.places

        return means, final_clusters, graph_pts, self.places
























































































































