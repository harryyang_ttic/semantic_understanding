cmake_minimum_required(VERSION 2.6.0)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(POD_NAME map-segmentation)
include(cmake/pods.cmake)

# require python
find_package(PythonInterp REQUIRED)

# install all python files in the python/ subdirectory
pods_install_python_packages(${CMAKE_CURRENT_SOURCE_DIR}/python)

# install a script "hello-python" that runs the hello.main python module
# This script gets installed to ${CMAKE_INSTALL_PREFIX}/bin/hello-python
# and automatically sets the correct python path.
pods_install_python_script(partition-map gridmapToTopology.partition_map)


pods_install_python_script(test-rrg rrg.test.test_rrg)

pods_install_python_script(test-rrg-sem rrg.test.test_rrg_sem2)

pods_install_python_script(test-rrg-sem-lcm rrg.test.test_rrg_sem_lcm_new)
