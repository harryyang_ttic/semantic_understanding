function rmpath_eigen_utils(recurse)
global eigen_utils_path_added
if ~eigen_utils_path_added
	return
end
eigen_utils_path_added = 0;
path = '/home/harry/Documents/Robotics/semantic-understanding/software/externals/common_utils/eigen-utils/matlab';
if nargin==0
    recurse = 1;
end
if recurse
    all_paths = genpath(path);
    split_paths = regexp(all_paths, ':','split');
    for ii=1:length(split_paths)
        ind = strfind(split_paths{ii},'.svn');
        if isempty(ind)
            rmpath(split_paths{ii})
        end
    end
else
    rmpath(path)
end
