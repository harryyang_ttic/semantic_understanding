function addpath_matlab_utils(recurse)
global matlab_utils_path_added
if matlab_utils_path_added
	return;
end
matlab_utils_path_added = 1;
path = '/home/harry/Documents/Robotics/semantic-understanding/software/externals/common_utils/matlab-utils/matlab';
if nargin==0
    recurse = 1;
end
if recurse
    all_paths = genpath(path);
    split_paths = regexp(all_paths, ':','split');
    for ii=1:length(split_paths)
        ind = strfind(split_paths{ii},'.svn');
        if isempty(ind)
            addpath(split_paths{ii})
        end
    end
else
    rmpath(path)
end
