#ifndef MATH3D_H
#define MATH3D_H

#include <stdbool.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>
#include <assert.h>
#include <gsl_swig_utils/gsl_utilities.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef struct
{
  gsl_matrix * points_xy;
  double z_start;
  double z_end;
  int initialized; // should change all args to use pointers, because this doesn't play nice with swig.
} math3d_prism_t;

math3d_prism_t math3d_prism_init();
void math3d_prism_free(math3d_prism_t p);
bool math3d_higher_than(math3d_prism_t p1, math3d_prism_t p2);
bool math3d_starts_higher_than(math3d_prism_t p1, math3d_prism_t p2); 
bool math3d_supports(math3d_prism_t p1, math3d_prism_t p2);
bool math3d_intersects(math3d_prism_t p1, math3d_prism_t p2);

#ifdef __cplusplus
}
#endif


#endif

