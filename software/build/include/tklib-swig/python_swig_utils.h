#ifndef PYTHON_SWIG_UTILS_H
#define PYTHON_SWIG_UTILS_H
#include <Python.h>
#include <gsl/gsl_matrix.h>

#ifdef __cplusplus
extern "C" {
#endif

    gsl_matrix * psu_matrix_from_python(PyObject * input);
    PyObject * psu_vector_to_python(gsl_vector * vector);
    gsl_matrix * psu_python_to_matrix(PyObject * input);

#ifdef __cplusplus
}
#endif

#endif

