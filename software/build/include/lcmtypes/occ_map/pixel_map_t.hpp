/** THIS IS AN AUTOMATICALLY GENERATED FILE.  DO NOT MODIFY
 * BY HAND!!
 *
 * Generated by lcm-gen
 **/

#include <lcm/lcm_coretypes.h>

#ifndef __occ_map_pixel_map_t_hpp__
#define __occ_map_pixel_map_t_hpp__

#include <vector>

namespace occ_map
{

class pixel_map_t
{
    public:
        int64_t    utime;

        int32_t    dimensions[2];

        double     xy0[2];

        double     xy1[2];

        double     mpp;

        uint8_t    compressed;

        int8_t     data_type;

        int32_t    datasize;

        std::vector< uint8_t > mapData;

    public:
        static const int8_t   TYPE_UNKNOWN = 0;
        static const int8_t   TYPE_FLOAT = 1;
        static const int8_t   TYPE_DOUBLE = 2;
        static const int8_t   TYPE_INT32 = 3;
        static const int8_t   TYPE_UINT32 = 4;
        static const int8_t   TYPE_INT16 = 5;
        static const int8_t   TYPE_UINT16 = 6;
        static const int8_t   TYPE_INT8 = 7;
        static const int8_t   TYPE_UINT8 = 8;

    public:
        /**
         * Encode a message into binary form.
         *
         * @param buf The output buffer.
         * @param offset Encoding starts at thie byte offset into @p buf.
         * @param maxlen Maximum number of bytes to write.  This should generally be
         *  equal to getEncodedSize().
         * @return The number of bytes encoded, or <0 on error.
         */
        inline int encode(void *buf, int offset, int maxlen) const;

        /**
         * Check how many bytes are required to encode this message.
         */
        inline int getEncodedSize() const;

        /**
         * Decode a message from binary form into this instance.
         *
         * @param buf The buffer containing the encoded message.
         * @param offset The byte offset into @p buf where the encoded message starts.
         * @param maxlen The maximum number of bytes to reqad while decoding.
         * @return The number of bytes decoded, or <0 if an error occured.
         */
        inline int decode(const void *buf, int offset, int maxlen);

        /**
         * Retrieve the 64-bit fingerprint identifying the structure of the message.
         * Note that the fingerprint is the same for all instances of the same
         * message type, and is a fingerprint on the message type definition, not on
         * the message contents.
         */
        inline static int64_t getHash();

        /**
         * Returns "pixel_map_t"
         */
        inline static const char* getTypeName();

        // LCM support functions. Users should not call these
        inline int _encodeNoHash(void *buf, int offset, int maxlen) const;
        inline int _getEncodedSizeNoHash() const;
        inline int _decodeNoHash(const void *buf, int offset, int maxlen);
        inline static int64_t _computeHash(const __lcm_hash_ptr *p);
};

int pixel_map_t::encode(void *buf, int offset, int maxlen) const
{
    int pos = 0, tlen;
    int64_t hash = getHash();

    tlen = __int64_t_encode_array(buf, offset + pos, maxlen - pos, &hash, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = this->_encodeNoHash(buf, offset + pos, maxlen - pos);
    if (tlen < 0) return tlen; else pos += tlen;

    return pos;
}

int pixel_map_t::decode(const void *buf, int offset, int maxlen)
{
    int pos = 0, thislen;

    int64_t msg_hash;
    thislen = __int64_t_decode_array(buf, offset + pos, maxlen - pos, &msg_hash, 1);
    if (thislen < 0) return thislen; else pos += thislen;
    if (msg_hash != getHash()) return -1;

    thislen = this->_decodeNoHash(buf, offset + pos, maxlen - pos);
    if (thislen < 0) return thislen; else pos += thislen;

    return pos;
}

int pixel_map_t::getEncodedSize() const
{
    return 8 + _getEncodedSizeNoHash();
}

int64_t pixel_map_t::getHash()
{
    static int64_t hash = _computeHash(NULL);
    return hash;
}

const char* pixel_map_t::getTypeName()
{
    return "pixel_map_t";
}

int pixel_map_t::_encodeNoHash(void *buf, int offset, int maxlen) const
{
    int pos = 0, tlen;

    tlen = __int64_t_encode_array(buf, offset + pos, maxlen - pos, &this->utime, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __int32_t_encode_array(buf, offset + pos, maxlen - pos, &this->dimensions[0], 2);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __double_encode_array(buf, offset + pos, maxlen - pos, &this->xy0[0], 2);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __double_encode_array(buf, offset + pos, maxlen - pos, &this->xy1[0], 2);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __double_encode_array(buf, offset + pos, maxlen - pos, &this->mpp, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __byte_encode_array(buf, offset + pos, maxlen - pos, &this->compressed, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __int8_t_encode_array(buf, offset + pos, maxlen - pos, &this->data_type, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __int32_t_encode_array(buf, offset + pos, maxlen - pos, &this->datasize, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    if(this->datasize > 0) {
        tlen = __byte_encode_array(buf, offset + pos, maxlen - pos, &this->mapData[0], this->datasize);
        if(tlen < 0) return tlen; else pos += tlen;
    }

    return pos;
}

int pixel_map_t::_decodeNoHash(const void *buf, int offset, int maxlen)
{
    int pos = 0, tlen;

    tlen = __int64_t_decode_array(buf, offset + pos, maxlen - pos, &this->utime, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, &this->dimensions[0], 2);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __double_decode_array(buf, offset + pos, maxlen - pos, &this->xy0[0], 2);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __double_decode_array(buf, offset + pos, maxlen - pos, &this->xy1[0], 2);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __double_decode_array(buf, offset + pos, maxlen - pos, &this->mpp, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __byte_decode_array(buf, offset + pos, maxlen - pos, &this->compressed, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __int8_t_decode_array(buf, offset + pos, maxlen - pos, &this->data_type, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, &this->datasize, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    if(this->datasize) {
        this->mapData.resize(this->datasize);
        tlen = __byte_decode_array(buf, offset + pos, maxlen - pos, &this->mapData[0], this->datasize);
        if(tlen < 0) return tlen; else pos += tlen;
    }

    return pos;
}

int pixel_map_t::_getEncodedSizeNoHash() const
{
    int enc_size = 0;
    enc_size += __int64_t_encoded_array_size(NULL, 1);
    enc_size += __int32_t_encoded_array_size(NULL, 2);
    enc_size += __double_encoded_array_size(NULL, 2);
    enc_size += __double_encoded_array_size(NULL, 2);
    enc_size += __double_encoded_array_size(NULL, 1);
    enc_size += __byte_encoded_array_size(NULL, 1);
    enc_size += __int8_t_encoded_array_size(NULL, 1);
    enc_size += __int32_t_encoded_array_size(NULL, 1);
    enc_size += __byte_encoded_array_size(NULL, this->datasize);
    return enc_size;
}

int64_t pixel_map_t::_computeHash(const __lcm_hash_ptr *)
{
    int64_t hash = 0xf2584cd266709883LL;
    return (hash<<1) + ((hash>>63)&1);
}

}

#endif
