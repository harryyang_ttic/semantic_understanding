import sys
import os
from lcm import EventLog
from rrg.rrg_gridmap import rrg_gridmap
from scipy import transpose, zeros
from erlcm.graph_t import graph_t

fname_in = sys.argv[1]

fname_out = os.path.splitext(fname_in)[0]+"_with_topology.lcmlog"

map_log_original = EventLog(fname_in,"r")
map_log_new = EventLog(fname_out,"w",overwrite=True)

for e in map_log_original:
  if e.channel == "GRIDMAP":
    map_log_new.write_event(e.timestamp,"GRIDMAP",e.data)
    
myrrg = rrg_gridmap(sys.argv[1],None,True)
myrrg.create(1200)

nodes_pts = myrrg.nodes_pts
graph = myrrg.graph
means, pts, labels = myrrg._cluster_regions_spectral(alpha=0.01)

graph = graph_t()
graph.point_count=len(nodes_pts)

for i in range(len(nodes_pts)):
  graph_node = graph_node_t()
  graph_node.x = nodes_pts[i][0]
  graph_node.y = nodes_pts[i][1]
  graph_node.edge_count = len(graph[i])
  for j in range(len(graph[i])):
    graph_node.edges.append(graph[i][j])
  graph_node.cluster_id = labels[i]
  graph.graph_nodes.append(graph_node)

map_log_new.write_event(e.timestamp,"GRAPH",graph.encode())        

        


