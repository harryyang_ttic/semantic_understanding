from pyTklib import *
from scipy import arctan2, transpose, cos, sin, array

class rrt_graph:
    def __init__(self, init_loc):
        tklib_init_rng(0)
        self.root_I = 0
        self.nodes_pts = [init_loc]
        self.children_I = {}
        self.parents_I = {}
        self.expansion_dist = 1.0

    #this adds a node to the RRT
    def expand_graph(self):
        sloc = self.map.get_random_open_location(0.0)
        
        #plot([sloc[0]], [sloc[1]], 'go')

        #get nearest neighbor
        i, = kNN_index(sloc, transpose(self.nodes_pts), 1);

        from_loc = self.nodes_pts[int(i)]

        #plot([from_loc[0]], [from_loc[1]], 'yo', markersize=10)
        theta = arctan2(sloc[1]-from_loc[1], sloc[0]-from_loc[0])
        
        #ray trace
        d, = self.map.ray_trace(from_loc[0], from_loc[1], [theta])

        d_true = tklib_euclidean_distance(from_loc, sloc);
        if(d > d_true):
            d=d_true
        elif(d <= d_true and d > self.expansion_dist):
            d = self.expansion_dist
        else:
            return
    
        new_node = [from_loc[0]+d*cos(theta), from_loc[1]+d*sin(theta)]
        
        self.nodes_pts.append(new_node)
        
        if(self.children_I.has_key(int(i))):
            self.children_I[int(i)].append((len(self.nodes_pts)-1))
        else:
            self.children_I[int(i)] = [(len(self.nodes_pts)-1)]

        if(self.parents_I.has_key(len(self.nodes_pts)-1)):
            self.parents_I[len(self.nodes_pts)-1].append(int(i))
        else:
            self.parents_I[len(self.nodes_pts)-1] = [int(i)]

class rrt_gridmap(rrt_graph):
    def __init__(self, map_filename, init_loc=None):
        self.map_filename = map_filename
        print "Called"
        
        self.map = None
        self.map = self.get_map()
        
        if(init_loc == None):
            self.init_loc = self.map.get_random_open_location(0.5)
        else:
            self.init_loc = init_loc

        rrt_graph.__init__(self, self.init_loc)
        
    def get_map(self):
        if(self.map == None):
            self.map = tklib_log_gridmap()
            self.map.load_carmen_map(self.map_filename)

        return self.map

    def create(self, num_expansions=100):
        map = self.get_map()
        
        for i in range(num_expansions):
            self.expand_graph()

    def get_leaves(self):
        leaves = []
        for i in range(len(pts_XY[0])):
            if(not self.children_I.has_key(i)):
                leaves.append(pts_XY[:,i])
        
        return transpose(leaves)
    
    def get_path_from_root(self, loc_xy):
        i, = kNN_index(loc_xy, transpose(self.nodes_pts), 1);

        path = [int(i)]
        while(self.parents_I.has_key(path[-1])):
            path.append(self.parents_I[path[-1]][0])
        
        return transpose(array(self.nodes_pts).take(list(reversed(path)), axis=0))
            
        
a = rrt_gridmap("/home/sachih/wheelchair/carmen3D/tour_mapserver/stata_final_fl_0.map.gz")
        
