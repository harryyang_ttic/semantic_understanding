from sys import argv
from tag_util import point, save_polygons
from pyTklib.pyTklib import tklib_log_gridmap

infile = open(argv[1], 'r')

s_hash = []
for l in infile:
    s_hash.append(eval(l))


pts = []
for l in s_hash:
    x, y =  l['loc'][0]
    #pts.append(point(x, y, l['name']))
    pts.append([x,y])

gm = tklib_log_gridmap()
gm.load_carmen_map(argv[3])

pts_ind = [gm.to_index(p) for p in pts]
print "pts:", pts
print "pts_ind:", pts_ind

ret_pts = []
for i, p in enumerate(pts_ind):
    ret_pts.append(point(p[0], p[1], s_hash[i]['name']+"_"+str(s_hash[i]['id'])))

print "saving polygons", ret_pts 
save_polygons([], ret_pts, argv[2])
