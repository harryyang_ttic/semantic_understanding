from rrg.rrg_gridmap_semantic import rrg_gridmap_semantic
import tklib.carmen_maptools
from pylab import *
from sys import argv
from scipy import transpose, zeros
from scipy.cluster.hierarchy import fclusterdata
from pyTklib.pyTklib import tklib_euclidean_distance
from tklib.spectral_clustering import spectral_clustering_W
from tklib.spectral_clustering_utils import dists2weights_perona


#load locations from tag file

def load_locs(placelist_filename, floor_id):
    f = open(placelist_filename, 'r')
    places = []
    line  = f.readline()
    while(line != ""):
        a = eval(line)
        print a
        if(a['floor']==int(floor_id)):
            places.append(a)
        line  = f.readline()
    print "\tDone loading places"

    return places

def consolidate_locs(loc):

    dist_loc = []

    #Combining multiple locations

    count = 0;

    for i in range(len(loc)):
        loc1 = loc[i]
        print "\t" , loc1
        if loc1['type'] == 'place' and loc1['pos']=='at': #we treat the near ones as objects
            if len(dist_loc) ==0:
                curr_loc = {'name':loc1['name'], 'id':count,'loc':[array([loc1['x'],loc1['y']])]}
                count+=1
                dist_loc.insert(0,curr_loc)
            else:
                match = 0
                print "Current Loc \n\t" , loc1
                for j in dist_loc:
                    if loc1['name'] == j['name'] and loc1['pos']=='at':
                        print "\tConsidered Loc \n\t", j
                        for k in j['loc']:
                            temp_loc = array([loc1['x'],loc1['y']])
                            dist = sqrt(pow(loc1['x']-k[0],2) + pow(loc1['y']-k[1],2)) 
                            
                            print "\t\t" , k

                            if (dist < 4.0):
                                print "\t\t=== Multiple locs found for the same label"
                                j['loc'].insert(0,array([loc1['x'],loc1['y']]))  
                                match = 1
                            break
                if (match ==0):
                    #add new tagged location 
                    curr_loc = {'name':loc1['name'], 'id':count,'loc':[array([loc1['x'],loc1['y']])]}
                    dist_loc.insert(0,curr_loc)
                    count+=1

    return dist_loc

def test1():
    ion()

    final_locs = None
    
    #there is a tag file in the argument - load the file 
    if(len(argv) == 4):
        print "Location : " , argv[2] , " Floor : " , argv[3]
        places = load_locs(argv[2], argv[3])
        final_locs = consolidate_locs(places)
        
    #show the rrg points
    print argv[1]
    if("direction_floor_8_full" in argv[1]):
        myrrg = rrg_gridmap_semantic(argv[1], [30.0,18.0])
    elif("direction_floor_1" in argv[1]):
        myrrg = rrg_gridmap_semantic(argv[1], [86.0,58.0])
    elif("don_" in argv[1]):
        myrrg = rrg_gridmap_semantic(argv[1], [63.266557, 101.616793], final_locs)
    else:        
        myrrg = rrg_gridmap_semantic(argv[1], [39.604680999999999, 56.481360000000002], final_locs)
    
    mymap = myrrg.get_map()
    
    myrrg.create(2500)
    #myrrg.create(3000) #increased the no if points - seems to sample most of the map
    
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    
    #plotting the map with the rrg nodes 
    pts_XY = transpose(myrrg.nodes_pts)
    plot(pts_XY[0], pts_XY[1], 'ro')



    '''print "plotting connections"
    for i in myrrg.graph.keys():
        for j in myrrg.graph[i]:
            p1 = pts_XY[:,i]
            p2 = pts_XY[:,j]
            plot([p1[0], p2[0]], [p1[1], p2[1]], 'k-')
    '''
    #show the paths generated
    figure()
    #carmen_maptools.plot_tklib_log_gridmap(mymap)
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    #print a path 
    if("direction_floor_8_full" in argv[1]):
        #d, mypath = myrrg.get_path([23.0,83.4], [26.0,56.0])
        #d, mypath = myrrg.get_path([30.0,18.0], [23.0,83.4])
        D, Paths = myrrg.get_path([30.0,18.0], transpose([[26.0,56.0]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')
    elif("direction_floor_1" in argv[1]):
        #d, mypath = myrrg.get_path([118.0,11.8], [45.0, 63.9])
        #d, mypath = myrrg.get_path([86.4, 124.0], [45.0, 63.9])
        D, Paths = myrrg.get_path([118.0, 11.8], transpose([[86.4, 124]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')

    elif("don" in argv[1]):
        D, Paths = myrrg.get_path([63.266557, 101.616793], transpose([[14.268274, 48.709615]]))
        mypath = Paths[0]
        d=D[0]
        print "distance", d
        print "path", mypath
        plot(mypath[0], mypath[1], 'ko-')


    #show the clustering
    figure()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    
    #Do the clustering     
    means, final_clusters,  graph_pts = myrrg.cluster_regions_spectral_semantic(alpha=0.5)#, places=final_locs)
    title("clustered regions")
    plot(means[0], means[1], 'g^')
    
    show()
    

if __name__ == "__main__":
    test1()
