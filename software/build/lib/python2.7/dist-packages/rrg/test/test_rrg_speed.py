from unittest import *
from rrg.rrg_gridmap import rrg_gridmap
from sys import argv
from math import pi
import cProfile
from scipy import *
from pylab import *
import tklib.carmen_maptools

def test_partitions():
    partitions = {}
    dests_poses = []
    print "creating rrg"
    myrrg = rrg_gridmap(argv[1], [30.0,18.0])
    myrrg.create(1200)
    
    print "clustering regions"
    #get the destinations
    dests = myrrg.cluster_regions_spectral(0.5)
    for i in range(len(dests[0])):
        for orient in [0.0, pi/2.0, pi, -pi/2.0]:
            d = dests[:,i]
            pose_dest = [d[0], d[1], orient]    
            dests_poses.append(pose_dest)

    dests = dests_poses
    print "computing source to dest"
    #compute the paths between all destinations and the partition according to the 
    #  result
    ion()
    mymap = myrrg.get_map()
    carmen_maptools.plot_tklib_log_gridmap(mymap, cmap="carmen_cmap_grey")
    pplt,=plot([], [], 'k-')
    for i, pst in enumerate(dests):
        print "destination:", i, "of", len(dests)
        partitions[i] = {}
        
        D, Paths_XYTh_Int = myrrg.get_path_xyth_interpolate(pst, transpose(dests))
        print D
        for j in range(len(D)):
            pplt.set_data(Paths_XYTh_Int[j][0], Paths_XYTh_Int[j][1])
            draw()
            #raw_input()
    return partitions


if __name__=="__main__":
    cProfile.run("test_partitions()", 'test_rrg_speed.prof')

