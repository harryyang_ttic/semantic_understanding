# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/cmake-gui

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/pod-build

# Include any dependencies generated for this target.
include CMakeFiles/lcmtypes_xsens.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/lcmtypes_xsens.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/lcmtypes_xsens.dir/flags.make

CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o: CMakeFiles/lcmtypes_xsens.dir/flags.make
CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o: ../lcmtypes/c/lcmtypes/xsens_ins_t.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/pod-build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o"
	/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -fPIC -o CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o   -c /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/lcmtypes/c/lcmtypes/xsens_ins_t.c

CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.i"
	/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -fPIC -E /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/lcmtypes/c/lcmtypes/xsens_ins_t.c > CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.i

CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.s"
	/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -fPIC -S /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/lcmtypes/c/lcmtypes/xsens_ins_t.c -o CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.s

CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o.requires:
.PHONY : CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o.requires

CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o.provides: CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o.requires
	$(MAKE) -f CMakeFiles/lcmtypes_xsens.dir/build.make CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o.provides.build
.PHONY : CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o.provides

CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o.provides.build: CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o

# Object files for target lcmtypes_xsens
lcmtypes_xsens_OBJECTS = \
"CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o"

# External object files for target lcmtypes_xsens
lcmtypes_xsens_EXTERNAL_OBJECTS =

lib/liblcmtypes_xsens.a: CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o
lib/liblcmtypes_xsens.a: CMakeFiles/lcmtypes_xsens.dir/build.make
lib/liblcmtypes_xsens.a: CMakeFiles/lcmtypes_xsens.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking C static library lib/liblcmtypes_xsens.a"
	$(CMAKE_COMMAND) -P CMakeFiles/lcmtypes_xsens.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/lcmtypes_xsens.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/lcmtypes_xsens.dir/build: lib/liblcmtypes_xsens.a
.PHONY : CMakeFiles/lcmtypes_xsens.dir/build

CMakeFiles/lcmtypes_xsens.dir/requires: CMakeFiles/lcmtypes_xsens.dir/lcmtypes/c/lcmtypes/xsens_ins_t.c.o.requires
.PHONY : CMakeFiles/lcmtypes_xsens.dir/requires

CMakeFiles/lcmtypes_xsens.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/lcmtypes_xsens.dir/cmake_clean.cmake
.PHONY : CMakeFiles/lcmtypes_xsens.dir/clean

CMakeFiles/lcmtypes_xsens.dir/depend:
	cd /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/pod-build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/pod-build /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/pod-build /home/harry/Documents/Robotics/semantic-understanding/software/drivers/xsens/pod-build/CMakeFiles/lcmtypes_xsens.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/lcmtypes_xsens.dir/depend

