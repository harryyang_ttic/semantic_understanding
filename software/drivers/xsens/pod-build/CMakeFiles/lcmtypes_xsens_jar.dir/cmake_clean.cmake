FILE(REMOVE_RECURSE
  "../lcmtypes/c"
  "../lcmtypes/cpp"
  "../lcmtypes/java"
  "../lcmtypes/python"
  "CMakeFiles/lcmtypes_xsens_jar"
  "lcmtypes_xsens.jar"
  "../lcmtypes/java/xsens/ins_t.class"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/lcmtypes_xsens_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
