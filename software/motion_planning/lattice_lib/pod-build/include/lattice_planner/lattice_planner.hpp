#ifndef __dp_planner_hpp__
#define __dp_planner_hpp__

#include <vector>
#include <occ_map/VoxelMap.hpp>
#include <occ_map/PixelMap.hpp>
#include <lcm/lcm.h>
#include <bot_lcmgl_client/lcmgl.h>

#ifdef __APPLE
#include <GL/gl.h>
#else
#include <GL/gl.h>
#endif

namespace dp_planner {


#define DPP_UNK_THRESH .01
#define DPP_OCC_THRESH .1


    class Waypoint {
    public:
        Waypoint(double x=0, double y=0, double z=0, double theta=0) :
            x(x), y(y), z(z), theta(theta)
        {
        }
        Waypoint(const double xyzt[4]) :
            x(xyzt[0]), y(xyzt[1]), z(xyzt[2]), theta(xyzt[3])
        {
        }
        double x;
        double y;
        double z;
        double theta;
    };

    occ_map::FloatVoxelMap * createUtilityMap(const occ_map::FloatVoxelMap * voxmap, const double start[3]);

    

    bool
    dpPathToGoal(const occ_map::FloatVoxelMap * voxmap, const double start_xyzt[4], const double goal_xyzt[4],
                 std::vector<Waypoint>& waypoints);


    /**
     * 2D Pixel Map DP Planner
     */
    class Waypoint2d {
    public:
        Waypoint2d(double x=0, double y=0, double theta=0) :
            x(x), y(y), theta(theta) { }
        Waypoint2d(const double xyt[3]) :
            x(xyt[0]), y(xyt[1]), theta(xyt[2]) { }
        double x;
        double y;
        double theta;
    };

    occ_map::FloatPixelMap * createUtilityMap(const occ_map::FloatPixelMap *pixmap, const double start[2]);
    occ_map::FloatPixelMap * dilateMap(const occ_map::FloatPixelMap *pixmap, double dilation_size);

    occ_map::FloatPixelMap * createCostMap(const occ_map::FloatPixelMap *pixmap, double dilation_size, double fixed_size);
    bool dpPathToGoal(const occ_map::FloatPixelMap *pixmap, const double start_xyt[3], const double goal_xyt[3],
                      std::vector<Waypoint2d>& waypoints);

    bool dpPathToGoalLattice(const occ_map::FloatPixelMap * pixmap,
                             const occ_map::FloatPixelMap * distmap,
                             const double start_xyt[3], const double goal_xyt[3],
                             std::vector<Waypoint2d>& waypoints, lcm_t *lcm);

    occ_map::IntPixelMap* createDistanceMap(const occ_map::FloatPixelMap *pixmap, lcm_t *lcm);

    occ_map::FloatPixelMap* createDistanceCostMap(const occ_map::FloatPixelMap *pixmap, lcm_t *lcm, double r_radius, double s_radius);

    //occ_map::FloatPixelMap* createDistanceCostMap(const occ_map::FloatPixelMap *pixmap, lcm_t *lcm);

    typedef struct _xy_t{
        double xy[2];
    } xy_t;

    class PointList2d{
    public:
        xy_t *points;
        int no_points;
    };

    occ_map::FloatPixelMap *creatDistanceMapFromPoints(PointList2d p_list, lcm_t *lcm, xy_t xy0, 
                                              xy_t xy1, double res);
    
    occ_map::FloatPixelMap * updateDistanceCostMap(occ_map::FloatPixelMap *pixmap, occ_map::FloatPixelMap *distmap, 
                              double r_radius, double s_radius, 
                              xy_t center, double update_radius, 
                              PointList2d *p_list);
}

#endif
