/*
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "lattice_planner.hpp"
#include <vector>
#include <list>
#include <queue>
#include <bot_core/bot_core.h>



using namespace std;
using namespace occ_map;

namespace dp_planner {


    /*
     * Utility class for maintaining the score of candidate contour merges
     */
        
    class FrontierElement2d {
    public:
        float value;
        int ixy[2];
        int goal_ixy[2]; 
        double obs_val; 
        FrontierElement2d(float v, int _ixy[2], int g_ixy[2]) :
            value(v)
        {
            ixy[0] = _ixy[0];
            ixy[1] = _ixy[1];
            goal_ixy[0] = g_ixy[0];
            goal_ixy[1] = g_ixy[1];
            obs_val = 0;
        }

        FrontierElement2d(float v, int _ixy[2], int g_ixy[2], double _obs) :
            value(v)
        {
            ixy[0] = _ixy[0];
            ixy[1] = _ixy[1];
            goal_ixy[0] = g_ixy[0];
            goal_ixy[1] = g_ixy[1];
            obs_val = _obs;
        }

         FrontierElement2d(float v, int _ixy[2]) :
            value(v)
        {
            ixy[0] = _ixy[0];
            ixy[1] = _ixy[1];
        }
    };

    class GraphNode {
    public:
        float value;
        double xyt[3]; 
        int ixy[2];
        std::vector<GraphNode *> neighbours; 
        GraphNode(float v, double _xyt[3]) :
            value(v)
        {
            xyt[0] = _xyt[0];
            xyt[1] = _xyt[1];
            xyt[2] = _xyt[2];
        }

        void addNeighbour(GraphNode *node){
            neighbours.push_back(node);
        }
    };
    
    /*
     * comparator class that allows us to put the joins into a priority_queue
     */
    class FrontierCompare2d {
    public:
        //if picking the first element to explore - this should be the current value + - cost to go??
        bool operator()(const FrontierElement2d & f1, const FrontierElement2d & f2)
        {
            return (f1.value + hypot(f1.goal_ixy[0] - f1.ixy[0], 
                                     f1.goal_ixy[1] - f1.ixy[1]))> 
                (f2.value +  hypot(f2.goal_ixy[0] - f2.ixy[0], 
                                      f2.goal_ixy[1] - f2.ixy[1]));
        }
    };

    class FrontierWeightedCompare2d {
    public:
        //if picking the first element to explore - this should be the current value + - cost to go??
        bool operator()(const FrontierElement2d & f1, const FrontierElement2d & f2)
        {
            double epsilon = 2.0;

            return (f1.value + epsilon * hypot(f1.goal_ixy[0] - f1.ixy[0], 
                                     f1.goal_ixy[1] - f1.ixy[1]))> 
                (f2.value +  epsilon * hypot(f2.goal_ixy[0] - f2.ixy[0], 
                                      f2.goal_ixy[1] - f2.ixy[1]));
        }
    };

    class GraphCompare {
    public:
        bool operator()(const GraphNode & f1, const GraphNode & f2)
        {
            return f1.value > f2.value;
        }
    };


    // sets utility in meters
    FloatPixelMap * createUtilityMap(const FloatPixelMap * pixmap, const double start[2]) {
        if (pixmap->readValue(start) > 0) {
            fprintf(stderr, "ERROR: start point is in an obstacle!\n");
            return NULL;
        }

        // create a blank map with cells initialized to -1
        FloatPixelMap * utility = new FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);

        int ixy[2] = { 0 };
        utility->worldToTable(start, ixy);
        utility->writeValue(ixy, 0);
        //type 1 - normal A* search 
        //type 2 - weighted A* search 
        
        priority_queue<FrontierElement2d, std::vector<FrontierElement2d>, FrontierCompare2d> frontier;
        //this one doesnt matter here - as we are using the exact cost 
        
        //priority_queue<FrontierElement2d, std::vector<FrontierElement2d>, FrontierCompare2d> frontier;
        frontier.push(FrontierElement2d(0, ixy));

        int count = 0;
        while (!frontier.empty()) {
            count++;

            const FrontierElement2d f = frontier.top();

            assert(pixmap->readValue(f.ixy) < DPP_UNK_THRESH);

            // add all the pixel neighbors
            int ixyn[2] = { 0 };
            for (int i = -1; i <= 1; i++) {
                ixyn[0] = f.ixy[0] + i;
                for (int j = -1; j <= 1; j++) {
                    ixyn[1] = f.ixy[1] + j;
                    // skip myself
                    if (i == 0 && j == 0)
                        continue;
                    // skip if out of map
                    if (!utility->isInMap(ixyn))
                        continue;
                    // if we've already set the utility, it's optimal
                    if (utility->readValue(ixyn) >= 0)
                        continue;
                    // check that cell is in free space
                    if (pixmap->readValue(ixyn) < DPP_UNK_THRESH) {
                        float dist = sqrt(i*i + j*j) * pixmap->metersPerPixel; // distance in m
                        frontier.push(FrontierElement2d(f.value + dist, ixyn));

                        // set a high value for the neighbor so that it doesn't get pushed
                        // on to the frontier again
                        // when the neighbor is expanded on the queue, it's utility will be
                        // corrected
                        utility->writeValue(ixyn, 1e9);
                    }
                    else if (pixmap->readValue(ixyn) > DPP_OCC_THRESH) {
                        utility->writeValue(ixyn, INFINITY);
                    }
                    else {
                        utility->writeValue(ixyn, INFINITY);
                    }
                }
            }
            utility->writeValue(f.ixy, f.value);

            frontier.pop();
        }

        return utility;
    }

    /**
     * Dilate pixel map.  Dilates obstacle cells outward in x and y directions, with
     * (+/- dilation[i] / 2) pixels
     */
    FloatPixelMap * dilateMap(const FloatPixelMap *pixmap, double dilation_size)
    {
        FloatPixelMap* pixmap_dilated = new FloatPixelMap(pixmap);
        FloatPixelMap* tmp_map = new FloatPixelMap(pixmap);
        FloatPixelMap* dst_map;
        FloatPixelMap* src_map;

        int size_x = pixmap->dimensions[0];
        int size_y = pixmap->dimensions[1];
        int half_wsize[2];
        for(int i=0; i < 2; i++) {
            half_wsize[i] = (int)ceil(0.5 * dilation_size / pixmap->metersPerPixel);
        }

        int ixy[2];

        //src_map = pixmap;
        dst_map = tmp_map;

        // X pass
        for(int iy=0; iy<size_y; iy++) {
            ixy[1] = iy;
            int half_ws = half_wsize[0];
            for(int ix=0; ix<size_x; ix++) {
                float worst_val = -INFINITY;
                for(int off=-half_ws; off<half_ws; off++) {
                    ixy[0] = ix + off;
                    if(ixy[0] < 0 || ixy[0] >= size_x)
                        continue;
                    float val = pixmap->readValue(ixy);
                    if(val > worst_val)
                        worst_val = val;
                }
                ixy[0] = ix;
                dst_map->writeValue(ixy, worst_val);
            }
        }

        src_map = tmp_map;
        dst_map = pixmap_dilated;

        // Y pass
        for(int ix=0; ix<size_x; ix++) {
            ixy[0] = ix;
            int half_ws = half_wsize[1];
            for(int iy=0; iy<size_y; iy++) {
                float worst_val = -INFINITY;
                for(int off=-half_ws; off<half_ws; off++) {
                    ixy[1] = iy + off;
                    if(ixy[1] < 0 || ixy[1] >= size_y)
                        continue;
                    float val = src_map->readValue(ixy);
                    if(val > worst_val)
                        worst_val = val;
                }
                ixy[1] = iy;
                dst_map->writeValue(ixy, worst_val);
            }
        }

        delete tmp_map;

        return pixmap_dilated;
    }


    FloatPixelMap * createCostMap(const FloatPixelMap *pixmap, double dilation_size, 
                                  double fixed_size)
    {
        FloatPixelMap* pixmap_dilated = new FloatPixelMap(pixmap);

        int size_x = pixmap->dimensions[0];
        int size_y = pixmap->dimensions[1];

        //this is the total affected size
        int dil_radius = dilation_size / pixmap->metersPerPixel;
        
        //this is the fixed size - should be the radius of the robot
        int fixed_radius = fixed_size / pixmap->metersPerPixel;

        for (int i=0; i < size_x; i++){
            for (int j=0; j < size_y; j++){
                int xy0[2] = {i,j};
                //freespace
                if(pixmap->readValue(xy0) < 0.6)
                    continue;

                for(int k= -dil_radius; k<= dil_radius; k++){
                    for(int l= -dil_radius; l<= dil_radius; l++){
                        int xy[2] = {i+k, j+l};
                        if(xy[0] < 0 || xy[1] < 0 || xy[0] > size_x-1|| xy[1] > size_y - 1){
                            continue;
                        }
                        if(hypot(k,l) > dil_radius)
                            continue;
                        else if(hypot(k,l) > fixed_radius){
                            //drop off the cost
                            double dil_ratio = 1 - (hypot(k,l) - fixed_radius)/ (dil_radius - fixed_radius); 
                            
                            if(dil_ratio < 0)
                                dil_ratio = 0;                          
                            //need to do this check - otherwise it will reset the empty spaces
                            if(pixmap_dilated->readValue(xy) < 0.99 * dil_ratio){
                                pixmap_dilated->writeValue(xy, 0.99 * dil_ratio);
                            }
                            //fprintf(stderr, "%d,%d - %f\n", xy[0], xy[1],   0.99 * dil_ratio);
                        }
                        //create a cliff 
                        else if(hypot(k,l) <= fixed_radius){
                            pixmap_dilated->updateValue(xy, 0.99);
                            //fprintf(stderr, "%d,%d - %f\n", xy[0], xy[1],   0.99);
                        }
                    }
                }
            }
        }           
                            
        return pixmap_dilated;
    }

    FloatPixelMap * updateDistanceCostMap(FloatPixelMap *pixmap, FloatPixelMap *distmap_orig, 
                              double r_radius, double s_radius, 
                              xy_t center, double update_radius, 
                              PointList2d *p_list)
    {
        FloatPixelMap *distmap = new FloatPixelMap(distmap_orig);
        //carve out a radius in the cost map - and fill in these obstacle points
        double x0 = -update_radius;
        double x1 = update_radius;
        double y0 = -update_radius;
        double y1 = update_radius;
        
        double res = pixmap->metersPerPixel;
        double dxy[2];

        for(double x= x0; x <= x1 ; x+= res){
            for(double y= y0; y <= y1 ; y+= res){
                if(hypot(x,y) < update_radius){
                    dxy[0] = x+ center.xy[0];
                    dxy[1] = y+ center.xy[1];
                    if(pixmap->isInMap(dxy))
                        pixmap->writeValue(dxy, 0.0);
                }
            }
        }  

        //we dont clear the distance map around the edges - because otherwise we will have to fill them in again 
        fprintf(stderr,"Pointer %p \n" , (void *) distmap);
        for(double x= x0 + s_radius; x <= x1 - s_radius ; x+= res){
            for(double y= y0 + s_radius; y <= y1 - s_radius; y+= res){
                if(hypot(x,y) < update_radius){
                    dxy[0] = x+ center.xy[0];
                    dxy[1] = y+ center.xy[1];
                    if(distmap->isInMap(dxy))
                        distmap->writeValue(dxy, 0.0);
                }
            }
        }  
        
        //fill up the pixmap
        for(int i=0; i< p_list->no_points; i++){
            if(pixmap->isInMap(p_list->points[i].xy)){
                pixmap->writeValue(p_list->points[i].xy, 0.99);
            }
        }

        //we should update around the area
        //this prob should be bounded by the original map - or make sure that we dont go out of map
        double xy0[2] = {center.xy[0] - update_radius - s_radius, center.xy[1] - update_radius - s_radius};
        double xy1[2] = {center.xy[0] + update_radius + s_radius, center.xy[1] + update_radius + s_radius};
        
        IntPixelMap* visited_map = new IntPixelMap(xy0, xy1, pixmap->metersPerPixel, 0);

        int robot_radius = (int) (r_radius/pixmap->metersPerPixel);  

        int sweep_radius = (int) (s_radius /pixmap->metersPerPixel); 

        fprintf(stderr,"Robot Radius : %d, Sweep Radius : %d\n", robot_radius, sweep_radius);
        
        for(int d = 1; d < sweep_radius+1; d++){
            for(int i=0; i< p_list->no_points; i++){
                int ixy[2];
                pixmap->worldToTable(p_list->points[i].xy, ixy);
                
                for(int j=-d; j<=d; j++){
                    int ixyu[2] = {ixy[0] + j, ixy[1] + d};
                    int ixyd[2] = {ixy[0] + j, ixy[1] - d};

                    double dxyu[2];
                    double dxyd[2];
                    pixmap->tableToWorld(ixyu, dxyu);
                    pixmap->tableToWorld(ixyd, dxyd);

                    //                    fprintf(stderr, "is in map : %d\n", distmap->isInMap(ixyu));
                    //this point in in the map and not visited
                    if(distmap->isInMap(ixyu) && visited_map->readValue(dxyu) == 0){
                        double dist_cost = 1 - fmax(hypot(j, d) - robot_radius, 0) / sweep_radius; 
                        double max_dist_cost = fmax(distmap->readValue(ixyu), dist_cost);
                        distmap->writeValue(ixyu, max_dist_cost);      
                        visited_map->writeValue(dxyu,1);
                    }
                    
                    //this point in in the map and not visited
                    if(distmap->isInMap(ixyd) && visited_map->readValue(dxyd) == 0){
                        double dist_cost = 1 - fmax(hypot(j, d) - robot_radius, 0) / sweep_radius; 
                        double max_dist_cost = fmax(distmap->readValue(ixyd), dist_cost);
                        distmap->writeValue(ixyd, max_dist_cost);
                        visited_map->writeValue(dxyd,1);
                    }
                }

                for(int j=-d; j<=d; j++){
                    int ixyu[2] = {ixy[0] + d, ixy[1] + j};
                    int ixyd[2] = {ixy[0] - d, ixy[1] + j};

                    double dxyu[2];
                    double dxyd[2];
                    pixmap->tableToWorld(ixyu, dxyu);
                    pixmap->tableToWorld(ixyd, dxyd);

                    //this point in in the map and not visited
                    if(distmap->isInMap(ixyu) && visited_map->readValue(dxyu) == 0){
                        double dist_cost = 1 - fmax(hypot(j, d) - robot_radius, 0) / sweep_radius; 
                        double max_dist_cost = fmax(distmap->readValue(ixyu), dist_cost);
                        distmap->writeValue(ixyu, max_dist_cost);
                        visited_map->writeValue(dxyu,1);
                    }

                    //this point in in the map and not visited
                    if(distmap->isInMap(ixyd) && visited_map->readValue(dxyd) == 0){
                        double dist_cost = 1 - fmax(hypot(j, d) - robot_radius, 0) / sweep_radius; 
                        double max_dist_cost = fmax(distmap->readValue(ixyd), dist_cost);
                        distmap->writeValue(ixyd, max_dist_cost);
                        visited_map->writeValue(dxyd,1);
                    }
                }      
            }   
        }
        delete visited_map;
        return distmap;
    }
      
    FloatPixelMap *creatDistanceMapFromPoints(PointList2d p_list, lcm_t *lcm, xy_t xy0, 
                                              xy_t xy1, double res){
        
        FloatPixelMap* distance_map = new FloatPixelMap(xy0.xy, xy1.xy, res, 0);

        for(int i=0; i< p_list.no_points; i++){
            if(distance_map->isInMap(p_list.points[i].xy)){
                distance_map->writeValue(p_list.points[i].xy, 0.99);
            }
        }
        return distance_map;
    }

    IntPixelMap* createDistanceMap(const FloatPixelMap *pixmap, lcm_t *lcm)
    {
        //this tells us the index of the parent of the current node 
        IntPixelMap* distance_map = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, 100000);

        int size_x = pixmap->dimensions[0];
        int size_y = pixmap->dimensions[1];

        vector<int> obs_indices;

        //a big chunk of the map is unexplored
        //add visited indexes??
        IntPixelMap* visited_map = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, 0);

        int count = 0;
        int unobs_count = 0;
        int empty_count = 0;

        for (int i=0; i < size_x; i++){
            for (int j=0; j < size_y; j++){
                int xy0[2] = {i,j};
                if(pixmap->readValue(xy0)>=0.9){
                    //occupied
                    count++;
                    obs_indices.push_back(pixmap->getInd(xy0));
                    visited_map->writeValue(xy0, 1);
                    distance_map->writeValue(xy0, 0);
                }

                else if(pixmap->readValue(xy0)== 0.5){
                    //occupied
                    unobs_count++;
                    //obs_indices.push_back(pixmap->getInd(xy0));
                    visited_map->writeValue(xy0, 1);
                    distance_map->writeValue(xy0, 10000);
                }
                else{
                    empty_count++;
                }
            }
        }

        int visited_count = count + unobs_count;
        int total_count = size_x * size_y; 

        fprintf(stderr,"Obs Count : %d UnObs : %d -> Total : %d ++ Empty : %d\n", count, unobs_count, size_x * size_y, empty_count);

        int filled_count = 0;
        
        int sweep_radius = 20; 

        for(int d = 1; d < sweep_radius; d++){
            for (int i=0;i<obs_indices.size();i++){
                //fill the values of the distance to obs at increasing radius
                int obs_ind = obs_indices[i]; 
                //fprintf(stderr,"Ind : %d\n", obs_ind);
                int ixy[2];
                pixmap->indToLoc(obs_ind, ixy);

                for(int j=-d; j<=d; j++){
                    int ixyu[2] = {ixy[0] + j, ixy[1] + d};
                    int ixyd[2] = {ixy[0] + j, ixy[1] - d};

                    //this point in in the map and not visited
                    if(visited_map->isInMap(ixyu) && visited_map->readValue(ixyu) == 0){
                        int dist = fmin(distance_map->readValue(ixyu), d);
                        distance_map->writeValue(ixyu, dist);      
                        visited_map->writeValue(ixyu,1);
                        visited_count++;
                        filled_count++;
                    }

                    //this point in in the map and not visited
                    if(visited_map->isInMap(ixyd) && visited_map->readValue(ixyd) == 0){
                        int dist = fmin(distance_map->readValue(ixyd), dist);
                        distance_map->writeValue(ixyd, d);
                        visited_map->writeValue(ixyd,1);
                        visited_count++;
                        filled_count++;
                    }
                }

                for(int j=-d; j<=d; j++){
                    int ixyu[2] = {ixy[0] + d, ixy[1] + j};
                    int ixyd[2] = {ixy[0] - d, ixy[1] + j};

                    //this point in in the map and not visited
                    if(visited_map->isInMap(ixyu) && visited_map->readValue(ixyu) == 0){
                        int dist = fmin(distance_map->readValue(ixyu), d);
                        distance_map->writeValue(ixyu, dist);
                        visited_map->writeValue(ixyu,1);
                        visited_count++;
                        filled_count++;
                    }

                    //this point in in the map and not visited
                    if(visited_map->isInMap(ixyd) && visited_map->readValue(ixyd) == 0){
                        int dist = fmin(distance_map->readValue(ixyd), d);
                        distance_map->writeValue(ixyd, dist);
                        visited_map->writeValue(ixyd,1);
                        visited_count++;
                        filled_count++;
                    }
                }              
            }
            fprintf(stderr, "Visited Count : %d => Total Count : %d +++ Filled Count : %d/%d\n", visited_count, total_count, filled_count, 
                    empty_count);
            if(total_count == visited_count){
                break;
            }
        }

        if(0){
            bot_lcmgl_t *lcmgl = bot_lcmgl_init(lcm, "distance_map");

            lcmglBegin(GL_POINTS);
            lcmglColor3f(1.0, 0.0, 0.0);
            //lcmglPointSize(10);

            for (int i=0; i < size_x; i++){
                for (int j=0; j < size_y; j++){
                    int ixyd[2] = {i,j};
                
                    lcmglColor3f(fmax(1 -distance_map->readValue(ixyd)/((double)sweep_radius),0),0,0);//cost / 700);
                    double dxy[2];
                    distance_map->tableToWorld(ixyd, dxy);
                    lcmglVertex3d( dxy[0], dxy[1], 0 );
                }
            }

        
            lcmglEnd();
            bot_lcmgl_switch_buffer (lcmgl);
        }

        return distance_map;
    }

    
    FloatPixelMap* createDistanceCostMap(const FloatPixelMap *pixmap, lcm_t *lcm, double r_radius, double s_radius)
    {
        //this tells us the index of the parent of the current node 
        FloatPixelMap* distance_map = new FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, 0);

        int size_x = pixmap->dimensions[0];
        int size_y = pixmap->dimensions[1];

        vector<int> obs_indices;

        //a big chunk of the map is unexplored
        //add visited indexes??
        IntPixelMap* visited_map = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, 0);

        int robot_radius = (int) (r_radius/pixmap->metersPerPixel);  //3; 

        int count = 0;
        int unobs_count = 0;
        int empty_count = 0;

        for (int i=0; i < size_x; i++){
            for (int j=0 ; j < size_y; j++){
                int xy0[2] = {i,j};
                if(pixmap->readValue(xy0)>=0.9){
                    //occupied
                    count++;
                    obs_indices.push_back(pixmap->getInd(xy0));
                    visited_map->writeValue(xy0, 1);
                    distance_map->writeValue(xy0, 1);
                }

                else if(pixmap->readValue(xy0)== 0.5){
                    //occupied
                    unobs_count++;
                    //obs_indices.push_back(pixmap->getInd(xy0));
                    visited_map->writeValue(xy0, 1);
                    distance_map->writeValue(xy0, 1);
                }
                else{
                    empty_count++;
                }
            }
        }
        
        int visited_count = count + unobs_count;
        int total_count = size_x * size_y; 

        fprintf(stderr,"Obs Count : %d UnObs : %d -> Total : %d ++ Empty : %d\n", count, unobs_count, size_x * size_y, empty_count);

        int filled_count = 0;
        
        int sweep_radius = (int) (s_radius /pixmap->metersPerPixel); 

        fprintf(stderr,"Robot Radius : %d, Sweep Radius : %d\n", robot_radius, sweep_radius);

        for(int d = 1; d < sweep_radius+1; d++){
            for (int i=0;i<obs_indices.size();i++){
                //fill the values of the distance to obs at increasing radius
                int obs_ind = obs_indices[i]; 
                int ixy[2];
                pixmap->indToLoc(obs_ind, ixy);

                for(int j=-d; j<=d; j++){
                    int ixyu[2] = {ixy[0] + j, ixy[1] + d};
                    int ixyd[2] = {ixy[0] + j, ixy[1] - d};

                    //this point in in the map and not visited
                    if(visited_map->isInMap(ixyu) && visited_map->readValue(ixyu) == 0){
                        double dist_cost = 1 - fmax(hypot(j, d) - robot_radius, 0) / sweep_radius; 
                        double max_dist_cost = fmax(distance_map->readValue(ixyu), dist_cost);
                        distance_map->writeValue(ixyu, max_dist_cost);      
                        visited_map->writeValue(ixyu,1);
                        visited_count++;
                        filled_count++;
                    }
                    
                    //this point in in the map and not visited
                    if(visited_map->isInMap(ixyd) && visited_map->readValue(ixyd) == 0){
                        double dist_cost = 1 - fmax(hypot(j, d) - robot_radius, 0) / sweep_radius; 
                        double max_dist_cost = fmax(distance_map->readValue(ixyd), dist_cost);
                        distance_map->writeValue(ixyd, max_dist_cost);
                        visited_map->writeValue(ixyd,1);
                        visited_count++;
                        filled_count++;
                    }
                    
                }

                for(int j=-d; j<=d; j++){
                    int ixyu[2] = {ixy[0] + d, ixy[1] + j};
                    int ixyd[2] = {ixy[0] - d, ixy[1] + j};

                    //this point in in the map and not visited
                    if(visited_map->isInMap(ixyu) && visited_map->readValue(ixyu) == 0){
                        double dist_cost = 1 - fmax(hypot(j, d) - robot_radius, 0) / sweep_radius; 
                        double max_dist_cost = fmax(distance_map->readValue(ixyu), dist_cost);
                        distance_map->writeValue(ixyu, max_dist_cost);
                        visited_map->writeValue(ixyu,1);
                        visited_count++;
                        filled_count++;
                    }

                    //this point in in the map and not visited
                    if(visited_map->isInMap(ixyd) && visited_map->readValue(ixyd) == 0){
                        double dist_cost = 1 - fmax(hypot(j, d) - robot_radius, 0) / sweep_radius; 
                        double max_dist_cost = fmax(distance_map->readValue(ixyd), dist_cost);
                        distance_map->writeValue(ixyd, max_dist_cost);
                        visited_map->writeValue(ixyd,1);
                        visited_count++;
                        filled_count++;
                    }
                }              
            }
            if(total_count == visited_count){
                break;
            }
        }

        bot_lcmgl_t *lcmgl = bot_lcmgl_init(lcm, "distance_map");

        lcmglBegin(GL_POINTS);
        lcmglColor3f(1.0, 0.0, 0.0);

        for (int i=0; i < size_x; i++){
            for (int j=0; j < size_y; j++){
                int ixyd[2] = {i,j};
                
                lcmglColor3f(distance_map->readValue(ixyd),0,0);
                double dxy[2];
                distance_map->tableToWorld(ixyd, dxy);
                lcmglVertex3d( dxy[0], dxy[1], 0 );
            }
        }

        
        lcmglEnd();
        bot_lcmgl_switch_buffer (lcmgl);

        delete visited_map;
        obs_indices.clear();

        return distance_map;
    }

    static bool waypoints_are_collinear(const Waypoint2d& a, const Waypoint2d& b, const Waypoint2d& c)
    {
        double m[2] = { c.x - a.x, c.y - a.y };
        double n[2] = { b.x - a.x, b.y - a.y };
        double t = (m[0] * n[0] + m[1] * n[1]) / (m[0] * m[0] + m[1] * m[1]);
        double cp[2] = { a.x + t * m[0], a.y + t * m[1] };
        double d[2] = { b.x - cp[0], b.y - cp[1] };
        double dist = sqrt(d[0] * d[0] + d[1] * d[1]);
        return dist < 1e-3;
    }

    void getPath(const occ_map::FloatPixelMap * pixmap, const std::vector<int> &path_indices,
                 const Waypoint2d start_wypt, const Waypoint2d & goal_wypt, std::vector<Waypoint2d> &waypoints)
    {
        waypoints.clear();
        int nwp = 0;
        fprintf(stderr, "Getting Path, World is:\n");
        for (int i = 0; i < path_indices.size(); i++) {
            int ixy[2];
            pixmap->indToLoc(path_indices[i], ixy);
            double xyt[3]={ 0 };
            pixmap->tableToWorld(ixy, xyt);

            fprintf(stderr, "\tx:%f\ty%f\n", xyt[0], xyt[1]);

            Waypoint2d wp(xyt);

            // sparsify path
            if (nwp > 1 && waypoints_are_collinear(waypoints[nwp - 2], waypoints[nwp - 1], wp)){
                waypoints[nwp - 1] = wp; //update prev
                continue;
            }
            waypoints.push_back(wp);
            nwp++;
        }
        waypoints[0]= start_wypt; //Go from exactly where we started, not discretized version...

        //set the yaw for the waypoints
        for (int i=1;i<waypoints.size();i++){
            const Waypoint2d& prev_wp = waypoints[i - 1];
            Waypoint2d& wp = waypoints[i];
            double dx = wp.x - prev_wp.x;
            double dy = wp.y - prev_wp.y;
            wp.theta = atan2(dy, dx);
        }
        waypoints[0].theta =waypoints[1].theta; //rotate in place first?
        waypoints.push_back(goal_wypt); //add the goal waypt for final yaw...
    }

    void getPathPruned(const occ_map::FloatPixelMap * pixmap, const std::vector<int> &path_indices,
                 const Waypoint2d start_wypt, const Waypoint2d & goal_wypt, std::vector<Waypoint2d> &waypoints)
    {
        waypoints.clear();
        int nwp = 0;
        fprintf(stderr, "Getting Path, World is:\n");
        for (int i = 0; i < path_indices.size(); i++) {
            int ixy[2];
            pixmap->indToLoc(path_indices[i], ixy);
            double xyt[3]={ 0 };
            pixmap->tableToWorld(ixy, xyt);

            fprintf(stderr, "\tx:%f\ty%f\n", xyt[0], xyt[1]);

            Waypoint2d wp(xyt);

            if(nwp > 1){
                fprintf(stderr,"Dist : %f\n", hypot(waypoints[nwp - 2].x - wp.x , waypoints[nwp - 2].y - wp.y));
            }

            // sparsify path
            if ((nwp > 1 && waypoints_are_collinear(waypoints[nwp - 2], waypoints[nwp - 1], wp))
                || (nwp > 1 && hypot(waypoints[nwp - 2].x - wp.x , waypoints[nwp - 2].y - wp.y) < 5)) {

                waypoints[nwp - 1] = wp; //update prev
                continue;
            }
            waypoints.push_back(wp);
            nwp++;
        }
        waypoints[0]= start_wypt; //Go from exactly where we started, not discretized version...

        //set the yaw for the waypoints
        for (int i=1;i<waypoints.size();i++){
            const Waypoint2d& prev_wp = waypoints[i - 1];
            Waypoint2d& wp = waypoints[i];
            double dx = wp.x - prev_wp.x;
            double dy = wp.y - prev_wp.y;
            wp.theta = atan2(dy, dx);
        }

        //we dont need to add the start point - remove it 

        waypoints[0].theta =waypoints[1].theta; //rotate in place first?
        //waypoints.erase(waypoints.begin());

        waypoints.push_back(goal_wypt); //add the goal waypt for final yaw...
    }

    bool dpPathToGoalLattice(const occ_map::FloatPixelMap * pixmap, const occ_map::FloatPixelMap * dist_map, const double start_xyt[3], const double goal_xyt[3],
                              std::vector<Waypoint2d>& waypoints, lcm_t *lcm)
    {
        if (pixmap->readValue(start_xyt) > 0.99) {
            fprintf(stderr, "ERROR: start point is in an obstacle!\n");
            return false;
        }
        if (pixmap->readValue(goal_xyt) > 0.99) {
            fprintf(stderr, "ERROR: goal point is in an obstacle!\n");
            return false;
        }

        FloatPixelMap* cost_to_go = new FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, 1000000);

        //this tells us the index of the parent of the current node 
        IntPixelMap* parents = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);

        //this tells us the index of the parent of the current node 
        IntPixelMap* dist_to_obstacles = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);
        
        //this tells us the index of the parent of the current node 
        IntPixelMap* visited = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, 0);

        IntPixelMap* explored = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, 0);

        vector<int> path_indices;

        int ixy[2] = { 0 };
        cost_to_go->worldToTable(goal_xyt, ixy);
        int goal_ind = cost_to_go->getInd(ixy);
        //set the cost to go for the goal to be zero 
        cost_to_go->writeValue(ixy, 0);
        visited->writeValue(ixy, 1);
        explored->writeValue(ixy, 1);
        //add the goal as the first freontier element 
        priority_queue<FrontierElement2d, std::vector<FrontierElement2d>, FrontierCompare2d> frontier;
        
        //this is the start position in index
        int ixy_start[2];
        cost_to_go->worldToTable(start_xyt, ixy_start);

        frontier.push(FrontierElement2d(0, ixy, ixy_start, pixmap->readValue(ixy_start)));

        int count = 0;

        //while there are unexplored nodes 
        //explorer the least costly one - till we come to the 
        //start node 
        bot_lcmgl_t *lcmgl = bot_lcmgl_init(lcm, "lattice_graph");

        lcmglBegin(GL_POINTS);
        lcmglColor3f(1.0, 0.0, 0.0);

        double max_cost = 0;
          
        int64_t start_time = bot_timestamp_now();
        
        int size_x = pixmap->dimensions[0];
        int size_y = pixmap->dimensions[1];

        while (!frontier.empty()) {
            count++;

            //get the least costly element in the queue
            const FrontierElement2d f = frontier.top();
            visited->writeValue(f.ixy, 1);

            if(count %10000 == 0)
                fprintf(stderr,"Count : %d - Total : %d\n", count,  size_x * size_y);
            
            if (f.ixy[0] == ixy_start[0] && f.ixy[1] == ixy_start[1]) {
                // found a path.  Traverse from start to goal to get the path
                fprintf(stderr, "FOUND PATH:\n");
                break;
            }  

            //add all the neighbors
            int ixyn[2] = { 0 };
            for (int i = -1; i <= 1; i++) {
                ixyn[0] = f.ixy[0] + i;
                for (int j = -1; j <= 1; j++) {
                    ixyn[1] = f.ixy[1] + j;
                    
                    //ignore if this is current node                    
                    if (i == 0 && j == 0){
                        continue;
                    }
                                        
                    //ignore if the point is not in the map - or if the node has been visited 
                    if (!cost_to_go->isInMap(ixyn) || visited->readValue(ixyn)){
                        continue;
                    }
                    
                    //unobserved
                    if (pixmap->readValue(ixyn) == 0.5){
                        continue;
                    }

                    //not occupied - but can be close to obstacle
                    if (pixmap->readValue(ixyn) < 0.99 && dist_map->readValue(ixyn) < 0.99) { 
                        float dist = sqrt(i * i + j * j);
                        //if this is a known empty point - add the frontier queue
                        double dxy[3] = {0,0,0}; 
                        pixmap->tableToWorld(ixyn, dxy);

                        double scale = 5.0;

                        //dist + risk of travelling 

                        double cost = f.value + dist + scale * dist_map->readValue(ixyn); 

                        //adding to the frontier list
                        if(explored->readValue(ixyn) == 0){
                            frontier.push(FrontierElement2d(cost, ixyn, ixy_start, fmax(f.obs_val, pixmap->readValue(ixyn))));
                        }
                        
                                               
                        if(cost_to_go->readValue(ixyn) < 0 || cost < cost_to_go->readValue(ixyn)){
                            //add the parent to the parent list 
                            parents->writeValue(ixyn, cost_to_go->getInd(f.ixy));
                            cost_to_go->writeValue(ixyn, cost);

                                                      
                        }
                        
                        if (ixyn[0] == ixy_start[0] && ixyn[1] == ixy_start[1]) {
                            // found a path.  Traverse from start to goal to get the path
                            fprintf(stderr, "FOUND PATH Outside :\n");
                        }  

                        explored->writeValue(ixyn,1);

                        if(max_cost < cost){
                            max_cost = cost;
                        }
                        
                        lcmglColor3f(cost/700,0,0);
                        lcmglVertex3d( dxy[0], dxy[1], 0 );
                    }
                    else if (pixmap->readValue(ixyn) >= pixmap->readValue(ixyn)) {
                        cost_to_go->writeValue(ixyn, INFINITY);
                    }
                    else {
                        cost_to_go->writeValue(ixyn, INFINITY);
                    }
                }
            }
            cost_to_go->writeValue(f.ixy, f.value);

            frontier.pop();
        }
             
        fprintf(stderr,"Max Cost : %f\n", max_cost);

        //rescale the cost map 
        for (int i=0; i < size_x; i++){
            for (int j=0 ; j < size_y; j++){
                int xy0[2] = {i,j};
                double c_cost = cost_to_go->readValue(xy0); 
                cost_to_go->writeValue(xy0, fmin(c_cost, max_cost)/ max_cost); 
            }
        }
            
        const occ_map_pixel_map_t *o_msg = cost_to_go->get_pixel_map_t(bot_timestamp_now()); 
        
        occ_map_pixel_map_t_publish(lcm, "PIXEL_MAP",  o_msg);

        int64_t end_time = bot_timestamp_now();

        fprintf(stderr,"Time to explore : %f\n", (end_time -start_time) /1.0e6);

        fprintf(stderr,"Adding neighbours -Done : %d - Max cost : %f\n", count, max_cost);

        //this adds a set of neighbours - we need to adjust how the costs are calculated though 

        lcmglEnd();
        bot_lcmgl_switch_buffer (lcmgl);

        lcmgl = bot_lcmgl_init(lcm, "lattice_path_extract_other");

        lcmglBegin(GL_LINES);
        lcmglColor3f(0, .5, 1.0);

        if( parents->readValue(ixy_start) != -1){
            int ixy_current[2] = {ixy_start[0], ixy_start[1]};
            
            while(!(ixy_current[0] == ixy[0] && ixy_current[1] == ixy[1])){
                double min_cost = 10000000; 
                int ixyn[2] = { ixy_current[0], ixy_current[1]};

                int ixy_low[2];
                for (int i = -1; i <= 1; i++) {
                    ixyn[0] = ixy_current[0] + i;
                    for (int j = -1; j <= 1; j++) {
                        ixyn[1] = ixy_current[1] + j;
                        double cs = cost_to_go->readValue(ixyn);
                        if(cs < min_cost){
                            min_cost = cs; 
                            ixy_low[0] = ixyn[0];
                            ixy_low[1] = ixyn[1];
                        }
                    }
                }
                double dp_xy[2] = {0,0};
                cost_to_go->tableToWorld(ixy_low, dp_xy);
                lcmglVertex3d( dp_xy[0], dp_xy[1], 0 );
                //fprintf(stderr, "\t- x:%f\ty:%f\n", dp_xy[0], dp_xy[1]);
                ixy_current[0] = ixy_low[0];
                ixy_current[1] = ixy_low[1];            
            }
        }

        lcmglEnd();
        bot_lcmgl_switch_buffer (lcmgl);

        lcmgl = bot_lcmgl_init(lcm, "lattice_path_extract");

        lcmglBegin(GL_LINES);
        lcmglColor3f(0, 0.0, 1.0);
        //this should end at the goal 
        if( parents->readValue(ixy_start) != -1){
            fprintf(stderr, "FOUND PATH:\n");
            
            //search neighbours - for the lowest cost path 
            
            

            int pxy[2] = {0,0};
            int cur_ind = parents->readValue(ixy_start); // cost_to_go->getInd(ixyn);

            //what if we extract path based on the cost map - and not parents
            
            //the parents need to be updated
            
            
            //fprintf(stderr, "cur_ind = %d\n", cur_ind);
            //cost_to_go->indToLoc(cur_ind, pxy);
            //fprintf(stderr, "pxy:[0] %d\t[1]%d\n", pxy[0], pxy[1]);
            for (; cur_ind != goal_ind; cur_ind = parents->data[cur_ind]) {
                path_indices.push_back(cur_ind);
                cost_to_go->indToLoc(cur_ind, pxy);

                double dp_xy[2] = {0,0};
                cost_to_go->tableToWorld(pxy, dp_xy);
                lcmglVertex3d( dp_xy[0], dp_xy[1], 0 );
                //fprintf(stderr, "\t[%d]- x:%f\ty:%f\n", cur_ind, dp_xy[0], dp_xy[1]);
            }
            lcmglEnd();
            bot_lcmgl_switch_buffer (lcmgl);
            path_indices.push_back(cur_ind);
            getPathPruned(cost_to_go, path_indices, Waypoint2d(start_xyt), Waypoint2d(goal_xyt), waypoints);
            delete cost_to_go;
            delete parents;
            delete visited;
            delete explored;
            return true;
        }
        else{
            fprintf(stderr,"No Path found\n");
        }

        delete cost_to_go;
        delete parents;
        delete visited;
        delete explored;

        return false;
    }


    bool dpPathToGoal(const occ_map::FloatPixelMap * pixmap, const double start_xyt[3], const double goal_xyt[3],
                      std::vector<Waypoint2d>& waypoints)
    {
        if (pixmap->readValue(start_xyt) > 0) {
            fprintf(stderr, "ERROR: start point is in an obstacle!\n");
            return false;
        }
        if (pixmap->readValue(goal_xyt) > 0) {
            fprintf(stderr, "ERROR: goal point is in an obstacle!\n");
            return false;
        }

        FloatPixelMap* cost_to_go = new FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);

        //this tells us the index of the parent of the current node 
        IntPixelMap* parents = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);
        
        
        vector<int> path_indices;

        int ixy[2] = { 0 };
        cost_to_go->worldToTable(goal_xyt, ixy);
        int goal_ind = cost_to_go->getInd(ixy);
        //set the cost to go for the goal to be zero 
        cost_to_go->writeValue(ixy, 0);

        //add the goal as the first freontier element 
        priority_queue<FrontierElement2d, std::vector<FrontierElement2d>, FrontierCompare2d> frontier;
        //add it for exploration 
        frontier.push(FrontierElement2d(0, ixy));
        
        //this is the start position in index
        int ixy_start[2];
        cost_to_go->worldToTable(start_xyt, ixy_start);

        //fprintf(stderr, "DP-Path-To_goal: Start x:%.2f y:%.2f t:%.2f\t Goal x:%.2f y:%.2f t:%.2f\n", start_xyt[0], start_xyt[1], start_xyt[2], goal_xyt[0], goal_xyt[1], goal_xyt[2]);
        //fprintf(stderr, "START: x:%d y:%d -- GOAL: x:%d y:%d\n", ixy_start[0], ixy_start[1], ixy[0], ixy[1]);

        int count = 0;
        //while there are unexplored nodes 
        //explorer the least costly one - till we come to the 
        //start node 
        while (!frontier.empty()) {
            count++;

            //get the least costly element in the queue
            const FrontierElement2d f = frontier.top();

            assert(pixmap->readValue(f.ixy) < DPP_UNK_THRESH);
            //add all the neighbors
            int ixyn[2] = { 0 };
            for (int i = -1; i <= 1; i++) {
                ixyn[0] = f.ixy[0] + i;
                for (int j = -1; j <= 1; j++) {
                    ixyn[1] = f.ixy[1] + j;
                    if (i == 0 && j == 0)
                        continue;
                    if (!cost_to_go->isInMap(ixyn) || cost_to_go->readValue(ixyn) >= 0)
                        continue;
                    if (pixmap->readValue(ixyn) < DPP_UNK_THRESH) {
                        float dist = sqrt(i * i + j * j);
                        //if this is a known empty point - add the frontier queue
                        frontier.push(FrontierElement2d(f.value + dist, ixyn));
                        //add the parent to the parent list 
                        parents->writeValue(ixyn, cost_to_go->getInd(f.ixy));

                        if (ixyn[0] == ixy_start[0] && ixyn[1] == ixy_start[1]) {
                            // found a path.  Traverse from start to goal to get the path
                            fprintf(stderr, "FOUND PATH:\n");
                            //int pxy[2];
                            //fprintf(stderr, "ixy:[0] %d\t[1] %d\n", ixyn[0], ixyn[1]);

                            int cur_ind = cost_to_go->getInd(ixyn);
                            //fprintf(stderr, "cur_ind = %d\n", cur_ind);
                            //cost_to_go->indToLoc(cur_ind, pxy);
                            //fprintf(stderr, "pxy:[0] %d\t[1]%d\n", pxy[0], pxy[1]);
                            for (; cur_ind != goal_ind; cur_ind = parents->data[cur_ind]) {
                                path_indices.push_back(cur_ind);
                                //cost_to_go->indToLoc(cur_ind, pxy);
                                //fprintf(stderr, "\t- x:%d\ty:%d\n", pxy[0], pxy[1]);
                            }
                            path_indices.push_back(cur_ind);
                            getPath(cost_to_go, path_indices, Waypoint2d(start_xyt), Waypoint2d(goal_xyt), waypoints);
                            delete cost_to_go;
                            delete parents;
                            return true;
                        }

                        // set a high value for the neighbor so that it doesn't get pushed
                        // on to the frontier again
                        cost_to_go->writeValue(ixyn, 1e9);
                    }
                    else if (pixmap->readValue(ixyn) > DPP_OCC_THRESH) {
                        cost_to_go->writeValue(ixyn, INFINITY);
                    }
                    else {
                        cost_to_go->writeValue(ixyn, INFINITY);
                    }
                }
            }
            cost_to_go->writeValue(f.ixy, f.value);

            frontier.pop();
        }

        delete cost_to_go;
        delete parents;

        return false;
    }

}//namespace
