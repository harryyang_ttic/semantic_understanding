/*
 * makeTraversabilityMap.cpp
 *
 *  Created on: Sep 3, 2010
 *      Author: abachrac
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "lattice_planner.hpp"
#include <vector>
#include <list>
#include <queue>


using namespace std;
using namespace occ_map;

namespace dp_planner {

/*
 * Utility class for maintaing the score of candidate contour merges
 */
class FrontierElement {
public:
  float value;
  int ixyz[3];
  FrontierElement(float v, int _ixyz[3]) :
    value(v)
  {
    ixyz[0] = _ixyz[0];
    ixyz[1] = _ixyz[1];
    ixyz[2] = _ixyz[2];
  }
};

/*
 * comparator class that allows us to put the joins into a priority_queue
 */
class FrontierCompare {
public:
  bool operator()(const FrontierElement & f1, const FrontierElement & f2)
  {
    return f1.value > f2.value;
  }
};

FloatVoxelMap * createUtilityMap(const FloatVoxelMap * voxmap, const double start[3])
{
  if (voxmap->readValue(start) > 0) {
    fprintf(stderr, "ERROR: start point is in an obstacle!\n");
    return NULL;
  }

  FloatVoxelMap * utility = new FloatVoxelMap(voxmap->xyz0, voxmap->xyz1, voxmap->metersPerPixel, -1);

  int ixyz[3] = { 0 };
  utility->worldToTable(start, ixyz);
  utility->writeValue(ixyz, 0);
  priority_queue<FrontierElement, std::vector<FrontierElement>, FrontierCompare> frontier;
  frontier.push(FrontierElement(0, ixyz));

  int count = 0;
  while (!frontier.empty()) {
    count++;

    const FrontierElement f = frontier.top();

    assert(voxmap->readValue(f.ixyz) < DPP_UNK_THRESH);
    //add all the neighbors //TODO: do we care about more neighbors?
    int ixyzn[3] = { 0 };
    for (int i = -1; i <= 1; i++) {
      ixyzn[0] = f.ixyz[0] + i;
      for (int j = -1; j <= 1; j++) {
        ixyzn[1] = f.ixyz[1] + j;
        for (int k = -1; k <= 1; k++) {
          ixyzn[2] = f.ixyz[2] + k;
          if (i == 0 && j == 0 && k == 0)
            continue;
          if (!utility->isInMap(ixyzn) || utility->readValue(ixyzn) >= 0)
            continue;
          if (voxmap->readValue(ixyzn) < DPP_UNK_THRESH) {
            float dist = sqrt(i * i + j * j + k * k);
            frontier.push(FrontierElement(f.value + dist, ixyzn));

            // set a high value for the neighbor so that it doesn't get pushed
            // on to the frontier again
            utility->writeValue(ixyzn, 1e9);
          }
          else if (voxmap->readValue(ixyzn) > DPP_OCC_THRESH) {
            utility->writeValue(ixyzn, INFINITY);
          }
          else {
            utility->writeValue(ixyzn, INFINITY);
          }
        }
      }
    }
    utility->writeValue(f.ixyz, f.value);

    frontier.pop();

    if (count % 100000 == 0) {
      printf("dp_entry #%d, frontier size=%jd of %d\n", count, frontier.size(), voxmap->num_cells);
    }
  }

  return utility;
}

static bool waypoints_are_collinear(const Waypoint& a, const Waypoint& b, const Waypoint& c)
{
  double m[3] = { c.x - a.x, c.y - a.y, c.z - a.z };
  double n[3] = { b.x - a.x, b.y - a.y, b.z - a.z };
  double t = (m[0] * n[0] + m[1] * n[1] + m[2] * n[2]) / (m[0] * m[0] + m[1] * m[1] + m[2] * m[2]);
  double cp[3] = { a.x + t * m[0], a.y + t * m[1], a.z + t * m[2], };
  double d[3] = { b.x - cp[0], b.y - cp[1], b.z - cp[2] };
  double dist = sqrt(d[0] * d[0] + d[1] * d[1] + d[2] * d[2]);
  return dist < 1e-3;
}

void getPath(const occ_map::FloatVoxelMap * voxmap, const std::vector<int> &path_indices,
    const Waypoint start_wypt, const Waypoint & goal_wypt, std::vector<Waypoint> &waypoints)
{
  waypoints.clear();
  int nwp = 0;
  for (int i = 0; i < path_indices.size(); i++) {
    int ixyz[3];
    voxmap->indToLoc(path_indices[i], ixyz);
    double xyzt[4]={0};
    voxmap->tableToWorld(ixyz, xyzt);
    Waypoint wp(xyzt);
    if (nwp > 1 && waypoints_are_collinear(waypoints[nwp - 2], waypoints[nwp - 1], wp)){
      waypoints[nwp - 1] = wp; //update prev
      continue;
    }
    waypoints.push_back(wp);
    nwp++;
  }
  waypoints[0]= start_wypt; //Go from exactly where we started, not discretized version...

  //set the yaw for the waypoints
  for (int i=1;i<waypoints.size();i++){
    const Waypoint& prev_wp = waypoints[i - 1];
    Waypoint& wp = waypoints[i];
    double dx = wp.x - prev_wp.x;
    double dy = wp.y - prev_wp.y;
    wp.theta = atan2(dy, dx);
  }
  waypoints[0].theta =waypoints[1].theta; //rotate in place first?
  waypoints.push_back(goal_wypt); //add the goal waypt for final yaw...
}

typedef occ_map::VoxelMap<int> IntVoxelMap;

bool dpPathToGoal(const occ_map::FloatVoxelMap * voxmap, const double start_xyzt[4], const double goal_xyzt[4],
    std::vector<Waypoint>& waypoints)
{
  if (voxmap->readValue(start_xyzt) > 0) {
    fprintf(stderr, "ERROR: start point is in an obstacle!\n");
    return false;
  }
  if (voxmap->readValue(goal_xyzt) > 0) {
    fprintf(stderr, "ERROR: goal point is in an obstacle!\n");
    return false;
  }

  FloatVoxelMap* cost_to_go = new FloatVoxelMap(voxmap->xyz0, voxmap->xyz1, voxmap->metersPerPixel, -1);
  IntVoxelMap* parents = new IntVoxelMap(voxmap->xyz0, voxmap->xyz1, voxmap->metersPerPixel, -1);
  vector<int> path_indices;

  int ixyz[3] = { 0 };
  cost_to_go->worldToTable(goal_xyzt, ixyz);
  int goal_ind = cost_to_go->getInd(ixyz);
  cost_to_go->writeValue(ixyz, 0);
  priority_queue<FrontierElement, std::vector<FrontierElement>, FrontierCompare> frontier;
  frontier.push(FrontierElement(0, ixyz));

  int ixyz_start[3];
  cost_to_go->worldToTable(start_xyzt, ixyz_start);

  int count = 0;
  while (!frontier.empty()) {
    count++;

    const FrontierElement f = frontier.top();

    assert(voxmap->readValue(f.ixyz) < DPP_UNK_THRESH);
    //add all the neighbors
    int ixyzn[3] = { 0 };
    for (int i = -1; i <= 1; i++) {
      ixyzn[0] = f.ixyz[0] + i;
      for (int j = -1; j <= 1; j++) {
        ixyzn[1] = f.ixyz[1] + j;
        for (int k = -1; k <= 1; k++) {
          ixyzn[2] = f.ixyz[2] + k;
          if (i == 0 && j == 0 && k == 0)
            continue;
          if (!cost_to_go->isInMap(ixyzn) || cost_to_go->readValue(ixyzn) >= 0)
            continue;
          if (voxmap->readValue(ixyzn) < DPP_UNK_THRESH) {
            float dist = sqrt(i * i + j * j + k * k);
            frontier.push(FrontierElement(f.value + dist, ixyzn));

            parents->writeValue(ixyzn, cost_to_go->getInd(f.ixyz));

            if (ixyzn[0] == ixyz_start[0] && ixyzn[1] == ixyz_start[1] && ixyzn[2] == ixyz_start[2]) {
              // found a path.  Traverse from start to goal to get the path

              int cur_ind = cost_to_go->getInd(ixyzn);
              for (; cur_ind != goal_ind; cur_ind = parents->data[cur_ind]) {
                path_indices.push_back(cur_ind);
              }
              path_indices.push_back(cur_ind);
              delete cost_to_go;
              delete parents;
              getPath(voxmap, path_indices,Waypoint(start_xyzt),Waypoint(goal_xyzt), waypoints);
              return true;
            }

            // set a high value for the neighbor so that it doesn't get pushed
            // on to the frontier again
            cost_to_go->writeValue(ixyzn, 1e9);
          }
          else if (voxmap->readValue(ixyzn) > DPP_OCC_THRESH) {
            cost_to_go->writeValue(ixyzn, INFINITY);
          }
          else {
            cost_to_go->writeValue(ixyzn, INFINITY);
          }
        }
      }
    }
    cost_to_go->writeValue(f.ixyz, f.value);

    frontier.pop();
  }

  delete cost_to_go;
  delete parents;

  return false;
}

}//namespace
