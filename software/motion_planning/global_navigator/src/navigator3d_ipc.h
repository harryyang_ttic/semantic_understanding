/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#ifndef NAVIGATOR3D_IPC_H
#define NAVIGATOR3D_IPC_H

#include "navigator3d_messages.h"
#include <interfaces/map3d.h>

#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <lcmtypes/er_lcmtypes.h>
#include <bot_frames/bot_frames.h>
//#include <lcmtypes/wheelchair3d_lcmtypes.h>

#ifdef __cplusplus
extern "C" {
#endif

  int carmen_navigator_initialize_ipc(void);

    //void carmen_navigator_publish_status(void);
  int  carmen3d_navigator_publish_waypoint(erlcm_map_p map, carmen_traj_point_t* robot_pos, erlcm_point_t* goal_global, carmen_robot_config_t* robot_config, erlcm_navigator_goal_msg_t* curr_goal_msg);
  int carmen3d_navigator_publish_stop_waypoint();
  void carmen3d_navigator_publish_plan(erlcm_map_p map3d, lcm_t *lcm, 
				       erlcm_navigator_goal_msg_t* curr_goal_msg, 
				       int reached_clicked_goal, int waypoint_ind, BotFrames *frames, int64_t utime);//BotTrans local_to_global);

  void carmen_navigator_publish_autonomous_stopped
  (carmen_navigator_reason_t reason);
  void carmen_navigator_initialize_ant(int port, char *module);

#ifdef __cplusplus
}
#endif

#endif
