#include <bot/bot_core.h>
#include <lcm/lcm.h>
#include <carmen3d/lcm_utils.h>
#include <carmen3d/carmen3d_lcmtypes.h>
#include <carmen3d/agile-globals.h>

static lcm_t * lcm;

int main(int argc, char **argv)
{
  lcm = globals_get_lcm();

  erlcm_point_t goal;
  memset(&goal, 0, sizeof(carmen_point_t));

  int valid = 0;

  // navigator_goal_tool <x> <y> <z> <yaw>
  if (argc >= 3) {
    valid = 1;

    if (sscanf(argv[1], "%lf", &goal.x) != 1)
      valid = 0;
    if (sscanf(argv[2], "%lf", &goal.y) != 1)
      valid = 0;
    if (sscanf(argv[3], "%lf", &goal.z) != 1)
      valid = 0;
    if (argc >= 5 && sscanf(argv[4], "%lf", &goal.yaw) != 1) {
      valid = 0;
    }
    else {
      goal.yaw = carmen_degrees_to_radians(goal.yaw);
    }

    // sanity checks
    if (goal.z < 0.1 || goal.z > 3.5) {
      printf("Error: Height must be between 0.1 and 3.5m\n");
      valid = 0;
    }
    if (goal.yaw < -M_PI || goal.yaw > M_PI) {
      printf("Error: yaw value must be betw -180 & 180\n");
      valid = 0;
    }

    // send waypoint
    if (valid) {
      erlcm_navigator_goal_msg_t msg;
      msg.goal = goal;
      msg.use_theta = 1;
      msg.utime = bot_timestamp_now();
      msg.nonce = random();
      msg.sender = CARMEN3D_NAVIGATOR_GOAL_MSG_T_SENDER_WAYPOINT_TOOL;
      erlcm_navigator_goal_msg_t_publish(lcm, NAV_GOAL_CHANNEL, &msg);
      fprintf(stderr, "Publishing Navigator Goal: x %f y %f z %f yaw %f\n", goal.x, goal.y, goal.z, goal.yaw);
    }
  }

  if (!valid) {
    printf("input is invalid. Please enter:\n");
    printf("%s <x(m)> <y(m)> <z(m)> <yaw(deg)>\n", argv[0]);
    exit(1);
  }

  return 0;
}
