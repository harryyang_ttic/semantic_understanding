/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <er_carmen/carmen.h>

#ifndef NO_ZLIB
#include <zlib.h>
#endif

#include "planner3d_interface.h"
#include "navigator3d_ipc.h"
#include "navigator3d.h"
#include "navigator3d_interface.h"
#include <interfaces/map3d_interface.h>
#include <assert.h>

#include <lcm/lcm.h>
#include <er_common/carmen3d_global.h>
#include <lcmtypes/er_lcmtypes.h>
#include <er_lcmtypes/lcm_channel_names.h>


int carmen_navigator_autonomous_status_dummy()
{
    return 0;
}

static int paths_are_same(carmen3d_planner_path_p path1, carmen3d_planner_path_p path2)
{
    carmen_traj_point_t point1, point2;
    int index;

    if (path1->length != path2->length)
        return 0;

    for (index = 0; index < path1->length; index++) {
        point1 = path1->points[index];
        point2 = path2->points[index];
        if (fabs(point1.x - point2.x) > 0.1 || fabs(point1.y - point2.y) > 0.1)
            return 0;
    }
    return 1;
}

int carmen3d_navigator_publish_stop_waypoint(lcm_t * lcm)
{
    extern int64_t currentStateTime;
    //carmen3d_controller_publish_stop_waypoint(lcm, currentStateTime); //use the most recent time from state_estimator so things work in simulation
    return 1;
}

int is_2dequal_carmen3d_point(double pt1_x, double pt1_y, double pt2_x, double pt2_y)
{
    if (fabs(pt1_x - pt2_x) < MAP_EPS && fabs(pt1_y - pt2_y) < MAP_EPS)
        return 1;
    else
        return 0;
}

int carmen3d_navigator_publish_waypoint(erlcm_map_p map, carmen_traj_point_t* robot_pos __attribute__((unused)),
					erlcm_point_t* goal_global, carmen_robot_config_t* robot_config __attribute__((unused)),
					erlcm_navigator_goal_msg_t* curr_goal_msg)
{
    static lcm_t * lcm = NULL;
    if (lcm == NULL)
        lcm = bot_lcm_get_global(NULL); //*** for some wierd reason compler complains tht 
    //it cant find the def
    //lcm = bot_lcm_get_global(NULL);//globals_get_lcm();

    carmen3d_planner_status_t status;
    erlcm_waypoint_msg_t waypt_msg;
    memset(&waypt_msg, 0, sizeof(waypt_msg));
    static erlcm_waypoint_msg_t prev_waypt_msg;

    carmen3d_planner_get_status(&status);
    if (status.path.length > 1) {

        //Check that the planned path reaches goal, otherwise don't publish waypt.
        carmen_point_t plan_last_waypt, plan_last_waypt_global;
        plan_last_waypt.x = status.path.points[status.path.length - 1].x;
        plan_last_waypt.y = status.path.points[status.path.length - 1].y;
        plan_last_waypt_global = carmen3d_map_map_to_global_coordinates(plan_last_waypt, map);
        double dx = goal_global->x - plan_last_waypt_global.x;
        double dy = goal_global->y - plan_last_waypt_global.y;
        double dist = sqrt((dx*dx) + (dy*dy));
        //    if (is_2dequal_carmen3d_point(goal_global->x, goal_global->y, plan_last_waypt_global.x, plan_last_waypt_global.y)
        if (dist > 0.50) {
            fprintf(stderr, "Plan (%f, %f) does not reach goal (%f, %f), not publishing waypt!\n",
                    plan_last_waypt_global.x, plan_last_waypt_global.y,
                    goal_global->x, goal_global->y);
            return 2;
        }

        carmen_point_t map_waypoint;
        carmen_point_t global_waypoint;
        map_waypoint.x = status.path.points[1].x;
        map_waypoint.y = status.path.points[1].y;
        //TBD: Have the ability to decide if yaw should be use previous or should use nav plan.
        map_waypoint.theta = carmen_normalize_theta(status.path.points[1].theta);
        if (status.planned_map_config.x_size == map->config.x_size && status.planned_map_config.y_size
            == map->config.y_size && status.planned_map_config.resolution == map->config.resolution) {

            global_waypoint = carmen3d_map_map_to_global_coordinates(map_waypoint, map);
            waypt_msg.pos.x = global_waypoint.x;
            waypt_msg.pos.y = global_waypoint.y;
            if (curr_goal_msg->sender == ERLCM_NAVIGATOR_GOAL_MSG_T_SENDER_MISSION_PLANNER)
                waypt_msg.pos.z = curr_goal_msg->goal.z;
            else
                waypt_msg.pos.z = ERLCM_WAYPOINT_MSG_T_USE_PREVIOUS;
            waypt_msg.pos.yaw = global_waypoint.theta;// CARMEN3D_WAYPOINT_MSG_T_USE_PREVIOUS;
            //      waypt_msg.pos.yaw =  CARMEN3D_WAYPOINT_MSG_T_USE_PREVIOUS;
            waypt_msg.vel.x = curr_goal_msg->velocity;
            waypt_msg.vel.y = curr_goal_msg->velocity;
            waypt_msg.vel.z = ERLCM_WAYPOINT_MSG_T_USE_DEFAULT;
            waypt_msg.vel.yaw = ERLCM_WAYPOINT_MSG_T_USE_DEFAULT;
            waypt_msg.waypt_type = ERLCM_WAYPOINT_MSG_T_TYPE_WAYPT;
            waypt_msg.utime = bot_timestamp_now();
            waypt_msg.nonce = random();

            if (carmen3d_dist(&prev_waypt_msg.pos, &waypt_msg.pos) > .05 || fabs(prev_waypt_msg.pos.yaw - waypt_msg.pos.yaw) >(1.0/180.0*M_PI) ){// || carmen3d_angle_subtract(prev_waypt_msg.pos.yaw, waypt_msg.pos.yaw) > (1.0/180.0*M_PI)) {
                prev_waypt_msg = waypt_msg;

                waypt_msg.sender = ERLCM_WAYPOINT_MSG_T_SENDER_NAVIGATOR;
                //TODO: this should be a command line option
                //        carmen3d_controller_sanity_check_and_publish(lcm, WAYPOINT_NAVIGATOR_GOAL_CHANNEL, &waypt_msg);
                //carmen3d_controller_sanity_check_and_publish(lcm, WAYPOINT_COMMAND_CHANNEL, &waypt_msg);

                printf("Sending X=%+4.2f Y=%+4.2f Z=%+4.2f yaw=%+4.2f vX=%+4.2f vY=%+4.2f vZ=%+4.2f type=%d\n",
                       waypt_msg.pos.x, waypt_msg.pos.y, waypt_msg.pos.z, carmen_radians_to_degrees(waypt_msg.pos.yaw),
                       waypt_msg.vel.x, waypt_msg.vel.y, waypt_msg.vel.z, waypt_msg.waypt_type);

            }
            return 1;
        }
        else
            return 0;
    }
    // }

    return 1;

}

void carmen3d_navigator_publish_plan(erlcm_map_p map3d, lcm_t *lcm, 
				     erlcm_navigator_goal_msg_t* curr_goal_msg, 
				     int reached_clicked_goal, 
				     int waypoint_ind, BotFrames *frames, int64_t utime)				     
{
    carmen3d_planner_status_t status;
    carmen_navigator_plan_message plan_msg;
  
    static carmen_point_t prev_goal = { -1.0, -1.0, -1.0 };

    static carmen3d_planner_path_t prev_path = { NULL, 0, 0 };

    plan_msg.timestamp = carmen_get_time();
    plan_msg.host = carmen_get_host();

    carmen3d_planner_get_status(&status);

    if (curr_goal_msg == NULL) {
        return;
    }

    if (paths_are_same(&(status.path), &prev_path) && (prev_goal.x == status.goal.x && prev_goal.y == status.goal.y)) {
        if (status.path.length > 0)
            free(status.path.points);
        return;
    }
    else {
        prev_goal = status.goal;

        if (prev_path.length > 0)
            free(prev_path.points);
        prev_path.length = status.path.length;
        prev_path.points = status.path.points;
        prev_path.capacity = status.path.length;
    }

    int i;
    carmen_point_t temp_pt;
    carmen_point_t temp_global_pt;

    // LCM
    // publishes 2 messages, the point list of the plan, and a full PLAN RESULT message
    erlcm_point_list_t plan;
    erlcm_nav_plan_result_t plan_result;

    // fill in the point lists in both structures
    plan.num_points = status.path.length;
    //plan.use_ind = waypoint_ind;
    plan.points = (erlcm_point_t*) calloc(plan.num_points, sizeof(erlcm_point_t));
    plan_result.plan.num_points = status.path.length;
    plan_result.plan.points = (erlcm_point_t*) calloc(plan_result.plan.num_points, sizeof(erlcm_point_t));
    erlcm_waypoint_msg_t waypt_msg;
    memset(&waypt_msg, 0, sizeof(erlcm_waypoint_msg_t));

    waypt_msg.utime = bot_timestamp_now();
    if(waypoint_ind == -1){
        waypt_msg.nonce = 1;
    }

    for (i = 0; i < status.path.length; i++) {
        temp_pt.x = status.path.points[i].x;
        temp_pt.y = status.path.points[i].y;
        temp_pt.theta = status.path.points[i].theta;
        temp_global_pt = carmen3d_map_map_to_global_coordinates(temp_pt, map3d);

        double pos_global[3] = {temp_global_pt.x, temp_global_pt.y, 0};
        double quat_global[4];
        double pos_local[3];
        double rpy[3] = {.0,.0, temp_global_pt.theta};
        double rpy_local[3];
        double quat_local[4];
        bot_roll_pitch_yaw_to_quat(rpy, quat_global);

        BotTrans global_to_local;
        bot_frames_get_trans_with_utime (frames, "global", "local", utime, &global_to_local);
      
        bot_frames_transform_vec (frames, "global", "local",  pos_global, pos_local);

        bot_quat_mult (quat_local, global_to_local.rot_quat, quat_global);

        bot_quat_to_roll_pitch_yaw (quat_local, rpy_local);

        plan.points[i].x = pos_local[0];//temp_global_pt.x;
        plan.points[i].y = pos_local[1];//temp_global_pt.y;
        plan.points[i].yaw = rpy_local[2];//temp_global_pt.theta;
        if(waypoint_ind == i){
            waypt_msg.pos.x = temp_global_pt.x;
            waypt_msg.pos.y = temp_global_pt.y;
            waypt_msg.pos.yaw = temp_global_pt.theta;
        }
        // ignore z,r,p for now
        plan.points[i].z = 0;
        plan.points[i].roll = 0;
        plan.points[i].pitch = 0;

        plan_result.plan.points[i] = plan.points[i];
    }

    // publish the point list plan
    if (status.path.length > 0)
        erlcm_point_list_t_publish(lcm, NAV_PLAN_CHANNEL, &plan);


    // fill in plan result
    if (status.path.length <= 0 && !reached_clicked_goal) {
        plan_result.plan_result = ERLCM_NAV_PLAN_RESULT_T_RESULT_NO_PLAN;
    }
    else if (reached_clicked_goal) {
        plan_result.plan_result = ERLCM_NAV_PLAN_RESULT_T_RESULT_PLAN_AT_GOAL;
    }
    else if (is_2dequal_carmen3d_point(curr_goal_msg->goal.x, curr_goal_msg->goal.y, plan.points[plan.num_points - 1].x,
                                       plan.points[plan.num_points - 1].y)) {
        plan_result.plan_result = ERLCM_NAV_PLAN_RESULT_T_RESULT_PLAN_REACH_GOAL;
    
    }
    else{
        plan_result.plan_result = ERLCM_NAV_PLAN_RESULT_T_RESULT_PLAN_PARTIAL;
    }
    plan_result.utime = bot_timestamp_now();
    plan_result.navgoal_msg = *curr_goal_msg;

  
    erlcm_waypoint_msg_t_publish(lcm,"WAYPOINT_GOAL_CMD", &waypt_msg);
    erlcm_nav_plan_result_t_publish(lcm, NAV_PLAN_RESULT_CHANNEL, &plan_result);

    fprintf(stderr,"Waypoint Index : %d Path Length : %d\n", waypoint_ind, status.path.length); 

    fprintf(stderr,"No of waypoints to go : %d\n", (status.path.length - waypoint_ind) +1);

    if(status.path.length > 0 && waypoint_ind >=0){
    
        int no_waypoints = (status.path.length - waypoint_ind); 

        erlcm_goal_list_t goal_list; 

        goal_list.utime = bot_timestamp_now();
        goal_list.sender_id = 0;
        goal_list.num_goals = no_waypoints;
        goal_list.goals = calloc(no_waypoints,sizeof(erlcm_goal_t));

        fprintf(stderr,"Now waypoint Theta : %f\n", status.path.points[status.path.length -1].theta); 

        BotTrans global_to_local;
        bot_frames_get_trans_with_utime (frames, "global", "local", utime, &global_to_local);

        for(int i=0; i < no_waypoints; i++){
      
            temp_pt.x = status.path.points[waypoint_ind+i].x;
            temp_pt.y = status.path.points[waypoint_ind+i].y;
            temp_pt.theta = status.path.points[waypoint_ind+i].theta;
            temp_global_pt = carmen3d_map_map_to_global_coordinates(temp_pt, map3d);

            double pos_global[3] = {temp_global_pt.x, temp_global_pt.y, 0};
            double quat_global[4];
            double pos_local[3];
            double rpy[3] = {.0,.0, temp_global_pt.theta};
            double rpy_local[3];
            double quat_local[4];
            bot_roll_pitch_yaw_to_quat(rpy, quat_global);

      
      
            bot_frames_transform_vec (frames, "global", "local",  pos_global, pos_local);

            bot_quat_mult (quat_local, global_to_local.rot_quat, quat_global);

            bot_quat_to_roll_pitch_yaw (quat_local, rpy_local);

            goal_list.goals[i].pos[0] = pos_local[0];//temp_global_pt.x;
            goal_list.goals[i].pos[1] = pos_local[1];//temp_global_pt.y;
      
            goal_list.goals[i].theta = rpy_local[2];//temp_global_pt.theta; 
      
            goal_list.goals[i].size[0] = 0.7;//0.4;//self->region_goal.size[0];
            goal_list.goals[i].size[1] = 0.7;//0.4;//self->region_goal.size[0];
            //if(i< no_waypoints - 1){ //ignore the heading for the actual goal

      
            goal_list.goals[i].heading_tol = 0.3; 
            goal_list.goals[i].use_theta = 1;
            goal_list.goals[i].do_turn_only = 0;
            //}
        }
        erlcm_goal_list_t_publish (lcm, "RRTSTAR_GOALS", &goal_list);
        free(goal_list.goals);
    }
}
