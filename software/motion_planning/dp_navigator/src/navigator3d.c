
/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <getopt.h>
#include <stdlib.h>
#include <string.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <lcm/lcm.h>

#include <bot_core/bot_core.h>
#include <bot_param/param_client.h>

#include <bot_lcmgl_client/lcmgl.h>
#include <bot_frames/bot_frames.h>
#include <bot_param/param_client.h>

#include <lcmtypes/bot2_param.h>
#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/su_lcmtypes.h>
#include <er_lcmtypes/lcm_channel_names.h>

#include <er_common/lcm_utils.h>
#include <er_carmen/carmen.h>
#include <interfaces/map3d_interface.h>
#include <interfaces/robot3d_interface.h>
#include <interfaces/localize3d_interface.h>
#include <check_gridmap/check_gridmap.h>


#include "navigator3d.h"
#include "navigator3d_ipc.h"
#include "planner3d_interface.h"
#include "conventional3d.h"

#define VERBOSE 0

// Threshold used to determine when doors and elevators are open
#define COLLISION_OBS_MAX_THRESHOLD 100

typedef void(*handler)(int);

typedef struct _state_t{
    carmen_map_p nav_map;
    erlcm_map_p nav3d_map;
    erlcm_gridmap_t* gridmap_msg;

    gboolean verbose;

    int use_check_gridmap_for_doors;
    check_gridmap_t* check_gridmap;

    carmen_map_placelist_t placelist;
    carmen_robot_config_t robot_config;
    carmen_navigator_config_t nav_config;

  int autonomous_status;
    int publishWaypt;

  int use_planar_lidar;

    carmen_traj_point_t robot_position; // In map coordinates
    int64_t currentStateTime;
    carmen_point_t robot_gfs_global; // In global coordinates
    carmen_point_t clicked_map_goal; //In map coordinates
    carmen_point_t clicked_map_goal_global;
    int clicked_map_goal_exists;
    int plan_use_theta;
    carmen_point_t final_goal_point;
    carmen_base_odometry_message odometry;
    carmen_base_odometry_message corrected_odometry;
    carmen_base_odometry_message last_odometry;
    carmen_point_t kinect_global_pose;
    int have_frontlaser; 
    int have_rearlaser; 
    carmen_robot_laser_message frontlaser, rearlaser;
    carmen_localize_globalpos_message mappos;
    double front_laser_offset;
    double rear_laser_offset;
    double kinect_front_offset;
    double front_laser_angle_offset;
    double rear_laser_angle_offset;
    int reached_clicked_goal;
    int ignore_new_goals;
    erlcm_navigator_goal_msg_t* curr_goal_msg;
    erlcm_navigator_goal_msg_t* end_goal_msg;
    erlcm_goal_feasibility_querry_t *goal_feasibilty_msg;
    int republish_path;
    int outstanding_cost_calulation;
    double partial_cost_value;
    //we need a data structure to communicate back the result of feasibility 
    //this will constitute the waypoint set that will be given piece by piece to the navigator 
    //this will have the set of ordered waypoints that need to be traveled through 
    //and the final goal destination 
    erlcm_goal_feasibility_querry_t result_msg; 
    int prune_dist;

    sulcm_rect_list_t *sim_rects_list;

    double er_door_elevator_pos[3];

    //added security to prevent the navigator trying to go to goals on a differnt floor
    int goal_floor;
    int current_floor_no;

    //lcmgl drawing
    bot_lcmgl_t *lcmgl_navigator;
    bot_lcmgl_t *lcmgl_elevator_check;
    bot_lcmgl_t *lcmgl_waypoints;
    bot_lcmgl_t *lcmgl_cost_querry;
    bot_lcmgl_t *lcmgl_nav_goal;
    bot_lcmgl_t *lcmgl_navigator_goal;


    //elevator goals
    erlcm_elevator_goal_list_t *elevator_goal;

    //need to make this cleaner
    int check_elevator_mode;
    int elevator_mode;
    int check_door_mode;
    int got_new_check;
    int elevator_controller_mode;
    int goal_is_best_view;
    int at_best_view;

    int64_t last_laser_time; 

    lcm_t * lcm;
  BotFrames *frames;
  //walter 
  bot_core_pose_t *last_bot_pose_local;
    bot_core_pose_t *last_bot_pose_deadreckon_global; 

} state_t; 

void carmen_navigator_start_autonomous(state_t *s);
void carmen_navigator_stop_autonomous(state_t *s);
void carmen3d_navigator_clear_goal(state_t *s);
int carmen_navigator_autonomous_status(state_t *s);
void plan_to_goal(state_t *s, erlcm_navigator_goal_msg_t *msg, char* channel);

static void
on_sim_rects(const lcm_recv_buf_t * rbuf, const char *channel, 
             const sulcm_rect_list_t *msg, void *user)
{
    state_t *self = (state_t*) user;    

    if(self->sim_rects_list != NULL){
        sulcm_rect_list_t_destroy(self->sim_rects_list);
    }
    self->sim_rects_list = sulcm_rect_list_t_copy(msg);
    
}

void carmen3d_navigator_goal_changed(double x, double y, double z, double theta, int use_theta, int publish, state_t *s);

void request_floor_map(int floor_no, state_t *s);

static void on_deadreckon_pose(const lcm_recv_buf_t *rbuf, const char * channel,
                               const bot_core_pose_t * msg, void * user) 
{
    state_t *s = (state_t *) user;

    // Transform from LOCAL frame to GLOBAL frame
    double pos_global[3], quat_global[4];
    BotTrans local_to_global;
    bot_frames_get_trans_with_utime (s->frames, "local", "global", msg->utime, &local_to_global);

    bot_frames_transform_vec (s->frames, "local", "global", msg->pos, pos_global);
    bot_quat_mult (quat_global, local_to_global.rot_quat, msg->orientation);

    if(s->last_bot_pose_deadreckon_global != NULL){
        bot_core_pose_t_destroy(s->last_bot_pose_deadreckon_global);
    }
    s->last_bot_pose_deadreckon_global = bot_core_pose_t_copy(msg);

    // Copy over the new pose and orientation
    memcpy (s->last_bot_pose_deadreckon_global->pos, pos_global, 3*sizeof(double));
    memcpy (s->last_bot_pose_deadreckon_global->orientation, quat_global, 4*sizeof(double));

}



static inline void copy_carmen_taj_point_to_carmen3d_traj_point(carmen_traj_point_t *point,erlcm_traj_point_t *c3d_point)
{
    memset(c3d_point,0,sizeof(erlcm_traj_point_t));
    c3d_point->x = point->x;
    c3d_point->y = point->y ;
    c3d_point->theta = point->theta;
    c3d_point->t_vel = point->t_vel;
    c3d_point->r_vel = point->r_vel;
}

static inline void copy_carmen_taj_to_carmen3d_traj(carmen_traj_point_t *traj,erlcm_traj_point_t *c3d_taj, int size)
{
    int i;
    for (i=0;i<size;i++){
        copy_carmen_taj_point_to_carmen3d_traj_point(&traj[i],&c3d_taj[i]);
    }
}

static inline void copy_carmen_point_to_point2d_t(carmen_point_t *cp, point2d_t *pp){
    pp->x = cp->x;
    pp->y = cp->y;
}

static inline void copy_point2d_t_to_carmen_point(point2d_t *pp, carmen_point_t *cp){
    cp->x = pp->x;
    cp->y = pp->y;
}

static void generate_next_motion_command(state_t *s);
static int draw_next_waypoint(state_t *s);

int get_min_ind(int *values, int size){
    int min_value = 100000; 
    int min_ind = -1;
    for(int i=0; i< size; i++){
        if(values[i] == -1)
            continue;
        if(values[i] < min_value){
            min_value = values[i];
            min_ind = i;
        }
    }
    //*minimum_value = min_value; 
    values[min_ind] = -1;
    return min_ind; 
}


static void convert_portal_list_to_msg(carmen3d_portal_list_t p_list, int type, 
                                       carmen3d_portal_t *elevator, double cost, state_t *s){
    
    fprintf(stderr, " $$$$$$$$ Type : %d Cost to Goal : %f Current Cost : %f $$$$$$$$$\n", type , 
            s->result_msg.cost_to_goal, cost);

    if(type == 0 || type == 1){
        //reset cost 
        fprintf(stderr,"\t Resetting Result msg\n");  
        s->result_msg.cost_to_goal  = 0;
        //new result -reset, calculate and publish
        if(s->result_msg.portal_list.portals !=NULL){
            fprintf(stderr, " ------ Freeing Old Portals\n");
            free(s->result_msg.portal_list.portals);
            s->result_msg.portal_list.portals = NULL;
        }
        s->result_msg.portal_list.no_of_portals = 0;
    }

    //if the first half of the results - then add the elevator here 
    if(type == 2 && elevator != NULL){
        //add the elevator to the end 

        s->result_msg.portal_list.portals = (erlcm_portal_node_t *) realloc( s->result_msg.portal_list.portals, 
                                                                          (s->result_msg.portal_list.no_of_portals + 1) * sizeof(erlcm_portal_node_t)); 
        
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].type = ERLCM_PORTAL_NODE_T_PORTAL_ELEVATOR; 

        //we flip this becasue the first node is the outside and the second node is the inside 
        //since we are exiting the elevator - it needs to be the other way around 
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].xy0[0] = elevator->global_portal_points[1].x;
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].xy0[1] = elevator->global_portal_points[1].y;
        
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].xy1[0] = elevator->global_portal_points[0].x;
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].xy1[1] = elevator->global_portal_points[0].y;

        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].floor_no = elevator->floor_no;
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].floor_ind = elevator->floor_ind;

        s->result_msg.portal_list.no_of_portals +=1;        
    }

    //lets print it for now 
    int *rank = calloc(p_list.no_portals, sizeof(int));
    int count = 0; 
    for(int i=0; i < p_list.no_portals ; i++){
        if(p_list.portal_list[i].pass_through){
            rank[i] = p_list.portal_list[i].path_ind; 
            count++;
        }
        else{
            rank[i] = -1; 
        }
    }

    int min_val = 100000;
    
    s->result_msg.portal_list.portals = (erlcm_portal_node_t *) realloc(s->result_msg.portal_list.portals, 
                                                                     (s->result_msg.portal_list.no_of_portals + count) * 
                                                                     sizeof(erlcm_portal_node_t)); 
    

    fprintf(stderr, "Current No of Portals : %d New : %d\n", 
            s->result_msg.portal_list.no_of_portals, 
            s->result_msg.portal_list.no_of_portals + count);
    
    int c_portals = s->result_msg.portal_list.no_of_portals; 

    fprintf(stderr, "==== Portals in Path ==== \n");
    
    for(int j=0; j < count ; j++){
        //get min values 

        int c_ind = get_min_ind(rank, p_list.no_portals); 
        fprintf(stderr, "\t %d => Portal ID : %d : %f,%f => %f,%f, Closer Node : %d\n", 
                j, p_list.portal_list[c_ind].portal_id, 
                p_list.portal_list[c_ind].global_portal_points[0].x, 
                p_list.portal_list[c_ind].global_portal_points[0].y, 
                p_list.portal_list[c_ind].global_portal_points[1].x,
                p_list.portal_list[c_ind].global_portal_points[1].y, 
                p_list.portal_list[c_ind].closer_node); 

        s->result_msg.portal_list.portals[c_portals + j].type = ERLCM_PORTAL_NODE_T_PORTAL_DOOR; 
        if(p_list.portal_list[c_ind].closer_node == 0){
            s->result_msg.portal_list.portals[c_portals + j].xy0[0] = p_list.portal_list[c_ind].global_portal_points[0].x;
            s->result_msg.portal_list.portals[c_portals + j].xy0[1] = p_list.portal_list[c_ind].global_portal_points[0].y;
            
            s->result_msg.portal_list.portals[c_portals + j].xy1[0] = p_list.portal_list[c_ind].global_portal_points[1].x;
            s->result_msg.portal_list.portals[c_portals + j].xy1[1] = p_list.portal_list[c_ind].global_portal_points[1].y;
        }
        else{
            s->result_msg.portal_list.portals[c_portals + j].xy1[0] = p_list.portal_list[c_ind].global_portal_points[0].x;
            s->result_msg.portal_list.portals[c_portals + j].xy1[1] = p_list.portal_list[c_ind].global_portal_points[0].y;
            
            s->result_msg.portal_list.portals[c_portals + j].xy0[0] = p_list.portal_list[c_ind].global_portal_points[1].x;
            s->result_msg.portal_list.portals[c_portals + j].xy0[1] = p_list.portal_list[c_ind].global_portal_points[1].y;
        }
        s->result_msg.portal_list.portals[c_portals + j].floor_no = p_list.portal_list[c_ind].floor_no;
        s->result_msg.portal_list.portals[c_portals + j].floor_ind = p_list.portal_list[c_ind].floor_ind;
        //add this to the portal 
    }
 
    s->result_msg.portal_list.no_of_portals += count; 

    //if the last part of the results - add the elevator here 

    if(type == 1 && elevator != NULL){
        //add the elevator to the end 
        
        s->result_msg.portal_list.portals = (erlcm_portal_node_t *) realloc( s->result_msg.portal_list.portals, 
                                                                          (s->result_msg.portal_list.no_of_portals + 1) * sizeof(erlcm_portal_node_t)); 
        
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].type = ERLCM_PORTAL_NODE_T_PORTAL_ELEVATOR; 
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].xy0[0] = elevator->global_portal_points[0].x;
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].xy0[1] = elevator->global_portal_points[0].y;
        
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].xy1[0] = elevator->global_portal_points[1].x;
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].xy1[1] = elevator->global_portal_points[1].y;

        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].floor_no = elevator->floor_no;
        s->result_msg.portal_list.portals[s->result_msg.portal_list.no_of_portals].floor_ind = elevator->floor_ind;

        s->result_msg.portal_list.no_of_portals +=1;        
    }

    s->result_msg.goal = s->goal_feasibilty_msg->goal;
    s->result_msg.goal_floor_no = s->goal_feasibilty_msg->goal_floor_no;
    
    s->result_msg.utime = s->goal_feasibilty_msg->utime; 
    
    if(type == 0 || type == 1){
        s->result_msg.cost_to_goal = cost;
    }
    if(type == 2){
        if(s->result_msg.cost_to_goal < 0){
            s->result_msg.cost_to_goal = -10000; 
        }
        else{
            s->result_msg.cost_to_goal += cost; 
        }
    }
    //add the cost result here

    if(type == 0 || type == 2){
        //publish the result 
        erlcm_goal_feasibility_querry_t_publish(s->lcm, "GOAL_FEASIBILITY_RESULT", &(s->result_msg)); 
    }
    
    free(rank);
}

//handles outstanding cost querry messages
static void calculate_cost_querry(int c_floor_no, state_t *s)
{
    //assume that all places will be reachable from the elevators 
    
    //we will detect the portals by checking if the path from robot to elevators/elevators to goal 
    //or robot to goal pass through any doors 
    //handle the latest cost querry message 
    if(s->goal_feasibilty_msg == NULL){
        fprintf(stdout,"No cost querry message in memory - Skipping\n");
        return;
    }

    bot_lcmgl_t *lcmgl = s->lcmgl_cost_querry; 
    lcmglPointSize(6);
    //check if the goal is on the same floor 
    if(s->goal_feasibilty_msg->goal_floor_no == c_floor_no && 
       !s->outstanding_cost_calulation ){//current_floor_no){

        s->outstanding_cost_calulation = 0;
        
        carmen_point_t goal_global;
        goal_global.x = s->goal_feasibilty_msg->goal.x;
        goal_global.y = s->goal_feasibilty_msg->goal.y;
        carmen_point_t  goal_map = carmen3d_map_global_to_map_coordinates(goal_global, s->nav3d_map);
        lcmglColor3f(1.0, 0.0,0.0);

        // Transform from GLOBAL to LOCAL
        double gp_global[] = {goal_global.x, goal_global.y, 0};
        double gp_local[3];

        bot_frames_transform_vec (s->frames, "global", "local", gp_global, gp_local);

        // Set z=0, though this shouldn't be necessary as it should be zero already
        gp_local[2] = 0;
        lcmglCircle(gp_local, 0.1);

        point2d_t goal = {goal_map.x, 
                          goal_map.y};
        
        //check cost from robot to goal 
        fprintf(stderr, "Goal is on the same floor\n");
        fprintf(stderr, "\n Goal on Map : %f,%f\n", goal.x, goal.y);

        //pass in the door list on the current floor 
        //and get back feasibility, cost and which doors the path passes through 
        //first check if the goal and the robot are in the same floor 
        int portal_count = 0;
        //if so get path and check if the path goes through any portal 
        for(int i=0; i < s->goal_feasibilty_msg->portal_list.no_of_portals; i++){
            if(s->goal_feasibilty_msg->portal_list.portals[i].floor_no == c_floor_no /*current_floor_no*/
               && s->goal_feasibilty_msg->portal_list.portals[i].type == 0){ //doors 
                fprintf(stderr, "Portals on the Current Floor : (%f,%f) => (%f,%f)\n", 
                        s->goal_feasibilty_msg->portal_list.portals[i].xy0[0],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy0[1],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy1[0],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]);

                //check distance from robot to this portal 
                portal_count++; 
            }        
        }

        carmen3d_portal_list_t p_list;
        p_list.no_portals = portal_count; 
        p_list.portal_list = calloc(portal_count, sizeof(carmen3d_portal_t));

        portal_count = 0;
        for(int i=0; i < s->goal_feasibilty_msg->portal_list.no_of_portals; i++){
            if(s->goal_feasibilty_msg->portal_list.portals[i].floor_no == c_floor_no/*current_floor_no*/
               && s->goal_feasibilty_msg->portal_list.portals[i].type == 0){

                carmen_point_t p_global;
                p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy0[0];
                p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy0[1];
                carmen_point_t p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);
                
                lcmglColor3f(1.0, .0,0.5);

                // Transform from GLOBAL to LOCAL for LCMGL rendering
                double gp_global[3] = {p_global.x, p_global.y, 0};
                double gp_local[3];

                bot_frames_transform_vec (s->frames, "global", "local", gp_global, gp_local);
                gp_local[2] = 0;
                lcmglCircle(gp_local, 0.2);

                p_list.portal_list[portal_count].portal_id = i;
                point2d_t p1 = {p_map.x, p_map.y};

                p_list.portal_list[portal_count].portal_points[0] = p1;
                
                point2d_t g_p1 = {s->goal_feasibilty_msg->portal_list.portals[i].xy0[0], 
                                  s->goal_feasibilty_msg->portal_list.portals[i].xy0[1]};
                p_list.portal_list[portal_count].global_portal_points[0] = g_p1; 
                p_list.portal_list[portal_count].floor_no = s->goal_feasibilty_msg->portal_list.portals[i].floor_no;
                p_list.portal_list[portal_count].floor_ind = s->goal_feasibilty_msg->portal_list.portals[i].floor_ind;
                
                p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy1[0];
                p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy1[1];
                p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);

                double gp1_global[3] = {p_global.x, p_global.y, 0};
                double gp1_local[3];
                bot_frames_transform_vec (s->frames, "global", "local", gp1_global, gp1_local);
                gp1_local[2] = 0;

                lcmglCircle(gp1_local, 0.2);
                
                point2d_t p2 = {p_map.x, p_map.y};

                fprintf(stderr, "\n Portal on Map %d : %f,%f => %f, %f\n", i, p1.x, p1.y, p2.x, p2.y);

                p_list.portal_list[portal_count].portal_points[1] = p2;

                point2d_t g_p2 = {s->goal_feasibilty_msg->portal_list.portals[i].xy1[0], 
                                  s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]};
                p_list.portal_list[portal_count].global_portal_points[1] = g_p2; 
            
                portal_count++; 
                //check distance from robot to this portal 
            }        
        }
        
        double cost = carmen3d_planner_compute_path_costs_and_portals(p_list, 
                                                                      goal,
                                                                      &(s->nav_config), &(s->robot_config),
                                                                      -1, s->lcmgl_cost_querry);

        
        convert_portal_list_to_msg(p_list, 0 , NULL, cost, s);


        //we need to send this back in a proper format 

        if(lcmgl){
            bot_lcmgl_switch_buffer (lcmgl);  
        }
        
    }

    else if(s->goal_feasibilty_msg->goal_floor_no == c_floor_no && 
            s->outstanding_cost_calulation ){

        s->outstanding_cost_calulation = 0;

        carmen3d_portal_t elevator; 

        int elevator_found = 0; 

        //now we need to find the same elevator on the new floor 
        // and set that as the new robot location 

        for(int i=0; i < s->goal_feasibilty_msg->portal_list.no_of_portals; i++){
            if(s->goal_feasibilty_msg->portal_list.portals[i].floor_no == c_floor_no /*current_floor_no*/
               && s->goal_feasibilty_msg->portal_list.portals[i].type == 1){ //elevator
                carmen_point_t p_global;
                
                //we need a check to see if the elevator is in the goal floor 

                p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy0[0];
                p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy0[1];
                carmen_point_t p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);
                
                lcmglColor3f(1.0, .0,0.5);

                // Transform from GLOBAL to LOCAL for LCMGL
                double gp_global[3] = {p_global.x, p_global.y, 0};
                double gp_local[3];

                bot_frames_transform_vec (s->frames, "global", "local", gp_global, gp_local);
                gp_local[2] = 0;

                lcmglCircle(gp_local, 0.2);

                elevator.portal_id = i;
                point2d_t p1 = {p_map.x, p_map.y};                

                elevator.portal_points[0] = p1;

                point2d_t g_p1 = {s->goal_feasibilty_msg->portal_list.portals[i].xy0[0], 
                                  s->goal_feasibilty_msg->portal_list.portals[i].xy0[1]};
                
                elevator.global_portal_points[0] = g_p1; 

                p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy1[0];
                p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy1[1];
                p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);

                // Transform from GLOBAL to LOCAL for LCMGL
                double gp1_global[3] = {p_global.x, p_global.y, 0};
                double gp1_local[3];
                
                bot_frames_transform_vec (s->frames, "global", "local", gp1_global, gp1_local);
                gp1_local[2] = 0;

                lcmglCircle(gp1_local, 0.2);
                
                point2d_t p2 = {p_map.x, p_map.y};

                fprintf(stderr, "\n Portal on Map %d : %f,%f => %f, %f\n", i, p1.x, p1.y, p2.x, p2.y);

                elevator.portal_points[1] = p2;

                point2d_t g_p2 = {s->goal_feasibilty_msg->portal_list.portals[i].xy1[0], 
                                  s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]};
                
                elevator.global_portal_points[1] = g_p2; 

                elevator.floor_no = s->goal_feasibilty_msg->portal_list.portals[i].floor_no;
                elevator.floor_ind = s->goal_feasibilty_msg->portal_list.portals[i].floor_ind;


                fprintf(stderr, "Elevator on the Current Floor : (%f,%f) => (%f,%f)\n", 
                        s->goal_feasibilty_msg->portal_list.portals[i].xy0[0],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy0[1],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy1[0],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]);
                
                //for now handling only one elevator situation 
                elevator_found = 1;
                break;                
            }        
        }
        
        if(elevator_found){
        
            carmen_point_t goal_global;
            goal_global.x = s->goal_feasibilty_msg->goal.x;
            goal_global.y = s->goal_feasibilty_msg->goal.y;
            carmen_point_t  goal_map = carmen3d_map_global_to_map_coordinates(goal_global, s->nav3d_map);
            lcmglColor3f(1.0, 0.0,0.0);

            // Transform from GLOBAL to LOCAL for LCMGL
            double gp_global[3] = {goal_global.x, goal_global.y, 0};
            double gp_local[3];

            bot_frames_transform_vec (s->frames, "global", "local", gp_global, gp_local);
            gp_local[2] = 0;

            lcmglCircle(gp_local, 0.1);

            point2d_t goal = {goal_map.x, 
                              goal_map.y};
        
            //check cost from robot to goal 
            fprintf(stderr, "Second Part of the calculation - \n");
            fprintf(stderr, "\n Goal on Map : %f,%f\n", goal.x, goal.y);
        
            int portal_count = 0;
            //if so get path and check if the path goes through any portal 
            for(int i=0; i < s->goal_feasibilty_msg->portal_list.no_of_portals; i++){
                if(s->goal_feasibilty_msg->portal_list.portals[i].floor_no == c_floor_no /*current_floor_no*/
                   && s->goal_feasibilty_msg->portal_list.portals[i].type == 0){ //doors 
                    fprintf(stderr, "Portals on the Current Floor : (%f,%f) => (%f,%f)\n", 
                            s->goal_feasibilty_msg->portal_list.portals[i].xy0[0],
                            s->goal_feasibilty_msg->portal_list.portals[i].xy0[1],
                            s->goal_feasibilty_msg->portal_list.portals[i].xy1[0],
                            s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]);

                    //check distance from robot to this portal 
                    portal_count++; 
                }        
            }

            carmen3d_portal_list_t p_list;
            p_list.no_portals = portal_count; 
            p_list.portal_list = calloc(portal_count, sizeof(carmen3d_portal_t));

            portal_count = 0;
            for(int i=0; i < s->goal_feasibilty_msg->portal_list.no_of_portals; i++){
                if(s->goal_feasibilty_msg->portal_list.portals[i].floor_no == c_floor_no/*current_floor_no*/
                   && s->goal_feasibilty_msg->portal_list.portals[i].type == 0){

                    carmen_point_t p_global;
                    p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy0[0];
                    p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy0[1];
                    carmen_point_t p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);
                
                    lcmglColor3f(1.0, .0,0.5);

                    // Transform from GLOBAL to LOCAL for LCMGL
                    double gp_global[3] = {p_global.x, p_global.y, 0};
                    double gp_local[3];

                    bot_frames_transform_vec (s->frames, "global", "local", gp_global, gp_local);
                    gp_local[2] = 0;

                    lcmglCircle(gp_local, 0.2);

                    p_list.portal_list[portal_count].portal_id = i;
                    point2d_t p1 = {p_map.x, p_map.y};

                    p_list.portal_list[portal_count].portal_points[0] = p1;

                    point2d_t g_p1 = {s->goal_feasibilty_msg->portal_list.portals[i].xy0[0], 
                                      s->goal_feasibilty_msg->portal_list.portals[i].xy0[1]};
                
                    p_list.portal_list[portal_count].global_portal_points[0] = g_p1; 


                    p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy1[0];
                    p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy1[1];
                    p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);

                    // Transform from GLOBAL to LOCAL for LCMGL
                    double gp1_global[3] = {p_global.x, p_global.y, 0};
                    double gp1_local[3];

                    bot_frames_transform_vec (s->frames, "global", "local", gp1_global, gp1_local);
                    gp1_local[2] = 0;

                    lcmglCircle(gp1_local, 0.2);
                
                    point2d_t p2 = {p_map.x, p_map.y};

                    fprintf(stderr, "\n Portal on Map %d : %f,%f => %f, %f\n", i, p1.x, p1.y, p2.x, p2.y);

                    p_list.portal_list[portal_count].portal_points[1] = p2;

                    point2d_t g_p2 = {s->goal_feasibilty_msg->portal_list.portals[i].xy1[0], 
                                      s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]};
                
                    p_list.portal_list[portal_count].global_portal_points[1] = g_p2;

                    p_list.portal_list[portal_count].floor_no = s->goal_feasibilty_msg->portal_list.portals[i].floor_no;
                    p_list.portal_list[portal_count].floor_ind = s->goal_feasibilty_msg->portal_list.portals[i].floor_ind;                    
            
                    portal_count++; 
                    //check distance from robot to this portal 
                }        
            }
        
            double cost = carmen3d_planner_compute_path_costs_and_portals_from_elevator(p_list, 
                                                                                        goal,
                                                                                        &elevator,
                                                                                        &(s->nav_config), &(s->robot_config),
                                                                                        -1, s->lcmgl_cost_querry);

            s->partial_cost_value += cost; 

            
            convert_portal_list_to_msg(p_list, 2 , &elevator , cost, s);

            //free the p_list
            if(p_list.portal_list != NULL){
                free(p_list.portal_list);
            }

            //we need to send this back in a proper format 

            fprintf(stderr, " Full Cost of Path : %f\n" , s->partial_cost_value);

            //respond with the result 

            if(lcmgl){
                bot_lcmgl_switch_buffer (lcmgl);  
            }
        }
        else{
            fprintf(stderr, "+++++++++= Error : Could not find elevator on the goal floor\n");

            ///******* Send an error message 
            carmen3d_portal_list_t p_list;
            p_list.no_portals = 0;
            p_list.portal_list = NULL;
            
            convert_portal_list_to_msg(p_list, 1 , NULL , -10000, s);

        }
    }

    //else - more complicated check 
    else{

        s->partial_cost_value = 0;
        //check distance from this floor to an elevator reaching the goal floor 
        //and distance from elevator to goal 

        carmen_point_t goal_global;

        carmen3d_portal_t elevator; 

        int elevator_found = 0; 

        for(int i=0; i < s->goal_feasibilty_msg->portal_list.no_of_portals; i++){
            if(s->goal_feasibilty_msg->portal_list.portals[i].floor_no == c_floor_no /*current_floor_no*/
               && s->goal_feasibilty_msg->portal_list.portals[i].type == 1){ //elevator
                carmen_point_t p_global;
                
                //we need a check to see if the elevator goes to that floor as well 

                p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy0[0];
                p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy0[1];
                carmen_point_t p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);
                
                lcmglColor3f(1.0, .0,0.5);

                // Transform from GLOBAL to LOCAL for LCMGL
                double gp_global[3] = {p_global.x, p_global.y, 0};
                double gp_local[3];

                bot_frames_transform_vec (s->frames, "global", "local", gp_global, gp_local);
                gp_local[2] = 0;

                lcmglCircle(gp_local, 0.2);

                elevator.portal_id = i;
                point2d_t p1 = {p_map.x, p_map.y};

                elevator.portal_points[0] = p1;

                point2d_t g_p1 = {s->goal_feasibilty_msg->portal_list.portals[i].xy0[0], 
                                  s->goal_feasibilty_msg->portal_list.portals[i].xy0[1]};
                
                elevator.global_portal_points[0] = g_p1;
                

                p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy1[0];
                p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy1[1];
                p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);

                // Transform from GLOBAL to LOCAL for LCMGL
                double gp1_global[3] = {p_global.x, p_global.y, 0};
                double gp1_local[3];

                bot_frames_transform_vec (s->frames, "global", "local", gp1_global, gp1_local);
                gp1_local[2] = 0;

                lcmglCircle(gp1_local, 0.2);
                
                point2d_t p2 = {p_map.x, p_map.y};

                fprintf(stderr, "\n Portal on Map %d : %f,%f => %f, %f\n", i, p1.x, p1.y, p2.x, p2.y);

                elevator.portal_points[1] = p2;

                point2d_t g_p2 = {s->goal_feasibilty_msg->portal_list.portals[i].xy1[0], 
                                  s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]};
                
                elevator.global_portal_points[1] = g_p2; 

                elevator.floor_no = s->goal_feasibilty_msg->portal_list.portals[i].floor_no;
                elevator.floor_ind = s->goal_feasibilty_msg->portal_list.portals[i].floor_ind;

                fprintf(stderr, "Elevator on the Current Floor : (%f,%f) => (%f,%f)\n", 
                        s->goal_feasibilty_msg->portal_list.portals[i].xy0[0],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy0[1],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy1[0],
                        s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]);
                
                //for now handling only one elevator situation 
                elevator_found = 1;
                break;
                
            }        
        }

        if(elevator_found == 1){

            //here our immediate goal is the elevator
            goal_global.x = s->goal_feasibilty_msg->goal.x;
            goal_global.y = s->goal_feasibilty_msg->goal.y;
            carmen_point_t  goal_map = carmen3d_map_global_to_map_coordinates(goal_global, s->nav3d_map);
            lcmglColor3f(1.0, 0.0,0.0);

            // Transform from GLOBAL to LOCAL for LCMGL
            double gp_global[3] = {goal_global.x, goal_global.y, 0};
            double gp_local[3];

            bot_frames_transform_vec (s->frames, "global", "local", gp_global, gp_local);
            gp_local[2] = 0;

            lcmglCircle(gp_local, 0.1);

            point2d_t goal = {goal_map.x, 
                              goal_map.y};
        
            //check cost from robot to goal 
            fprintf(stderr, " +++++ Goal is on Different floor\n");

            //pass in the door list on the current floor 
            //and get back feasibility, cost and which doors the path passes through 
            //first check if the goal and the robot are in the same floor 
            int portal_count = 0;
            //if so get path and check if the path goes through any portal 
            for(int i=0; i < s->goal_feasibilty_msg->portal_list.no_of_portals; i++){
                if(s->goal_feasibilty_msg->portal_list.portals[i].floor_no == c_floor_no /*current_floor_no*/
                   && s->goal_feasibilty_msg->portal_list.portals[i].type == 0){ //doors 
                    fprintf(stderr, "Portals on the Current Floor : (%f,%f) => (%f,%f)\n", 
                            s->goal_feasibilty_msg->portal_list.portals[i].xy0[0],
                            s->goal_feasibilty_msg->portal_list.portals[i].xy0[1],
                            s->goal_feasibilty_msg->portal_list.portals[i].xy1[0],
                            s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]);

                    //check distance from robot to this portal 
                    portal_count++; 
                }        
            }

            carmen3d_portal_list_t p_list;
            p_list.no_portals = portal_count; 
            p_list.portal_list = calloc(portal_count, sizeof(carmen3d_portal_t));

            portal_count = 0;
            for(int i=0; i < s->goal_feasibilty_msg->portal_list.no_of_portals; i++){
                
                if(s->goal_feasibilty_msg->portal_list.portals[i].floor_no == c_floor_no /*current_floor_no*/
                   && s->goal_feasibilty_msg->portal_list.portals[i].type == 0){

                    carmen_point_t p_global;
                    p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy0[0];
                    p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy0[1];
                    carmen_point_t p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);
                
                    lcmglColor3f(1.0, .0,0.5);

                    // Transform from GLOBAL to LOCAL for LCMGL
                    double gp_global[3] = {p_global.x, p_global.y, 0};
                    double gp_local[3];

                    bot_frames_transform_vec (s->frames, "global", "local", gp_global, gp_local);
                    gp_local[2] = 0;

                    lcmglCircle(gp_local, 0.2);

                    p_list.portal_list[portal_count].portal_id = i;
                    point2d_t p1 = {p_map.x, p_map.y};

                    p_list.portal_list[portal_count].portal_points[0] = p1;

                    point2d_t g_p1 = {s->goal_feasibilty_msg->portal_list.portals[i].xy0[0], 
                                      s->goal_feasibilty_msg->portal_list.portals[i].xy0[1]};
                
                    p_list.portal_list[portal_count].global_portal_points[0] = g_p1; 

                    p_global.x = s->goal_feasibilty_msg->portal_list.portals[i].xy1[0];
                    p_global.y =  s->goal_feasibilty_msg->portal_list.portals[i].xy1[1];
                    p_map = carmen3d_map_global_to_map_coordinates(p_global, s->nav3d_map);

                    // Transform from GLOBAL to LOCAL for LCMGL
                    double gp1_global[3] = {p_global.x, p_global.y, 0};
                    double gp1_local[3];

                    bot_frames_transform_vec (s->frames, "global", "local", gp1_global, gp1_local);
                    gp1_local[2] = 0;

                    lcmglCircle(gp1_local, 0.2);
                
                    point2d_t p2 = {p_map.x, p_map.y};

                    fprintf(stderr, "\n Portal on Map %d : %f,%f => %f, %f\n", i, p1.x, p1.y, p2.x, p2.y);

                    p_list.portal_list[portal_count].portal_points[1] = p2;

                    point2d_t g_p2 = {s->goal_feasibilty_msg->portal_list.portals[i].xy1[0], 
                                      s->goal_feasibilty_msg->portal_list.portals[i].xy1[1]};
                
                    p_list.portal_list[portal_count].global_portal_points[1] = g_p2;
            
                    p_list.portal_list[portal_count].floor_no = s->goal_feasibilty_msg->portal_list.portals[i].floor_no;
                    p_list.portal_list[portal_count].floor_ind = s->goal_feasibilty_msg->portal_list.portals[i].floor_ind;

                    portal_count++; 
                    //check distance from robot to this portal 
                }        
            }
        
            double cost = carmen3d_planner_compute_elevator_path_costs_and_portals(p_list, 
                                                                                   &elevator,
                                                                                   &(s->nav_config), &(s->robot_config),
                                                                                   -1, s->lcmgl_cost_querry);


            s->partial_cost_value = cost; 

            
            convert_portal_list_to_msg(p_list, 1 , &elevator , cost, s);

            if(p_list.portal_list != NULL){
                free(p_list.portal_list);
            }

            if(/*current_floor_no*/ c_floor_no != s->goal_feasibilty_msg->goal_floor_no){
                fprintf(stderr, "Cost Querry for a different floor - Requesting New map\n");    
                s->outstanding_cost_calulation = 1;
                request_floor_map(s->goal_feasibilty_msg->goal_floor_no, s);
                return;
            }
        }

        else{
            fprintf(stderr, " Error +++++++ No elevator Found on this floor \n"); 
            carmen3d_portal_list_t p_list;
            p_list.no_portals = 0;
            p_list.portal_list = NULL;
            convert_portal_list_to_msg(p_list, 2 , NULL , -10000, s);

        }
    }
}

static void update_positions(int publish_plan, state_t *s)
{
    static carmen_point_t last_position = { 0, 0, 0 };
    static double last_timestamp = 0.0;

    s->corrected_odometry = s->odometry;
    carmen_localize_correct_odometry(&(s->corrected_odometry), &s->mappos);

    if (!s->nav_config.dont_integrate_odometry) {
        s->robot_position.x = s->corrected_odometry.x;
        s->robot_position.y = s->corrected_odometry.y;
        s->robot_position.theta = s->corrected_odometry.theta;
    }
    else {
        s->robot_position.x = s->mappos.globalpos.x;
        s->robot_position.y = s->mappos.globalpos.y;
        s->robot_position.theta = s->mappos.globalpos.theta;
    }

    carmen3d_planner_update_robot(&s->robot_position, &s->nav_config, &s->robot_config);

    last_timestamp = s->mappos.timestamp;
    last_position = s->mappos.globalpos;

    carmen3d_planner_status_t status;
    carmen3d_planner_get_status(&status);

    if (status.path.length > 0) {
        carmen_point_t map_final_waypoint;
        map_final_waypoint.x = status.path.points[status.path.length - 1].x;
        map_final_waypoint.y = status.path.points[status.path.length - 1].y;
        double goal_robot_dist = sqrt(pow(s->clicked_map_goal.x - s->robot_position.x, 2) + pow(s->clicked_map_goal.y
                                                                                          - s->robot_position.y, 2));
        double final_waypt_robot_dist = sqrt(pow(map_final_waypoint.x - s->robot_position.x, 2) + pow(map_final_waypoint.y
                                                                                                   - s->robot_position.y, 2));
        if (s->publishWaypt && s->curr_goal_msg)
            carmen3d_navigator_publish_waypoint(s->nav3d_map, &s->robot_position, &(s->curr_goal_msg->goal), &s->robot_config,
                                                s->curr_goal_msg);
    }
    //  carmen_navigator_publish_status();
    int next_waypoint = -1;

    next_waypoint = draw_next_waypoint(s);
  

    //we need to have a valid current goal msg
    if(publish_plan){

	  //BotTrans global_to_local;
        //bot_frames_get_trans_with_utime (s->frames, "global", "local", s->curr_goal_msg->utime, &global_to_local);
        //this utime is right only when we are publishing
	  fprintf(stderr, "publish plan harry\n");
	  carmen3d_navigator_publish_plan(s->nav3d_map, s->lcm, s->curr_goal_msg, s->reached_clicked_goal, next_waypoint, s->frames, s->curr_goal_msg->utime);//global_to_local);
    }
    //this is where the waypoints are published
}

//check if elevator is reachable 
static void check_elevator_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
                                   const erlcm_goal_t * msg, void * user) {
    state_t *s = (state_t *) user;

    fprintf(stderr, "Check elevator message recevied : Checking for elevator reachability\n");
    fprintf(stderr, "Elevator msg : %f,%f\n" , msg->pos[0], msg->pos[1]);    

    // This query is in the local frame. Transform to global
    double xyz_local[] = {msg->pos[0], msg->pos[1], 0};
    double xyz_global[3];
    bot_frames_transform_vec (s->frames, "local", "global", xyz_local, xyz_global);

    s->er_door_elevator_pos[0] = xyz_global[0];
    s->er_door_elevator_pos[1] = xyz_global[1];
    s->check_elevator_mode = 1;
    s->got_new_check = 1;

    // Render in the LOCAL frame
    bot_lcmgl_t *lcmgl = s->lcmgl_elevator_check;
    if(lcmgl){
        lcmglPointSize(10);
        lcmglColor3f(1.0, .0,.0);
        lcmglCircle(s->er_door_elevator_pos, 0.4);
        bot_lcmgl_switch_buffer (lcmgl);
    }
}

//check if door is reachable 
static void check_door_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
                               const erlcm_goal_t * msg, void * user) {

    state_t *s = (state_t *) user;

    fprintf(stderr, "Check Door message recevied : Checking for door reachability\n");
    fprintf(stderr, "Door msg : %f,%f\n" , msg->pos[0], msg->pos[1]);

    // This query is in the local frame. Transform to global
    double xyz_local[] = {msg->pos[0], msg->pos[1], 0};
    double xyz_global[3];
    bot_frames_transform_vec (s->frames, "local", "global", xyz_local, xyz_global);

    s->er_door_elevator_pos[0] = xyz_global[0];
    s->er_door_elevator_pos[1] = xyz_global[1];

    s->check_door_mode = 1;
    s->got_new_check = 1;

    // Render in the LOCAL frame
    bot_lcmgl_t *lcmgl = s->lcmgl_elevator_check;
    if(lcmgl){
        lcmglPointSize(10);
        lcmglColor3f(0.0, 0.0,1.0);
        lcmglCircle(s->er_door_elevator_pos, 0.4);
        bot_lcmgl_switch_buffer (lcmgl);
    }
}


static void waypoint_nav_status_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
                                        const erlcm_navigator_status_t * msg, void * user) {

    state_t *s = (state_t *) user;

	fprintf(stderr,"+=+=+=+=+=Waypoint Goal Reached+=+=+=+=+=\n");
    if(msg->goal_reached ==1){
        if(!s->elevator_controller_mode){
            s->reached_clicked_goal = 1;
            if(s->verbose){
#if VERBOSE
            fprintf(stderr,"+=+=+=+=+=Waypoint Goal Reached+=+=+=+=+=\n");
#endif 
            }
            carmen3d_planner_clear_goal(&s->nav_config);
            s->clicked_map_goal_exists = 0;
            update_positions(0, s);
        }
        //check what this goal point was
    }

}

//this querry should be sent by either the DM or the tour map server 
// we need to get the proper map also 
static void navigator_cost_feasibility_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
                                               const erlcm_goal_feasibility_querry_t * msg, void * user) {

    state_t *s = (state_t *) user;

    if(s->goal_feasibilty_msg !=NULL){
        erlcm_goal_feasibility_querry_t_destroy(s->goal_feasibilty_msg);
    }
    //keep the last cost querry message in memory
    s->goal_feasibilty_msg = erlcm_goal_feasibility_querry_t_copy(msg);
  
    fprintf(stderr,"Goal Feasibility Handler\n");
    fprintf(stderr,"Goal Floor : %d\n", msg->goal_floor_no);  

    calculate_cost_querry(s->current_floor_no, s);
}

static void pose_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
                         const bot_core_pose_t * msg, void * user) {

  //walter
    state_t *s = (state_t *) user;
    
    if(s->nav_map ==NULL || s->gridmap_msg==NULL ){
        return;
    }

	if (s->last_bot_pose_local)
	  bot_core_pose_t_destroy (s->last_bot_pose_local);

	s->last_bot_pose_local = bot_core_pose_t_copy (msg);

	return;
	
    s->currentStateTime = msg->utime;
    static bot_core_pose_t *prevUpdate = NULL;
    static int first_update = 1;
    static double last_update_time = 0;

    
    // Transform pose from LOCAL to GLOBAL frame
    double pos_local[3] = {msg->pos[0], msg->pos[1], msg->pos[2]};
    double pos_global[3];
    double quat_global[3], rpy_global[3];
        
    BotTrans local_to_global;
    bot_frames_get_trans (s->frames, "local", "global", &local_to_global);
    bot_quat_mult (quat_global, local_to_global.rot_quat, msg->orientation);
    bot_quat_to_roll_pitch_yaw (quat_global, rpy_global);

    bot_frames_transform_vec (s->frames, "local", "global", pos_local, pos_global);

    // prevUpdate is in the LOCAL frame
    if (prevUpdate == NULL) {
        prevUpdate = bot_core_pose_t_copy(msg);
    }

    if (s->ignore_new_goals) {
        carmen3d_planner_status_t status;
        carmen3d_planner_get_status(&status);
        if (status.path.length > 0)
            s->reached_clicked_goal = 0; 
                                              
        //we need to allow configurable thresholds for the goal 
        //using a single threshold for now 
        if (fabs(pos_global[0] - s->clicked_map_goal_global.x) < 1.0 && fabs(pos_global[1] - s->clicked_map_goal_global.y) < 1.0) {
            s->reached_clicked_goal = 1;
            //------Here is where the goal reached is checked
            fprintf(stderr,"+=+=+=+=+=Goal Reached+=+=+=+=+=\n");
            

            carmen3d_planner_clear_goal(&s->nav_config);
            s->clicked_map_goal_exists = 0;
            update_positions(s->republish_path,s);
      
            //send back the goal msg we have 
            
            if(s->curr_goal_msg !=NULL){
                // transform from GLOBAL to LOCAL frame
                erlcm_navigator_goal_msg_t *curr_goal_msg_local = 
                    erlcm_navigator_goal_msg_t_copy (s->curr_goal_msg);

                double xyz_global[] = {s->curr_goal_msg->goal.x, s->curr_goal_msg->goal.y, 0};
                double xyz_local[3];
                double rpy_global[] = {0, 0, s->curr_goal_msg->goal.yaw};
                double quat_global[4], quat_local[4], rpy_local[3];

                bot_frames_transform_vec (s->frames, "global", "local", xyz_global, xyz_local);
                
                BotTrans global_to_local;
                bot_frames_get_trans (s->frames, "global", "local", &global_to_local);
                bot_roll_pitch_yaw_to_quat (rpy_global, quat_global);
                bot_quat_mult (quat_local, local_to_global.rot_quat, quat_global);
                bot_quat_to_roll_pitch_yaw (quat_local, rpy_local);

                curr_goal_msg_local->goal.x = xyz_local[0];
                curr_goal_msg_local->goal.x = xyz_local[1];
                curr_goal_msg_local->goal.yaw = rpy_local[2];

                erlcm_navigator_goal_msg_t_publish(s->lcm, "NAVIGATOR_GOAL_REACHED", 
                                                   curr_goal_msg_local);

                //Note - this will break envoy stuff
                /*erlcm_speech_cmd_t s_msg; 
                s_msg.utime = bot_timestamp_now();
                s_msg.cmd_type = "PATH_STATUS";
                s_msg.cmd_property = "AT_GOAL";
                erlcm_speech_cmd_t_publish(s->lcm, "PATH_STATUS", &s_msg);*/

                //free the current msg 
                erlcm_navigator_goal_msg_t_destroy(s->curr_goal_msg);
                erlcm_navigator_goal_msg_t_destroy(curr_goal_msg_local);
            }
        }
    }
    else{
        //Added hack to get goal reached command - bypassing the old topo navigator - will break envoy code 
        if(s->curr_goal_msg !=NULL && s->reached_clicked_goal == 0){
            if (fabs(pos_global[0] - s->clicked_map_goal_global.x) < 1.0 && fabs(pos_global[1] - s->clicked_map_goal_global.y) < 1.0) {
                erlcm_speech_cmd_t s_msg; 
                s_msg.utime = bot_timestamp_now();
                s_msg.cmd_type = "PATH_STATUS";
                s_msg.cmd_property = "AT_GOAL";
                erlcm_speech_cmd_t_publish(s->lcm, "PATH_STATUS", &s_msg);
                s->reached_clicked_goal = 1;
            }
        }
    }

    if (s->nav3d_map) {

        s->robot_gfs_global.x = pos_global[0];
        s->robot_gfs_global.y = pos_global[1];

        //bot_quat_to_roll_pitch_yaw(msg->orientation, rpy);
        //s->robot_gfs_global.theta = rpy[2];
        s->robot_gfs_global.theta = rpy_global[2];
        s->mappos.globalpos = carmen3d_map_global_to_map_coordinates(s->robot_gfs_global, s->nav3d_map);
        s->mappos.timestamp = msg->utime/1e6;

        if(s->check_elevator_mode || s->check_door_mode){
            //now is the tricky part
            //when the goal becomes accesible - the map should have been updated
            //ideally it should cycle through the two or more elevators to see if they are reachable

            if(s->curr_goal_msg != NULL){
                erlcm_navigator_goal_msg_t_destroy(s->curr_goal_msg);
            }
            s->curr_goal_msg = (erlcm_navigator_goal_msg_t *) calloc(1, sizeof(erlcm_navigator_goal_msg_t));
      
            int goal_reachable = 0;             

            if (s->got_new_check) {
                fprintf(stderr,"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                if (s->check_elevator_mode)
                    fprintf(stderr,"Elevator Mode : Checking if elevator is reachable (%f,%f)\n",
                            s->er_door_elevator_pos[0],s->er_door_elevator_pos[1]);
                else if (s->check_door_mode)
                    fprintf(stderr,"Doorway Mode : Checking if other side of doorway is reachable (%f,%f)\n",
                            s->er_door_elevator_pos[0],s->er_door_elevator_pos[1]);
                s->got_new_check = 0;
            }

            // er_door_elevator_pos in GLOBAL frame
            s->curr_goal_msg->goal.x = s->er_door_elevator_pos[0];
            s->curr_goal_msg->goal.y = s->er_door_elevator_pos[1];

            int door_is_open = 0;
            // For the check_gridmap check, the positions should be in the LOCAL frame
            if (s->use_check_gridmap_for_doors) {
                // Update and check the gridmap to see if the straight-line path to the position is valid
                check_gridmap_update (s->check_gridmap);

                // Transform positions from GLOBAL to LOCAL frame
                double goal_xyz_global[] = {s->curr_goal_msg->goal.x, s->curr_goal_msg->goal.y, 0};
                double robot_xyz_global[] = {s->robot_gfs_global.x, s->robot_gfs_global.y, 0};
                double goal_xyz_local[3], robot_xyz_local[3];

                bot_frames_transform_vec (s->frames, "global", "local", goal_xyz_global, goal_xyz_local);
                bot_frames_transform_vec (s->frames, "global", "local", robot_xyz_global, robot_xyz_local);

                double theta = atan2 (goal_xyz_local[1] - robot_xyz_local[1], 
                                      goal_xyz_local[0] - robot_xyz_local[0]);
                                      
                int is_forward = 1;
                int failsafe = 0; // when >= 1, reduces the vehicle footprint (width)
                struct check_path_result path_res;
                check_gridmap_check_path (s->check_gridmap, is_forward, failsafe,
                                          robot_xyz_local[0], robot_xyz_local[1], theta,
                                          goal_xyz_local[0], goal_xyz_local[1], theta,
                                          &path_res);

                //fprintf (stdout, "Checking path from (x,y) = (%.2f, %.2f), theta = %.2f --> (x,y) = (%.2f, %.2f), theta = %.2f\n",
                //         s->robot_gfs_global.x, s->robot_gfs_global.y, s->robot_gfs_global.theta,
                //         s->curr_goal_msg->goal.x, s->curr_goal_msg->goal.y, s->robot_gfs_global.theta);
                //fprintf (stdout, "path_res.cost = %.2f, path_res.obs_max = %.2f\n\n\n",
                //         path_res.cost, path_res.obs_max);

                if ( (path_res.cost > -1) && (path_res.obs_max > -1) && (path_res.obs_max < COLLISION_OBS_MAX_THRESHOLD))
                    door_is_open = 1;
            }
            // For Carmen-based map queries, the positions should be in the GLOBAL frame
            else {
                s->nav_config.map_update_radius = 5.0;

                carmen_point_t sim_rect_center;

                // The sim_rects_list comes from the MAP_SERVER and is in the GLOBAL frame already
                if(s->sim_rects_list){
                    carmen_point_t g_sim_rect_center;
                    g_sim_rect_center.x = s->sim_rects_list->xy[0];
                    g_sim_rect_center.y = s->sim_rects_list->xy[1];
                    g_sim_rect_center.theta = 0;
                        
                    sim_rect_center = carmen3d_map_global_to_map_coordinates(g_sim_rect_center, s->nav3d_map);
                }

                // The frontlaser and rearlaser robot_laser messages include pose in the GLOBAL frame
                if(s->have_rearlaser)
                    carmen3d_planner_update_map(&s->frontlaser, &s->rearlaser,  
                                                s->sim_rects_list, &sim_rect_center, 
                                                &s->nav_config, 
                                                &s->robot_config, s->mappos.globalpos);//, s->lcm, s->nav3d_map);
                else
                    carmen3d_planner_update_map(&s->frontlaser, NULL,s->sim_rects_list, &sim_rect_center, 
                                                &s->nav_config, 
                                                &s->robot_config, s->mappos.globalpos);//, s->lcm, s->nav3d_map);
                

                carmen3d_navigator_goal_changed(s->curr_goal_msg->goal.x, s->curr_goal_msg->goal.y, s->curr_goal_msg->goal.z, 
                                                s->curr_goal_msg->goal.yaw, 0, 0, s);
                
                carmen3d_planner_status_t status;
                carmen3d_planner_get_status(&status);
              
                if(status.path.length > 0)
                    door_is_open = 1;
            }
            
            //we need to set the heading to face the elevator door - depending on which floor we are getting out of
            s->reached_clicked_goal = 0;
            s->clicked_map_goal_exists = 1;
            //carmen_navigator_start_autonomous();
            
            // Is the door/elevator open?
            if (door_is_open) {
                goal_reachable = 1;
                
                fprintf(stderr," ***** %s (%f,%f) is reachable - going there\n",
                        s->check_elevator_mode ? "Elevator" : "Doorway",
                        s->er_door_elevator_pos[0], 
                        s->er_door_elevator_pos[1]);

                erlcm_speech_cmd_t msg;
                msg.utime = bot_timestamp_now();
                if(s->check_elevator_mode){
                    msg.cmd_type = "OPEN_ELEVATOR"; 
                    msg.cmd_property = ""; 
                    erlcm_speech_cmd_t_publish (s->lcm, "ELEVATOR_STATUS", &msg);
                }
                else if(s->check_door_mode){
                    msg.cmd_type = "DOOR_STATUS"; 
                    msg.cmd_property = "OPEN"; 
                    erlcm_speech_cmd_t_publish (s->lcm, "DOOR_STATUS", &msg);
                }

                s->check_elevator_mode = 0;
                s->check_door_mode = 0;

                s->clicked_map_goal_exists = 0;

                bot_lcmgl_t *lcmgl = s->lcmgl_elevator_check;
                if(lcmgl){
                    bot_lcmgl_switch_buffer (lcmgl);
                }
            }
            else{
                //this will be continously published - might need to handle an error if publishing for too long
                erlcm_speech_cmd_t msg;
                msg.utime = bot_timestamp_now();
                if(s->check_elevator_mode){
                    msg.cmd_type = "ELEVATOR_STATUS"; 
                    msg.cmd_property = "CLOSED"; 
                    erlcm_speech_cmd_t_publish (s->lcm, "ELEVATOR_STATUS", &msg);
                }
                else if(s->check_door_mode){
                    msg.cmd_type = "DOOR_STATUS"; 
                    msg.cmd_property = "CLOSED"; 
                    erlcm_speech_cmd_t_publish (s->lcm, "DOOR_STATUS", &msg);
                }
            }
                s->clicked_map_goal_exists = 0;
        }      
      
        carmen3d_planner_update_robot_map(s->mappos.globalpos.x, s->mappos.globalpos.y,
                                          s->mappos.globalpos.theta, &s->nav_config, &s->robot_config);
        //only update if we've moved more than .1m
        if (bot_vector_dist_3d(msg->pos, prevUpdate->pos) > .1 || first_update || carmen_get_time() - last_update_time > 1) {
            if (prevUpdate != NULL)
                bot_core_pose_t_destroy(prevUpdate);
            prevUpdate = bot_core_pose_t_copy(msg);
			fprintf(stderr,"pose handler\n");
            update_positions(s->republish_path,s);
            first_update = 0;
            last_update_time = carmen_get_time();
        }
    }
    
    static int64_t last_utility_pub = -1;
    double *cost_ptr = carmen_conventional_get_costs_ptr();//carmen_conventional_get_utility_ptr();
    double * conv_ptr = carmen_conventional_get_utility_ptr();
    float * map_ptr = get_planner_map();
    
    if(conv_ptr != NULL){
        //fprintf(stderr,"Have utility map : %f\n", (msg->utime - last_utility_pub)/1e6);
    }

    //we dont need to publish this 

    if (conv_ptr != NULL && s->gridmap_msg != NULL && 0 && msg->utime - last_utility_pub > 1e6){
        //fprintf(stderr,"Publishing Utility Map\n");
        last_utility_pub = msg->utime;
        double util_min_value = 970;
        double scale = 1.0 / (1000.0 - util_min_value);
        double shift = -1 * util_min_value * scale;

        erlcm_gridmap_t * lcm_map = carmen3d_map_create_compressed_carmen3d_gridmap_t_from_double(conv_ptr,
                                                                                                  "utility_map", s->gridmap_msg, scale, shift);

        erlcm_gridmap_t * lcm_cost_map = carmen3d_map_create_compressed_carmen3d_gridmap_t_from_double(cost_ptr,
                                                                                                       "utility_map", s->gridmap_msg, 1, 0);

        erlcm_gridmap_t * lcm_updated_map = carmen3d_map_create_compressed_carmen3d_gridmap_t(map_ptr,
                                                                                              "utility_map", s->gridmap_msg, 1, 0);
        //&frontier3d_map->config, &frontier3d_map->midpt, frontier3d_map->sizeX2,
        //  frontier3d_map->sizeY2, 0);
        lcm_map->utime = bot_timestamp_now();
        lcm_cost_map->utime = bot_timestamp_now();
        lcm_updated_map->utime = bot_timestamp_now();
        erlcm_gridmap_t_publish(s->lcm, FRONTIER_UTILITY_MAP_CHANNEL, lcm_updated_map);
        erlcm_gridmap_t_publish(s->lcm, NAVIGATOR_UTILITY_MAP_CHANNEL, lcm_map);
        erlcm_gridmap_t_publish(s->lcm, "NAVIGATOR_COST_MAP", lcm_cost_map);
        erlcm_gridmap_t_destroy(lcm_map);
        erlcm_gridmap_t_destroy(lcm_cost_map);
        erlcm_gridmap_t_destroy(lcm_updated_map);
    }
}

void request_nav_map(state_t *s)
{
    /* subscripe to map, and wait for it to come in... */
    sulcm_map_request_msg_t msg;
    msg.utime =  carmen_get_time()*1e6;
    msg.requesting_prog = "NAVIGATOR";
    fprintf(stderr,"Requesting map \n");

    sulcm_map_request_msg_t_publish(s->lcm,"MAP_REQUEST_CHANNEL",&msg);

    carmen_warn("done.\n");
}

void request_floor_map(int floor_no, state_t *s)
{
    /* subscripe to map, and wait for it to come in... */
    erlcm_map_request_msg_t msg;
    msg.utime =  carmen_get_time()*1e6;
    msg.requesting_prog = "NAVIGATOR";
    fprintf(stderr,"Requesting floor map \n");
    msg.floor_no = floor_no;
    erlcm_map_request_msg_t_publish(s->lcm,"SFMAP_REQUEST_CHANNEL",&msg);    
    
}

static int draw_next_waypoint(state_t *s)
{
    carmen_traj_point_t waypoint;
    int waypoint_index;
    int waypoint_status;
    int is_goal;
    command_t command;
    carmen3d_planner_status_t nstatus;
    memset(&nstatus,0,sizeof(carmen3d_planner_status_t));

    command.tv = 0;
    command.rv = 0;

    waypoint = s->robot_position;

    carmen3d_planner_get_status(&nstatus);
    waypoint_status = carmen3d_planner_next_waypoint(&waypoint, &waypoint_index, &is_goal, &s->nav_config);

    /* goal is reached */

    if (waypoint_status > 0) {
	  //means goal reached
        s->autonomous_status = 0;
        //carmen_navigator_publish_autonomous_stopped(CARMEN_NAVIGATOR_GOAL_REACHED_v);
        carmen_verbose("Autonomous off Motion command %.2f %.2f\n", command.tv, carmen_radians_to_degrees(command.rv));

        command.tv = 0;
        command.rv = 0;
        return -1;
    }

    if (waypoint_status < 0) {
        //means that there is no valid path to goal
        command.tv = 0;
        command.rv = 0;
        return -1;
    }

    bot_lcmgl_t *lcmgl = s->lcmgl_navigator;

    lcmglPointSize(6);

    bot_lcmgl_line_width(lcmgl, 4);

    carmen_point_t temp_pt;
    carmen_point_t temp_global_pt;

  

    for(int i=0;i< nstatus.path.length;i++){
        if(i == waypoint_index)
            continue;

        lcmglColor3f(1.0,.0,.0);
        temp_pt.x = nstatus.path.points[i].x;
        temp_pt.y = nstatus.path.points[i].y;
        temp_pt.theta = nstatus.path.points[i].theta;
        temp_global_pt = carmen3d_map_map_to_global_coordinates(temp_pt, s->nav3d_map);

        // Convert from GLOBAL to LOCAL for LCMGL
        double waypoint_curr_global[3] = { temp_global_pt.x, temp_global_pt.y, 0.0};
        double waypoint_curr_local[3];
        
        double rpy_global[] = {0, 0, temp_global_pt.theta};
        double quat_global[4], rpy_local[3], quat_local[4];

        bot_frames_transform_vec (s->frames, "global", "local", waypoint_curr_global, waypoint_curr_local);
        waypoint_curr_local[2] = 0;

        lcmglCircle(waypoint_curr_local, 0.1);
    
        BotTrans global_to_local;
        bot_frames_get_trans (s->frames, "global", "local", &global_to_local);
        
        bot_roll_pitch_yaw_to_quat (rpy_global, quat_global);
        bot_quat_mult (quat_local, global_to_local.rot_quat, quat_global);
        bot_quat_to_roll_pitch_yaw (quat_local, rpy_local);

        //lets draw a line as well 
		// lcmglColor3f(1.0,1.0,.0);
	lcmglColor3f(1,0,0);
        lcmglBegin(GL_LINES);
        lcmglVertex3d(waypoint_curr_local[0], waypoint_curr_local[1], 0);
        double f_x[2] = {waypoint_curr_local[0] + 0.2 * cos(rpy_local[2]), waypoint_curr_local[1]  + 0.2 * sin(rpy_local[2]) };
        lcmglVertex3d(f_x[0], f_x[1], 0.0);
        lcmglEnd();
    
    }

    //lcmglColor3f(0.0, 1.0,1.0);
    lcmglColor3f(1,0,0);
  

    temp_pt.x = nstatus.path.points[waypoint_index].x;
    temp_pt.y = nstatus.path.points[waypoint_index].y;
    temp_pt.theta = nstatus.path.points[waypoint_index].theta;
    temp_global_pt = carmen3d_map_map_to_global_coordinates(temp_pt, s->nav3d_map);

    // Transform from GLOBAL to LOCAL for LCMGL
    double waypoint_curr_global[3] = { temp_global_pt.x, temp_global_pt.y, 0.0};
    double waypoint_curr_local[3];
        
    double rpy_global[] = {0, 0, temp_global_pt.theta};
    double quat_global[4], rpy_local[3], quat_local[4];
    
    bot_frames_transform_vec (s->frames, "global", "local", waypoint_curr_global, waypoint_curr_local);
    waypoint_curr_local[2] = 0;
    
    lcmglCircle(waypoint_curr_local, 0.1);
    
    BotTrans global_to_local;
    bot_frames_get_trans (s->frames, "global", "local", &global_to_local);
    
    bot_roll_pitch_yaw_to_quat (rpy_global, quat_global);
    bot_quat_mult (quat_local, global_to_local.rot_quat, quat_global);
    bot_quat_to_roll_pitch_yaw (quat_local, rpy_local);


    //lets draw a line as well 
    lcmglColor3f(1.0,1.0,.0);
    lcmglBegin(GL_LINES);
    lcmglVertex3d(waypoint_curr_local[0], waypoint_curr_local[1], 0);
    double f_x[2] = {waypoint_curr_local[0] + 0.2 * cos(rpy_local[2]), waypoint_curr_local[1]  + 0.2 * sin(rpy_local[2]) };
    lcmglVertex3d(f_x[0], f_x[1], 0.0);
    lcmglEnd();

    bot_lcmgl_switch_buffer (lcmgl);
  
    return waypoint_index;
}


static void generate_next_motion_command(state_t *s)
{
    carmen_traj_point_t waypoint;
    int waypoint_index;
    int waypoint_status;
    int is_goal;
    command_t command;
    carmen3d_planner_status_t status;

    command.tv = 0;
    command.rv = 0;

    waypoint = s->robot_position;

    waypoint_status = carmen3d_planner_next_waypoint(&waypoint, &waypoint_index, &is_goal, &s->nav_config);

    /* goal is reached */

    if (waypoint_status > 0) {
        //means goal reached
        s->autonomous_status = 0;
        //carmen_navigator_publish_autonomous_stopped(CARMEN_NAVIGATOR_GOAL_REACHED_v);

        carmen_verbose("Autonomous off Motion command %.2f %.2f\n", command.tv, carmen_radians_to_degrees(command.rv));
        
        command.tv = 0;
        command.rv = 0;
        return;
    }

    if (waypoint_status < 0) {
        //means that there is no valid path to goal
        command.tv = 0;
        command.rv = 0;
        return;
    }
#if VERBOSE
    fprintf(stderr,"===Issuing Carmen Command to robot\n");
    fprintf(stderr,"Current pos: %.1f %.1f %.1f next %.1f %.1f\n",
            s->robot_position.x, s->robot_position.y, carmen_radians_to_degrees(s->robot_position.theta), waypoint.x, waypoint.y);
#endif
    carmen_verbose("Current pos: %.1f %.1f %.1f next %.1f %.1f\n",
                   s->robot_position.x, s->robot_position.y,
                   carmen_radians_to_degrees(s->robot_position.theta), waypoint.x, waypoint.y);
}

//add planar laser handlers to this 

/*void lcm_robotlaser_to_carmen_laser(erlcm_robot_laser_t *msg, carmen_robot_laser_message *robot, 
                                    double laser_offset, double laser_ang_offset)
{
    robot->config.accuracy = .1;
    robot->config.laser_type = HOKUYO_UTM;
    robot->config.remission_mode = REMISSION_NONE;
    robot->config.angular_resolution = msg->laser.radstep;
    robot->config.fov = msg->laser.nranges * msg->laser.radstep;
    robot->config.maximum_range = 30; //TODO:not sure what to do about this hardcoded value
    robot->config.start_angle = msg->laser.rad0 + laser_ang_offset; //not sure if this also needs to be changed
    robot->num_readings = msg->laser.nranges;
    robot->range = msg->laser.ranges;
    robot->robot_pose.x = msg->pose.pos[0];
    robot->robot_pose.y = msg->pose.pos[1];
    double rpy[3] = {0,0,0};
    bot_quat_to_roll_pitch_yaw (msg->pose.orientation, rpy) ;
    robot->robot_pose.theta = rpy[2];
    robot->laser_pose.x = robot->robot_pose.x + laser_offset*cos(rpy[2]);
    robot->laser_pose.y = robot->robot_pose.y + laser_offset*sin(rpy[2]);
    robot->laser_pose.theta = rpy[2];
    robot->timestamp = msg->utime/1e6;
}
*/

void carmen3d_correct_laser(carmen_robot_laser_message *laser, 
		       carmen_localize_globalpos_message *globalpos, double laser_offset, double angle_offset)
{
    laser->laser_pose.x = globalpos->globalpos.x + laser_offset*cos(globalpos->globalpos.theta);
    laser->laser_pose.y = globalpos->globalpos.y + laser_offset*sin(globalpos->globalpos.theta);
    laser->laser_pose.theta = globalpos->globalpos.theta + angle_offset;
}

void on_planar_lidar(const lcm_recv_buf_t *rbuf, const char * channel,
                     const bot_core_planar_lidar_t * msg, void * user)
{
    state_t *s = (state_t *) user;

    static bot_core_planar_lidar_t *static_msg = NULL;
    if (static_msg != NULL)
        bot_core_planar_lidar_t_destroy(static_msg);
    static_msg = bot_core_planar_lidar_t_copy(msg);

    carmen_robot_laser_message *carmen_msg = NULL;

    if(!strcmp(channel, "SKIRT_FRONT")){
        s->last_laser_time = static_msg->utime;
        s->have_frontlaser = 1;
        carmen_msg = &(s->frontlaser);
    } 
    else{
        s->have_rearlaser = 1; 
        carmen_msg = &(s->rearlaser);
    }

    if (s->mappos.timestamp <= 0 || !(s->nav_config.map_update_obstacles
                                   || s->nav_config.map_update_freespace)){
        return;
    }

     for (int i=0;i<static_msg->nranges;i++){
        if (static_msg->ranges[i]< 0.1  || 
            static_msg->ranges[i]> 30.0){
            static_msg->ranges[i]= 30.0;
        }

        //static_msg->ranges[i]= 10.0;
    }

    memset(carmen_msg, 0, sizeof(carmen_robot_laser_message));    
    double rpy[3];
    // bot_quat_to_roll_pitch_yaw(s->last_bot_pose_deadreckon_global->orientation, rpy);
    //carmen_msg->laser_pose.theta = rpy[2];
    carmen_msg->robot_pose.x = s->mappos.globalpos.x;//s->last_bot_pose_deadreckon_global->pos[0];
    carmen_msg->robot_pose.y = s->mappos.globalpos.y;//s->last_bot_pose_deadreckon_global->pos[1];
    carmen_msg->robot_pose.theta = s->mappos.globalpos.theta;//rpy[2];
    carmen_msg->config.accuracy = .1;
    carmen_msg->config.laser_type = HOKUYO_UTM;
    carmen_msg->config.remission_mode = REMISSION_NONE;
    carmen_msg->config.angular_resolution = static_msg->radstep;
    carmen_msg->config.fov = static_msg->nranges * static_msg->radstep;
    carmen_msg->config.maximum_range = 20.0;//s->laser_proj->max_range;
    carmen_msg->config.start_angle = static_msg->rad0;
    if(!strcmp(channel, "SKIRT_FRONT")){
        carmen_msg->config.start_angle = static_msg->rad0; 
    }
    else{
        carmen_msg->config.start_angle = static_msg->rad0;
    }

    carmen_msg->id = 0;
    
    carmen_msg->num_readings = static_msg->nranges;
    //ranges get over written 
    if(!carmen_msg->range){
        carmen_msg->range = calloc(static_msg->nranges, sizeof(float));
    }
    
    memcpy(carmen_msg->range, static_msg->ranges, static_msg->nranges *sizeof(float));
    carmen_msg->num_remissions = static_msg->nintensities;
    carmen_msg->remission = static_msg->intensities;
    carmen_msg->timestamp = bot_timestamp_to_double(static_msg->utime);
    carmen_msg->host = (char *) "lcm";
    
    carmen3d_correct_laser(&s->frontlaser, &s->mappos, s->front_laser_offset, s->front_laser_angle_offset);
  
    carmen3d_correct_laser(&s->rearlaser, &s->mappos,s->rear_laser_offset, s->rear_laser_angle_offset);

    if(!strcmp(channel, "SKIRT_FRONT")){
        carmen_point_t sim_rect_center;

        // In the GLOBAL frame
        if(s->sim_rects_list){
            carmen_point_t g_sim_rect_center;
            g_sim_rect_center.x = s->sim_rects_list->xy[0];
            g_sim_rect_center.y = s->sim_rects_list->xy[1];
            g_sim_rect_center.theta = 0;
            
            sim_rect_center = carmen3d_map_global_to_map_coordinates(g_sim_rect_center, s->nav3d_map);
        }
        
        if(s->have_rearlaser){         

            carmen3d_planner_update_map(&s->frontlaser, &s->rearlaser, 
                                        s->sim_rects_list, &sim_rect_center, 
                                        &s->nav_config, &s->robot_config, s->mappos.globalpos);//, s->lcm, s->nav3d_map);
        }
        else{
            carmen3d_planner_update_map(&s->frontlaser, NULL, 
                                        s->sim_rects_list, &sim_rect_center, 
                                        &s->nav_config, &s->robot_config, s->mappos.globalpos);//, s->lcm, s->nav3d_map);
        }
        int next_waypoint = -1;
        next_waypoint = draw_next_waypoint(s);        
    }
}

/*static void lcm_robot_frontlaser_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
                                         const erlcm_robot_laser_t * msg, void * user) {

    state_t *s = (state_t *) user;

    // Transform pose in robot_laser_t into GLOBAL reference frame
    double pos_global[3], quat_global[4];
    BotTrans local_to_global;
    bot_frames_get_trans_with_utime (s->frames, "local", "global", msg->pose.utime, &local_to_global);

    bot_frames_transform_vec (s->frames, "local", "global", msg->pose.pos, pos_global);
    bot_quat_mult (quat_global, local_to_global.rot_quat, msg->pose.orientation);

    static erlcm_robot_laser_t* staticmsg = NULL;
    s->have_frontlaser = 1;
    if (staticmsg != NULL) {
        erlcm_robot_laser_t_destroy(staticmsg);
    }
    staticmsg = erlcm_robot_laser_t_copy(msg);

    memcpy (staticmsg->pose.pos, pos_global, 3*sizeof(double));
    memcpy (staticmsg->pose.orientation, quat_global, 4*sizeof(double));    


    if (s->mappos.timestamp <= 0 || !(s->nav_config.map_update_obstacles
                                   || s->nav_config.map_update_freespace)){
        return;
    }

    lcm_robotlaser_to_carmen_laser(staticmsg,&s->frontlaser, s->front_laser_offset, s->front_laser_angle_offset);
  
    carmen3d_correct_laser(&s->frontlaser, &s->mappos, s->front_laser_offset, s->front_laser_angle_offset);
  
    carmen3d_correct_laser(&s->rearlaser, &s->mappos,s->rear_laser_offset, s->rear_laser_angle_offset);

    //carmen3d_planner_update_map(&frontlaser, &nav_config, &s->robot_config, 1);
  
    carmen_point_t sim_rect_center;

    // In the GLOBAL reference frame
    if(s->sim_rects_list){
        carmen_point_t g_sim_rect_center;
        g_sim_rect_center.x = s->sim_rects_list->xy[0];
        g_sim_rect_center.y = s->sim_rects_list->xy[1];
        g_sim_rect_center.theta = 0;
        
        sim_rect_center = carmen3d_map_global_to_map_coordinates(g_sim_rect_center, s->nav3d_map);
    }

    if(s->have_rearlaser){
        carmen3d_planner_update_map(&s->frontlaser, &s->rearlaser, 
                                    NULL, s->kinect_global_pose, 
                                    s->sim_rects_list, &sim_rect_center, 
                                    &s->nav_config, &s->robot_config, s->mappos.globalpos);
    }
    else{
        carmen3d_planner_update_map(&s->frontlaser, NULL, 
                                    NULL, s->kinect_global_pose, 
                                    s->sim_rects_list, &sim_rect_center, 
                                    &s->nav_config, &s->robot_config, s->mappos.globalpos);
    }
    int next_waypoint = -1;
    next_waypoint = draw_next_waypoint(s);
    }

static void lcm_robot_rearlaser_handler(const lcm_recv_buf_t *rbuf, const char * channel,
                                        const erlcm_robot_laser_t * msg, void * user)
{
    state_t *s = (state_t *) user;


    // Transform pose in robot_laser_t into GLOBAL reference frame
    double pos_global[3], quat_global[4];
    BotTrans local_to_global;
    bot_frames_get_trans_with_utime (s->frames, "local", "global", msg->pose.utime, &local_to_global);

    bot_frames_transform_vec (s->frames, "local", "global", msg->pose.pos, pos_global);
    bot_quat_mult (quat_global, local_to_global.rot_quat, msg->pose.orientation);

    //fprintf(stderr,"-----------R-----------------");
    static erlcm_robot_laser_t* staticmsg = NULL;
    s->have_rearlaser = 1;
    if (staticmsg != NULL) {
        erlcm_robot_laser_t_destroy(staticmsg);
    }
    staticmsg = erlcm_robot_laser_t_copy(msg);

    memcpy (staticmsg->pose.pos, pos_global, 3*sizeof(double));
    memcpy (staticmsg->pose.orientation, quat_global, 4*sizeof(double));    

    lcm_robotlaser_to_carmen_laser(staticmsg,&s->rearlaser, s->rear_laser_offset, s->rear_laser_angle_offset);
    }*/


void carmen3d_navigator_clear_goal(state_t *s)
{

    carmen3d_planner_clear_goal(&s->nav_config);
    s->clicked_map_goal_exists = 0;
    update_positions(0, s);
}

void carmen3d_navigator_goal_changed(double x, double y, double z, double theta, 
                                     int use_theta, int publish, state_t *s)
{

    if (s->ignore_new_goals) {
        if (!s->reached_clicked_goal) {
            printf("STILL GETTING TO GOAL, IGNORING NEW GOAL!\n");
            return;
        }
    }
    s->clicked_map_goal_exists = 1;
    s->clicked_map_goal_global.x = x;
    s->clicked_map_goal_global.y = y;
    s->clicked_map_goal_global.theta = theta;
    s->plan_use_theta = use_theta;

    s->curr_goal_msg->goal.x = x;
    s->curr_goal_msg->goal.y = y;
    s->curr_goal_msg->goal.yaw = theta;
    s->curr_goal_msg->use_theta = use_theta;
    
    bot_lcmgl_t *lcmgl = s->lcmgl_nav_goal; 

    lcmglPointSize(6);

    // Transform from GLOBAL to LOCAL frame for LCMGL
    double xyz_goal_global[3] = {s->clicked_map_goal_global.x, s->clicked_map_goal_global.y, 0};
    double xyz_goal_local[3];
    
    bot_frames_transform_vec (s->frames, "global", "local", xyz_goal_global, xyz_goal_local);
    xyz_goal_local[2] = 0;

    lcmglColor3f(0.0, 1.0,1.0);
    lcmglCircle(xyz_goal_local, 0.8);
    bot_lcmgl_switch_buffer (lcmgl);

    if (s->nav3d_map) {
        s->clicked_map_goal = carmen3d_map_global_to_map_coordinates(s->clicked_map_goal_global, s->nav3d_map);
        fprintf(stderr, "Got new goal, global:(%.2f,%.2f,%.2f) %.2f , map:(%.2f,%.2f) %.2f, mapSize=(%d,%d)\n",
                s->clicked_map_goal_global.x, s->clicked_map_goal_global.y, z, carmen_radians_to_degrees(theta), s->clicked_map_goal.x,
                s->clicked_map_goal.y, carmen_radians_to_degrees(s->clicked_map_goal.theta), s->nav3d_map->config.x_size, s->nav3d_map->config.y_size);

        carmen3d_planner_update_goal(&s->clicked_map_goal, !s->plan_use_theta, &s->nav_config, &s->robot_config, 1, s->lcmgl_waypoints);
        if(publish){
		  update_positions(1, s);
        }
        else{
            update_positions(0, s);
        }
    }
}

//**** Should remove this - not being used ****//

void carmen_navigator_start_autonomous(state_t *s) 
{
  //return;
    //not being used
    s->autonomous_status = 1;
    carmen3d_planner_reset_map(&s->robot_config);
    generate_next_motion_command(s);
}

void carmen_navigator_stop_autonomous(state_t *s)
{

    s->autonomous_status = 0;
    //carmen_navigator_publish_autonomous_stopped(CARMEN_NAVIGATOR_USER_STOPPED_v);
    //carmen_robot_velocity_command(0, 0);
    //lcm_carmen3d_robot_velocity_command(.0, .0,lcm);
}

int carmen_navigator_autonomous_status(state_t *s)
{
    return s->autonomous_status;
}

//***********************************************//

static void floor_status_handler(const lcm_recv_buf_t *rbuf, const char *channel, 
                                 const erlcm_floor_status_msg_t *msg, void *user)
{
  state_t *s = (state_t *) user;

    fprintf(stderr, "Floor Status Received : Ind %d No ; %d", msg->floor_ind, msg->floor_no);

    s->current_floor_no = msg->floor_no;
}

static void gridmap_handler(const lcm_recv_buf_t *rbuf, const char *channel, const erlcm_gridmap_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr, "+-+-+-+-+-+-New Floor Map+-+-+-+-+-+-\n");
    //carmen3d_navigator_clear_goal();
    //this clears the goal
    if (s->gridmap_msg != NULL) {
	  fprintf(stderr,"Goal Floor : ");
	  erlcm_gridmap_t_destroy(s->gridmap_msg);
    }
	fprintf(stderr,"Goal Floor : ");
	fprintf(stderr,"%d\n", msg->size);
    s->gridmap_msg = erlcm_gridmap_t_copy(msg);
    fprintf(stderr,"Goal Floor : ");
    // free old maps
    if (s->nav3d_map->complete_map != NULL) {
        carmen_map_destroy(&s->nav_map);
        free(s->nav3d_map->map);
        free(s->nav3d_map->complete_map);
    }
	fprintf(stderr,"Goal Floor : ");
    carmen3d_map_uncompress_lcm_map(s->nav3d_map, s->gridmap_msg);
    s->nav_map = carmen3d_map_map3d_map_copy(s->nav3d_map);
    set_nav_map(s->nav3d_map); 

    carmen3d_planner_set_map(s->nav_map, &s->robot_config);

    fprintf(stderr,"Goal Floor : %d Current Floor : %d\n", s->goal_floor,
            s->current_floor_no);

    if(s->goal_floor != s->current_floor_no){
        s->clicked_map_goal_exists = 0;
        fprintf(stderr,"--------Clearing Goal\n");
    }
    else{
        s->clicked_map_goal_exists = 1;
    }

    if (s->clicked_map_goal_exists) {
        s->clicked_map_goal = carmen3d_map_global_to_map_coordinates(s->clicked_map_goal_global, s->nav3d_map);
        carmen3d_planner_update_goal(&s->clicked_map_goal, !s->plan_use_theta, &s->nav_config, &s->robot_config, 1, s->lcmgl_waypoints);
    }
    update_positions(0, s);

    // force planner to update map & rebuild costs
    // build utility by dynamic programming to robot position
    carmen_conventional_dynamic_program(round(s->robot_position.x / s->nav_map->config.resolution), round(s->robot_position.y
                                                                                                          / s->nav_map->config.resolution));

    double * conv_ptr = carmen_conventional_get_utility_ptr();
    if (conv_ptr == NULL) {
        fprintf(stderr, "conventional map is null\n");
    }
    else {
        double util_min_value = 970;
        double scale = 1.0 / (1000.0 - util_min_value);
        double shift = -1 * util_min_value * scale;

        erlcm_gridmap_t * lcm_map = carmen3d_map_create_compressed_carmen3d_gridmap_t_from_double(conv_ptr,
                                                                                                  "utility_map", s->gridmap_msg, scale, shift);
        lcm_map->utime = bot_timestamp_now();
        erlcm_gridmap_t_publish(s->lcm, NAVIGATOR_UTILITY_MAP_CHANNEL, lcm_map);
        erlcm_gridmap_t_destroy(lcm_map);
    }
    fprintf(stderr,"Done processing new map\n");


    if(1){ //doing a test cost querry 
        //do the test querry here 
        fprintf(stdout,"Calculating Cost to goals\n");
        carmen_point_t portal_global;
        portal_global.x = 6.662;
        portal_global.y = 41.929;
        carmen_point_t  portal_local = carmen3d_map_global_to_map_coordinates(portal_global, s->nav3d_map);

        point2d_t portal;
        portal.x = portal_local.x;
        portal.y = portal_local.y;
	
        fprintf(stdout, "Portal %f,%f => %f,%f\n", portal_global.x, portal_global.y , portal.x, portal.y);

        int no_goals = 2;
        point2d_t *goals = (point2d_t *) calloc(no_goals, sizeof(point2d_t));
	
        carmen_point_t g1_global;
        g1_global.x = 12.762393;
        g1_global.y = -8.523449;
        carmen_point_t  g1_local = carmen3d_map_global_to_map_coordinates(g1_global, s->nav3d_map);


        goals[0].x = g1_local.x;
        goals[0].y = g1_local.y;

        carmen_point_t g2_global;
        g2_global.x = 24.075877;
        g2_global.y = -16.534822;
        carmen_point_t  g2_local = carmen3d_map_global_to_map_coordinates(g2_global, s->nav3d_map);

        goals[1].x = g2_local.x;
        goals[1].y = g2_local.y;

        double * costs = carmen3d_planner_compute_path_costs(goals, no_goals, portal, &s->nav_config, &s->robot_config, -1);
        //fprintf(stdout, "\t(%f,%f) =>%f,%f : %f\n", g1_global.x, g1_global.y, goals[0].x, goals[0].y, costs[0]);
        //fprintf(stdout, "\t(%f,%f) =>%f,%f : %f\n", g2_global.x, g2_global.y, goals[1].x, goals[1].y, costs[1]);
    }
  

}

//this is received when a new floor map is requested 
static void temp_floor_gridmap_handler(const lcm_recv_buf_t *rbuf, const char *channel, 
                                       const erlcm_floor_gridmap_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr, "+-+-+-+-+-+-Temp Floor Map+-+-+-+-+-+-\n");
    fprintf(stderr, "Floor : %d\n", msg->floor_no); 

    //we should only handle this if there is an outstanding cost calculation 
    if(!s->outstanding_cost_calulation){
        fprintf(stdout, "No outstanding cost calculation - Skipping\n");
        return;
    }
  
    static erlcm_gridmap_t *temp_gridmap_msg = NULL;
    if (temp_gridmap_msg != NULL) {
        erlcm_gridmap_t_destroy(temp_gridmap_msg);
    }
    temp_gridmap_msg = erlcm_gridmap_t_copy(&msg->gridmap);

    // free old maps - these will be restored at the end 
    if (s->nav3d_map->complete_map != NULL) {
        carmen_map_destroy(&s->nav_map);
        free(s->nav3d_map->map);
        free(s->nav3d_map->complete_map);
    }

    carmen3d_map_uncompress_lcm_map(s->nav3d_map, temp_gridmap_msg);
    s->nav_map = carmen3d_map_map3d_map_copy(s->nav3d_map);
    set_nav_map(s->nav3d_map); 
    carmen3d_planner_set_map(s->nav_map, &s->robot_config);
    //done processing the temparary map 
    calculate_cost_querry(msg->floor_no, s);  

    fprintf(stderr,"Restoring Actual Map...");
    //now restore the old map - and the old goals 
    if (s->nav3d_map->complete_map != NULL) {
        carmen_map_destroy(&s->nav_map);
        free(s->nav3d_map->map);
        free(s->nav3d_map->complete_map);
    }
    carmen3d_map_uncompress_lcm_map(s->nav3d_map, s->gridmap_msg);
    s->nav_map = carmen3d_map_map3d_map_copy(s->nav3d_map);

    carmen3d_planner_set_map(s->nav_map, &s->robot_config);
    fprintf(stderr,"Done\n");
    fprintf(stderr,"Goal Floor : %d Current Floor : %d\n", s->goal_floor,
            s->current_floor_no);

    if(s->goal_floor != s->current_floor_no){
        s->clicked_map_goal_exists = 0;
        fprintf(stderr,"--------Clearing Goal\n");
    }
    else{
        s->clicked_map_goal_exists = 1;
    }

    if (s->clicked_map_goal_exists) {
        s->clicked_map_goal = carmen3d_map_global_to_map_coordinates(s->clicked_map_goal_global, s->nav3d_map);
        carmen3d_planner_update_goal(&s->clicked_map_goal, !s->plan_use_theta, &s->nav_config, &s->robot_config, 1, s->lcmgl_waypoints);
    }
    update_positions(0, s);

    // force planner to update map & rebuild costs
    // build utility by dynamic programming to robot position
    carmen_conventional_dynamic_program(round(s->robot_position.x / s->nav_map->config.resolution), 
                                        round(s->robot_position.y / s->nav_map->config.resolution));    
}



static void floor_gridmap_handler(const lcm_recv_buf_t *rbuf, const char *channel, 
                            const erlcm_floor_gridmap_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr, "+-+-+-+-+-+-New Map+-+-+-+-+-+-");
    //this clears the goal
    if (s->gridmap_msg != NULL) {
        erlcm_gridmap_t_destroy(s->gridmap_msg);
    }
    s->gridmap_msg = erlcm_gridmap_t_copy(msg);

    // free old maps
    if (s->nav3d_map->complete_map != NULL) {
        carmen_map_destroy(&s->nav_map);
        free(s->nav3d_map->map);
        free(s->nav3d_map->complete_map);
    }
    carmen3d_map_uncompress_lcm_map(s->nav3d_map, s->gridmap_msg);
    s->nav_map = carmen3d_map_map3d_map_copy(s->nav3d_map);
  
    set_nav_map(s->nav3d_map); 

    carmen3d_planner_set_map(s->nav_map, &s->robot_config);

    if (s->clicked_map_goal_exists) {
        s->clicked_map_goal = carmen3d_map_global_to_map_coordinates(s->clicked_map_goal_global, s->nav3d_map);
        carmen3d_planner_update_goal(&s->clicked_map_goal, !s->plan_use_theta, &s->nav_config, &s->robot_config, 1, s->lcmgl_waypoints);
    }
    update_positions(0, s);

    // force planner to update map & rebuild costs
    // build utility by dynamic programming to robot position
    carmen_conventional_dynamic_program(round(s->robot_position.x / s->nav_map->config.resolution), round(s->robot_position.y
                                                                                                    / s->nav_map->config.resolution));

    //double * /*cost_ptr*/conv_ptr = carmen_conventional_get_costs_ptr();//carmen_conventional_get_utility_ptr();
    double * conv_ptr = carmen_conventional_get_utility_ptr();
    if (conv_ptr == NULL) {
        //fprintf(stderr, "conventional map is null\n");
    }
    else {
        double util_min_value = 970;
        double scale = 1.0 / (1000.0 - util_min_value);
        double shift = -1 * util_min_value * scale;

        erlcm_gridmap_t * lcm_map = carmen3d_map_create_compressed_carmen3d_gridmap_t_from_double(conv_ptr,
                                                                                                  "utility_map", s->gridmap_msg, scale, shift);
        lcm_map->utime = bot_timestamp_now();
        erlcm_gridmap_t_publish(s->lcm, NAVIGATOR_UTILITY_MAP_CHANNEL, lcm_map);
        erlcm_gridmap_t_destroy(lcm_map);
    }
    fprintf(stderr,"Done processing new map\n");
}

//new goal message
static void on_floor_goal(const lcm_recv_buf_t *rbuf, const char *channel,
                                   const erlcm_navigator_floor_goal_msg_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr, "Ignoring the Floor ID - Treating as the current floor\n");
	
    plan_to_goal(s, &(msg->goal_msg), "NAV_GOAL_LOCAL");
}

void plan_to_goal(state_t *s, erlcm_navigator_goal_msg_t *msg, char* channel){
    s->nav_config.map_update_radius = 2.5;

    //if we get this type of message make the floor the same as the current floor
    //received navigator goal

    fprintf(stderr,"$$$$$$$$$$ Received Navigator Goal\n");
    if(s->curr_goal_msg != NULL){
        erlcm_navigator_goal_msg_t_destroy(s->curr_goal_msg);
    }

    s->curr_goal_msg = erlcm_navigator_goal_msg_t_copy(msg);

	//walter
	// Convert the most recent robot pose into the global (map) frame
	double pos_local[3] = {s->last_bot_pose_local->pos[0], s->last_bot_pose_local->pos[1],
						   s->last_bot_pose_local->pos[2]}; 
    double pos_global[3];
    double quat_global[3], rpy_global[3];

	// Get the current transformation from local to global (map) frame
    BotTrans local_to_global;
    bot_frames_get_trans (s->frames, "local", "global", &local_to_global);

	// Get the roll, pitch, and yaw in the global frame
	bot_quat_mult (quat_global, local_to_global.rot_quat, s->last_bot_pose_local->orientation);
    bot_quat_to_roll_pitch_yaw (quat_global, rpy_global);

	// Transform the robot's position in the local frame into the global frame
    bot_frames_transform_vec (s->frames, "local", "global", pos_local, pos_global);


	// Now, use pos_global as the robot's starting position when doing the graph search

	
    
    //convert the frame if its sent in the local frame 
    if(!strcmp(channel, "NAV_GOAL_LOCAL")){
        //convert to global frame 

        double pos_global[3], quat_local[4], quat_global[4], rpy_global[3];
        double pos_local[3] = {msg->goal.x, msg->goal.y, msg->goal.z};
        BotTrans local_to_global;
        
        bot_frames_get_trans_with_utime (s->frames, "local", "global", msg->utime, &local_to_global);

        bot_frames_transform_vec (s->frames, "local", "global", pos_local, pos_global);
        
        double rpy_local[3] = {0,0, msg->goal.yaw};
        bot_roll_pitch_yaw_to_quat (rpy_local, quat_local);
        bot_quat_mult (quat_global, local_to_global.rot_quat, quat_local);
        bot_quat_to_roll_pitch_yaw (quat_global, rpy_global);
                
        //substitue the original msg
        fprintf(stderr,"Substituing and converting to global\n");
        s->curr_goal_msg->goal.x = pos_global[0];
        s->curr_goal_msg->goal.y = pos_global[1];
        s->curr_goal_msg->goal.z = pos_global[2];
        s->curr_goal_msg->goal.yaw = rpy_global[2];
    }

    s->reached_clicked_goal = 0;
    s->clicked_map_goal_exists = 1;

    carmen_point_t sim_rect_center;

    if(s->sim_rects_list){
        //sim rects should be converted to the global frame also 
        /*double local_to_global[12];
        
        //sim rects are in the local frame 
        if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "local",
                                                      "global", s->last_laser_time,
                                                      local_to_global)) {
            fprintf(stderr,"Frame Error\n");
            return;
        }
        
        double gl[3] = {s->sim_rects_list->xy[0], s->sim_rects_list->xy[1], 0}; 
        double ll[3]; 
        bot_vector_affine_transform_3x4_3d (local_to_global, gl , ll);  
         
        carmen_point_t g_sim_rect_center;
        g_sim_rect_center.x = ll[0];
        g_sim_rect_center.y = ll[1];
        g_sim_rect_center.theta = 0;*/
        
        carmen_point_t g_sim_rect_center;
        g_sim_rect_center.x = s->sim_rects_list->xy[0];
        g_sim_rect_center.y = s->sim_rects_list->xy[1];
        g_sim_rect_center.theta = 0;
         
        sim_rect_center = carmen3d_map_global_to_map_coordinates(g_sim_rect_center, s->nav3d_map);
    }


	// Here is where you can do the graph search from pos_global to s->curr_goal_msg;
	// ......

	// Now that you've identified the path through the graph you can publish the sequence of waypoints
	// just as it's done in navigator3d_ipc.c. Note that the waypoints in your path are in the global
	// frame, so just like navigator3d_ipc.c, you will convert them to the local frame first and then
	// publish these local frame points


	//return;

	
    s->nav_config.map_update_radius = 10.0;

    //carmen3d_correct_laser(&s->frontlaser, &s->mappos, s->front_laser_offset, s->front_laser_angle_offset);
  
    //carmen3d_correct_laser(&s->rearlaser, &s->mappos,s->rear_laser_offset, s->rear_laser_angle_offset);

    carmen3d_planner_update_map(&s->frontlaser, &s->rearlaser,  
                                s->sim_rects_list, &sim_rect_center, &s->nav_config, 
                                &s->robot_config, s->mappos.globalpos);//, s->lcm, s->nav3d_map);
	

    static int64_t last_utility_pub = -1;
    double *cost_ptr = carmen_conventional_get_costs_ptr();//carmen_conventional_get_utility_ptr();
	
    double * conv_ptr = carmen_conventional_get_utility_ptr();	
	
    float * map_ptr = get_planner_map();
	
    if(conv_ptr != NULL){
        //fprintf(stderr,"Have utility map : %f\n", (msg->utime - last_utility_pub)/1e6);
    }

    //we dont need to publish this 

    if (conv_ptr != NULL && s->gridmap_msg != NULL){
        //fprintf(stderr,"Publishing Utility Map\n");
        last_utility_pub = s->curr_goal_msg->utime;
        double util_min_value = 970;
        double scale = 1.0 / (1000.0 - util_min_value);
        double shift = -1 * util_min_value * scale;
		
        erlcm_gridmap_t * lcm_map = carmen3d_map_create_compressed_carmen3d_gridmap_t_from_double(conv_ptr,
                                                                                                  "utility_map", s->gridmap_msg, scale, shift);

        erlcm_gridmap_t * lcm_cost_map = carmen3d_map_create_compressed_carmen3d_gridmap_t_from_double(cost_ptr,
                                                                                                       "utility_map", s->gridmap_msg, 1, 0);

        erlcm_gridmap_t * lcm_updated_map = carmen3d_map_create_compressed_carmen3d_gridmap_t(map_ptr,
                                                                                              "utility_map", s->gridmap_msg, 1, 0);
        //&frontier3d_map->config, &frontier3d_map->midpt, frontier3d_map->sizeX2,
        //  frontier3d_map->sizeY2, 0);
        lcm_map->utime = bot_timestamp_now();
        lcm_cost_map->utime = bot_timestamp_now();
        lcm_updated_map->utime = bot_timestamp_now();
        erlcm_gridmap_t_publish(s->lcm, FRONTIER_UTILITY_MAP_CHANNEL, lcm_updated_map);
        erlcm_gridmap_t_publish(s->lcm, NAVIGATOR_UTILITY_MAP_CHANNEL, lcm_map);
        erlcm_gridmap_t_publish(s->lcm, NAVIGATOR_UTILITY_MAP_CHANNEL, lcm_cost_map);
        erlcm_gridmap_t_destroy(lcm_map);
        erlcm_gridmap_t_destroy(lcm_cost_map);
        erlcm_gridmap_t_destroy(lcm_updated_map);
    }
    
	
    s->goal_floor = s->current_floor_no; //assume that the goal is in the same floor as the current

    if((int)s->curr_goal_msg->use_theta==1){
        //if use theta might need to do something funcky with this //
        //add a intermediate goal behind the actual goal in the direction in which we should be facing

        fprintf(stderr,"Goal Command is go to heading - need to add a sub goal if needed\n : %f,%f,%f\n", 
                s->curr_goal_msg->goal.x, s->curr_goal_msg->goal.y, s->curr_goal_msg->goal.yaw);
    
    
        //check if the goal is reachable first
        carmen3d_navigator_goal_changed(s->curr_goal_msg->goal.x, s->curr_goal_msg->goal.y, s->curr_goal_msg->goal.z, 
                                        s->curr_goal_msg->goal.yaw, (int) s->curr_goal_msg->use_theta, 1, s);
        carmen3d_planner_status_t status;
        carmen3d_planner_get_status(&status);

        if(status.path.length > 0){
		  fprintf(stderr,"The Goal is reachable\n");
            //check the direction of the last waypoint and the goal and the required heading at goal
            if(status.path.length > 1){
                carmen_point_t plan_last_waypt, plan_last_waypt_global;
                plan_last_waypt.x = status.path.points[status.path.length - 1].x;
                plan_last_waypt.y = status.path.points[status.path.length - 1].y;
                plan_last_waypt_global = carmen3d_map_map_to_global_coordinates(plan_last_waypt, s->nav3d_map);

                carmen_point_t plan_nlast_waypt, plan_nlast_waypt_global;
                plan_nlast_waypt.x = status.path.points[status.path.length - 2].x;
                plan_nlast_waypt.y = status.path.points[status.path.length - 2].y;
                plan_nlast_waypt_global = carmen3d_map_map_to_global_coordinates(plan_nlast_waypt, s->nav3d_map);

                double heading_angle = atan2(plan_last_waypt.y - plan_nlast_waypt.y, plan_last_waypt.x - plan_nlast_waypt.x);

                fprintf(stderr,"Goal Heading 2 : %f, Heading from last wp : %f\n",  180*s->curr_goal_msg->goal.yaw/M_PI, status.path.points[status.path.length - 1].theta * 180 /M_PI);//heading_angle * 180/M_PI);
            }
        }
        else{
		  fprintf(stderr,"Issue - No path to goal\n");
        }
    }
    else{
        carmen3d_navigator_goal_changed(s->curr_goal_msg->goal.x, s->curr_goal_msg->goal.y, s->curr_goal_msg->goal.z, s->curr_goal_msg->goal.yaw, (int) s->curr_goal_msg->use_theta, 1, s);

        double path_cost = cost_of_path();
        fprintf(stderr,"Cost of Path : %f\n", path_cost);
    }

}

//new goal message
static void on_goal(const lcm_recv_buf_t *rbuf, const char *channel,
                                   const erlcm_navigator_goal_msg_t *msg, void *user)
{
    state_t *s = (state_t *) user;
	fprintf(stderr,"on_goal_harry");
    plan_to_goal(s, msg, channel);
}

static void navigator_shutdown(int signo __attribute__ ((unused)) )
{
    static int done = 0;
    fprintf(stderr,"shutting down!\n");
    exit(0);
}

static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
           "options are:\n"
           "--help,        -h    display this message \n"
           "--verbose,     -v    enable verbose output\n"
           "--publish,     -p    publish waypoints \n"
           "--init_map,    -i    read the initial_map from the passed in file \n", funcName);
    exit(1);

}

static void read_parameters(int argc, char **argv, state_t *s)
{
    //Parse command line options
    s->publishWaypt = 0;

    const char *optstring = "hvpr";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' }, 
                                  { "verbose", no_argument, 0, 'v' }, 
                                  { "publish", no_argument, 0, 'p' }, 
                                  { "republish", no_argument, 0, 'r' }, 
                                  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'h':
            usage(argv[0]);
            break;
        case 'v':
            {
                s->verbose = 1;
                break;
            }
        case 'r':
            {
                s->republish_path = 1;
                break;
            }
        case 'p':
            {
                s->publishWaypt = 1;
                break;
            }
        default:
            {
                usage(argv[0]);
                break;
            }
        }
    }

    if (s->publishWaypt) {
        printf("Publishing Waypoints!\n");
    }
    else
	  printf("NOT Publishing Waypoints!\n");


    BotParam * conf = bot_param_new_from_server(s->lcm, 1);

    s->robot_config.approach_dist = bot_param_get_double_or_fail(conf, "navigator.min_approach_dist"); //unsure
    s->robot_config.side_dist = bot_param_get_double_or_fail(conf, "navigator.min_side_dist"); //unsure
    s->robot_config.length = bot_param_get_double_or_fail(conf, "navigator.robot_length"); //used in convenctional
    s->robot_config.width = bot_param_get_double_or_fail(conf, "navigator.robot_width"); //used in convenctional

    s->nav_config.goal_size = bot_param_get_double_or_fail(conf, "navigator.goal_size");
    s->nav_config.waypoint_tolerance = bot_param_get_double_or_fail(conf, "navigator.waypoint_tolerance");
    s->nav_config.goal_theta_tolerance = bot_param_get_double_or_fail(conf, "navigator.goal_theta_tolerance");

    s->nav_config.replan_frequency = bot_param_get_double_or_fail(conf, "navigator.replan_frequency");
    s->nav_config.smooth_path = bot_param_get_boolean_or_fail(conf, "navigator.smooth_path");
    s->nav_config.plan_to_nearest_free_point = 0;
    s->ignore_new_goals = bot_param_get_boolean_or_fail(conf, "navigator.ignore_new_goals_until_completed");

    BotFrames *frames = bot_frames_get_global (s->lcm, conf);

    // Find the position of the forward-facing LIDAR
    char *coord_frame;
    coord_frame = bot_param_get_planar_lidar_coord_frame (conf, "SKIRT_FRONT");
    if (!coord_frame)
        fprintf (stderr, "\tError determining front laser coordinate frame\n");

    BotTrans front_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame, "body", &front_laser_to_body))
        fprintf (stderr, "\tError determining front laser coordinate frame\n");

    s->front_laser_offset = front_laser_to_body.trans_vec[0];

    double front_rpy[3];
    bot_quat_to_roll_pitch_yaw (front_laser_to_body.rot_quat, front_rpy);
    s->front_laser_angle_offset = front_rpy[2];


    // Now for the rear laser
    coord_frame = NULL;
    coord_frame = bot_param_get_planar_lidar_coord_frame (conf, "SKIRT_REAR");

    if (!coord_frame)
        fprintf (stderr, "\tError determining rear laser coordinate frame\n");

    BotTrans rear_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame, "body", &rear_laser_to_body))
        fprintf (stderr, "\tError determining LIDAR coordinate frame\n");
 
    s->rear_laser_offset = rear_laser_to_body.trans_vec[0];

    double rear_rpy[3];
    bot_quat_to_roll_pitch_yaw (rear_laser_to_body.rot_quat, rear_rpy);
    s->rear_laser_angle_offset = rear_rpy[2];

    // Now for the Kinect
    BotTrans kinect_to_body;
    if (!bot_frames_get_trans (frames, "HEAD", "body", &kinect_to_body))
        fprintf (stderr, "\tError determining Kinect coordinate frame\n");
 
    s->kinect_front_offset = kinect_to_body.trans_vec[0];


    /* 
     * double position[3];
     * char key[2048];
     * sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_FRONT");
     * if(3 != bot_param_get_double_array(conf, key, position, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
     * }
     * s->front_laser_offset = position[0];
     * 
     * sprintf(key, "%s.%s.%s.position", "calibration", "planar_lidars", "SKIRT_REAR");
     * if(3 != bot_param_get_double_array(conf, key, position, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
     * }
     * s->rear_laser_offset = position[0];
     * 
     * sprintf(key, "%s.%s.%s.position", "calibration", "kinect", "KINECT_FORWARD");
     * if(3 != bot_param_get_double_array(conf, key, position, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tKinect Pos : (%f,%f,%f)\n",position[0], position[1], position[2]);
     * }
     * s->kinect_front_offset = position[0];
     * 
     * double rpy[3];
     * sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_FRONT");
     * if(3 != bot_param_get_double_array(conf, key, rpy, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tFront Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
     * }
     * s->front_laser_angle_offset  = carmen_degrees_to_radians(rpy[2]);
     * 
     * sprintf(key, "%s.%s.%s.rpy", "calibration", "planar_lidars", "SKIRT_REAR");
     * if(3 != bot_param_get_double_array(conf, key, rpy, 3)){
     *     fprintf(stderr,"\tError Reading Params\n");
     * }else{
     *     fprintf(stderr,"\tRear Laser RPY : (%f,%f,%f)\n",rpy[0], rpy[1], rpy[2]);
     * }
     * s->rear_laser_angle_offset = carmen_degrees_to_radians(rpy[2]);
     */

    //IGNORED PARAMS!
    s->robot_config.max_t_vel = -1;//bot_conf_get_double_or_fail(conf, "navigator.max_t_vel");
    s->robot_config.max_r_vel = -1;//bot_conf_get_double_or_fail(conf, "navigator.max_r_vel");

    s->robot_config.acceleration = -1;//bot_conf_get_double_or_fail(conf, "navigator.acceleration");
    s->robot_config.reaction_time = -1;//bot_conf_get_double_or_fail(conf, "navigator.reaction_time");

    s->nav_config.map_update_radius = 2.5; //1000.0;//2.5;//-1;// bot_conf_get_double_or_fail(conf, "navigator.map_update_radius");
    s->nav_config.num_lasers_to_use = 360;//-1;//bot_conf_get_int_or_fail(conf, "navigator.num_lasers_to_use");
    s->nav_config.map_update_obstacles = 1;//-1;//bot_conf_get_boolean_or_fail(conf, "navigator.map_update_obstacles");
    s->nav_config.map_update_freespace = 1;//0;//-1;//bot_conf_get_boolean_or_fail(conf, "navigator.map_update_freespace");
    s->nav_config.dont_integrate_odometry = -1; //bot_conf_get_boolean_or_fail(conf, "navigator.dont_integrate_odometry");


    if (s->nav_config.goal_size < s->robot_config.approach_dist) {
        carmen_warn("%sBad things will happen when the approach distance is\n"
                    "less than the navigator goal size. Changing navigator\n"
                    "goal size to be %f (robot approach distance).%s\n\n", carmen_red_code, s->robot_config.approach_dist,
                    carmen_normal_code);
        s->nav_config.goal_size = s->robot_config.approach_dist;
    }
}

int main(int argc, char **argv)
{
    int c;
    setlinebuf(stdout);

    state_t *s = calloc(1, sizeof(state_t));

    s->lcm = bot_lcm_get_global(NULL);

    BotParam *param = bot_param_new_from_server (s->lcm, 0);
    s->frames = bot_frames_get_global (s->lcm, param);

    handler handler_func;
    int x, y;
    carmen_offlimits_p offlimits;
    int num_offlimits_segments;
    char *goal_string;

    s->nav_map = NULL;
    s->nav3d_map = NULL;
    s->verbose = 0;
    s->use_check_gridmap_for_doors = 0;
    s->check_gridmap = NULL;
    s->ignore_new_goals = 1;
    s->curr_goal_msg = NULL;
    s->end_goal_msg = NULL;
    s->goal_feasibilty_msg = NULL; 
    s->goal_floor = -1;
    s->current_floor_no = -1;
    s->elevator_goal = NULL;
    s->last_bot_pose_deadreckon_global = NULL;
    s->use_planar_lidar = 1;
	s->gridmap_msg = NULL;

    s->result_msg.portal_list.portals = NULL;

    s->lcmgl_navigator = bot_lcmgl_init (s->lcm, "FULL_NAVIGATOR");
    s->lcmgl_waypoints = NULL;//bot_lcmgl_init (s->lcm, "WAYPOINT_INFO");
    s->lcmgl_cost_querry = bot_lcmgl_init (s->lcm, "COST_QUERRY");
    s->lcmgl_elevator_check = bot_lcmgl_init (s->lcm, "PORTAL_CHECK");
    s->lcmgl_navigator_goal = bot_lcmgl_init (s->lcm, "FULL_NAVIGATOR_GOAL");
    s->lcmgl_nav_goal = bot_lcmgl_init (s->lcm, "NAVGOAL");

    read_parameters(argc, argv, s);

    handler_func = navigator_shutdown;
    signal(SIGINT, handler_func);

    s->nav_map = (carmen_map_p) calloc(1, sizeof(carmen_map_t));
    carmen_test_alloc(s->nav_map);

    s->nav3d_map = (erlcm_map_p) calloc(1, sizeof(erlcm_map_t));
    s->nav3d_map->map = NULL;
    s->nav3d_map->complete_map = NULL;
    s->sim_rects_list = NULL;

    carmen_test_alloc(s->nav3d_map);

    if (s->use_check_gridmap_for_doors == 1)
	  s->check_gridmap = check_gridmap_create (0, 0, TRUE, TRUE, FALSE, FALSE, 0.1);
    else
      s->check_gridmap = NULL;

    erlcm_gridmap_t_subscribe(s->lcm, "MAP_SERVER", gridmap_handler,
                                    s);

	

    /*if(!s->use_planar_lidar){
	  erlcm_robot_laser_t_subscribe(s->lcm,"ROBOT_LASER", lcm_robot_frontlaser_handler, s);
        erlcm_robot_laser_t_subscribe(s->lcm,"REAR_ROBOT_LASER", lcm_robot_rearlaser_handler, s);
    }
    else{*/
    //fprintf(stderr, "Using planar lidar\n");
    //bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_FRONT" , on_planar_lidar, s);
    //bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_REAR" , on_planar_lidar, s);
    //bot_core_pose_t_subscribe (s->lcm, "ROBOT_DEADRECKON", on_deadreckon_pose, s);
    //}
    
    

    erlcm_navigator_status_t_subscribe(s->lcm,"WAYPOINT_NAV_STATUS", waypoint_nav_status_handler,s);

    
    erlcm_goal_t_subscribe(s->lcm, "CHECK_ELEVATOR", check_elevator_handler, s);
    erlcm_goal_t_subscribe(s->lcm, "CHECK_DOOR", check_door_handler, s);

    erlcm_navigator_goal_msg_t_subscribe(s->lcm, "NAV_GOAL", on_goal, s);
    //erlcm_navigator_floor_goal_msg_t_subscribe(s->lcm, "NAV_GOAL_FLOOR", on_floor_goal, s);
    erlcm_navigator_goal_msg_t_subscribe(s->lcm, "NAV_GOAL_LOCAL", on_goal, s);

    bot_core_pose_t_subscribe(s->lcm, POSE_CHANNEL, pose_handler, s);

    erlcm_goal_feasibility_querry_t_subscribe(s->lcm, "GOAL_FEASIBILITY_CHECK", navigator_cost_feasibility_handler, s);

	//erlcm_floor_status_msg_t_subscribe(s->lcm,"CURRENT_FLOOR_STATUS",floor_status_handler, 
	// s);
       
    //sulcm_rect_list_t_subscribe(s->lcm, "MAP_SERVER_RECTS", on_sim_rects, s);
	
    //get the map 
    request_nav_map(s);

    lcm_dispatch(s->lcm);

    return 0;
}
