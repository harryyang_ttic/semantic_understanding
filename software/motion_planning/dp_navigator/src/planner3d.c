/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <er_carmen/carmen.h>
#include <assert.h>
#include "planner3d_interface.h"
#include "navigator3d.h"
#include "conventional3d.h"
#include "trajectory3d.h"
#include "map3d_modify.h"



#define EMPTY 0.01
#define NUM_ACTIONS 8
#define VERBOSE 0

int carmen3d_planner_x_offset[NUM_ACTIONS] = {0, 1, 1, 1, 0, -1, -1, -1};
int carmen3d_planner_y_offset[NUM_ACTIONS] = {-1, -1, 0, 1, 1, 1, 0, -1};

carmen_map_t *carmen3d_planner_map = NULL;

carmen_map_config_t map_config_used_for_planning;

static carmen_map_t *true_map = NULL;
static erlcm_map_p nav3d_map = NULL;
static int have_plan = 0;

static int allow_any_orientation = 0;
static carmen_point_t requested_goal;
static carmen_point_t intermediate_goal;
static int goal_is_accessible;

static carmen_traj_point_t robot;
static carmen3d_planner_path_t path = {NULL, 0, 0};
static carmen_traj_point_t waypoint_act;  //keeps track of the next waypoint

static int goal_set = 0;
static int new_goal_set = 0;

carmen_inline static int 
is_empty(double value) 
{
    return (value >= 0) && (value <= EMPTY);
}


carmen_inline static int
is_in_map(int x, int y, carmen_map_p map)
{
    if (x < 0 || x >= map->config.x_size)
        return 0;
    if (y < 0 || y >= map->config.y_size)
        return 0;
    return 1;
}

void set_nav_map(erlcm_map_p g_nav3d_map){
    nav3d_map = g_nav3d_map; 
    fprintf(stderr, "(((((((((((Setting Nav Map))))))))))\n");
}

//reuse this to check visibility
static int 
is_path_to_next_waypoint_free(int old_wp_x, int old_wp_y, int new_wp_x, int new_wp_y)
{
    int X, Y;
    carmen_bresenham_param_t params;
    double current_map_value;

    carmen_get_bresenham_parameters(old_wp_x, old_wp_y, new_wp_x, new_wp_y, &params);
  
    do {
	  carmen_get_current_point(&params, &X, &Y);
        if (!is_in_map(X, Y, carmen3d_planner_map)){
            //there is an out of map point on the line between the current and old waypoint
            return 0;
        }
        current_map_value = carmen3d_planner_map->map[X][Y];
        //modified_map_value = modify_map->map[X][Y];
    
        if (!is_empty(current_map_value)){  
            //there is an occupied point on the line between the current and old waypoint
            return 0;
        }
    } while (carmen_get_next_point(&params));
  
    return (1);
}


int call_path_extract(carmen_navigator_config_t *nav_conf)
{
    carmen_world_point_t world_point;

    carmen_map_point_t map_point;

    world_point.pose.x = waypoint_act.x;
    world_point.pose.y = waypoint_act.y;
    world_point.pose.theta = waypoint_act.theta;
    world_point.map = carmen3d_planner_map;

    carmen_world_to_map(&world_point, &map_point);

    carmen_world_point_t robot_point;

    carmen_map_point_t robot_map_point;

    robot_point.pose.x = robot.x;
    robot_point.pose.y = robot.y;
    robot_point.pose.theta = robot.theta;
    robot_point.map = carmen3d_planner_map;

    carmen_world_to_map(&robot_point, &robot_map_point);

    int is_path_clear = 0;

    is_path_clear  = is_path_to_next_waypoint_free(map_point.x,map_point.y, robot_map_point.x,robot_map_point.y);

    float distance_to_waypoint;
    distance_to_waypoint = carmen_distance_traj(&waypoint_act, &robot);

    float waypoint_angle = atan2(world_point.pose.y - robot.y, world_point.pose.x - robot.x);
  
    float heading_difference = waypoint_angle - robot.theta;
  
    //fprintf(stderr, " Path Free : %d Waypoint_tol %f , Distance to WP : %f\n", is_path_clear,nav_conf->waypoint_tolerance , distance_to_waypoint);  //Sachi
    //compare the heading of the robot and the angle of the goal to the robot
    //fprintf(stderr,"Robot heading : %f, Goal Heading : %f, Differnce : %f\n", robot.theta, waypoint_angle, 	  heading_difference);
	
    if( fabs(heading_difference) < M_PI/2  && (is_path_clear && (distance_to_waypoint > nav_conf->waypoint_tolerance)))/* if (extract_path_from_value_function() < 0) ... */ {
        //fprintf(stderr, " Skipping Path regeneration \n" );
        return 1;  //The path is clear to the next way point
    }
    else
        return 0;
}

static int
extract_path_from_value_function(void)
{
    int position;
    carmen_traj_point_t path_point;
    carmen_map_point_t cur_point, prev_point, map_goal;

    if (!have_plan){
        //fprintf(stdout,"-----Exiting\n");
        return -1;
    }

    carmen3d_planner_util_clear_path(&path);
    carmen3d_planner_util_add_path_point(robot, &path);

    carmen_trajectory_to_map(&robot, &cur_point, carmen3d_planner_map);

	fprintf(stderr,"requested goal %f %f\n", requested_goal.x, requested_goal.y);

    if (goal_is_accessible) {
        map_goal.x = carmen_round(requested_goal.x /
                                  carmen3d_planner_map->config.resolution);
        map_goal.y = carmen_round(requested_goal.y /
                                  carmen3d_planner_map->config.resolution);
		fprintf(stderr,"map goal %f %f\n", map_goal.x, map_goal.y);
        map_goal.map = carmen3d_planner_map;
    } else {
        map_goal.x = carmen_round(intermediate_goal.x /
                                  carmen3d_planner_map->config.resolution);
        map_goal.y = carmen_round(intermediate_goal.y /
                                  carmen3d_planner_map->config.resolution);
        map_goal.map = carmen3d_planner_map;
    }

    do {
	  prev_point = cur_point;
	
        carmen_conventional_find_best_action(&cur_point);
        carmen_map_to_trajectory(&cur_point, &path_point);
		
        position = carmen3d_planner_util_add_path_point(path_point, &path);
		
        if (cur_point.x == map_goal.x && cur_point.y == map_goal.y)
            return 0;
    } while (cur_point.x != prev_point.x || cur_point.y != prev_point.y);

    return -1;
}

static void
compute_cost(int start_index, int end_index, double *cost_along_path,
	     double *min_cost)
{
    carmen_traj_point_p start_point;
    carmen_traj_point_p end_point;

    carmen_map_point_t p1;
    carmen_map_point_t p2;

    carmen_bresenham_param_t params;

    int x, y;
    double total_cost, cur_cost = 0;

    start_point = carmen3d_planner_util_get_path_point(start_index, &path);
    carmen_trajectory_to_map(start_point, &p1, carmen3d_planner_map);
    end_point = carmen3d_planner_util_get_path_point(end_index, &path);
    carmen_trajectory_to_map(end_point, &p2, carmen3d_planner_map);

    carmen_get_bresenham_parameters(p1.x, p1.y, p2.x, p2.y, &params);

    total_cost = 0;
    carmen_get_current_point(&params, &x, &y);
    *min_cost = carmen_conventional_get_cost(x, y);
    while (carmen_get_next_point(&params)) {
        carmen_get_current_point(&params, &x, &y);
        cur_cost = carmen_conventional_get_cost(x, y);
        total_cost += cur_cost;
        if (cur_cost < *min_cost)
            *min_cost = cur_cost;
    }

    *cost_along_path = total_cost;
}

double
cost_of_path(void)
{
    int path_index;
    double cost, min_cost;
    double total_cost;

    total_cost = 0.0;
    for (path_index = 1; path_index < path.length;
         path_index++)
        {
            compute_cost(path_index-1, path_index, &cost, &min_cost);
            total_cost += cost;
        }

    return total_cost;
}

int 
check_portals_in_path(carmen3d_portal_list_t portals, bot_lcmgl_t *lcmgl)
{
    int path_index;

    for (path_index = 0; path_index < path.length; path_index++){
        carmen_point_t waypt;
        carmen_point_t g_waypt;
        waypt.x = path.points[path_index].x;
        waypt.y = path.points[path_index].y;
	
        g_waypt = carmen3d_map_map_to_global_coordinates(waypt, nav3d_map);
        
        
        if(lcmgl){
            double pos[3] = {g_waypt.x,g_waypt.y,.0};
            lcmglCircle(pos, 0.1);
        }
    }
    
    for(int i=0; i < portals.no_portals; i++){
        int ind0 = -1;
        int ind1 = -1; 
        
        double min_dist1 = 1000; 
        double min_dist2 = 1000; 
        
        for (path_index = 0; path_index < path.length; path_index++){
            //check if in path 
            double dist1 = hypot(path.points[path_index].x - portals.portal_list[i].portal_points[0].x, path.points[path_index].y - portals.portal_list[i].portal_points[0].y);
            double dist2 = hypot(path.points[path_index].x - portals.portal_list[i].portal_points[1].x, path.points[path_index].y - portals.portal_list[i].portal_points[1].y);
            if(dist1 < min_dist1){
                min_dist1 = dist1;  
                ind0 = path_index; 
            }  
            if(dist2 < min_dist2){
                min_dist2 = dist2;  
                ind1 = path_index; 
            }
        }

        fprintf(stderr, "Min Dist %d => %f   %d => %f\n", 
                ind0, min_dist1, ind1, min_dist2);
        if(min_dist1 < 0.5 && min_dist2 < 0.5 ){
        //if(min_dist1 < 1.0 && min_dist2 < 1.0 ){
            fprintf(stderr, " Path goes through Portal : %d\n", portals.portal_list[i].portal_id); 
            carmen_point_t waypt;
            carmen_point_t g_waypt;
            waypt.x = path.points[ind0].x;
            waypt.y = path.points[ind0].y;
	
            g_waypt = carmen3d_map_map_to_global_coordinates(waypt, nav3d_map);
        
            lcmglColor3f(1.0, .2,1.0);
            if(lcmgl){
                double pos[3] = {g_waypt.x,g_waypt.y,.0};
                lcmglCircle(pos, 0.25);
            }

            waypt.x = path.points[ind1].x;
            waypt.y = path.points[ind1].y;
	
            g_waypt = carmen3d_map_map_to_global_coordinates(waypt, nav3d_map);
        
            lcmglColor3f(1.0, .0,1.0);
            if(lcmgl){
                double pos[3] = {g_waypt.x,g_waypt.y,.0};
                lcmglCircle(pos, 0.25);
            }
        
            portals.portal_list[i].pass_through = 1; 
            
            //this should be used for ordering - lower is closer 
            portals.portal_list[i].path_ind = (int) (ind0 + ind1)/2;

            if(ind0 < ind1){
                portals.portal_list[i].closer_node = 0;
            }
            else if (ind0 > ind1){
                portals.portal_list[i].closer_node = 1;
            }
            else{
                fprintf(stderr, "Error : Both Portals are near the same path point\n");
            }
        }
        else{
            fprintf(stderr, "Portal %d is not in Path \n", portals.portal_list[i].portal_id);
        }
    }
    return 0;
}

static void
smooth_path(carmen_navigator_config_t *nav_conf, bot_lcmgl_t *lcmgl)
{
    int path_index;
    int new_path_counter;
    double cost_along_prev, cost_along_next;
    double min_cost_prev, min_cost_next;
    double new_cost, new_min_cost;
    double prev_cost;

    prev_cost = cost_of_path();

    new_path_counter = 0;
    path_index = 1;

    while (path.length > 2 && carmen_distance_traj
           (&robot, carmen3d_planner_util_get_path_point(1, &path)) <
           nav_conf->goal_size) {
        carmen3d_planner_util_delete_path_point(1, &path);
    }

    //bot_lcmgl_t *lcmgl = bot_lcmgl_init( , "Node_cost");
    if(0){
        while (path_index < path.length-1){
            compute_cost(path_index-1, path_index, &cost_along_prev, &min_cost_prev);
            compute_cost(path_index, path_index+1, &cost_along_next, &min_cost_next);
            compute_cost(path_index-1, path_index+1, &new_cost, &new_min_cost);

            double threshold = bot_to_radians(90); 

            //check the angle as well
            //double heading_difference = fabs(bot_mod2pi(path.points[path_index].theta - path.points[path_index - 1].theta)) + fabs(bot_mod2pi(path.points[path_index + 1].theta - path.points[path_index].theta));
    
            double heading_difference = fabs(bot_mod2pi(path.points[path_index + 1].theta - path.points[path_index].theta));

    
            double cost_threshold = 1e-6;

            if ((cost_along_prev+cost_along_next+cost_threshold < new_cost ||
                 min_cost_next < new_min_cost || min_cost_prev < new_min_cost))
                {
                    path_index++;

                    carmen_point_t waypt;
                    carmen_point_t g_waypt;
                    waypt.x = path.points[path_index].x;
                    waypt.y = path.points[path_index].y;
		
                    g_waypt = carmen3d_map_map_to_global_coordinates(waypt, nav3d_map);

                    if(lcmgl && 0){
                        double pos[3] = {g_waypt.x,g_waypt.y,.0};
                        char node_info[512];
                        lcmglColor3f(0.0, 0.0,1.0);
                        sprintf(node_info,"[%d] : %f => %f", path_index, bot_to_degrees(heading_difference), cost_along_prev+cost_along_next - new_cost); 
                        bot_lcmgl_text(lcmgl, pos, node_info);
                    }
                }
            else
                {
                    if(0){//lcmgl && !(cost_along_prev+cost_along_next+cost_threshold < new_cost ||
                        //	       min_cost_next < new_min_cost || min_cost_prev < new_min_cost)){
                        carmen_point_t waypt;
                        carmen_point_t g_waypt;
                        waypt.x = path.points[path_index].x;
                        waypt.y = path.points[path_index].y;
		
                        g_waypt = carmen3d_map_map_to_global_coordinates(waypt, nav3d_map);
                        double pos[3] = {g_waypt.x,g_waypt.y,.0};
                        char node_info[512];
                        lcmglColor3f(1.0, .0,.0);
                        sprintf(node_info,"[%d] : %f => %f", path_index, bot_to_degrees(heading_difference), cost_along_prev+cost_along_next - new_cost); 
                        bot_lcmgl_text(lcmgl, pos, node_info);
                    }
	
                    carmen3d_planner_util_delete_path_point(path_index, &path);
                }
        }
    }
  
    path_index = 1;

    double *heading_diff = (double *) calloc(path.length-2, sizeof(double)); 

    double threshold = bot_to_radians(45);  //was 60 

    while (path_index < path.length-2){
        //check the angle as well
        //double heading_difference = fabs(bot_mod2pi(path.points[path_index].theta - path.points[path_index - 1].theta)) + fabs(bot_mod2pi(path.points[path_index + 1].theta - path.points[path_index].theta));
        //fprintf(stderr, "Theta : %f\n", path.points[path_index].theta);
        heading_diff[path_index -1] = fabs(bot_mod2pi(path.points[path_index + 1].theta - path.points[path_index-1].theta));
        //				     - bot_mod2pi(path.points[path_index].theta - path.points[path_index-1].theta));
        path_index++;
    }
  
    path_index = 1;
    int ind = 0;
    double dist_from_last = 0; 
    double gap_threshold_high = 3.0; //was 10 
    double gap_threshold_low = 2.0; 
  
  
    while (path_index < path.length-1){
        double gap = hypot(path.points[path_index].x - path.points[path_index-1].x, path.points[path_index].y - path.points[path_index-1].y);   

        //add the distance metric 
    
        if (path_index == 0 || (heading_diff[ind] > threshold && gap > gap_threshold_low) || gap > gap_threshold_high){                 
	
            carmen_point_t waypt;
            carmen_point_t g_waypt;
            waypt.x = path.points[path_index].x;
            waypt.y = path.points[path_index].y;
		
            g_waypt = carmen3d_map_map_to_global_coordinates(waypt, nav3d_map);

            if(lcmgl){
			  double pos[3] = {g_waypt.x,g_waypt.y,.0};
                char node_info[512];
                lcmglColor3f(0.0, 0.0,1.0);
                
                lcmglCircle(pos, 0.2);
                
                sprintf(node_info,"[%d] : %f => %f", path_index, bot_to_degrees(heading_diff[ind]), gap); 
                bot_lcmgl_text(lcmgl, pos, node_info);
            }

            path_index++;
        }
        else{
            //fprintf(stderr,"Pruning\n");
            if(lcmgl && 0){//lcmgl && !(cost_along_prev+cost_along_next+cost_threshold < new_cost ||
                //	       min_cost_next < new_min_cost || min_cost_prev < new_min_cost)){
                carmen_point_t waypt;
                carmen_point_t g_waypt;
                waypt.x = path.points[path_index].x;
                waypt.y = path.points[path_index].y;
	  
                g_waypt = carmen3d_map_map_to_global_coordinates(waypt, nav3d_map);
                double pos[3] = {g_waypt.x,g_waypt.y,.0};
                char node_info[512];
                lcmglColor3f(1.0, .0,.0);
                lcmglCircle(pos, 0.2);
                //sprintf(node_info,"[%d] : %f => %f", path_index, bot_to_degrees(heading_diff[ind]), gap); 
                //bot_lcmgl_text(lcmgl, pos, node_info);
            }
	
            carmen3d_planner_util_delete_path_point(path_index, &path);
        }
        ind++;
    }

    free(heading_diff);

    if(lcmgl){
        bot_lcmgl_switch_buffer (lcmgl);  
    }
  
    new_cost = cost_of_path();
}

static void
smooth_path_dense(carmen_navigator_config_t *nav_conf) /*Original pruning function*/
{
    int path_index;
    int new_path_counter;
    double cost_along_prev, cost_along_next;
    double min_cost_prev, min_cost_next;
    double new_cost, new_min_cost;
    double prev_cost;

    prev_cost = cost_of_path();

    new_path_counter = 0;
    path_index = 1;

    while (path.length > 2 && carmen_distance_traj
           (&robot, carmen3d_planner_util_get_path_point(1, &path)) <
           nav_conf->goal_size) {
        carmen3d_planner_util_delete_path_point(1, &path);
    }

    while (path_index < path.length-1)
        {
            compute_cost(path_index-1, path_index, &cost_along_prev, &min_cost_prev);
            compute_cost(path_index, path_index+1, &cost_along_next, &min_cost_next);
            compute_cost(path_index-1, path_index+1, &new_cost, &new_min_cost);

            if (cost_along_prev+cost_along_next+1e-6 < new_cost ||
                min_cost_next < new_min_cost || min_cost_prev < new_min_cost)
                {
                    path_index++;
                }
            else
                {
                    carmen3d_planner_util_delete_path_point(path_index, &path);
                }
        }

    new_cost = cost_of_path();
}


static int find_nearest_free_point_to_goal(void)
{
    carmen_world_point_t goal_world;
    double *util_ptr;
    int x, y;
    double closest_free_dist;
    carmen_map_point_t closest_free;
    double dist;
    int goal_x, goal_y;

    goal_x = carmen_round(robot.x / carmen3d_planner_map->config.resolution);
    goal_y = carmen_round(robot.y / carmen3d_planner_map->config.resolution);

    carmen_conventional_dynamic_program(goal_x, goal_y);

    util_ptr = carmen_conventional_get_utility_ptr();

    if (util_ptr == NULL) {
        carmen_warn("No accessible goal.\n");
        return 0;
    }

    goal_x = carmen_round(requested_goal.x /
                          carmen3d_planner_map->config.resolution);
    goal_y = carmen_round(requested_goal.y /
                          carmen3d_planner_map->config.resolution);

    closest_free_dist = MAXDOUBLE;
    closest_free.map = carmen3d_planner_map;
    for (x = 0; x < carmen3d_planner_map->config.x_size; x++)
        for (y = 0; y < carmen3d_planner_map->config.y_size; y++) {
            dist = hypot(x-goal_x, y-goal_y);
            //ABE: this was segfaulting cuz the utility map size wasn't the same as the regular map... hopefully using the accessor fixes it
            //although this is probably a sign of something worse happening :-/
            double utilVal = carmen_conventional_get_utility(x,y);
            if (utilVal >= 0 && dist < closest_free_dist) {
                closest_free.x = x;
                closest_free.y = y;
                closest_free_dist = dist;
            }
        }

    if (closest_free_dist > MAXDOUBLE/2) {
        carmen_warn("No accessible goal.\n");
        return 0;
    }

    carmen_conventional_dynamic_program(closest_free.x, closest_free.y);

    carmen_map_to_world(&closest_free, &goal_world);
    intermediate_goal.x = goal_world.pose.x;
    intermediate_goal.y = goal_world.pose.y;

    return 1;
}

static void
plan(carmen_navigator_config_t *nav_conf, int force)
{
    static int old_goal_x, old_goal_y;
    static carmen_traj_point_t old_robot;
    static carmen_map_point_t map_pt;
    static double last_plan_time = 0;
    int goal_x, goal_y;

    //fprintf(stderr, "Forced Replan : %d\n", force);

    goal_x = carmen_round(requested_goal.x /
                          carmen3d_planner_map->config.resolution);
    goal_y = carmen_round(requested_goal.y /
                          carmen3d_planner_map->config.resolution);

    //this is getting returned - perhapse because the robot change 
    if(old_goal_x == goal_x && old_goal_y == goal_y){
        if (nav_conf->replan_frequency > 0 && force ==0) {
            if (carmen_get_time() - last_plan_time <
                1.0/nav_conf->replan_frequency) {
                return;
            }
        }
    }

    //fprintf(stderr,"========Planning Path=========\n");
#if VERBOSE
    carmen_verbose("Doing DP to %d %d\n", goal_x, goal_y);
#endif
    carmen_conventional_dynamic_program(goal_x , goal_y);

    carmen_trajectory_to_map(&robot, &map_pt, carmen3d_planner_map);
    /*  if (goal_x < 0 && goal_y < 0){
        have_plan = 0;
        }*/
    if (carmen_conventional_get_utility(map_pt.x, map_pt.y) < 0) {
        //fprintf(stderr,"-------------------Goal not reachable---------------\n");
        goal_is_accessible = 0;
        if (nav_conf->plan_to_nearest_free_point){
            //fprintf(stderr,"Looking for the nearest Free point\n");
            have_plan = find_nearest_free_point_to_goal();
        }
    } else {
        have_plan = 1;
        goal_is_accessible = 1;
    }
    last_plan_time = carmen_get_time();
    old_goal_x = goal_x;
    old_goal_y = goal_y;
    old_robot = robot;
}

static void 
regenerate_trajectory_skip(carmen_navigator_config_t *nav_conf)//skips regenerates at times 
{
    if(carmen3d_planner_map == NULL){
	  return;
    }
    struct timeval start, end;
    int sec, msec;
    int index;
    carmen_traj_point_p path_point;
  
    gettimeofday(&start, NULL);

    int skip_regenerate =0;
  
    skip_regenerate = call_path_extract(nav_conf);

    //fprintf(stderr, " Skip Regenerate %d Gotten a new goal %d \n", skip_regenerate,  new_goal_set);
	
    if (skip_regenerate && new_goal_set==0){

        //fprintf(stderr, " Skipping Path regeneration \n" );

        index = 1;
        while (index < path.length-1) {
            path.points[index].theta = 
                atan2(path.points[index+1].y-path.points[index].y,
                      path.points[index+1].x-path.points[index].x);
            index++;
        }
        if (path.length > 1) {
            if (!goal_is_accessible || allow_any_orientation)
                path.points[path.length-1].theta = 
                    path.points[path.length-2].theta;
            else
                path.points[path.length-1].theta = requested_goal.theta;
        }
    
        /* End of adding path orientations */
    
        gettimeofday(&end, NULL);
        msec = end.tv_usec - start.tv_usec;
        sec = end.tv_sec - start.tv_sec;
        if (msec < 0) {
            msec += 1e6;
            sec--;
        }

    }
	   
    else{    
        new_goal_set = 0;

        if (extract_path_from_value_function() < 0) 
            carmen3d_planner_util_clear_path(&path);

        else{
            //fprintf(stderr,"Regenerating Traj \n");
            //fprintf(stderr,"New path length : %d \n" , path.length);
            //fprintf(stderr, " *** Regenrating Path Distance: %f <  %f Path Clear : %d \n", distance_to_waypoint,nav_conf->waypoint_tolerance, is_path_clear  );
            if (nav_conf->smooth_path)
                smooth_path(nav_conf, NULL);

            /* Add path orientations in */
            index = 1;
            while (index < path.length-1) {
                path.points[index].theta = 
                    atan2(path.points[index+1].y-path.points[index].y,
                          path.points[index+1].x-path.points[index].x);
                index++;
            }
            if (path.length > 1) {
                if (!goal_is_accessible || allow_any_orientation)
                    path.points[path.length-1].theta = 
                        path.points[path.length-2].theta;
                else
                    path.points[path.length-1].theta = requested_goal.theta;
            }
    
            /* End of adding path orientations */
    
            gettimeofday(&end, NULL);
            msec = end.tv_usec - start.tv_usec;
            sec = end.tv_sec - start.tv_sec;
            if (msec < 0) {
                msec += 1e6;
                sec--;
            }
            //	  carmen_verbose("Took %d sec %d msec\n", sec, msec);
            for (index = 0; index < path.length; index++) {
                path_point = carmen3d_planner_util_get_path_point(index, &path);
                carmen_verbose("%.1f %.1f %.1f %.2f\n", path_point->x, 
                               path_point->y, 
                               carmen_radians_to_degrees(path_point->theta), 
                               path_point->t_vel);
            }
        }
    } /* if (extract_path_from_value_function() < 0) ... else ... */
}

//we need to make regenerate trajectory polymorphic somehow
static void
regenerate_trajectory(carmen_navigator_config_t *nav_conf, bot_lcmgl_t *lcmgl)
{
    struct timeval start, end;
    int sec, msec;
    int index;
    carmen_traj_point_p path_point;

    gettimeofday(&start, NULL);
    //fprintf(stderr,"Have Plan : %d\n", have_plan);
  
    if (extract_path_from_value_function() < 0){
        carmen3d_planner_util_clear_path(&path);
        //fprintf(stderr,"Exiting \n");
    }
    else {
        if (nav_conf->smooth_path)
            smooth_path(nav_conf, lcmgl);
        //	  Refine_Path_Using_Velocities();

        map_config_used_for_planning = carmen3d_planner_map->config;
        
        // Add path orientations in - this should not happen here 
        if (path.length >= 1) {
            path.points[path.length-1].theta = requested_goal.theta;
        }
        /*
          index = 1;
          while (index < path.length-1) {
          path.points[index].theta =
          atan2(path.points[index+1].y-path.points[index].y,
          path.points[index+1].x-path.points[index].x);
          index++;
          }
          if (path.length > 1) {
          if (!goal_is_accessible || allow_any_orientation)
          path.points[path.length-1].theta =
	  path.points[path.length-2].theta;
          else
          path.points[path.length-1].theta = requested_goal.theta;
          }
        */
        // End of adding path orientations

        gettimeofday(&end, NULL);
        msec = end.tv_usec - start.tv_usec;
        sec = end.tv_sec - start.tv_sec;
        if (msec < 0) {
            msec += 1e6;
            sec--;
        }
        //	  carmen_verbose("Took %d sec %d msec\n", sec, msec);

        int map_index_x; int map_index_y;
        int last_unexplored_waypt = path.length;
        if (path.length > 1){
            int init_index = 0;
            int found_start_index = 0;
            while (!found_start_index && init_index < path.length){
                path_point = carmen3d_planner_util_get_path_point(init_index, &path);
                map_index_x = (int)(path_point->x/
                                    carmen3d_planner_map->config.resolution);
                map_index_y = (int)(path_point->y/
                                    carmen3d_planner_map->config.resolution);
                if (carmen3d_planner_map->map[map_index_x][map_index_y] <
                    MAP_FREE_VALUE + MAP_EPS){
                    found_start_index = 1;
                }
                init_index++;
            }

            //      fprintf(stderr,"\nInit %d ", init_index);
            for (index = path.length-1; index > init_index-2; index--) {
                path_point = carmen3d_planner_util_get_path_point(index, &path);
                map_index_x = (int)(path_point->x/
                                    carmen3d_planner_map->config.resolution);
                map_index_y = (int)(path_point->y/
                                    carmen3d_planner_map->config.resolution);
                if (carmen3d_planner_map->map[map_index_x][map_index_y] >
                    MAP_UNEXPLORED_VALUE - MAP_EPS)
                    last_unexplored_waypt = index;
                //      fprintf(stderr, "%.2f ", carmen3d_planner_map->map[map_index_x][map_index_y]);
                carmen_verbose("%.1f %.1f %.1f %.2f\n", path_point->x,
                               path_point->y,
                               carmen_radians_to_degrees(path_point->theta),
                               path_point->t_vel);
            }
            path.length= last_unexplored_waypt;
        }
    } // if (extract_path_from_value_function() < 0) ... else ... 
}

int
carmen3d_planner_update_robot(carmen_traj_point_p new_position,
                              carmen_navigator_config_t *nav_conf,
                              carmen_robot_config_t* robot_conf)
{
    static carmen_traj_point_t old_position;
    static int first_time = 1;

    if (!carmen3d_planner_map){
        //fprintf(stderr,"No Planner map yet\n");
	  return 0;
    }

    //fprintf(stderr,"===Robot Loc %f,%f\n", new_position->x, new_position->y);

    if (new_position->x < 0 || new_position->y < 0 ||
        new_position->x >
        carmen3d_planner_map->config.resolution*
        carmen3d_planner_map->config.x_size ||
        new_position->y >
        carmen3d_planner_map->config.resolution*carmen3d_planner_map->config.y_size)
        return 0;

    robot = *new_position;

    //  if (!first_time && carmen_distance_traj(new_position, &old_position) <
    //      carmen3d_planner_map->config.resolution)
    //    return 0;
    //fprintf(stderr,"Regenerating Traj\n");
  
    //***Sachi - this should be taking in the different pruning options 

	fprintf(stderr,"update robot\n");
    regenerate_trajectory(nav_conf, NULL);
  
    //  check_plan_replan_smaller_robot_width(robot_conf, nav_conf);
    old_position = *new_position;

    return 1;
}

int
carmen3d_planner_update_goal(carmen_point_p new_goal, int any_orientation,
			     carmen_navigator_config_t *nav_conf, carmen_robot_config_t* robot_conf, int force_replan, bot_lcmgl_t *lcmgl)
{
    if (!carmen3d_planner_map)
        return 0;

    carmen_verbose("Set Goal to X: %f Y: %f (Max allowable %d %d)\n",
                   new_goal->x, new_goal->y, carmen3d_planner_map->config.x_size,
                   carmen3d_planner_map->config.y_size);

    requested_goal = *new_goal;
    allow_any_orientation = any_orientation;
    goal_set = 1;
    new_goal_set = 1;
    plan(nav_conf, force_replan);
	fprintf(stderr,"update goal\n");
    regenerate_trajectory(nav_conf, lcmgl);
  
    //  check_plan_replan_smaller_robot_width(robot_conf, nav_conf);
    return 1;
}

void  carmen3d_planner_clear_goal(carmen_navigator_config_t *nav_conf){
    if (!carmen3d_planner_map)
        return;

    have_plan = 0;
    regenerate_trajectory(nav_conf, NULL);


}

void carmen3d_planner_update_grid(carmen_map_p new_map,
                                  carmen_traj_point_p new_position,
                                  carmen_robot_config_t *robot_conf,
                                  carmen_navigator_config_t *nav_conf)
{
    //fprintf(stderr,"Updating Grid\n");
    carmen_world_point_t world_point;

    carmen_map_point_t map_point;
    carmen3d_planner_map = new_map;

    if (true_map != NULL)
        carmen_map_destroy(&true_map);

    true_map = carmen_map_copy(carmen3d_planner_map);

    world_point.pose.x = new_position->x;
    world_point.pose.y = new_position->y;
    world_point.pose.theta = new_position->theta;
    world_point.map = carmen3d_planner_map;

    carmen_world_to_map(&world_point, &map_point);

    carmen_conventional_build_costs(robot_conf, &map_point, nav_conf);

    if (!goal_set)
        return;

    plan(nav_conf, 0);

	fprintf(stderr,"update grid\n");
    regenerate_trajectory(nav_conf, NULL);
    //  check_plan_replan_smaller_robot_width(robot_conf, nav_conf);
}

float* get_planner_map(){
    return carmen3d_planner_map->complete_map;//NULL;//carmen3d_planner_map;
}

void
carmen3d_planner_set_map(carmen_map_p new_map, carmen_robot_config_t *robot_conf)
{
    have_plan = 0;
    //fprintf(stderr,"Set Map\n");
    carmen3d_planner_map = new_map;

    if (true_map != NULL)
        carmen_map_destroy(&true_map);

    true_map = carmen_map_copy(carmen3d_planner_map);

    map_modify_clear(true_map, carmen3d_planner_map);
    carmen_conventional_build_costs(robot_conf, NULL, NULL);
}


void
carmen3d_planner_reset_map(carmen_robot_config_t *robot_conf)
{
    map_modify_clear(true_map, carmen3d_planner_map);
    carmen_conventional_build_costs(robot_conf, NULL, NULL);
}


void
carmen3d_planner_update_robot_map(double x, double y, double theta,  
				  carmen_navigator_config_t *nav_conf,
				  carmen_robot_config_t *robot_conf)
//carmen_world_point_t* robot_pos,//double robot_x, double robot_y,
{
    if (carmen3d_planner_map == NULL)
        return;

    carmen_world_point_t world_point;
    world_point.pose.x = x;
    world_point.pose.y = y;
    world_point.pose.theta = theta;
    world_point.map = carmen3d_planner_map;
  
    //carmen_world_point_t world_point;
    map_modify_update_robot(nav_conf, &world_point, true_map, carmen3d_planner_map);
}

void
carmen3d_planner_update_map(carmen_robot_laser_message *front_laser_msg,
			    carmen_robot_laser_message *rear_laser_msg,
                            erlcm_rect_list_t *rects,
                            carmen_point_p simrect_center, 
			    carmen_navigator_config_t *nav_conf,
			    carmen_robot_config_t *robot_conf, 
			    carmen_point_t robot_global_point)//, lcm_t *lcm, erlcm_map_p nav3d_map)
{
    carmen_world_point_t world_point;
    carmen_map_point_t map_point;

    //fprintf(stderr,"Called\n");

    if (carmen3d_planner_map == NULL)
        return;

    if(front_laser_msg != NULL){
        world_point.pose.x = front_laser_msg->laser_pose.x;
        world_point.pose.y = front_laser_msg->laser_pose.y;
        world_point.pose.theta = front_laser_msg->laser_pose.theta;
#if VERBOSE
        fprintf(stderr," World Pose - Front Laser : %f,%f -> %f\n", world_point.pose.x, 
                world_point.pose.y, world_point.pose.theta);
#endif        
        world_point.map = carmen3d_planner_map;
        
        map_modify_update(front_laser_msg, nav_conf, &world_point, true_map, carmen3d_planner_map, 1, 50.0);//, lcm, nav3d_map);
    }

    if(rear_laser_msg !=NULL){

        //fprintf(stderr,"Updating Rear Obs\n");
        world_point.pose.x = rear_laser_msg->laser_pose.x;
        world_point.pose.y = rear_laser_msg->laser_pose.y;
        world_point.pose.theta = rear_laser_msg->laser_pose.theta;

#if VERBOSE
        fprintf(stderr,"Rear Laser %f,%f,%f\n",  world_point.pose.x,  world_point.pose.y,  world_point.pose.theta);
#endif
    
        map_modify_update(rear_laser_msg, nav_conf, &world_point, true_map, carmen3d_planner_map, 0, 50.0);//, lcm, nav3d_map);
    }

    //simrects 

    if(rects !=NULL){
        //simrects are in the global frame 
        map_modify_update_simrect(rects, simrect_center, nav_conf, true_map, carmen3d_planner_map);        
    }
    else{
        //fprintf(stderr, "No Sim rects"); 
    }
    world_point.pose.x = robot_global_point.x;
    world_point.pose.y = robot_global_point.y;
    world_point.pose.theta = robot_global_point.theta;

    //this should be the robot position not the laser position
    carmen_world_to_map(&world_point, &map_point);

    //this is where the cost is rebuilt -- seems to be an issue with how the map is rebuilt
    //map point here is not the robot pos 

    carmen_conventional_build_costs(robot_conf, &map_point, nav_conf);

    if (!goal_set)
        return;

    plan(nav_conf, 0);

	fprintf(stderr,"update map\n");
    regenerate_trajectory(nav_conf, NULL);
    //  check_plan_replan_smaller_robot_width(robot_conf, nav_conf);
}

void
carmen3d_planner_get_status(carmen3d_planner_status_p status)
{
    int index;

    status->goal = requested_goal;
    status->robot = robot;
    status->goal_set = goal_set;
    status->path.length = path.length;
    status->planned_map_config = map_config_used_for_planning;
    if (status->path.length > 0) {
        status->path.points = (carmen_traj_point_p)
            calloc(status->path.length, sizeof(carmen_traj_point_t));
        carmen_test_alloc(status->path.points);
        for (index = 0; index < status->path.length; index++)
            status->path.points[index] = path.points[index];
    } else  {
        status->path.points = NULL;
    }
    return;
}

// Add a check for planners with goals-- if we're just tracking'
// then there is no goal

int
carmen3d_planner_next_waypoint(carmen_traj_point_p waypoint,
                               int* waypoint_index,
                               int *is_goal,
                               carmen_navigator_config_t *nav_conf)
{
    carmen_traj_point_p point;
    int next_point;
    double delta_dist, delta_theta;

    regenerate_trajectory(nav_conf, NULL);
    //  check_plan_replan_smaller_robot_width(robot_conf, nav_conf);

    carmen_verbose("Finding waypoint: %.0f %.0f %.0f\n",
                   waypoint->x, waypoint->y,
                   carmen_radians_to_degrees(waypoint->theta));

    if (path.length <= 1)
        return -1;

    next_point = 0;
    do {
        next_point++;
        point = carmen3d_planner_util_get_path_point(next_point, &path);

        if (path.length - next_point < 2)
            break;
        delta_dist = carmen_distance_traj(waypoint, point);
    } while (delta_dist < nav_conf->waypoint_tolerance);

    delta_dist = carmen_distance_traj(waypoint, point);

    if (delta_dist < nav_conf->goal_size && path.length - next_point == 1) {
        if (allow_any_orientation || !goal_is_accessible)
            return 1;
        delta_theta = fabs(waypoint->theta - requested_goal.theta);
        if (delta_theta < nav_conf->goal_theta_tolerance)
            return 1;
    }

    waypoint_act.x = point->x;
    waypoint_act.y = point->y;
    waypoint_act.theta = point->theta;
    waypoint_act.t_vel = point->t_vel;
    waypoint_act.r_vel = point->r_vel;
  
    //fprintf(stderr, " Waypoint being updated (%f,%f) \n",waypoint_act.x, waypoint_act.y );

    if (path.length <= 2)
        *is_goal = 1;
    else
        *is_goal = 0;

    *waypoint = *point;
    *waypoint_index = next_point;

    //fprintf(stderr,"Waypoint is: %.0f %.0f %.0f (goal %d)\n",
    //		 waypoint->x, waypoint->y,
    //		 carmen_radians_to_degrees(waypoint->theta), *is_goal);

    return 0;
}

//carmen_navigator_map_message *
carmen3d_navigator3d_map_message *
carmen3d_planner_get_map_message(carmen_navigator_map_t map_type)
{
  carmen3d_navigator3d_map_message *reply;
    int size, x_Size, y_Size;
    float *map_ptr;
    double *dbl_ptr;

    reply = (carmen3d_navigator3d_map_message *)
        calloc(1, sizeof(carmen3d_navigator3d_map_message));
    carmen_test_alloc(reply);

    x_Size = carmen3d_planner_map->config.x_size;
    y_Size = carmen3d_planner_map->config.y_size;
    size = x_Size * y_Size;

    reply->data = (unsigned char *)calloc(size, sizeof(float));
    carmen_test_alloc(reply->data);

    reply->config = carmen3d_planner_map->config;

    switch (map_type) {
    case CARMEN_NAVIGATOR_MAP_v:
        if (true_map)
            map_ptr = true_map->complete_map;
        else
            map_ptr = carmen3d_planner_map->complete_map;
        break;
    case CARMEN_NAVIGATOR_COST_v:
        dbl_ptr = carmen_conventional_get_costs_ptr();
        if (dbl_ptr == NULL) {
            reply->size = 0;
            reply->map_type = -1;
            return reply;
        }
        {
            int index;
            float *mp;
            double *db_p;
            map_ptr = (float *)calloc(size, sizeof(float));
            carmen_test_alloc(map_ptr);
            mp = map_ptr;
            db_p = dbl_ptr;
            for (index = 0; index < size; index++)
                *(mp++) = (float)*(dbl_ptr++);
        }
        break;
    case CARMEN_NAVIGATOR_UTILITY_v:
        dbl_ptr = carmen_conventional_get_utility_ptr();
        if (dbl_ptr == NULL) {
            reply->size = 0;
            reply->map_type = -1;
            return reply;
        }
        {
            int index;
            float *mp;
            double *db_p;
            map_ptr = (float *)calloc(size, sizeof(float));
            carmen_test_alloc(map_ptr);
            mp = map_ptr;
            db_p = dbl_ptr;
            for (index = 0; index < size; index++)
                *(mp++) = (float)*(dbl_ptr++);
        }
        break;
    default:
	  carmen_warn("Request for unsupported data type : %d.\n", map_type);
        reply->size = 0;
        reply->map_type = -1;
        return reply;
    }
    reply->size = size*sizeof(float);
    reply->map_type = map_type;
    memcpy(reply->data, map_ptr, size*sizeof(float));
    if (map_type == CARMEN_NAVIGATOR_UTILITY_v)
        free(map_ptr);
    return reply;
}

double *carmen3d_planner_get_utility(void)
{
    return carmen_conventional_get_utility_ptr();
}

int carmen3d_planner_goal_reachable(void)
{
    if (goal_set && goal_is_accessible)
        return 1;

    return 0;
}

double carmen3d_planner_cost_of_current_path()
{
    if( carmen3d_planner_goal_reachable() == 0 )
        return LONG_MAX;
    double c = cost_of_path();
    if( c <= 0 )
        return LONG_MAX;
    return c;
}


//compute the cost between the goal and a list of locations
double* carmen3d_planner_compute_path_costs(const point2d_t* locs, int nlocs,
					    const point2d_t goal,
					    carmen_navigator_config_t* nav_conf,
					    carmen_robot_config_t* robot_conf,
					    double cost_for_no_path )
{
    //locs and goal should be in map coordinates 
    if( carmen3d_planner_map == NULL ){
	  fprintf(stderr,"No Map\n");
        return (double*)calloc( nlocs, sizeof(double) );
    
    }
    // now create the return array
    double *cost_array = (double*)calloc( nlocs, sizeof(double) );
    carmen_test_alloc( cost_array );
  
    // precheck for freespace
    if( carmen_conventional_is_free_space( goal.x, goal.y ) == 0 ) {
        fprintf(stderr,"Portal Not in free space\n");
        // just return max cost since no path
        for( int i = 0; i < nlocs; ++i ) {
            cost_array[i] = cost_for_no_path;
        }
        return cost_array;
    }

    // precheck if only one location
    if( nlocs == 1 ) {
        if( carmen_conventional_is_free_space( locs[0].x, locs[0].y ) == 0 ) {
            fprintf(stderr,"Goal Not in free space\n");
            cost_array[0] = cost_for_no_path;
            return cost_array;
        }
    }

    double res = carmen3d_planner_map->config.resolution;

    // save the old goal and robot position
    carmen_point_t saved_goal = requested_goal;
    int saved_allow_any_orientation = allow_any_orientation;
    carmen_traj_point_t saved_robot = robot;

    // clear the internal state ( if any )
    carmen3d_planner_clear_goal( nav_conf );
    //carmen_planner_reset_map( robot_conf );
    carmen3d_planner_util_clear_path(&path);
  
    // set the goal
    carmen_point_t new_goal;
    new_goal.x = goal.x;
    new_goal.y = goal.y;
    new_goal.theta = saved_goal.theta;

    //force a replan 
    carmen3d_planner_update_goal( &new_goal, 0, nav_conf, robot_conf, 1, NULL);
  
    // Ok, go through every location and compute path to it
    // then store the cost
    for( int i = 0; i < nlocs; ++i ) {
        carmen_traj_point_t new_robot = saved_robot;
        //these locs are the non-elevator goals - the goal here is the elevator or portal 
        //we require the cost to each same floor location from the elevator 
        new_robot.x = locs[i].x; 
        new_robot.y = locs[i].y; 

        fprintf(stderr,"Cost Robot Loc %f,%f\n", new_robot.x, new_robot.y);
        int counter= 0;
        int retry_max = 1;
        while( counter < retry_max ) {
            ++counter;
            carmen3d_planner_update_robot( &new_robot, nav_conf, robot_conf );
            //this update goal is needed to force a replan 
            carmen3d_planner_update_goal( &new_goal, 0, nav_conf, robot_conf, 1, NULL);
            double path_cost = cost_of_path();
            if( path_cost <= 0 || carmen_conventional_is_free_space( locs[i].x, locs[i].y ) == 0 || goal_is_accessible == 0 ) {
                if( 0 ) {
                    fprintf( stderr, "  -- NO PATH between (%f %f ) and (%f %f) \n",
                             new_robot.x, new_robot.y, new_goal.x, new_goal.y );
                }
                fprintf(stderr,"No Path : %f\n", path_cost);
                path_cost = cost_for_no_path;
            } else {
                counter = retry_max;
            }
            cost_array[ i ] = path_cost;
        }
    }

    // ok, now restore hte original goal nad robot 
    carmen3d_planner_update_robot( &saved_robot, nav_conf, robot_conf );

    //need to be careful that we actually have a goal 
    carmen3d_planner_update_goal( &saved_goal, saved_allow_any_orientation, 
                                  nav_conf, robot_conf, 1, NULL);
  

    // return the costs
    return cost_array;

}


//compute the cost between the goal and a list of locations
double* carmen3d_planner_compute_path_costs_to_goal(const point2d_t* locs, int nlocs,
                                                    const point2d_t goal,
                                                    const point2d_t robot_position,
                                                    carmen_navigator_config_t* nav_conf,
                                                    carmen_robot_config_t* robot_conf,
                                                    double cost_for_no_path )

{
    //locs and goal should be in map coordinates 
    if( carmen3d_planner_map == NULL ){
        fprintf(stderr,"No Map\n");
        return (double*)calloc( nlocs, sizeof(double) );
    
    }
    
    // now create the return array
    double *cost_array = (double*)calloc( nlocs, sizeof(double) );
    carmen_test_alloc( cost_array );
  
    // precheck for freespace
    if( carmen_conventional_is_free_space( goal.x, goal.y ) == 0 ) {
        fprintf(stderr,"Portal Not in free space\n");
        // just return max cost since no path
        for( int i = 0; i < nlocs; ++i ) {
            cost_array[i] = cost_for_no_path;
        }
        return cost_array;
    }

    // precheck if only one location
    if( nlocs == 1 ) {
        if( carmen_conventional_is_free_space( locs[0].x, locs[0].y ) == 0 ) {
            fprintf(stderr,"Goal Not in free space\n");
            cost_array[0] = cost_for_no_path;
            return cost_array;
        }
    }

    double res = carmen3d_planner_map->config.resolution;

    // save the old goal and robot position
    carmen_point_t saved_goal = requested_goal;
    int saved_allow_any_orientation = allow_any_orientation;
    carmen_traj_point_t saved_robot = robot;

    // clear the internal state ( if any )
    carmen3d_planner_clear_goal( nav_conf );
    //carmen_planner_reset_map( robot_conf );
    carmen3d_planner_util_clear_path(&path);

    // set the goal
    carmen_point_t new_goal;
    new_goal.x = goal.x;
    new_goal.y = goal.y;
    new_goal.theta = saved_goal.theta;

    //force a replan 
    carmen3d_planner_update_goal( &new_goal, 0, nav_conf, robot_conf, 1, NULL);
  
    // Ok, go through every location and compute path to it
    // then store the cost
    for( int i = 0; i < nlocs; ++i ) {
        carmen_traj_point_t new_robot = saved_robot;
        //these locs are the non-elevator goals - the goal here is the elevator or portal 
        //we require the cost to each same floor location from the elevator 
        new_robot.x = locs[i].x; 
        new_robot.y = locs[i].y; 

        fprintf(stderr,"Cost Robot Loc %f,%f\n", new_robot.x, new_robot.y);
        int counter= 0;
        int retry_max = 1;
        while( counter < retry_max ) {
            ++counter;
            carmen3d_planner_update_robot( &new_robot, nav_conf, robot_conf );
            //this update goal is needed to force a replan 
            carmen3d_planner_update_goal( &new_goal, 0, nav_conf, robot_conf, 1, NULL);
            double path_cost = cost_of_path();
            if( path_cost <= 0 || carmen_conventional_is_free_space( locs[i].x, locs[i].y ) == 0 || goal_is_accessible == 0 ) {
                if( 0 ) {
                    fprintf( stderr, "  -- NO PATH between (%f %f ) and (%f %f) \n",
                             new_robot.x, new_robot.y, new_goal.x, new_goal.y );
                }
                fprintf(stderr,"No Path : %f\n", path_cost);
                path_cost = cost_for_no_path;
            } else {
                counter = retry_max;
            }
            cost_array[ i ] = path_cost;
        }
    }

    // ok, now restore hte original goal nad robot 
    carmen3d_planner_update_robot( &saved_robot, nav_conf, robot_conf );

    //need to be careful that we actually have a goal 
    carmen3d_planner_update_goal( &saved_goal, saved_allow_any_orientation, 
                                  nav_conf, robot_conf, 1, NULL);
  

    // return the costs
    return cost_array;

    
    
}


//compute the cost between the elevator and a list of locations
//assuming we are given the closest elevator 
double carmen3d_planner_compute_elevator_path_costs_and_portals(carmen3d_portal_list_t portals, 
                                                       carmen3d_portal_t *elevator,
                                                       carmen_navigator_config_t* nav_conf,
                                                       carmen_robot_config_t* robot_conf,
                                                       double cost_for_no_path , 
                                                       bot_lcmgl_t *lcmgl)
{
    //assuming that all portals are of the current floor 

    //locs and goal should be in map coordinates 
    if( carmen3d_planner_map == NULL ){        
        fprintf(stderr,"No Map\n");
        return cost_for_no_path;
    }

    //we need to check which elevator point is closest - //or we can assume that the end point is the one in the elevator - lets make the assumption that the second point is the one inside the elevator - for now

    point2d_t elevator_point = {elevator->portal_points[0].x, elevator->portal_points[0].y};

    
    // precheck for freespace
    if( carmen_conventional_is_free_space( elevator_point.x, elevator_point.y ) == 0 ) {
        fprintf(stderr, "Error : Elevator is not in free space\n");
        //this should not happen 
        return cost_for_no_path;
    }    

    double res = carmen3d_planner_map->config.resolution;

    // save the old goal and robot position
    carmen_point_t saved_goal = requested_goal;
    int saved_allow_any_orientation = allow_any_orientation;
    carmen_traj_point_t saved_robot = robot;

    // clear the internal state ( if any )
    carmen3d_planner_clear_goal( nav_conf );
    //carmen_planner_reset_map( robot_conf );
    carmen3d_planner_util_clear_path(&path);
  
    // set the goal
    carmen_point_t new_goal;
    new_goal.x = elevator_point.x;
    new_goal.y = elevator_point.y;
    new_goal.theta = 0;//goal.theta;

    carmen_navigator_config_t temp_nav_config;
    
    memcpy(&temp_nav_config, nav_conf, sizeof(carmen_navigator_config_t));
    temp_nav_config.smooth_path = 0;

    carmen_traj_point_t new_robot = saved_robot;
    carmen3d_planner_update_robot( &new_robot, &temp_nav_config , robot_conf );

    int counter= 0;
    int retry_max = 1;
    double path_cost = cost_for_no_path;
    while( counter < retry_max ) {
        ++counter;
        //force a replan 
        carmen3d_planner_update_goal( &new_goal, 0, &temp_nav_config, robot_conf, 1, NULL);

        path_cost = cost_of_path();
        if( path_cost <= 0 || goal_is_accessible == 0 ) {
            if( 0 ) {
                fprintf( stderr, "  -- NO PATH between (%f %f ) and (%f %f) \n",
                         new_robot.x, new_robot.y, new_goal.x, new_goal.y );
            }
            fprintf(stderr,"No Path : %f\n", path_cost);
            path_cost = cost_for_no_path;
        }
    }

    

    //now check for portals that the path goes through 
    fprintf(stderr, " ----- Checking Portals in path \n");
    
    lcmglColor3f(0.0, 0.0,1.0);
    check_portals_in_path(portals, lcmgl);
        

    // ok, now restore hte original goal nad robot 
    carmen3d_planner_update_robot( &saved_robot, nav_conf, robot_conf );

    //need to be careful that we actually have a goal 
    carmen3d_planner_update_goal( &saved_goal, saved_allow_any_orientation, 
                                  nav_conf, robot_conf, 1, NULL);

    
    fprintf(stderr, "--------- Path Cost : %f\n", path_cost);
    // return the costs
    return path_cost;

}


//compute the cost between the elevator and a list of locations
//assuming we are given the closest elevator 
double carmen3d_planner_compute_path_costs_and_portals_from_elevator(carmen3d_portal_list_t portals, 
                                                                     const point2d_t goal,
                                                                     carmen3d_portal_t *elevator,
                                                                     carmen_navigator_config_t* nav_conf,
                                                                     carmen_robot_config_t* robot_conf,
                                                                     double cost_for_no_path , 
                                                                     bot_lcmgl_t *lcmgl)
{
    //assuming that all portals are of the current floor 
    
    //locs and goal should be in map coordinates 
    if( carmen3d_planner_map == NULL ){        
        fprintf(stderr,"No Map\n");
        return cost_for_no_path;
    }
    
    //we need to check which elevator point is closest - //or we can assume that the end point is the one in the elevator - lets make the assumption that the second point is the one inside the elevator - for now
    
    //considering from the exit node of the elevator 
    
    point2d_t elevator_point = {elevator->portal_points[1].x, elevator->portal_points[1].y};
    
    
    // precheck for freespace
    if( carmen_conventional_is_free_space( elevator_point.x, elevator_point.y ) == 0 ) {
        fprintf(stderr, "Error : Elevator is not in free space\n");
        //this should not happen 
        return cost_for_no_path;
    }    

    double res = carmen3d_planner_map->config.resolution;

    // save the old goal and robot position
    carmen_point_t saved_goal = requested_goal;
    int saved_allow_any_orientation = allow_any_orientation;
    carmen_traj_point_t saved_robot = robot;

    // clear the internal state ( if any )
    carmen3d_planner_clear_goal( nav_conf );
    //carmen_planner_reset_map( robot_conf );
    carmen3d_planner_util_clear_path(&path);
  
    // set the goal
    carmen_point_t new_goal;
    new_goal.x = goal.x;
    new_goal.y = goal.y; 
    new_goal.theta = 0;

    carmen_navigator_config_t temp_nav_config;
    
    memcpy(&temp_nav_config, nav_conf, sizeof(carmen_navigator_config_t));
    temp_nav_config.smooth_path = 0;

    carmen_traj_point_t new_robot;
    new_robot.x = elevator_point.x;
    new_robot.y = elevator_point.y;
    
    carmen3d_planner_update_robot( &new_robot, &temp_nav_config , robot_conf );

    int counter= 0;
    int retry_max = 1;
    double path_cost = cost_for_no_path;
    while( counter < retry_max ) {
        ++counter;
        //force a replan 
        carmen3d_planner_update_goal( &new_goal, 0, &temp_nav_config, robot_conf, 1, NULL);

        path_cost = cost_of_path();
        if( path_cost <= 0 || goal_is_accessible == 0 ) {
            if( 0 ) {
                fprintf( stderr, "  -- NO PATH between (%f %f ) and (%f %f) \n",
                         new_robot.x, new_robot.y, new_goal.x, new_goal.y );
            }
            fprintf(stderr,"No Path : %f\n", path_cost);
            path_cost = cost_for_no_path;
        }
    }

    

    //now check for portals that the path goes through 
    fprintf(stderr, " ----- Checking Portals in path \n");
    
    lcmglColor3f(1.0, 0.0,1.0);
    check_portals_in_path(portals, lcmgl);
        

    // ok, now restore hte original goal nad robot 
    carmen3d_planner_update_robot( &saved_robot, nav_conf, robot_conf );

    //need to be careful that we actually have a goal 
    carmen3d_planner_update_goal( &saved_goal, saved_allow_any_orientation, 
                                  nav_conf, robot_conf, 1, NULL);

    
    fprintf(stderr, "--------- Path Cost : %f\n", path_cost);
    // return the costs
    return path_cost;

}


//compute the cost between the goal and a list of locations
double carmen3d_planner_compute_path_costs_and_portals(carmen3d_portal_list_t portals, 
                                                       const point2d_t goal,
                                                       carmen_navigator_config_t* nav_conf,
                                                       carmen_robot_config_t* robot_conf,
                                                       double cost_for_no_path , 
                                                       bot_lcmgl_t *lcmgl)
{
    //assuming that all portals are of the current floor 

    //locs and goal should be in map coordinates 
    if( carmen3d_planner_map == NULL ){        
        fprintf(stderr,"No Map\n");
        return cost_for_no_path;
    }
    
    // precheck for freespace
    if( carmen_conventional_is_free_space( goal.x, goal.y ) == 0 ) {
        fprintf(stderr, "Goal is not in free space\n");
        return cost_for_no_path;
    }    

    double res = carmen3d_planner_map->config.resolution;

    // save the old goal and robot position
    carmen_point_t saved_goal = requested_goal;
    int saved_allow_any_orientation = allow_any_orientation;
    carmen_traj_point_t saved_robot = robot;

    // clear the internal state ( if any )
    carmen3d_planner_clear_goal( nav_conf );
    //carmen_planner_reset_map( robot_conf );
    carmen3d_planner_util_clear_path(&path);
  
    // set the goal
    carmen_point_t new_goal;
    new_goal.x = goal.x;
    new_goal.y = goal.y;
    new_goal.theta = 0;//goal.theta;

    carmen_navigator_config_t temp_nav_config;
    
    memcpy(&temp_nav_config, nav_conf, sizeof(carmen_navigator_config_t));
    temp_nav_config.smooth_path = 0;

    carmen_traj_point_t new_robot = saved_robot;
    carmen3d_planner_update_robot( &new_robot, &temp_nav_config , robot_conf );

    int counter= 0;
    int retry_max = 1;
    double path_cost = cost_for_no_path;
    while( counter < retry_max ) {
        ++counter;
        //force a replan 
        carmen3d_planner_update_goal( &new_goal, 0, &temp_nav_config, robot_conf, 1, NULL);

        path_cost = cost_of_path();
        if( path_cost <= 0 || goal_is_accessible == 0 ) {
            if( 0 ) {
                fprintf( stderr, "  -- NO PATH between (%f %f ) and (%f %f) \n",
                         new_robot.x, new_robot.y, new_goal.x, new_goal.y );
            }
            fprintf(stderr,"No Path : %f\n", path_cost);
            path_cost = cost_for_no_path;
        }
    }

    

    //now check for portals that the path goes through 
    fprintf(stderr, " ----- Checking Portals in path \n");
    
    lcmglColor3f(0.0, 0.0,1.0);
    check_portals_in_path(portals, lcmgl);
        

    // ok, now restore hte original goal nad robot 
    carmen3d_planner_update_robot( &saved_robot, nav_conf, robot_conf );

    //need to be careful that we actually have a goal 
    carmen3d_planner_update_goal( &saved_goal, saved_allow_any_orientation, 
                                  nav_conf, robot_conf, 1, NULL);

    
    fprintf(stderr, "--------- Path Cost : %f\n", path_cost);
    // return the costs
    return path_cost;

}
