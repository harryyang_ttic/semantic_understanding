/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover *
 * CARMEN is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <er_carmen/carmen.h>

#include "map3d_modify.h"

//#include <common3d/lcm_utils.h>
//#include <common3d/agile-globals.h>
//#include <interfaces/map3d_interface.h>
//#include <common3d/carmen3d_common.h>

#define LASER_HISTORY_LENGTH 10//5

#define EMPTY 0.01
#define FILLED .9
#define UNKNOWN -1

typedef struct {
    int x, y;
    double value;
} grid_cell_t, *grid_cell_p;

static grid_cell_p *laser_scan;
static int *scan_size;
static int *max_scan_size;
static int current_data_set = 0;

static grid_cell_p *kinect_scan;
static int *k_scan_size;
static int *k_max_scan_size;
static int k_current_data_set = 0;

static grid_cell_p *r_laser_scan;
static int *r_scan_size;
static int *r_max_scan_size;
static int r_current_data_set = 0;

carmen_inline static int
is_empty(double value)
{
    return (value >= 0) && (value <= EMPTY);
}

carmen_inline static int
is_filled(double value)
{
    return (value >= 0) && (value >= FILLED);
}

carmen_inline static int
is_in_map(int x, int y, carmen_map_p map)
{
    if (x < 0 || x >= map->config.x_size)
        return 0;
    if (y < 0 || y >= map->config.y_size)
        return 0;
    return 1;
}

static int
point_exists(int x, int y, double value)
{
    int num_points = scan_size[current_data_set];
    grid_cell_p cell = laser_scan[current_data_set];
    int index;

    if (num_points == 0)
        return 0;

    for (index = num_points-1; index >= 0; index--) {
        if (x == cell[index].x && y == cell[index].y &&
            fabs(value - cell[index].value) < .01)
            return 1;
    }
    return 0;
}

static void
add_filled_point(int x, int y, carmen_map_p true_map, carmen_map_p modify_map, int *scan_size, 
		 int *max_scan_size, int current_data_set, grid_cell_p *laser_scan)
{
    int num_points;
    grid_cell_p cell_list;
    double value;

    if (!is_in_map(x, y, modify_map))
        return;

    value = true_map->map[x][y];
    if (is_filled(value))
        return;

    /*   if (point_exists(x, y, FILLED)) */
    /*     return; */

    num_points = scan_size[current_data_set];
    if (num_points == max_scan_size[current_data_set]) {
        max_scan_size[current_data_set] *= 2;
        laser_scan[current_data_set] =
            realloc(laser_scan[current_data_set],
                    max_scan_size[current_data_set]*sizeof(grid_cell_t));
        carmen_test_alloc(laser_scan[current_data_set]);
    }

    cell_list = laser_scan[current_data_set];
    value = FILLED;
    /*      carmen_warn("Filling point %d (%d) %d %d\n", num_points, */
    /*  	 current_data_set, x, y); */
    cell_list[num_points].x = x;
    cell_list[num_points].y = y;
    cell_list[num_points].value = value;
    scan_size[current_data_set]++;
    modify_map->map[x][y] = 1.0;
}

static void
add_clear_robot_point(int x, int y, carmen_map_p true_map, carmen_map_p modify_map)
{
  
}

static void
add_clear_point(int x, int y, carmen_map_p true_map, carmen_map_p modify_map, int *scan_size, 
                int *max_scan_size, int current_data_set, grid_cell_p *laser_scan)
{
    int num_points = scan_size[current_data_set];
    grid_cell_p cell_list;
    double value;

    if (!is_in_map(x, y, modify_map))
        return;

    value = true_map->map[x][y];

    if (num_points == max_scan_size[current_data_set]) {
        max_scan_size[current_data_set] *= 2;
        laser_scan[current_data_set] =
            realloc(laser_scan[current_data_set],
                    max_scan_size[current_data_set]*sizeof(grid_cell_t));
        carmen_test_alloc(laser_scan[current_data_set]);
    }

    cell_list = laser_scan[current_data_set];
    value = EMPTY;
    cell_list[num_points].x = x;
    cell_list[num_points].y = y;
    cell_list[num_points].value = value;
    scan_size[current_data_set]++;
    modify_map->map[x][y] = value;
}

static void
update_existing_data(carmen_map_p true_map, carmen_map_p modify_map, int *scan_size, 
                     int *max_scan_size, int *current_data_set, grid_cell_p *laser_scan)
{
    grid_cell_p cell;
    int num_changed_points;
    int index;
    double value;
    int list_index;

    *current_data_set = (*current_data_set+1) % LASER_HISTORY_LENGTH;

    if (laser_scan[*current_data_set] == NULL) {
        laser_scan[*current_data_set] =
            (grid_cell_p)calloc(200, sizeof(grid_cell_t));
        carmen_test_alloc(laser_scan[*current_data_set]);
        max_scan_size[*current_data_set] = 200;
        scan_size[*current_data_set] = 0;
    }

    /* The oldest scan is erased here */
    cell = laser_scan[*current_data_set];

    num_changed_points = scan_size[*current_data_set];
    cell = laser_scan[*current_data_set];

    //reset the modified map - for the oldest scan 
    for (index = 0; index < num_changed_points; index++) {
        value = true_map->map[cell->x][cell->y];
        modify_map->map[cell->x][cell->y] = value;
        cell++;
    }

    scan_size[*current_data_set] = 0;

    /* The other scans are recopied in here */

    for (list_index = 0; list_index < LASER_HISTORY_LENGTH; list_index++) {
        cell = laser_scan[list_index];
        if (cell == NULL)
            continue;
        num_changed_points = scan_size[list_index];
        for (index = 0; index < num_changed_points; index++) {
            modify_map->map[cell->x][cell->y] = cell->value;
            /*        carmen_warn("Filling point %d of %d in list %d: %d %d\n", index, */
            /*  	   num_changed_points, list_index, cell->x, cell->y); */
            cell++;
        }
    }
}

void
trace_laser(int x_1, int y_1, int x_2, int y_2, carmen_map_p true_map,
	    carmen_map_p modify_map, int *scan_size, 
            int *max_scan_size, int current_data_set, grid_cell_p *laser_scan)
{
    carmen_bresenham_param_t params;
    int X, Y;
    double true_map_value;
    double modified_map_value;

    carmen_get_bresenham_parameters(x_1, y_1, x_2, y_2, &params);

    do {
        carmen_get_current_point(&params, &X, &Y);
        if (!is_in_map(X, Y, modify_map))
            break;
        true_map_value = true_map->map[X][Y];
        modified_map_value = modify_map->map[X][Y];
        //if (!is_empty(modified_map_value))
        add_clear_point(X, Y, true_map, modify_map, scan_size, max_scan_size, current_data_set, laser_scan);
        /*if (!is_empty(modified_map_value)  &&   is_empty(true_map_value))
          add_clear_point(X, Y, true_map, modify_map);*/
    } while (carmen_get_next_point(&params));
}

void
fill_line(int x_1, int y_1, int x_2, int y_2, carmen_map_p true_map,
          carmen_map_p modify_map)
{
    //    fprintf(stderr, 

    carmen_bresenham_param_t params;
    int X, Y;
    double true_map_value;
    double modified_map_value;

    carmen_get_bresenham_parameters(x_1, y_1, x_2, y_2, &params);

    do {
        carmen_get_current_point(&params, &X, &Y);
        if (!is_in_map(X, Y, modify_map))
            break;
        true_map_value = true_map->map[X][Y];
        modified_map_value = modify_map->map[X][Y];
        //if (!is_empty(modified_map_value))
        add_clear_point(X, Y, true_map, modify_map, scan_size, max_scan_size, current_data_set, laser_scan);
        /*if (!is_empty(modified_map_value)  &&   is_empty(true_map_value))
          add_clear_point(X, Y, true_map, modify_map);*/
    } while (carmen_get_next_point(&params));
}

void
map_modify_update_robot(carmen_navigator_config_t *config,
                        carmen_world_point_p world_point,
                        carmen_map_p true_map,
                        carmen_map_p modify_map)
{
    if(modify_map == NULL)
        return;
  
    static carmen_map_point_t map_point;
    static int first_time =1;
    if(!first_time){
        for(int k=-10; k < 10; k++){
            for(int j=-10; j < 10; j++){
                if(hypot(k, j)* modify_map->config.resolution < 0.5){
                    int temp_x =  carmen_round(map_point.x + k);
                    int temp_y = carmen_round(map_point.y + j);
                    if(is_in_map(temp_x, temp_y, modify_map)){
                        //  add_clear_point(temp_x, temp_y, true_map, modify_map);
                        //fprintf(stderr,"Adding Point (%d,%d)\n", temp_x, temp_y);
                        modify_map->map[temp_x][temp_y] = true_map->map[temp_x][temp_y];
                    }
                }
            }
        }
    }
    first_time = 0;

    carmen_world_to_map(world_point, &map_point);
    //clear the area around the robot
    for(int k=-10; k < 10; k++){
        for(int j=-10; j < 10; j++){
            if(hypot(k, j)* modify_map->config.resolution < 0.5){
                int temp_x =  carmen_round(map_point.x + k);
                int temp_y = carmen_round(map_point.y + j);
                if(is_in_map(temp_x, temp_y, modify_map)){
                    //  add_clear_point(temp_x, temp_y, true_map, modify_map);
                    //fprintf(stderr,"Adding Point (%d,%d)\n", temp_x, temp_y);
                    modify_map->map[temp_x][temp_y] = EMPTY;//1.0;//0.0;//1.0;
                }
            }
        }
    }
    //fprintf(stderr,"Done\n");
}

//this one should be done last - after the laser updates - otherwise they will clear the 
//obtacles added from the kinect

void
map_modify_update(carmen_robot_laser_message *laser_msg,
		  carmen_navigator_config_t *config,
		  carmen_world_point_p world_point,
		  carmen_map_p true_map,
		  carmen_map_p modify_map,
		  int front_laser, double max_range)// , lcm_t *lcm, erlcm_map_p nav3d_map)
{
    int index;
    double angle, separation;
    int laser_x, laser_y;
    double dist;
    int increment;
    carmen_map_point_t map_point;
    int count;

    int maxrange_beam;

    if (!config->map_update_freespace &&  !config->map_update_obstacles)
        return;

    if (true_map == NULL || modify_map == NULL)
        {
            carmen_warn("%s called with NULL map argument.\n", __FUNCTION__);
            return;
        }

    if (laser_scan == NULL && front_laser) {
        laser_scan = (grid_cell_p *)calloc(LASER_HISTORY_LENGTH, sizeof(grid_cell_p));
        carmen_test_alloc(laser_scan);

        scan_size = (int *)calloc(LASER_HISTORY_LENGTH, sizeof(int));
        carmen_test_alloc(scan_size);

        max_scan_size = (int *)calloc(LASER_HISTORY_LENGTH, sizeof(int));
        carmen_test_alloc(max_scan_size);
    }
    else if (r_laser_scan == NULL && !front_laser) {//rear laser 
        r_laser_scan = (grid_cell_p *)calloc(LASER_HISTORY_LENGTH, sizeof(grid_cell_p));
        carmen_test_alloc(r_laser_scan);

        r_scan_size = (int *)calloc(LASER_HISTORY_LENGTH, sizeof(int));
        carmen_test_alloc(r_scan_size);

        r_max_scan_size = (int *)calloc(LASER_HISTORY_LENGTH, sizeof(int));
        carmen_test_alloc(r_max_scan_size);
    }

    //this increments the length of the history
    //update_existing_data(true_map, modify_map, scan_size, max_scan_size, &current_data_set, laser_scan);
    if(front_laser){
        update_existing_data(true_map, modify_map, scan_size, max_scan_size, &current_data_set, laser_scan);
    }
    else{
        update_existing_data(true_map, modify_map, r_scan_size, r_max_scan_size, &r_current_data_set, r_laser_scan);
    }
    
    if (laser_msg->num_readings < 2000){//config->num_lasers_to_use)   {
        increment = 1;
        separation = laser_msg->config.angular_resolution;
    }
    else  {
        increment = laser_msg->num_readings / config->num_lasers_to_use + 1;
        if (increment != 1)
            separation = increment* laser_msg->config.angular_resolution;
        //separation = carmen_normalize_theta(laser_msg->config.fov / (config->num_lasers_to_use+1));
        else
            separation = laser_msg->config.angular_resolution;
    }

    angle = carmen_normalize_theta(world_point->pose.theta +
                                   laser_msg->config.start_angle);

    /*bot_lcmgl_t *lcmgl = bot_lcmgl_init (lcm, "MAP_UPDATE");

    bot_lcmgl_color3f(lcmgl, 1, 0, 0);

    carmen_point_t ll;
    ll.x = world_point->pose.x;
    ll.y = world_point->pose.y;
    ll.theta = world_point->pose.theta;
    carmen_point_t gl = carmen3d_map_map_to_global_coordinates(ll, nav3d_map);

    double cl[3] = {gl.x, gl.y, 0};
    lcmglCircle(cl, 2.0);
    lcmglCircle(cl, 0.3);

    //bot_lcmgl_vertex3f(lcmgl, world_point->pose.x, world_point->pose.y, 0);
    */
    carmen_world_to_map(world_point, &map_point);
  
    count = 0;
    for (index = 0; index < laser_msg->num_readings; index += increment) {
        if (laser_msg->range[index] < max_range)
            maxrange_beam = 0;
        else
            maxrange_beam = 1;

        if(config->map_update_freespace) {
            //fprintf(stderr,"--------Updating Freespace : Config Resolution : %f\n", modify_map->config.resolution);

            //dist = laser_msg->range[index] - modify_map->config.resolution;
            dist = fmin(laser_msg->range[index], config->map_update_radius);
            /*if (dist < 0)
              dist = laser_msg->range[index];
              //	dist = 0;
              if (dist > config->map_update_radius)
              dist = config->map_update_radius;*/

            laser_x = carmen_round(map_point.x + (cos(angle)*dist)/
                                   modify_map->config.resolution);
            laser_y = carmen_round(map_point.y + (sin(angle)*dist)/
                                   modify_map->config.resolution);

            /*//dist * cos(angle), dist *sin(angle) 
            carmen_point_t ll1;
            ll1.x = world_point->pose.x + dist * cos(angle);
            ll1.y = world_point->pose.y + dist *sin(angle);
            ll1.theta = world_point->pose.theta;
            carmen_point_t gl1 = carmen3d_map_map_to_global_coordinates(ll1, nav3d_map);

            double cl[3] = {gl1.x, gl1.y, 0};
            lcmglCircle(cl, 0.2);
            */

    //            bot_lcmgl_vertex3f(lcmgl, dist * cos(angle), dist *sin(angle) , 0);

            //trace_laser(map_point.x, map_point.y, laser_x, laser_y, true_map, modify_map, scan_size, max_scan_size, current_data_set, laser_scan);
            if(front_laser){
                trace_laser(map_point.x, map_point.y, laser_x, laser_y, true_map, modify_map, scan_size, max_scan_size, current_data_set, laser_scan);
            }
            else{
                trace_laser(map_point.x, map_point.y, laser_x, laser_y, true_map, modify_map, r_scan_size, r_max_scan_size, r_current_data_set, r_laser_scan);
            }
            //trace_laser(map_point.x, map_point.y, laser_x, laser_y, true_map, modify_map, k_scan_size, k_max_scan_size, k_current_data_set, kinect_scan);
        }

        if (config->map_update_obstacles) {
            // add obstacle only if it is not a maxrange reading!
            if (!maxrange_beam) {
                dist = laser_msg->range[index];
                /*if(front_laser){
                  dist = 1.0;
                  }*/

                laser_x = carmen_round(map_point.x + (cos(angle)*dist)/
                                       modify_map->config.resolution);
                laser_y = carmen_round(map_point.y + (sin(angle)*dist)/
                                       modify_map->config.resolution);

                if (is_in_map(laser_x, laser_y, modify_map)
                    && dist < config->map_update_radius){
                    //&&
                    //dist < (laser_msg->config.maximum_range - 2*modify_map->config.resolution)) {
                    count++;
                    //can modify to use the points directly - properly transformed 
                    if(front_laser){
                        add_filled_point(laser_x, laser_y, true_map, modify_map, scan_size, max_scan_size, current_data_set, laser_scan);
                    }
                    else{
                        add_filled_point(laser_x, laser_y, true_map, modify_map, r_scan_size, r_max_scan_size, r_current_data_set, r_laser_scan);
                    }
	  
                    //add_filled_point(laser_x, laser_y, true_map, modify_map, k_scan_size, k_max_scan_size, k_current_data_set, kinect_scan);	  
                }
            }
        }
        angle += separation;
    }
    /*
    bot_lcmgl_switch_buffer(lcmgl);
    */
}

void
map_modify_update_simrect(erlcm_rect_list_t *rects,
                          carmen_point_p simrect_center,
                          carmen_navigator_config_t *config,
                          carmen_map_p true_map,
                          carmen_map_p modify_map)
{
    if (true_map == NULL || modify_map == NULL){
        carmen_warn("%s called with NULL map argument.\n", __FUNCTION__);
        return;
    }
    
    for (int i=0; rects && i<rects->num_rects;i++) {
        erlcm_rect_t *rect = &rects->rects[i];
        double pos[2]= {simrect_center->x +rect->dxy[0], simrect_center->y +rect->dxy[1]}; 
        double size[2]={rect->size[0],rect->size[1]};
        double theta = simrect_center->theta + rect->theta; 

        carmen_world_point_t world_point = {pos[0], pos[1], theta};

        world_point.map = modify_map;

        carmen_map_point_t map_point; 

        carmen_world_to_map(&world_point, &map_point);

        //fprintf(stderr, "%f,%f => %d,%d\n", pos[0], pos[1], map_point.x, map_point.y);

        int r_size = (int) (hypot(size[0]/2, size[1]/2) / modify_map->config.resolution + 2);

        double c = cos(theta);
        double s = sin(theta);

        double s_r[2] = {size[0]/modify_map->config.resolution, size[1]/modify_map->config.resolution };

        for(int k=-r_size; k < r_size; k++){
            for(int j=-r_size; j < r_size; j++){
                double x1 = (k*c + j*s);
                double y1 = (-k*s + j*c); 
                
                if(fabs(x1) <= s_r[0]/2 && fabs(y1) <= s_r[1]/2){
                    int temp_x =  carmen_round(map_point.x + k);
                    int temp_y = carmen_round(map_point.y + j);
                    if(is_in_map(temp_x, temp_y, modify_map)){
                        modify_map->map[temp_x][temp_y] = 1.0;
                    }
                }
            }
        }
    }    
}


void
map_modify_clear(carmen_map_p true_map, carmen_map_p modify_map)
{
    //fprintf(stderr," !!!!!!!!!!! Map Reset \n");
    int index;

    if (scan_size == NULL)
        return;

    for (index = 0; index < LASER_HISTORY_LENGTH; index++)
        scan_size[index] = 0;

    memcpy(modify_map->complete_map, true_map->complete_map,
           true_map->config.x_size*true_map->config.y_size*sizeof(float));

    current_data_set = 0;
}
