/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/


/** @addtogroup navigator libplanner_interface **/
// @{

/** \file planner_interface.h
 * \brief Definition of interface to the shortest-path motion planner library.
 *
 * This file specifies the interface to the motion planner library. It does not
 * handle any IPC communication or move the robot in any way. It accepts a
 * map, current position of the robot and a goal, and provides a plan.  This
 * library is not thread-safe or re-entrant.
 **/

#ifndef PLANNER_INTERFACE3D_H
#define PLANNER_INTERFACE3D_H

#include <er_carmen/robot_messages.h>
#include <interfaces/map3d_interface.h>
#include "navigator3d_interface.h"
#include "navigator3d.h"
#include "navigator3d_messages.h"
#include <er_carmen/navigator_messages.h>
#include <bot_lcmgl_client/lcmgl.h>

#ifdef __cplusplus
extern "C" {
#endif

    /** The data structure returned by the motion planning library describing
        the shortest path from the current robot position to the goal. **/

    typedef struct {
        carmen_traj_point_p points;
        int length;
        int capacity;

    } carmen3d_planner_path_t, *carmen3d_planner_path_p;

    typedef struct {
        int portal_id; 
        point2d_t portal_points[2];
        point2d_t global_portal_points[2];        
        int pass_through;  //indicates that is passes through 
        int closer_node;// which node is closer 
        int floor_no;         
        int floor_ind;         
        int path_ind; //which is closer to the robot 
    } carmen3d_portal_t; 

    typedef struct {
        carmen3d_portal_t *portal_list; 
        int no_portals; 
    } carmen3d_portal_list_t; 

    /** The data structure describing what the motion planning library believes
        is the current robot position, goal and current plan. **/

    typedef struct {
        carmen_traj_point_t robot;
        carmen_point_t goal;
        carmen3d_planner_path_t path;
        int goal_set;
	  carmen_map_config_t planned_map_config;
    } carmen3d_planner_status_t, *carmen3d_planner_status_p;


    /** Updates the internal position of the robot inside the planner
        and regenerates the trajectory.
        Returns 1 if a new path was generated, otherwise returns 0 **/

    int carmen3d_planner_update_robot(carmen_traj_point_p new_position,
                                      carmen_navigator_config_t *nav_conf, 
                                      carmen_robot_config_t *robot_conf);

    void set_nav_map(erlcm_map_p g_nav3d_map); 

    /** Updates the current goal, replans and regenerates the trajectory.
        Returns 1 if a new path was generated, otherwise returns 0 */

    int carmen3d_planner_update_goal(carmen_point_p new_goal, int any_orientation,
                                     carmen_navigator_config_t *nav_conf, 
                                     carmen_robot_config_t* robot_conf, int force_replan, bot_lcmgl_t *lcmgl);

    float* get_planner_map();
  
    /** Assumes value of waypoint passed in is current robot position.
        Regenerates trajectory, searches along path for next waypoint
        that is more than nav_conf->goal_dist away from the current
        robot position. If this is the goal point, also checks to
        see if robot needs to turn to face goal orientation (only matters
        if the goal was specified with an orientation.
        Returns 1 if robot reached goal, returns -1 if no path
        exists, otherwise returns 0 and fills in next destination
        waypoint. waypoint_index is the index of the waypoint in the
        trajectory **/

    int carmen3d_planner_next_waypoint(carmen_traj_point_p waypoint,
                                       int* waypoint_index,
                                       int *is_goal,
                                       carmen_navigator_config_t *nav_conf);


    /** Updates the planner to have a new map. **/

    void carmen3d_planner_set_map(carmen_map_p map,
                                  carmen_robot_config_t *robot_conf);

    /** Clears any local modifications the planner may have made to
        its internal map. **/

    void carmen3d_planner_reset_map(carmen_robot_config_t *robot_conf);

  /** Adds a laser scan to the planner's internal map using ray-tracing.
        Scans are forgotten over time, or can be removed using
        carmen3d_planner_reset_map. **/

    void carmen3d_planner_update_map(carmen_robot_laser_message *front_laser_msg,
                                     carmen_robot_laser_message *rear_laser_msg,
                                     erlcm_rect_list_t *rects,
                                     carmen_point_p simrect_center,
                                     carmen_navigator_config_t *nav_config,
                                     carmen_robot_config_t *robot_conf, 
                                     carmen_point_t robot_global_point);//, lcm_t *lcm, erlcm_map_p nav3d_map);

    /** A helper function for replacing the map and initializing the robot
        function.
    **/

    void carmen3d_planner_update_grid(carmen_map_p new_map,
                                      carmen_traj_point_p new_position,
                                      carmen_robot_config_t *robot_conf,
                                      carmen_navigator_config_t *nav_conf);

    /** A helper function for extracting the internal representation
        of the map, cost map or utility function.
    **/

    carmen3d_navigator3d_map_message *carmen3d_planner_get_map_message
    (carmen_navigator_map_t map_type);

    /** Returns the current state of the plan.
     **/

    void carmen3d_planner_get_status(carmen3d_planner_status_p status);

    /** A helper function for getting the current utility function.
        Probably subsumed by carmen3d_planner_get_map_message, but
        returns the utility function as an array of doubles of same
        size as the map, in row-major order.
    **/

    double *carmen3d_planner_get_utility(void);

    /** A utility function for determining whether or not the goal is accessible
        from the current robot position. Returns 1 if the goal has been set and
        a feasible path exists from the robot to the goal, returns 0 otherwise.
    **/
  
    void
    carmen3d_planner_update_robot_map(double x, double y, double theta,  
                                      carmen_navigator_config_t *nav_conf,
                                      carmen_robot_config_t *robot_conf);

    int carmen3d_planner_goal_reachable(void);

    void  carmen3d_planner_clear_goal(carmen_navigator_config_t *nav_conf);

    double* carmen3d_planner_compute_path_costs( const point2d_t* locs, int nlocs,
                                                 const point2d_t goal,
                                                 carmen_navigator_config_t* nav_conf,
                                                 carmen_robot_config_t* robot_conf,
                                                 double cost_for_no_path );

    //compute the cost between the goal and a list of locations
    double* carmen3d_planner_compute_path_costs_to_goal(const point2d_t* locs, int nlocs,
                                                        const point2d_t goal,
                                                        const point2d_t robot_position,
                                                        carmen_navigator_config_t* nav_conf,
                                                        carmen_robot_config_t* robot_conf,
                                                        double cost_for_no_path );

    //compute the cost between the goal and a list of locations
    double carmen3d_planner_compute_path_costs_and_portals(carmen3d_portal_list_t portals, 
                                                           const point2d_t goal,
                                                           carmen_navigator_config_t* nav_conf,
                                                           carmen_robot_config_t* robot_conf,
                                                           double cost_for_no_path, 
                                                           bot_lcmgl_t *lcmgl);

    double carmen3d_planner_compute_elevator_path_costs_and_portals(carmen3d_portal_list_t portals, 
                                                       carmen3d_portal_t *elevator,
                                                       carmen_navigator_config_t* nav_conf,
                                                       carmen_robot_config_t* robot_conf,
                                                       double cost_for_no_path , 
                                                                    bot_lcmgl_t *lcmgl);

    double carmen3d_planner_compute_path_costs_and_portals_from_elevator(carmen3d_portal_list_t portals, 
                                                                     const point2d_t goal,
                                                                     carmen3d_portal_t *elevator,
                                                                     carmen_navigator_config_t* nav_conf,
                                                                     carmen_robot_config_t* robot_conf,
                                                                     double cost_for_no_path , 
                                                                         bot_lcmgl_t *lcmgl);

    /** returns the cost of hte current planned path
        returns LONG_MAX if no path
    */
    double carmen3d_planner_cost_of_current_path();

    double cost_of_path();

#ifdef __cplusplus
}
#endif

#endif
// @}
