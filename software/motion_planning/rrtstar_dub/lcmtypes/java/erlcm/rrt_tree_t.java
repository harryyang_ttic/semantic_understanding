/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package erlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class rrt_tree_t implements lcm.lcm.LCMEncodable
{
    public int num_nodes;
    public erlcm.rrt_node_t nodes[];
    public erlcm.rrt_traj_t traj_from_parent[];
    public int num_edges;
    public int edges[][];
 
    public rrt_tree_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x66e2d093bc9c8bf8L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(erlcm.rrt_tree_t.class))
            return 0L;
 
        classes.add(erlcm.rrt_tree_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + erlcm.rrt_node_t._hashRecursive(classes)
             + erlcm.rrt_traj_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeInt(this.num_nodes); 
 
        for (int a = 0; a < this.num_nodes; a++) {
            this.nodes[a]._encodeRecursive(outs); 
        }
 
        for (int a = 0; a < this.num_nodes; a++) {
            this.traj_from_parent[a]._encodeRecursive(outs); 
        }
 
        outs.writeInt(this.num_edges); 
 
        for (int a = 0; a < this.num_edges; a++) {
            for (int b = 0; b < 2; b++) {
                outs.writeInt(this.edges[a][b]); 
            }
        }
 
    }
 
    public rrt_tree_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public rrt_tree_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static erlcm.rrt_tree_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        erlcm.rrt_tree_t o = new erlcm.rrt_tree_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.num_nodes = ins.readInt();
 
        this.nodes = new erlcm.rrt_node_t[(int) num_nodes];
        for (int a = 0; a < this.num_nodes; a++) {
            this.nodes[a] = erlcm.rrt_node_t._decodeRecursiveFactory(ins);
        }
 
        this.traj_from_parent = new erlcm.rrt_traj_t[(int) num_nodes];
        for (int a = 0; a < this.num_nodes; a++) {
            this.traj_from_parent[a] = erlcm.rrt_traj_t._decodeRecursiveFactory(ins);
        }
 
        this.num_edges = ins.readInt();
 
        this.edges = new int[(int) num_edges][(int) 2];
        for (int a = 0; a < this.num_edges; a++) {
            for (int b = 0; b < 2; b++) {
                this.edges[a][b] = ins.readInt();
            }
        }
 
    }
 
    public erlcm.rrt_tree_t copy()
    {
        erlcm.rrt_tree_t outobj = new erlcm.rrt_tree_t();
        outobj.num_nodes = this.num_nodes;
 
        outobj.nodes = new erlcm.rrt_node_t[(int) num_nodes];
        for (int a = 0; a < this.num_nodes; a++) {
            outobj.nodes[a] = this.nodes[a].copy();
        }
 
        outobj.traj_from_parent = new erlcm.rrt_traj_t[(int) num_nodes];
        for (int a = 0; a < this.num_nodes; a++) {
            outobj.traj_from_parent[a] = this.traj_from_parent[a].copy();
        }
 
        outobj.num_edges = this.num_edges;
 
        outobj.edges = new int[(int) num_edges][(int) 2];
        for (int a = 0; a < this.num_edges; a++) {
            System.arraycopy(this.edges[a], 0, outobj.edges[a], 0, 2);        }
 
        return outobj;
    }
 
}

