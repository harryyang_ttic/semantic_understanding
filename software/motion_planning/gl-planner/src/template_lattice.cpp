#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>
#include <lcmtypes/bot_core_pose_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include <interfaces/map3d_interface.h>
#include <lattice_planner/lattice_planner.hpp>
#include <bot_lcmgl_client/lcmgl.h>
#include <er_lcmtypes/lcm_channel_names.h>
#include <bot_frames/bot_frames.h>

#ifdef __APPLE
#include <GL/gl.h>
#else
#include <GL/gl.h>
#endif

#define POSE_LIST_SIZE 10

using namespace dp_planner;

typedef struct
{
    BotParam   *b_server;
    lcm_t      *lcm;
    GMainLoop *mainloop;
    pthread_t  work_thread;

    //GList *pose_list;  
    erlcm_map_t global_map;
    erlcm_rect_list_t *sim_rects; 
    //uint8_t *complete_global_map; 
    occ_map::FloatPixelMap *fvm; 
    occ_map::FloatPixelMap *utility;
    occ_map::FloatPixelMap *distance;
    erlcm_navigator_goal_msg_t* curr_goal_msg;
    int clicked_map_goal_exists;
    int reached_clicked_goal;

    bot_core_planar_lidar_t *laser;
    bot_core_planar_lidar_t *r_laser;
    BotFrames *frames; 
    bot_core_pose_t *last_pose;
    int publish_all;
    int local_sensing;
    int verbose; 
} state_t;

static void on_pose(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_pose_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 
    if(s->last_pose !=NULL){
	bot_core_pose_t_destroy(s->last_pose);
    }
    s->last_pose = bot_core_pose_t_copy(msg);
    if(s->verbose){
	fprintf(stderr,"Pose : (%f,%f)\n", msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);
    }
}

//pthread
static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
	fprintf(stderr, " T - called ()\n");
	
	usleep(50000);
    }
}

void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    double max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    double max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");
  
    char **planar_lidar_names = bot_param_get_all_planar_lidar_names(b_param);
  
    if(planar_lidar_names) {
	for (int pind = 0; planar_lidar_names[pind] != NULL; pind++) {
	    fprintf(stderr, "Channel : %s\n", planar_lidar_names[pind]);
	}
    }
  
    g_strfreev(planar_lidar_names);
}

//doesnt do anything right now - timeout function
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
	fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}

int plan_to_goal(state_t *self){
    if(!self->last_pose || !self->curr_goal_msg)
        return -1;
    
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(self->lcm, "new_person_tracks");
    
    //double start[3] = { 21, -7, 0 };
    //double goal[3] = { 24.8, -19, 0 };
    double start[3] = {self->last_pose->pos[0], self->last_pose->pos[1], self->last_pose->pos[2] };
    double goal[3] = { self->curr_goal_msg->goal.x, self->curr_goal_msg->goal.y, 0};

    std::vector<Waypoint2d> waypoints;
    self->utility = createUtilityMap(self->fvm, start);
    bool have_path = dpPathToGoal(self->fvm, start, goal, waypoints);

    fprintf(stderr, "Have Path : %d\n", have_path);

    
    lcmglColor3f(1.0, 0.0, 0.0);
    
   
    for (int i=0;i<waypoints.size();i++){
        Waypoint2d& wp = waypoints[i];
        fprintf(stderr,"[%d] : %f,%f\n", i, wp.x, wp.y);
        //if(have_path){
        if(i==0){
            lcmglBegin(GL_LINES);
            lcmglVertex3d( start[0], start[1], start[2] );
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglEnd();
        }
        
        else if(i== waypoints.size()-1){
            lcmglBegin(GL_LINES);
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglVertex3d( goal[0], goal[1], goal[2] );
            lcmglEnd();
        }
        else{
            Waypoint2d& wp_back = waypoints[i-1];
            lcmglBegin(GL_LINES);
            lcmglVertex3d( wp_back.x, wp_back.y, 0 );
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglEnd();
        }
    }
    
    if(have_path){
        double s_pos[3] = {start[0], start[1], start[2] };
        lcmglCircle(s_pos, 0.2);

        for (int i=0;i<waypoints.size();i++){
            Waypoint2d& wp = waypoints[i];
            double l_pos[3] = {wp.x,wp.y,0};
            lcmglCircle(l_pos, 0.2);
        }

        double e_pos[3] = {goal[0], goal[1], goal[2] };
        lcmglCircle(e_pos, 0.2);
    }
        
    bot_lcmgl_switch_buffer (lcmgl);
}

int plan_to_goal_lattice(state_t *self){
    if(!self->last_pose || !self->curr_goal_msg)
        return -1;
    
    bot_lcmgl_t *lcmgl = bot_lcmgl_init(self->lcm, "Sparse_Path");
    
    double start[3] = {self->last_pose->pos[0], self->last_pose->pos[1], self->last_pose->pos[2] };
    double goal[3] = { self->curr_goal_msg->goal.x, self->curr_goal_msg->goal.y, 0};

    std::vector<Waypoint2d> waypoints;
    //self->utility = createUtilityMap(self->fvm, start);
    //update the map based on the new laser scans 

    occ_map::FloatPixelMap *dm = NULL;
    if(self->laser){
        double update_radius = 5.0;
        //carve out a radius in the cost map - and fill in these obstacle points
        double x0 = -update_radius;
        double x1 = update_radius;
        double y0 = -update_radius;
        double y1 = update_radius;
        
        double res = self->fvm->metersPerPixel;
        double dxy[2];

        //fprintf(stderr, "Clearing the map\n");
        /*for(double x= x0; x <= x1 ; x+= res){
            for(double y= y0; y <= y1 ; y+= res){
                if(hypot(x,y) < update_radius){
                    dxy[0] = x+s->last_pose->pos[0];
                    dxy[1] = y+s->last_pose->pos[1];
                    if(s->fvm->isInMap(dxy))
                        s->fvm->writeValue(dxy, 0.0);
                }
                //clearing and updating the distance map gets tricky
            }
        }  
        
        xy_t xy0;
        xy0.xy[0] = x0 + s->last_pose->pos[0];
        xy0.xy[1] = y0 + s->last_pose->pos[1];
        
        xy_t xy1;
        xy1.xy[0] = x1 + s->last_pose->pos[0];
        xy1.xy[1] = y1 + s->last_pose->pos[1];
        */
        
        bot_core_planar_lidar_t *laser = self->laser;
        
        bot_core_planar_lidar_t *rlaser = self->r_laser;

        PointList2d p_list; 
        p_list.no_points = 0;
        p_list.points = (xy_t *) calloc(2* laser->nranges, sizeof(xy_t));

        //add obstacles for the points the laser sees
        double sensor_to_body[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "SKIRT_FRONT",
                                                      "body", laser->utime,
                                                      sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from Skirt front to local!\n");
            return 0;        
        }

        double rear_sensor_to_body[12];
        if (rlaser && !bot_frames_get_trans_mat_3x4_with_utime (self->frames, "SKIRT_REAR",
                                                      "body", rlaser->utime,
                                                      rear_sensor_to_body)) {
            fprintf (stderr, "Error getting bot_frames transformation from Skirt front to local!\n");
            return 0;        
        }
        
        double body_to_local[12];
        if (!bot_frames_get_trans_mat_3x4_with_utime (self->frames, "body",
                                                      "local", laser->utime,
                                                      body_to_local)) {
            fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
            return 0;        
        }
        
        int64_t start_time = bot_timestamp_now();

        int decimation_factor = 3;
        
        for(int i=0; i < laser->nranges; i+=decimation_factor){
            double theta = laser->rad0 + 
                (double)i * laser->radstep;
            
            double pos[3] = {.0,.0,.0};
            pos[0] = laser->ranges[i]*cos(theta); 
            pos[1] = laser->ranges[i]*sin(theta); 
            
            double b_pos[3] = {.0,.0,.0};
            bot_vector_affine_transform_3x4_3d (sensor_to_body, pos, b_pos);  
            
            double dist = hypot(b_pos[0], b_pos[1]);
            if(dist < 0.5 || dist > update_radius)
                continue;
            
            double l_pos[3] = {.0,.0,.0};
            bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, l_pos);  
            
            p_list.points[p_list.no_points].xy[0] = l_pos[0];
            p_list.points[p_list.no_points].xy[1] = l_pos[1];
            p_list.no_points++;
        }

        if(rlaser){
            for(int i=0; i < rlaser->nranges; i+=decimation_factor){
                double theta = rlaser->rad0 + 
                    (double)i * rlaser->radstep;
            
                double pos[3] = {.0,.0,.0};
                pos[0] = rlaser->ranges[i]*cos(theta); 
                pos[1] = rlaser->ranges[i]*sin(theta); 
            
                double b_pos[3] = {.0,.0,.0};
                bot_vector_affine_transform_3x4_3d (rear_sensor_to_body, pos, b_pos);  
            
                double dist = hypot(b_pos[0], b_pos[1]);
                if(dist < 0.5 || dist > update_radius)
                    continue;
            
                double l_pos[3] = {.0,.0,.0};
                bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, l_pos);  
            
                p_list.points[p_list.no_points].xy[0] = l_pos[0];
                p_list.points[p_list.no_points].xy[1] = l_pos[1];
                p_list.no_points++;
            }
        }

        p_list.points = (xy_t *) realloc(p_list.points, p_list.no_points * sizeof(xy_t));
        
        fprintf(stderr,"Count : %d\n", p_list.no_points); 
        
        /*
          (occ_map::FloatPixelMap *pixmap, occ_map::FloatPixelMap *distmap, 
                              double r_radius, double s_radius, 
                              xy_t center, double update_radius, 
                              PointList2d *p_list);
         */


        xy_t center; 
        center.xy[0] = self->last_pose->pos[0];
        center.xy[1] = self->last_pose->pos[1];
        dm = updateDistanceCostMap(self->fvm, self->distance, 0.3, 1.0, center, update_radius, &p_list);
        free(p_list.points);

        int64_t end_time = bot_timestamp_now();
        fprintf(stderr,"Time to update map : %f\n", (end_time - start_time)/1.0e6);
    }
    
    bool have_path = 0;
    
    if(dm){
        have_path = dpPathToGoalLattice(self->fvm, dm, start, goal, waypoints, self->lcm);
        const occ_map_pixel_map_t *o_msg = dm->get_pixel_map_t(bot_timestamp_now());
        fprintf(stderr,"\nPublishing Map\n");
        //occ_map_pixel_map_t_publish(self->lcm, "PIXEL_MAP",  o_msg);
        delete dm;
    }
    else{
        have_path = dpPathToGoalLattice(self->fvm, self->distance, start, goal, waypoints, self->lcm);
        const occ_map_pixel_map_t *o_msg = self->distance->get_pixel_map_t(bot_timestamp_now());
        //self->fvm->get_pixel_map_t(bot_timestamp_now());
        fprintf(stderr,"\nPublishing Map\n");
        //occ_map_pixel_map_t_publish(self->lcm, "PIXEL_MAP",  o_msg);
    }

    fprintf(stderr, "Have Path : %d\n", have_path);

    erlcm_goal_list_t goal_list; 

    int drop_last = 0;

    //check if the last two waypoints are two close together
    if( waypoints.size() > 2){
        Waypoint2d& wp_last = waypoints[waypoints.size()-1];
        Waypoint2d& wp_prev = waypoints[waypoints.size()-2];

        double dist = hypot(wp_last.x - wp_prev.x , wp_last.y - wp_prev.y);

        fprintf(stderr,"Gap : %f\n", dist);
        if(dist < 1.0){
            //droping the one before last
            drop_last = 1;
        }        
    }
    

    goal_list.utime = bot_timestamp_now();
    goal_list.sender_id = 0;
    //first one is the robot position 
    goal_list.num_goals = waypoints.size() -1 - drop_last;
    goal_list.goals = (erlcm_goal_t *) calloc( waypoints.size()-1,sizeof(erlcm_goal_t));

    for (int i=0;i<waypoints.size()-1 - drop_last;i++){
        Waypoint2d& wp = waypoints[i+1];
        if(drop_last && i == (waypoints.size()-1 - drop_last)){
            wp = waypoints[i+2];
        }

        fprintf(stderr,"[%d] : %f,%f\n", i, wp.x, wp.y);
        
        /*temp_pt.x = status.path.points[waypoint_ind+i].x;
        temp_pt.y = status.path.points[waypoint_ind+i].y;
        temp_pt.theta = status.path.points[waypoint_ind+i].theta;
        temp_global_pt = carmen3d_map_map_to_global_coordinates(temp_pt, map3d);*/

        goal_list.goals[i].pos[0] = wp.x; 
        goal_list.goals[i].pos[1] = wp.y; 
      
        goal_list.goals[i].theta = wp.theta; 
      
        goal_list.goals[i].size[0] = 0.7;
        goal_list.goals[i].size[1] = 0.7;
      
        goal_list.goals[i].heading_tol = 0.3; 
        goal_list.goals[i].use_theta = 1;
        goal_list.goals[i].do_turn_only = 0;
    }

    erlcm_goal_list_t_publish (self->lcm, "RRTSTAR_GOALS", &goal_list);

    
    lcmglColor3f(1.0, 0.0, 0.0);
    
   
    for (int i=0;i<waypoints.size();i++){
        Waypoint2d& wp = waypoints[i];
        fprintf(stderr,"[%d] : %f,%f\n", i, wp.x, wp.y);
        //if(have_path){
        if(i==0){
            lcmglBegin(GL_LINES);
            lcmglVertex3d( start[0], start[1], start[2] );
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglEnd();
        }
        
        else if(i== waypoints.size()-1){
            lcmglBegin(GL_LINES);
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglVertex3d( goal[0], goal[1], goal[2] );
            lcmglEnd();
        }
        else{
            Waypoint2d& wp_back = waypoints[i-1];
            lcmglBegin(GL_LINES);
            lcmglVertex3d( wp_back.x, wp_back.y, 0 );
            lcmglVertex3d( wp.x, wp.y, 0 );
            lcmglEnd();
        }
    }
    
    if(have_path){
        double s_pos[3] = {start[0], start[1], start[2] };
        lcmglCircle(s_pos, 0.2);

        for (int i=0;i<waypoints.size();i++){
            Waypoint2d& wp = waypoints[i];
            double l_pos[3] = {wp.x,wp.y,0};
            lcmglCircle(l_pos, 0.2);
        }

        double e_pos[3] = {goal[0], goal[1], goal[2] };
        lcmglCircle(e_pos, 0.2);
    }
        
    bot_lcmgl_switch_buffer (lcmgl);
}


static void navigator_goal_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                                   const erlcm_navigator_goal_msg_t *msg, void *user)
{
    state_t *s = (state_t *) user;

    fprintf(stderr,"$$$$$$$$$$ Received Navigator Goal\n");
    if(s->curr_goal_msg != NULL){
        erlcm_navigator_goal_msg_t_destroy(s->curr_goal_msg);
    }

    s->curr_goal_msg = erlcm_navigator_goal_msg_t_copy(msg);
    s->reached_clicked_goal = 0;
    s->clicked_map_goal_exists = 1;

    //plan_to_goal(s);
    plan_to_goal_lattice(s);
    /*carmen_point_t sim_rect_center;

    if(s->sim_rects_list){
        carmen_point_t g_sim_rect_center;
        g_sim_rect_center.x = s->sim_rects_list->xy[0];
        g_sim_rect_center.y = s->sim_rects_list->xy[1];
        g_sim_rect_center.theta = 0;
        
        sim_rect_center = carmen3d_map_global_to_map_coordinates(g_sim_rect_center, s->nav3d_map);
        }*/
}

static void on_laser_1(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_planar_lidar_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 

    fprintf(stderr,".");

    if(s->laser){
        bot_core_planar_lidar_t_destroy(s->laser);
    }

    s->laser = bot_core_planar_lidar_t_copy(msg);
    
    bot_core_planar_lidar_t *laser = s->laser;

    //possibly the map update should happen periodically - instead of on lcm 
    //on_lcm might be too high

    if(!s->fvm )
        return;
        

    if(s->last_pose ==NULL){
        return;
    }

    //add obstacles for the points the laser sees
    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "SKIRT_FRONT",
                                                  "body", laser->utime,
                                                  sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from Skirt front to local!\n");
        return;        
    }

    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local", laser->utime,
                                                  body_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;        
    }

  
    
    double update_radius = 5.0;
    //carve out a radius in the cost map - and fill in these obstacle points
    double x0 = -update_radius;
    double x1 = update_radius;
    double y0 = -update_radius;
    double y1 = update_radius;

    double res = s->fvm->metersPerPixel;
    double dxy[2];

    //fprintf(stderr, "Clearing the map\n");
    for(double x= x0; x <= x1 ; x+= res){
        for(double y= y0; y <= y1 ; y+= res){
            if(hypot(x,y) < update_radius){
                dxy[0] = x+s->last_pose->pos[0];
                dxy[1] = y+s->last_pose->pos[1];
                if(s->fvm->isInMap(dxy))
                    s->fvm->writeValue(dxy, 0.0);
                }
            //clearing and updating the distance map gets tricky
        }
    }  

    for(int i=0; i < laser->nranges; i++){
        double theta = laser->rad0 + 
                (double)i * laser->radstep;

        double pos[3] = {.0,.0,.0};
        pos[0] = laser->ranges[i]*cos(theta); 
        pos[1] = laser->ranges[i]*sin(theta); 

        double b_pos[3] = {.0,.0,.0};
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos, b_pos);  
        
        double dist = hypot(b_pos[0], b_pos[1]);
        if(dist < 0.1 || dist > update_radius)
            continue;

        double l_pos[3] = {.0,.0,.0};
        bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, l_pos);  

        if(s->fvm->isInMap(l_pos)){
            s->fvm->writeValue(l_pos, 0.99);
        }

        //this is a lot of points to sweep
        /*if(s->distance){
            for(int d = 1; d < sweep_radius+1; d++){
                //fill the distance map also 
                s->distance
            }
            
            }*/
        

        
        //fill the map

        //we could fill the distance map simmilarly - since we know where the points are
    }
    
    if(s->distance){
        //update the current distance map
        delete s->distance; 
        s->distance = createDistanceCostMap(s->fvm, s->lcm, 0.3, 1.0);
    }
    else{
        s->distance = createDistanceCostMap(s->fvm, s->lcm, 0.3, 1.0);
    }
    static int count = 0;

    if(count >= 100){
        count = 0;
    }    

    if(0 && !count && s->distance){
            const occ_map_pixel_map_t *o_msg = s->distance->get_pixel_map_t(bot_timestamp_now());
            //self->fvm->get_pixel_map_t(bot_timestamp_now());
            fprintf(stderr,"\nPublishing Map\n");
            occ_map_pixel_map_t_publish(s->lcm, "PIXEL_MAP",  o_msg);
    }

    count++;
}


static void on_laser(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char * channel __attribute__((unused)),
			     const bot_core_planar_lidar_t * msg, void * user  __attribute__((unused))) {
    state_t *s = (state_t *) user; 

    fprintf(stderr,".");

    if(!s->last_pose){
        return;
    }

    if(!strcmp(channel,"SKIRT_FRONT")){
        if(s->laser){
            bot_core_planar_lidar_t_destroy(s->laser);
        }
        s->laser = bot_core_planar_lidar_t_copy(msg);
    }

    if(!strcmp(channel,"SKIRT_REAR")){
        if(s->r_laser){
            bot_core_planar_lidar_t_destroy(s->r_laser);
        }
        s->r_laser = bot_core_planar_lidar_t_copy(msg);
        return;
    }

    bot_core_planar_lidar_t *laser = s->laser;

    /*
    //add obstacles for the points the laser sees
    double sensor_to_body[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "SKIRT_FRONT",
                                                  "body", laser->utime,
                                                  sensor_to_body)) {
        fprintf (stderr, "Error getting bot_frames transformation from Skirt front to local!\n");
        return;        
    }

    double body_to_local[12];
    if (!bot_frames_get_trans_mat_3x4_with_utime (s->frames, "body",
                                                  "local", laser->utime,
                                                  body_to_local)) {
        fprintf (stderr, "Error getting bot_frames transformation from VELODYNE to local!\n");
        return;        
    }


    if(s->last_pose && s->fvm && s->distance && 0){
        PointList2d p_list; 
        p_list.no_points = 0;
        p_list.points = (xy_t *) calloc(laser->nranges, sizeof(xy_t));

        double update_radius = 5.0;
        
        for(int i=0; i < laser->nranges; i++){
            double theta = laser->rad0 + 
                (double)i * laser->radstep;

            double pos[3] = {.0,.0,.0};
            pos[0] = laser->ranges[i]*cos(theta); 
            pos[1] = laser->ranges[i]*sin(theta); 

            double b_pos[3] = {.0,.0,.0};
            bot_vector_affine_transform_3x4_3d (sensor_to_body, pos, b_pos);  
        
            double dist = hypot(b_pos[0], b_pos[1]);
            if(dist < 0.1 || dist > update_radius)
                continue;

            double l_pos[3] = {.0,.0,.0};
            bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, l_pos);  

            p_list.points[p_list.no_points].xy[0] = l_pos[0];
            p_list.points[p_list.no_points].xy[1] = l_pos[1];
            p_list.no_points++;
        }

        xy_t center; 
        center.xy[0] = s->last_pose->pos[0];
        center.xy[1] = s->last_pose->pos[1];
        occ_map::FloatPixelMap *dm  = updateDistanceCostMap(s->fvm, s->distance, 0.3, 1.0, center, update_radius, &p_list);
        free(p_list.points);

        const occ_map_pixel_map_t *o_msg = dm->get_pixel_map_t(bot_timestamp_now()); 
    
        occ_map_pixel_map_t_publish(s->lcm, "PIXEL_MAP",  o_msg);
        
        delete dm;
         
        }*/
    
    /*
    double update_radius = 5.0;
    //carve out a radius in the cost map - and fill in these obstacle points
    double x0 = -update_radius;
    double x1 = update_radius;
    double y0 = -update_radius;
    double y1 = update_radius;

    double res = s->fvm->metersPerPixel;
    double dxy[2];

    xy_t xy0;
    xy0.xy[0] = x0 + s->last_pose->pos[0];
    xy0.xy[1] = y0 + s->last_pose->pos[1];

    xy_t xy1;
    xy1.xy[0] = x1 + s->last_pose->pos[0];
    xy1.xy[1] = y1 + s->last_pose->pos[1];

    PointList2d p_list; 
    p_list.no_points = 0;
    p_list.points = (xy_t *) calloc(laser->nranges, sizeof(xy_t));

    for(int i=0; i < laser->nranges; i++){
        double theta = laser->rad0 + 
                (double)i * laser->radstep;

        double pos[3] = {.0,.0,.0};
        pos[0] = laser->ranges[i]*cos(theta); 
        pos[1] = laser->ranges[i]*sin(theta); 

        double b_pos[3] = {.0,.0,.0};
        bot_vector_affine_transform_3x4_3d (sensor_to_body, pos, b_pos);  
        
        double dist = hypot(b_pos[0], b_pos[1]);
        if(dist < 0.1 || dist > update_radius)
            continue;

        double l_pos[3] = {.0,.0,.0};
        bot_vector_affine_transform_3x4_3d (body_to_local, b_pos, l_pos);  

        p_list.points[p_list.no_points].xy[0] = l_pos[0];
        p_list.points[p_list.no_points].xy[1] = l_pos[1];
        p_list.no_points++;
    }

    fprintf(stderr,"Count : %d\n", p_list.no_points); 

    occ_map::FloatPixelMap *dm = creatDistanceMapFromPoints(p_list,s->lcm, xy0, xy1, 0.1 );
    
    fprintf(stderr,"Publishing\n");
    const occ_map_pixel_map_t *o_msg = dm->get_pixel_map_t(bot_timestamp_now()); 
    
    occ_map_pixel_map_t_publish(s->lcm, "PIXEL_MAP",  o_msg);
    
    delete dm;
        

    free(p_list.points);*/
}


//seems like there is an issue here 

static void
on_sim_rects(const lcm_recv_buf_t * rbuf, const char *channel, 
             const erlcm_rect_list_t *msg, void *user)
{
    state_t *s = (state_t *) user; 
    if(s->sim_rects != NULL){
	erlcm_rect_list_t_destroy(s->sim_rects);
    }
    s->sim_rects = erlcm_rect_list_t_copy(msg);

    static int first = 0;
    if(!first){
        first = 1;
        fprintf(stderr,"Sim rects Called\n");
        if(s->fvm ){
            int64_t start_time = bot_timestamp_now();

            //update cost map
            for (int i=0;  s->sim_rects && i<  s->sim_rects->num_rects;i++) {
                erlcm_rect_t *rect = & s->sim_rects->rects[i];
                double pos[2]= { s->sim_rects->xy[0]+rect->dxy[0],  s->sim_rects->xy[1]+rect->dxy[1]}; 
                /*int size[2]={(int)(rect->size[0] / self->fvm->metersPerPixel),
                  (int)(rect->size[1]/ self->fvm->metersPerPixel)}; */

                double res = s->fvm->metersPerPixel;
                //fprintf(stderr,"Dim : %f\n", s->fvm->metersPerPixel);

                double size[2] = {rect->size[0], rect->size[1]};
                double theta = rect->theta;
                double sn, cs;
                bot_fasttrig_sincos(theta, &sn, &cs);

                double x_bound = (size[0] / 2.0 * fabs(cs) + size[1] / 2.0 * fabs(sn));
                double y_bound = (size[0] / 2.0 * fabs(sn) + size[1] / 2.0 * fabs(cs));

                double x0 = pos[0] - x_bound;
                double x1 = pos[0] + x_bound;

                double y0 = pos[1] - y_bound;
                double y1 = pos[1] + y_bound;
                        
                double dxy[2] = {0,0};
                //scan the region in the cost map - and update the rectangle
                for(double x= x0; x <= x1 ; x+= res){
                    for(double y= y0; y <= y1 ; y+= res){
                        //check if within rectangle 
                        //and fill with obs
                        //fprintf(stderr,"Filling\n");
                        double dx = (x - pos[0]) * cs + (y - pos[1]) * sn;
                        double dy = -(x - pos[0]) * sn + (y - pos[1]) * cs;

                        if(fabs(dx) < size[0]/2 && fabs(dy) < size[1]/2){
                            dxy[0] = x;
                            dxy[1] = y;
                            if(s->fvm->isInMap(dxy))
                                s->fvm->writeValue(dxy, 0.99);
                            }
                    }
                }
            }

            int64_t end_time = bot_timestamp_now();

            fprintf(stderr,"Time to add Sim obs : %f\n" , (end_time - start_time) / 1.0e6); 

            const occ_map_pixel_map_t *o_msg = s->fvm->get_pixel_map_t(bot_timestamp_now()); 
    
            occ_map_pixel_map_t_publish(s->lcm, "PIXEL_MAP",  o_msg);
        }

    

        if(s->distance){
            //update the current distance map
            delete s->distance; 
            s->distance = createDistanceCostMap(s->fvm, s->lcm, 0.3, 1.0);
        }
        else{
            s->distance = createDistanceCostMap(s->fvm, s->lcm, 0.3, 1.0);
        }

        if(1){
            const occ_map_pixel_map_t *o_msg = s->distance->get_pixel_map_t(bot_timestamp_now());
            //self->fvm->get_pixel_map_t(bot_timestamp_now());
            fprintf(stderr,"\nPublishing Map\n");
            occ_map_pixel_map_t_publish(s->lcm, "PIXEL_MAP",  o_msg);

        }
    }
}

void subscribe_to_channels(state_t *s)
{
    bot_core_pose_t_subscribe(s->lcm, "POSE", on_pose ,s);
    erlcm_navigator_goal_msg_t_subscribe(s->lcm, "NAV_GOAL", navigator_goal_handler, s);
    
    bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_FRONT", on_laser, s); 
    bot_core_planar_lidar_t_subscribe(s->lcm, "SKIRT_REAR", on_laser, s); 
    //we need to have an option of not subscribing to obstacles
    /*if(self->local_sensing){
        erlcm_obstacle_list_t_subscribe (self->lcm, "OBSTACLES", on_obstacles, self);
        }*/

}  


static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
	   "options are:\n"
	   "--help,        -h    display this message \n"
	   "--publish_all, -a    publish robot laser messages for all channels \n"
	   "--verbose,     -v    be verbose", funcName);
    exit(1);

}

//add map handler that takes in the wheelchair gridmap 
static void on_map_server_gridmap(const lcm_recv_buf_t *rbuf, const char *channel, 
                                  const erlcm_gridmap_t *msg, void *user)
{
    
    state_t *self = (state_t *)user; 
    //
    //pthread_mutex_lock (&self->global_map_gridmap_mutex);
    
    fprintf (stdout, "on map_server_gridmap before: global_map.map = %p\n", (void*) self->global_map.map);
    fprintf (stdout, "on map_server_gridmap before: global_map.complete_map = %p\n", (void*) self->global_map.complete_map);
    
    if (self->global_map.complete_map != NULL) {
        fprintf (stdout, "freeing global_map.complete_map\n");
        free(self->global_map.complete_map);
        self->global_map.complete_map = NULL;
    }
    if (self->global_map.map != NULL){
        free(self->global_map.map);
        self->global_map.map = NULL;
    }   

    fprintf (stderr,"Done freeing\n");
    
    carmen3d_map_uncompress_lcm_map(&(self->global_map), msg);
    
    fprintf (stdout, "on map_server_gridmap after: global_map.map = %p\n", (void*) self->global_map.map);
    fprintf (stdout, "on map_server_gridmap after: global_map.complete_map = %p\n", (void*) self->global_map.complete_map);


    /*double xyz0[3] = { -20, -20, 0 };
    double xyz1[3] = { 20, 20, self->global_map.config.resolution };
    double mpp[3] = { self->global_map.config.resolution, 
    self->global_map.config.resolution, 
    self->global_map.config.resolution };*/
    
    double xy0[2] = { -20, -20};
    double xy1[3] = { 20, 20};
    double mpp =  self->global_map.config.resolution;
    
    carmen3d_map3d_map_index_to_global_coordinates(&xy0[0], &xy0[1], self->global_map.midpt,
                                                   self->global_map.map_zero, 
                                                   self->global_map.config.resolution, 
                                                   0,  
                                                   0); 

    carmen3d_map3d_map_index_to_global_coordinates(&xy1[0], &xy1[1], self->global_map.midpt,
                                                   self->global_map.map_zero, 
                                                   self->global_map.config.resolution, 
                                                   self->global_map.config.x_size,  
                                                   self->global_map.config.y_size); 

    fprintf(stderr, "Map extent : %f,%f => %f,%f\n", 
            xy0[0], xy0[1], 
            xy1[0], xy1[1]);//, xyz1[2]);
    
    //occ_map::FloatVoxelMap fvm(xyz0, xyz1, mpp, 0);
    occ_map::FloatPixelMap *fvm = new occ_map::FloatPixelMap(xy0, xy1, mpp, 0);

    double radius = 0.5; 
    
    //this is filling stuff up 
    double ixy[3] = {.0,.0};//,.0};
    //for (ixyz[2] = ; ixyz[2] < 10; ixyz[2] += .2) {
    int count_obs = 0;

    for (ixy[1] = xy0[1]; ixy[1] < xy1[1]; 
         ixy[1] += self->global_map.config.resolution) {
        for (ixy[0] = xy0[0]; ixy[0] < xy1[0]; ixy[0] += self->global_map.config.resolution) {
        
            carmen_point_t xyz_pt = {ixy[0], ixy[1], 0};
            int ig, jg;
            carmen3d_map3d_global_to_map_index_coordinates (xyz_pt, self->global_map.midpt, 
                                                            self->global_map.map_zero,
                                                            self->global_map.config.resolution, &ig, &jg);
            
            if ((ig < 0) || (ig > (self->global_map.config.x_size-1)) 
                || (jg < 0) || (jg > self->global_map.config.y_size-1))
                continue;

            //can dilate the map after

            if(self->global_map.map[ig][jg] > 0.7){
                count_obs++; 
                /*for(double i = -radius; i <= +radius; i+= self->global_map.config.resolution){
                    for(double j = -radius; j <= +radius; j+= self->global_map.config.resolution){
                        double ixy_a[3] = {ixy[0] + i , ixy[1] + j};
                        if(hypot(i,j) > radius){
                            continue;
                        }
                        //distance 
                        fvm->writeValue(ixy_a, 0.99);
                        double dist_ratio = 1 - (hypot(i,j)/ radius); 
                    }
                    }*/
                fvm->writeValue(ixy, 0.99);
            }
            
            if(self->global_map.map[ig][jg] == 0.5){
                count_obs++;
                fvm->writeValue(ixy, 0.5);
                //fvm->writeValue(ixy, 0.99);
            }
        }
    }

    fprintf(stderr,"Obs Count : %d\n", count_obs);

    //there is a dilation function - but it doesnt seem to work properly
    //self->distance = createDistanceMap(fvm);
    
    self->fvm = fvm;//createCostMap(fvm, 0.3, 0.3);

    self->distance = createDistanceCostMap(self->fvm, self->lcm, 0.3, 1.0);

    if(0){
        const occ_map_pixel_map_t *o_msg = self->distance->get_pixel_map_t(bot_timestamp_now());

        fprintf(stderr,"\nPublishing Map\n");
        occ_map_pixel_map_t_publish(self->lcm, "PIXEL_MAP",  o_msg);
    }
    fprintf(stderr,"Acquired gridmap\n");
}

void get_global_map(state_t *self)
{
    /* subscripe to map, and wait for it to come in... */
    erlcm_map_request_msg_t msg;
    msg.utime =  carmen_get_time()*1e6;
    msg.requesting_prog = "DP_PLANNER";
    
    
    erlcm_gridmap_t_subscribe(self->lcm, "MAP_SERVER", on_map_server_gridmap, self); 

    erlcm_rect_list_t_subscribe(self->lcm, "SIM_RECTS", on_sim_rects, self);
    erlcm_rect_list_t_subscribe(self->lcm, "MAP_SERVER_RECTS", on_sim_rects, self);

    int sent_map_req = 0;
    fprintf(stderr,"Sent Request\n");
    while (self->global_map.map == NULL) {
        if(!sent_map_req){
            sent_map_req = 1;
            erlcm_map_request_msg_t_publish(self->lcm,"MAP_REQUEST_CHANNEL",&msg);
        }
        //sleep();
        lcm_handle(self->lcm);
    }
    fprintf(stderr,"Got Global Map\n");  
    
    //we should convert this because of row major/column major issue 
}

int 
main(int argc, char **argv)
{

    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->last_pose = NULL;
    state->curr_goal_msg = NULL;
    state->sim_rects = NULL;
    state->laser = NULL;
    const char *optstring = "hav";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
				  { "publish_all", no_argument, 0, 'a' }, 
				  { "verbose", no_argument, 0, 'v' }, 
                                  { "l", no_argument, 0, 'l' }, 
				  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
	switch (c) {
	case 'h':
	    usage(argv[0]);
	    break;
	case 'a':
	    {
		fprintf(stderr,"Publishing all laser channels\n");
		break;
	    }
        case 'l':
	    {
                state->local_sensing = 1;
		fprintf(stderr,"Update local\n");
		break;
	    }
	case 'v':
	    {
		fprintf(stderr,"Verbose\n");
		state->verbose = 1;
		break;
	    }
	default:
	    {
		usage(argv[0]);
		break;
	    }
	}
    }

    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);

    state->frames = bot_frames_get_global (state->lcm, state->b_server);

    memset(&state->global_map, 0, sizeof(erlcm_map_t));
    state->global_map.complete_map = NULL;
    state->global_map.map = NULL;

    get_global_map(state);

    //pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
	printf("Couldn't create main loop\n");
	return -1;
    }

    //add lcm to mainloop 
    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    //g_timeout_add_seconds (1, heartbeat_cb, state);

    //adding proper exiting 
    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");

    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


