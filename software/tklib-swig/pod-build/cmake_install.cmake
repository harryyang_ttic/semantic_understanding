# Install script for directory: /home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/harry/Documents/Robotics/semantic-understanding/software/build")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/tklib-swig" TYPE FILE FILES
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/avs.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/gaussian.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/line.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/named_enum.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/probability.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/quaternion.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/box_window.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/datatypes.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/gridmapping.hpp"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/hurdle_extractor.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/nearest_neighbor.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/procrustes.hpp"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/spline.hpp"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/carmen_util.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/EKF2D.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/gsl_python.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/kmeans.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/math2d.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/math3d.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/noise_models.hpp"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/python_swig_utils.h"
    "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/c/tklib_log_gridmap.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libtklib_swig_utils.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libtklib_swig_utils.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libtklib_swig_utils.so"
         RPATH "/home/harry/Documents/Robotics/semantic-understanding/software/build/lib")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/pod-build/lib/libtklib_swig_utils.so")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libtklib_swig_utils.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libtklib_swig_utils.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libtklib_swig_utils.so"
         OLD_RPATH "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/pod-build/lib:/home/harry/Documents/Robotics/semantic-understanding/software/build/lib:"
         NEW_RPATH "/home/harry/Documents/Robotics/semantic-understanding/software/build/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libtklib_swig_utils.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/pod-build/pyTklib")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib/_pyTklib.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib/_pyTklib.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib/_pyTklib.so"
         RPATH "/home/harry/Documents/Robotics/semantic-understanding/software/build/lib")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib/_pyTklib.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib" TYPE MODULE FILES "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/pod-build/lib/_pyTklib.so")
  IF(EXISTS "$ENV{DESTDIR}/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib/_pyTklib.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib/_pyTklib.so")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib/_pyTklib.so"
         OLD_RPATH "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/pod-build/lib:/home/harry/Documents/Robotics/semantic-understanding/software/build/lib:"
         NEW_RPATH "/home/harry/Documents/Robotics/semantic-understanding/software/build/lib")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/harry/Documents/Robotics/semantic-understanding/software/build/lib/python2.7/dist-packages/pyTklib/_pyTklib.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/pod-build/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/harry/Documents/Robotics/semantic-understanding/software/tklib-swig/pod-build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
