/* File : pyTklib.i */
%module(directors="1") pyTklib
%{

#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_permutation.h"
#include <gsl_swig_utils/gsl_utilities.h>
#include "math2d.h"
#include "math3d.h"
#include "avs.h"
    //#include "spatial_features.h"
    //#include "spatial_feature_extractor.h"
#include "EKF2D.h"
#include "procrustes.hpp"
#include "kmeans.h"
#include "hurdle_extractor.h"
    //#include <carmen/carmen.h"
#include "carmen_util.h"
    //#include "crf_utils.h"
#include "tklib_log_gridmap.hpp"
#include "gridmapping.hpp"
#include "noise_models.hpp"
    //#include "simulator.h"
#include "carmen_util.h"
#include "spline.hpp"
    //#include "carmen_subscribe.h"
    //#include "carmen_publish.h"
#include "python_swig_utils.h"

    void tklib_swig_gsl_error_handler (const char * reason,
                                       const char * file,
                                       int line, 
                                       int gsl_errno) {
        printf("gsl file: %s:%d: %s (errno: %s (%d))\n", file, line, 
               reason, gsl_strerror(gsl_errno), gsl_errno);
        PyErr_SetString(PyExc_ValueError, reason);

        PyObject * traceback =  PyImport_ImportModule("traceback");
        PyObject * method = PyObject_GetAttrString(traceback, "print_stack");
        PyObject * emptyTuple = PyTuple_New(0);
        PyObject_Call(method, emptyTuple, NULL);
        abort();
    }

    %}

%init %{

    gsl_set_error_handler(tklib_swig_gsl_error_handler);
    %}

%typemap(out) carmen_gridmapping_map_message*{
	PyObject* pymap = PyList_New(0);

	if($1 == NULL)
	    return pymap;
	
	int i, j;
	for(i=0; i<$1->size1; i++){
        PyObject* row = PyList_New(0);
        for(j=0;j<$1->size2;j++){
            PyObject* pt = PyFloat_FromDouble($1->map[i*$1->size1+j]);
            PyList_Append(row, pt);
            Py_DECREF(pt);
        }
        PyList_Append(pymap, row);
        Py_DECREF(row);
	}
	
	//have mat and pyvec add to hash
	PyObject *retdict = PyDict_New();

	PyObject *x_origin = PyFloat_FromDouble($1->x_origin);	
	PyObject *y_origin = PyFloat_FromDouble($1->y_origin);	
	PyObject *resolution = PyFloat_FromDouble($1->resolution);	
	PyObject *timestamp = PyFloat_FromDouble($1->timestamp);	
	PyObject *host_name = PyString_FromString($1->host);


	PyDict_SetItemString(retdict, "map", pymap);
	PyDict_SetItemString(retdict, "x_origin", x_origin);	
	PyDict_SetItemString(retdict, "y_origin", y_origin);	
	PyDict_SetItemString(retdict, "resolution", resolution);	
	PyDict_SetItemString(retdict, "timestamp", timestamp);	
	PyDict_SetItemString(retdict, "host_name", host_name);	

	Py_DECREF(pymap);
	Py_DECREF(x_origin);
	Py_DECREF(y_origin);
	Py_DECREF(resolution);
	Py_DECREF(timestamp);
	Py_DECREF(host_name);	
	
	$result = retdict;
	//free(map);
 }



%typemap(out) carmen_ekf_message*{
	
	PyObject* pycov = PyList_New(0);

	if($1 == NULL)
	    return pycov;

	double*  cov = $1->covariance;
	int i, j;
	for(i=0; i<$1->size_mean; i++){
        PyObject* row = PyList_New(0);
        for(j=0;j<$1->size_mean;j++){
            PyObject* pt = PyFloat_FromDouble(cov[i*$1->size_mean + j]);
            PyList_Append(row, pt);
            Py_DECREF(pt);
        }
        PyList_Append(pycov, row);
        Py_DECREF(row);
	}
	

	PyObject* pymean = PyList_New(0);
	double* mean = $1->mean;
	for(i=0; i<$1->size_mean; i++){
        PyObject* pt = PyFloat_FromDouble(mean[i]);
        PyList_Append(pymean, pt);
        Py_DECREF(pt);
	}
	

	//have mat and pyvec add to hash
	PyObject *retdict = PyDict_New();
	PyObject *timestamp = PyFloat_FromDouble($1->timestamp);	
	PyObject *host_name = PyString_FromString($1->host);

	PyDict_SetItemString(retdict, "cov", pycov);
	PyDict_SetItemString(retdict, "mean", pymean);	
	PyDict_SetItemString(retdict, "timestamp", timestamp);	
	PyDict_SetItemString(retdict, "host_name", host_name);	

	Py_DECREF(pycov);
	Py_DECREF(pymean);
	Py_DECREF(timestamp);
	Py_DECREF(host_name);	


	$result = retdict;
 }

%typemap(out) carmen_gridmapping_ray_trace_message*{
	if($1 == NULL)
	    return PyList_New(0);

	PyObject* thetas = PyList_New(0);
	PyObject* readings = PyList_New(0);

	int i;
	for(i=0; i<$1->num_readings; i++){
		PyObject* pt = PyFloat_FromDouble($1->thetas[i]);
		PyList_Append(thetas, pt);
		PyObject* pt2 = PyFloat_FromDouble($1->readings[i]);
		PyList_Append(readings, pt2);
		Py_DECREF(pt);
		Py_DECREF(pt2);
    }


	PyObject *retdict = PyDict_New();
	PyObject* robot_pose = PyList_New(0);
	
	PyObject* x = PyFloat_FromDouble($1->x);
	PyObject* y = PyFloat_FromDouble($1->y);
	PyObject* theta = PyFloat_FromDouble($1->theta);
	PyList_Append(robot_pose, x);
	PyList_Append(robot_pose, y);
	PyList_Append(robot_pose, theta);

	PyDict_SetItemString(retdict, "robot_pose", robot_pose);
	PyDict_SetItemString(retdict, "thetas", thetas);
	PyDict_SetItemString(retdict, "readings", readings);	
	Py_DECREF(robot_pose);
	Py_DECREF(x);
	Py_DECREF(y);
	Py_DECREF(theta);	
	Py_DECREF(thetas);	
	Py_DECREF(readings);	

	$result = retdict;
 }

%typemap(out) carmen_map_p{
	PyObject* pymap = PyList_New(0);
	//fprintf(stderr, "here0\n");
	if($1 == NULL)
	    return pymap;

	float** themap = $1->map;
	int i, j;
	for(i=0; i<$1->config.x_size; i++){
        PyObject* row = PyList_New(0);
        for(j=0;j<$1->config.y_size;j++){
            PyObject* pt = PyFloat_FromDouble(themap[i][j]);
            PyList_Append(row, pt);
            Py_DECREF(pt);
        }
        PyList_Append(pymap, row);
        Py_DECREF(row);
	}

	//have mat and pyvec add to hash
	PyObject *retdict = PyDict_New();
	//fprintf(stderr, "here1\n");	
	PyObject *x_size = PyFloat_FromDouble($1->config.x_size);	
	PyObject *y_size = PyFloat_FromDouble($1->config.y_size);	
	PyObject *resolution = PyFloat_FromDouble($1->config.resolution);	
	PyObject *map_name = PyString_FromString($1->config.map_name);
	
		
	PyDict_SetItemString(retdict, "map", pymap);
	PyDict_SetItemString(retdict, "x_size", x_size);	
	PyDict_SetItemString(retdict, "y_size", y_size);	
	PyDict_SetItemString(retdict, "resolution", resolution);	
	PyDict_SetItemString(retdict, "map_name", map_name);	
	//fprintf(stderr, "here3\n");	
	Py_DECREF(pymap);
	Py_DECREF(x_size);
	Py_DECREF(y_size);
	Py_DECREF(resolution);	
	Py_DECREF(map_name);
	//fprintf(stderr, "here3\n");	
	$result = retdict;
    carmen_map_destroy(&$1);
 }




%typemap(out) EKF2D_filter_state*{
	PyObject* pyvec = PyList_New(0);

	if($1 == NULL)
	    return pyvec;

	gsl_vector* Uswig = $1->U;
	gsl_matrix* SIGMAswig = $1->SIGMA;
	gsl_vector* assoc_swig = $1->associations;
    int i;
	for(i=0; i<Uswig->size; i++){
        PyObject* pt = PyFloat_FromDouble(gsl_vector_get(Uswig, i));
        PyList_Append(pyvec, pt);
        Py_DECREF(pt);
	}

	/*save the associations*/
	PyObject* pyvec2 = PyList_New(0);
	if(assoc_swig != NULL){
		for(i=0; i<assoc_swig->size; i++){
            PyObject* pt = PyFloat_FromDouble(gsl_vector_get(assoc_swig, i));
            PyList_Append(pyvec2, pt);
            Py_DECREF(pt);
		}
	}

	PyObject* mat = PyList_New(0);

	if(SIGMAswig == NULL)
	    return mat;

	for(i=0; i<SIGMAswig->size1; i++){
        PyObject* row = PyList_New(0);	
        int j;
        for(j=0;j<SIGMAswig->size2;j++){
            PyObject* pt = PyFloat_FromDouble(gsl_matrix_get(SIGMAswig, i, j));
            PyList_Append(row, pt);
            Py_DECREF(pt);
        }
        PyList_Append(mat, row);
        Py_DECREF(row);
	}

	//have mat and pyvec add to hash
	PyObject *retdict = PyDict_New();
	PyDict_SetItemString(retdict, "U", pyvec);
	PyDict_SetItemString(retdict, "associations", pyvec2);
	PyDict_SetItemString(retdict, "SIGMA", mat);
		
	Py_DECREF(mat);
	Py_DECREF(pyvec);
	Py_DECREF(pyvec2);
		
	$result = retdict;

	gsl_matrix_free($1->SIGMA);
	gsl_vector_free($1->U);
	if($1->associations != NULL)
		gsl_vector_free($1->associations);
    free((EKF2D_filter_state *) $1);
	
 }


%typemap(out) gsl_vector*{
	PyObject* pyvec = PyList_New(0);

	if($1 == NULL)
	    return pyvec;

    int i;
	for(i=0; i<$1->size; i++){
        PyObject* pt = PyFloat_FromDouble(gsl_vector_get($1, i));
        PyList_Append(pyvec, pt);
        Py_DECREF(pt);
	}
	
	gsl_vector_free($1);
	$result = pyvec;
 }


%typemap(out) gsl_permutation*{
	PyObject* pyvec = PyList_New(0);

	if($1 == NULL)
	    return pyvec;

	gsl_vector* gsl_vec = tklib_permutation_to_vector($1);

    int i;
	for(i=0; i<gsl_vec->size; i++){
        PyObject* pt = PyInt_FromLong((long)gsl_vector_get(gsl_vec, i));
        PyList_Append(pyvec, pt);
        Py_DECREF(pt);
	}
	
	gsl_vector_free(gsl_vec);
	gsl_permutation_free($1);
	$result = pyvec;
 }


%typemap(out) gsl_matrix*{
	PyObject* mat = PyList_New(0);

	if($1 == NULL)
	    return mat;

    int i;
	for(i=0; i<$1->size1; i++){
        PyObject* row = PyList_New(0);	
        int j;
        for(j=0;j<$1->size2;j++){
            PyObject* pt = PyFloat_FromDouble(gsl_matrix_get($1, i, j));
            PyList_Append(row, pt);
            Py_DECREF(pt);
        }
        PyList_Append(mat, row);
        Py_DECREF(row);
	}

	gsl_matrix_free($1);
	$result = mat;
 }


%typemap(out) gsl_matrix_float*{
	PyObject* mat = PyList_New(0);

	if($1 == NULL)
	    return mat;

    int i;
	for(i=0; i<$1->size1; i++){
        PyObject* row = PyList_New(0);	
        int j;
        for(j=0;j<$1->size2;j++){
            PyObject* pt = PyFloat_FromDouble((double)gsl_matrix_float_get($1, i, j));
            PyList_Append(row, pt);
            Py_DECREF(pt);
        }
        PyList_Append(mat, row);
        Py_DECREF(row);
	}

	gsl_matrix_float_free($1);
	$result = mat;
 }

%typemap(in) erlcm_gridmap_t* {
    return NULL;
    if (!PySequence_Check($input)) {
        PyErr_SetString(PyExc_TypeError,"Expecting a sequence");
        return NULL;
    }

    
 }

%typemap(in) gsl_vector* {
    int i, my_len;
    if (!PySequence_Check($input)) {
        PyErr_SetString(PyExc_TypeError,"Expecting a sequence");
        return NULL;
    }

    my_len = PyObject_Length($input);

    if(my_len == 0){
        $1=NULL;
    }
    else{
        gsl_vector* vec = gsl_vector_calloc(my_len);

        for (i =0; i < my_len; i++) {
            PyObject *o = PySequence_GetItem($input,i);
            if (!PyFloat_Check(o) && !PyInt_Check(o)) {
                PyErr_SetString(PyExc_ValueError,"Expecting a sequence of doubles");
                return NULL;
            }
            gsl_vector_set(vec, i, PyFloat_AsDouble(o));
            Py_DECREF(o);
        }
        $1 = vec;
    }
 }



%typemap(in) gsl_vector_float* {
    int i, my_len;
    if (!PySequence_Check($input)) {
        PyErr_SetString(PyExc_TypeError,"Expecting a sequence");
        return NULL;
    }

    my_len = PyObject_Length($input);

    if(my_len == 0){
        $1=NULL;
    }
    else{
        gsl_vector_float* vec = gsl_vector_float_calloc(my_len);

        for (i =0; i < my_len; i++) {
            PyObject *o = PySequence_GetItem($input,i);
            if (!PyFloat_Check(o) && !PyInt_Check(o)) {
                PyErr_SetString(PyExc_ValueError,"Expecting a sequence of doubles");
                return NULL;
            }
            gsl_vector_float_set(vec, i, (float)PyFloat_AsDouble(o));
            Py_DECREF(o);
        }
        $1 = vec;
    }
 }

%typemap(freearg) gsl_vector * {

    if($1 != NULL)
        gsl_vector_free($1);
 }

%typemap(freearg) gsl_vector_float * {

    if($1 != NULL)
        gsl_vector_float_free($1);
 }


%typemap(freearg) gsl_matrix * {
    if($1 != NULL)
        gsl_matrix_free($1);
 }

%typemap(freearg) gsl_matrix_float * {
    if($1 != NULL)
        gsl_matrix_float_free($1);
 }

%typemap(in) gsl_matrix* {
    $1 = psu_matrix_from_python($input);
 }

%typemap(in) marray* {
    //printf("converting marray\n");
    if (!PySequence_Check($input)) {
        PyErr_SetString(PyExc_TypeError,"Expecting a sequence");
        return NULL;
    }
    else{
        //printf("wrap gsl_matrix2 %d\n", my_len2);  

        bool done = false;
        int rank = 0;  
        PyObject *o = $input;
        while(!done){
            if (PyList_Check(o)) {
                o = PyList_GetItem(o,0);
                rank+=1;
            }
            else
                done=true;
	  
        }

        size_t i = 0;
        done = false;
        size_t mat_size[rank];
        o = $input;
        while(!done){
            if (PyList_Check(o)){
                mat_size[i] = PyList_Size(o);
                //printf("size: %d, %d ", i, mat_size[i]);
                o = PyList_GetItem(o,0);
                i+=1;
            }
            else
                done=true;
        }
      
        //printf("\n");
      
        marray* mat = marray_calloc(rank, mat_size);
      
        size_t curr_index[rank];
        for(size_t i=0; i<rank; i++){
            curr_index[i] = 0;
        }
      
        done = false;
        while(!done){
            //printf("in loop\n");
            PyObject *o = $input;
            //PyObject *o_new;
	  	  	  	  
            for(size_t k=0; k<rank; k++){
                o = PyList_GetItem(o,curr_index[k]);
            }
            marray_set(mat, curr_index, PyFloat_AsDouble(o));
	    	  
            curr_index[0]+=1;
	  
            for(size_t i=0; i<rank; i++){
                if(curr_index[i] == mat_size[i]){
	      
                    if(i+1 < rank){
                        curr_index[i+1]+=1;
                        curr_index[i]=0;
                    }
                    else{
                        done = true;
                        break;
                    }
                }
                else
                    break;
            }
        }
           
        $1 = mat;
    }
 }

//// Map a Python sequence into any sized C double array
%typemap(in) double* {
    int i, my_len;
    if (!PySequence_Check($input)) {
        PyErr_SetString(PyExc_TypeError,"Expecting a sequence");
        return NULL;
    }

    my_len = PyObject_Length($input);
    double *temp = (double*)calloc(my_len,sizeof(double));
    for (i =0; i < my_len; i++) {
        PyObject *o = PySequence_GetItem($input,i);
        if (!PyFloat_Check(o) && !PyInt_Check(o)) {
            PyErr_SetString(PyExc_ValueError,"Expecting a sequence of doubles");
            return NULL;
        }
        temp[i] = PyFloat_AsDouble(o);
        Py_DECREF(o);
    }
    $1 = temp;
 }


//// Map a Python sequence into any sized C int array
%typemap(in) int* {
    int i, my_len;
    if (!PySequence_Check($input)) {
        PyErr_SetString(PyExc_TypeError,"Expecting a sequence");
        return NULL;
    }

    my_len = PyObject_Length($input);
    int *temp = (int*)calloc(my_len,sizeof(int));
    for (i =0; i < my_len; i++) {
        PyObject *o = PySequence_GetItem($input,i);
        if (!PyFloat_Check(o) && !PyInt_Check(o)) {
            PyErr_SetString(PyExc_ValueError,"Expecting a sequence of doubles");
            return NULL;
        }
        temp[i] = (int)PyFloat_AsDouble(o);
        Py_DECREF(O);
    }
    $1 = temp;
 }




//// Map a Python sequence into any sized C double array
%typemap(in) float* {
    int i, my_len;
    if (!PySequence_Check($input)) {
        PyErr_SetString(PyExc_TypeError,"Expecting a sequence");
        return NULL;
    }

    my_len = PyObject_Length($input);
    float *temp = (float*)calloc(my_len,sizeof(float));
    for (i =0; i < my_len; i++) {
        PyObject *o = PySequence_GetItem($input,i);
        if (!PyFloat_Check(o) && !PyInt_Check(o)) {
            PyErr_SetString(PyExc_ValueError,"Expecting a sequence of doubles");
            return NULL;
        }
        temp[i] = (float)PyFloat_AsDouble(o);
        Py_DECREF(o);
    }
    $1 = temp;
 }



%typemap(in) math3d_prism_t {
    math3d_prism_t prism = math3d_prism_init();
    {
        PyObject * zStart = PyObject_GetAttrString($input, "zStart");
        if (zStart == NULL) {
            PyErr_SetString(PyExc_ValueError,"Couldn't get zStart.");
            return NULL;
        }

        prism.z_start = PyFloat_AsDouble(zStart);
        Py_DECREF(zStart);
    }
    {
        PyObject * zEnd = PyObject_GetAttrString($input, "zEnd");
        if (zEnd == NULL) {
            PyErr_SetString(PyExc_ValueError,"Couldn't get zEnd.");
            return NULL;
        }

        prism.z_end = PyFloat_AsDouble(zEnd);
        Py_DECREF(zEnd);
    }
    {
        PyObject * points_xy = PyObject_GetAttrString($input, "points_xy");
        prism.points_xy = psu_matrix_from_python(points_xy);
        Py_DECREF(points_xy);
    }      
    $1 = prism;
 }

%typemap(freearg) math3d_prism_t {
    if ($1.initialized == 1) {
        math3d_prism_free($1);
    }
 }


%feature("director") pyTklibHandler;

//%include "carmen_messages.h"
%include "std_string.i"
%include "std_vector.i"
%include "typemaps.i"
%include "carrays.i"
%array_class(double, doubleArray);
%array_class(float, floatArray);
%include "procrustes.hpp"
%include "kmeans.h"
%include "probability.h"
%include "nearest_neighbor.h"
%include <gsl_swig_utils/gsl_utilities.h>
%include "math2d.h"
%include "math3d.h"
%include "avs.h"
 //%include "spatial_features.h"
 //%include "spatial_feature_extractor.h"
%include "line.h"
%include "box_window.h"
%include "gsl_python.h"
%include "hurdle_extractor.h"
%include "gaussian.h"
%include "EKF2D.h"
%include "tklib_log_gridmap.hpp"
%include "gridmapping.hpp"
 //%include "noise_models.hpp"
 //%include "simulator.h"
%include "carmen_util.h"
 //%include "crf_utils.h"
%include "spline.hpp"
%include "python_swig_utils.h"
 //%include "carmen_subscribe.h"
 //%include "carmen_publish.h"


namespace std{
    %template(vectori) vector<int>;
     %template(vectord) vector<double>;
     %template(vectors) vector<string>;
 }

/* turn on director wrapping Callback */
#define __attribute__(x)


// This tells SWIG to treat char ** as a special case
%typemap(in) char ** {
    /* Check if is a list */
    if (PyList_Check($input)) {
        int size = PyList_Size($input);
        int i = 0;
        $1 = (char **) malloc((size+1)*sizeof(char *));
        for (i = 0; i < size; i++) {
            PyObject *o = PyList_GetItem($input,i);
            if (PyString_Check(o))
                $1[i] = PyString_AsString(PyList_GetItem($input,i));
            else {
                PyErr_SetString(PyExc_TypeError,"list must contain strings");
                free($1);
                return NULL;
            }
        }
        $1[i] = 0;
    } else {
        PyErr_SetString(PyExc_TypeError,"not a list");
        return NULL;
    }
 }




//%include <carmen/ipc_wrapper.h>




