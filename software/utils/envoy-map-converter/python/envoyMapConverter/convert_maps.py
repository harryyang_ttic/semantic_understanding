import sys
from lcm import EventLog
from erlcm.multi_gridmap_t import multi_gridmap_t
from erlcm.floor_gridmap_t import floor_gridmap_t
from erlcm.gridmap_t import gridmap_t
from erlcm.rect_list_t import rect_list_t as erlcm_rect_list_t
from erlcm.rect_t import rect_t as erlcm_rect_t
from sulcm.rect_list_t import rect_list_t as sulcm_rect_list_t
from sulcm.rect_t import rect_t as sulcm_rect_t

fname_in = sys.argv[1]

if len(sys.argv) == 3:
    fname_out_prefix = sys.argv[2] + "_floor_"
else:
    fname_out_prefix = sys.argv[1] + "_floor_"

map_log_original = EventLog(fname_in, "r")

print >> sys.stderr, "opened %s" % fname_in

EventLogs = {}
#LogFiles = []
Floors = []
Maps = []


def get_filename (prefix, floor_no):
    return prefix + '%d.lcmlog' % (floor_no)


for e in map_log_original:

    if e.channel == "MULTI_FLOOR_MAPS":
        multi_grid = multi_gridmap_t.decode (e.data)

        for floor in multi_grid.maps:

            floor_no = floor.floor_no
            fname_out = get_filename(fname_out_prefix, floor_no)
            if floor_no not in EventLogs:
                map_log_new = EventLog(fname_out, "w", overwrite=True)
                EventLogs[floor_no] = map_log_new
            else:
                map_log_new = EventLogs[floor_no]

            if floor_no not in Maps:
                print "Saving map for floor %d to %s" % (floor_no, fname_out)
                gridmap = gridmap_t()
                gridmap.size = floor.gridmap.size
                gridmap.type = floor.gridmap.type
                gridmap.compressed = floor.gridmap.compressed
                gridmap.map_version = floor.gridmap.map_version
                gridmap.config = floor.gridmap.config
                gridmap.center = floor.gridmap.center
                gridmap.utime = floor.gridmap.utime
                gridmap.map = floor.gridmap.map
                gridmap.sizeX2 = floor.gridmap.sizeX2
                gridmap.sizeY2 = floor.gridmap.sizeY2
                
                map_log_new.write_event (e.timestamp, "GRIDMAP", gridmap.encode())
                Maps.append(floor_no)

    if e.channel == "SIM_RECTS":
        envoy_rects = erlcm_rect_list_t.decode (e.data)

        su_rects = sulcm_rect_list_t()
        su_rects.utime = envoy_rects.utime
        su_rects.xy[0] = envoy_rects.xy[0]
        su_rects.xy[1] = envoy_rects.xy[1]
        su_rects.num_rects = envoy_rects.num_rects

        rect_list = {}
        for envoy_rect in envoy_rects.rects:
            su_rect = sulcm_rect_t()
            su_rect.dxy[0] = envoy_rect.dxy[0]
            su_rect.dxy[1] = envoy_rect.dxy[1]
            su_rect.size[0] = envoy_rect.size[0]
            su_rect.size[1] = envoy_rect.size[1]
            su_rect.theta = envoy_rect.theta

            floor_no = envoy_rect.floor_no
            if envoy_rect.floor_no not in rect_list:
                rect_list[floor_no] = []
                
            rect_list[floor_no].append(su_rect)

        for floor_no in rect_list:
            su_rects = sulcm_rect_list_t()
            su_rects.utime = envoy_rects.utime
            su_rects.xy[0] = envoy_rects.xy[0]
            su_rects.xy[1] = envoy_rects.xy[1]
            su_rects.num_rects = len(rect_list[floor_no])

            for rect in rect_list[floor_no]:
                su_rects.rects.append(rect)


            fname_out = get_filename(fname_out_prefix, floor_no)
            if floor_no not in EventLogs:
                map_log_new = EventLog(fname_out, "w", overwrite=True)
                EventLogs[floor_no] = map_log_new
            else:
                map_log_new = EventLogs[floor_no]
                
        
            print "Saving sim rects for floor %d to %s" % (floor_no, fname_out)
            map_log_new.write_event (e.timestamp, "SIM_RECTS", su_rects.encode())
