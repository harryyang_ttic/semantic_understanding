/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General 
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <bot_core/bot_core.h>
#include <lcm/lcm.h>

#include <lcmtypes/er_lcmtypes.h>
#include <lcmtypes/bot_core_planar_lidar_t.h>

#include <bot_param/param_client.h>
#include <lcmtypes/bot2_param.h>
#include <bot_param/param_util.h>

#include "robot3d_interface.h"

#define MANUAL_THRESHOLD 3
#define JS_IGNORE_THRESHOLD 20
typedef struct
{
    double max_t_vel;
    double max_r_vel;
    double approach_dist; 
    double side_dist; 
    double length;
    double width;
    double acceleration;
    double deceleration;
    int allow_rear_motion; 
    double sensor_timeout; 
} robot3d_config_t; 

typedef struct
{
    double tv;
    double rv; 
    int64_t utime; 
} velocity_cmd_t; 


typedef struct
{
    BotParam   *b_server;
    lcm_t      *lcm;
    //    ATrans    *atrans;
    GMainLoop *mainloop;
    pthread_t  work_thread;
    int sleep_us;
    robot3d_config_t *config;
    //double pos[3]; 
    bot_core_pose_t *last_pose;
    bot_core_pose_t *last_deadreckon_pose;
    erlcm_raw_odometry_msg_t *last_base_msg; 
    velocity_cmd_t cmd; 
    int verbose; 
    int use_scan_matched; 
    int use_dead_reckoned;
    int publish_as_pose;

    erlcm_robot_status_t *status; 

    int robot_manual;  // 0 - standby 1 - manual 
    int j1_count;
    int j0_count;
} state_t;



static void lcm_pose_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                             const char * channel __attribute__((unused)),
                             const bot_core_pose_t * msg, 
                             void * user  __attribute__((unused))) {

    state_t *s = (state_t *) user; 

    if (s->use_dead_reckoned) {
        if(s->last_deadreckon_pose)
            bot_core_pose_t_destroy (s->last_deadreckon_pose);

        s->last_deadreckon_pose = bot_core_pose_t_copy (msg);

        //publish this back for global_navigator and for localizer if it isn't using robot_laser messages
        bot_core_pose_t_publish (s->lcm, "ROBOT_DEADRECKON", s->last_deadreckon_pose);
        
        if(s->verbose)
            fprintf(stderr,"Deadreckoned Pose : (%f,%f)\n",msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);

        if(s->publish_as_pose){
            bot_core_pose_t_publish (s->lcm, "POSE", s->last_deadreckon_pose);
        }

    }
    else {
        if(s->last_pose)
            bot_core_pose_t_destroy(s->last_pose);
       
        s->last_pose = bot_core_pose_t_copy(msg);
        if(s->verbose)
            fprintf(stderr,"Pose : (%f,%f)\n",msg->pos[0], msg->pos[1]);//, rpy[2], vel[0] , vel[1]);
    }
}

static void publish_vector_status(state_t *s, double distance, double angle)
{
    //Publishing lcm vector status message 
    erlcm_vector_msg_t lcm_msg;
    lcm_msg.distance = distance;
    lcm_msg.theta = angle;
    lcm_msg.utime = bot_timestamp_now (); 
    erlcm_vector_msg_t_publish(s->lcm,"ROBOT_VECTOR_STATUS",&lcm_msg);
}

void lcm_carmen_robot_send_base_velocity_command(state_t *s)
{
    erlcm_velocity_msg_t v;

    if (! (s->config->allow_rear_motion) && s->cmd.tv < 0)
        s->cmd.tv = 0.0;
  
    v.tv = bot_clamp(s->cmd.tv, -s->config->max_t_vel, 		      
                     s->config->max_t_vel);

    v.rv = bot_clamp(s->cmd.rv, -s->config->max_r_vel, 		      
                     s->config->max_r_vel);

    v.utime = bot_timestamp_now (); //carmen_get_time()*1e6;
  
    //erlcm_velocity_msg_t_publish(s->lcm,"BASE_VELOCITY_CMD",&v);
}

static void lcm_status_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                 const char * channel __attribute__((unused)), 
                                 const erlcm_robot_status_t * msg,
                                 void * user  __attribute__((unused)))
{  
  
    state_t *s = (state_t *) user; 
    if(s->status){
        erlcm_robot_status_t_destroy(s->status);
    }
    s->status = erlcm_robot_status_t_copy(msg);
}


static void lcm_velocity_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                                 const char * channel __attribute__((unused)), 
                                 const erlcm_velocity_msg_t * msg,
                                 void * user  __attribute__((unused)))
{  
  
    state_t *s = (state_t *) user; 
    if(s->verbose){
        fprintf(stderr,"Received LCM velocity command\n");
    }
  
    s->cmd.utime = msg->utime; 
  
    s->cmd.rv = msg->rv;
    s->cmd.tv = msg->tv;
  
    lcm_carmen_robot_send_base_velocity_command(s);
    publish_vector_status(s, 0, 0);
}


/*void lcm_base_full_stat_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
  const char * channel __attribute__((unused)), 
  const erlcm_full_stat_msg_t * msg,
  void * user  __attribute__((unused)))
  {
  state_t *s = (state_t *) user; 
  
  
  if (s->last_base_msg  != NULL) {
  //  static erlcm_raw_odometry_msg_t* staticmsg = NULL;
  double delta[3] = {s->last_base_msg->x - msg->x, 
  s->last_base_msg->y - msg->y,
  s->last_base_msg->theta - msg->theta}; 
  fprintf(stderr,"Odom Pose : (%f,%f, %f) , Delta : %f, Delta Theta : %f\n",msg->x, msg->y, msg->theta, hypot(delta[0], delta[1]), delta[2]);
  erlcm_raw_odometry_msg_t_destroy(s->last_base_msg);
  }
  s->last_base_msg = erlcm_raw_odometry_msg_t_copy(msg);
  //if(s->verbose){
  
  //}
  }*/

void lcm_base_odometry_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                               const char * channel __attribute__((unused)), 
                               const erlcm_raw_odometry_msg_t * msg,
                               void * user  __attribute__((unused)))
{
    state_t *s = (state_t *) user; 
  
    if (s->last_base_msg  != NULL) {
        //  static erlcm_raw_odometry_msg_t* staticmsg = NULL;
        double delta[3] = {s->last_base_msg->x - msg->x, 
                           s->last_base_msg->y - msg->y,
                           s->last_base_msg->theta - msg->theta};
        if(s->verbose){
            fprintf(stderr,"Odom Pose : (%f,%f, %f) , Delta : %f, Delta Theta : %f\n",msg->x, msg->y, msg->theta, hypot(delta[0], delta[1]), delta[2]);
        }
        erlcm_raw_odometry_msg_t_destroy(s->last_base_msg);
    }
    s->last_base_msg = erlcm_raw_odometry_msg_t_copy(msg);

    //publish this back 
    bot_core_pose_t b_pose; 
    memset(&b_pose, 0, sizeof(bot_core_pose_t));

    b_pose.utime = msg->utime; 
    b_pose.pos[0] = msg->x;
    b_pose.pos[1] = msg->y;

    double rpy_body_to_local_level[] = {0, 0, msg->theta};
    double q_body_to_local_level[4];
    bot_roll_pitch_yaw_to_quat (rpy_body_to_local_level, b_pose.orientation);

    bot_core_pose_t_publish (s->lcm, "ROBOT_DEADRECKON", &b_pose);

    if(s->publish_as_pose){
        bot_core_pose_t_publish (s->lcm, "POSE", &b_pose);
    }
    
    //if(s->verbose){
  
    //}
    /*lcm_odometry_to_carmen_odometry(staticmsg,&carmen_robot_latest_odometry);
      base_odometry_handler();*/
}


static void *track_work_thread(void *user)
{
    state_t *s = (state_t*) user;

    printf("obstacles: track_work_thread()\n");

    while(1){
        //      fprintf(stderr, " T - called ()\n");
        //do better handling of this - not sure if this is needed - might be needed for applying controls 
        //velocity status
        if(s->last_base_msg  != NULL){
            erlcm_velocity_msg_t vel_msg;
            vel_msg.utime = s->last_base_msg->utime;
            vel_msg.tv = s->last_base_msg->tv;
            vel_msg.rv = s->last_base_msg->rv;
            erlcm_velocity_msg_t_publish(s->lcm,"ROBOT_VELOCITY_STATUS",&vel_msg);
            if(s->verbose){
                fprintf(stderr,"V");
            }
        }
        usleep(s->sleep_us);
    }
}


void read_parameters_from_conf(state_t *s)
{
    BotParam *b_param = s->b_server; 
    robot3d_config_t *config = s->config;
    config->max_t_vel =  bot_param_get_double_or_fail(b_param, "robot.max_t_vel");
    config->max_r_vel = bot_param_get_double_or_fail(b_param, "robot.max_r_vel");

    config->approach_dist = bot_param_get_double_or_fail(b_param, "robot.min_approach_dist");
    config->side_dist = bot_param_get_double_or_fail(b_param, "robot.min_side_dist");

    config->length = bot_param_get_double_or_fail(b_param, "robot.length");
    config->width = bot_param_get_double_or_fail(b_param, "robot.width");

    config->acceleration =  bot_param_get_double_or_fail(b_param, "robot.acceleration");
    config->deceleration =  bot_param_get_double_or_fail(b_param, "robot.deceleration");
  
    config->allow_rear_motion = bot_param_get_boolean_or_fail(b_param,"robot.allow_rear_motion");
    config->sensor_timeout = bot_param_get_double_or_fail(b_param, "robot.sensor_timeout");
}

//doesnt do anything right now 
gboolean heartbeat_cb (gpointer data)
{
    state_t *s = (state_t *)data;
  
    //do the periodic stuff 
    if(s->verbose){
        fprintf(stderr, "Callback - Timeout\n");
    }
    //return true to keep running
    return TRUE;
}

void subscribe_to_channels(state_t *s)
{
    if(s->use_scan_matched)
        erlcm_raw_odometry_msg_t_subscribe(s->lcm, "SM_ODOMETRY", lcm_base_odometry_handler,s);
    else if (s->use_dead_reckoned)
        bot_core_pose_t_subscribe (s->lcm, "POSE_DEADRECKON", lcm_pose_handler, s);
    else
        erlcm_raw_odometry_msg_t_subscribe(s->lcm, "ODOMETRY", lcm_base_odometry_handler,s); 

    //command handlers 
    erlcm_velocity_msg_t_subscribe(s->lcm, "ROBOT_VELOCITY_CMD", 
                                   lcm_velocity_handler, s);

    erlcm_robot_status_t_subscribe(s->lcm, "ROBOT_STATUS", 
                                   lcm_status_handler, s);
}

static void usage(char * funcName)
{
    printf("Usage: %s [options]\n"
           "options are:\n"
           "--help,          -h    display this message \n"
           "--use-scanmatch  -s    use scan-matched odometry \n"
           "--use-deadreckon -d    use dead-reckoned pose \n"
           "--pose           -p    only publish odometry (default) or deadreackon pose as pose (no robot laser) \n"
           "--verbose,       -v    be verbose\n", funcName);
    exit(1);

}

int 
main(int argc, char **argv)
{
    g_thread_init(NULL);
    setlinebuf (stdout);
    state_t *state = (state_t*) calloc(1, sizeof(state_t));
    state->sleep_us = 100*1e3;
    state->last_pose = NULL;
    state->last_base_msg = NULL;
    state->status = NULL;

    //this should be updated by the wheelchair ??

    const char *optstring = "shvdrp";
    struct option long_opts[] = { { "help", no_argument, 0, 'h' },
                                  { "publish_as_pose", no_argument, 0, 'p' }, 
                                  { "verbose", no_argument, 0, 'v' }, 
                                  { "use-scanmatch", no_argument, 0, 's' }, 
                                  { "use-deadreckon", no_argument, 0, 'd' },
                                  { 0, 0, 0, 0 } };

    int c;
    while ((c = getopt_long(argc, argv, optstring, long_opts, 0)) >= 0) {
        switch (c) {
        case 'h':
            usage(argv[0]);
            break;
        case 'p':
            {
                fprintf(stderr,"Publishing as pose\n");
                state->publish_as_pose = 1;
                break;
            }
        case 's':
            {
                fprintf(stderr,"Using Scan matched Odom\n");
                state->use_scan_matched = 1;
                break;
            }
        case 'd':
            {
                fprintf(stderr,"Using dead-reckon-based pose\n");
                state->use_dead_reckoned = 1;
                break;
            }
        case 'v':
            {
                fprintf(stderr,"Verbose\n");
                state->verbose = 1;
                break;
            }
        default:
            {
                usage(argv[0]);
                break;
            }
        }
    }

    if (state->use_scan_matched && state->use_dead_reckoned) {
        fprintf (stderr, "Using both scan matched and dead-reckoned pose currently not supported\n");
        return -1;
    }
    

    state->config = (robot3d_config_t *) calloc(1, sizeof(robot3d_config_t));
    //this does not attach the lcm to glib mainloop - so we need to do this mannully
    state->lcm =  bot_lcm_get_global(NULL);
    state->b_server = bot_param_new_from_server(state->lcm, 1);

    pthread_create(&state->work_thread, NULL, track_work_thread, state);

    subscribe_to_channels(state);
  
    state->mainloop = g_main_loop_new( NULL, FALSE );  
  
    if (!state->mainloop) {
        printf("Couldn't create main loop\n");
        return -1;
    }

    bot_glib_mainloop_attach_lcm (state->lcm);

    read_parameters_from_conf(state);

    /* heart beat*/
    g_timeout_add_seconds (1, heartbeat_cb, state);

    //g_timeout_source_new () - to add ms level timeout source

    bot_signal_pipe_glib_quit_on_kill (state->mainloop);
    
    fprintf(stderr, "Starting Main Loop\n");
    ///////////////////////////////////////////////
    g_main_loop_run(state->mainloop);
  
    bot_glib_mainloop_detach_lcm(state->lcm);
}


