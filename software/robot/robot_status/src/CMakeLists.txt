add_definitions(
#    -ggdb3 
    -std=gnu99
    )

include_directories(
    ${GLIB2_INCLUDE_DIRS}
    ${LCMTYPES_INCLUDE_DIRS}
    )

add_executable(er-robot-status main.c)

#target_link_libraries (er-robot-status ${LCMTYPES_LIBS})

pods_use_pkg_config_packages(er-robot-status
    glib-2.0
    gthread-2.0
    lcm 
    bot2-core 
    lcmtypes_er-lcmtypes
    lcmtypes_bot2-core
    bot2-param-client)
    

pods_install_executables(er-robot-status)
