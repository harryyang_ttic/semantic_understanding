# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/cmake-gui

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build

# Include any dependencies generated for this target.
include src/CMakeFiles/er-map-editor.dir/depend.make

# Include the progress variables for this target.
include src/CMakeFiles/er-map-editor.dir/progress.make

# Include the compile flags for this target's objects.
include src/CMakeFiles/er-map-editor.dir/flags.make

src/CMakeFiles/er-map-editor.dir/map_editor.c.o: src/CMakeFiles/er-map-editor.dir/flags.make
src/CMakeFiles/er-map-editor.dir/map_editor.c.o: ../src/map_editor.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/CMakeFiles/er-map-editor.dir/map_editor.c.o"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/er-map-editor.dir/map_editor.c.o   -c /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor.c

src/CMakeFiles/er-map-editor.dir/map_editor.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/er-map-editor.dir/map_editor.c.i"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor.c > CMakeFiles/er-map-editor.dir/map_editor.c.i

src/CMakeFiles/er-map-editor.dir/map_editor.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/er-map-editor.dir/map_editor.c.s"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor.c -o CMakeFiles/er-map-editor.dir/map_editor.c.s

src/CMakeFiles/er-map-editor.dir/map_editor.c.o.requires:
.PHONY : src/CMakeFiles/er-map-editor.dir/map_editor.c.o.requires

src/CMakeFiles/er-map-editor.dir/map_editor.c.o.provides: src/CMakeFiles/er-map-editor.dir/map_editor.c.o.requires
	$(MAKE) -f src/CMakeFiles/er-map-editor.dir/build.make src/CMakeFiles/er-map-editor.dir/map_editor.c.o.provides.build
.PHONY : src/CMakeFiles/er-map-editor.dir/map_editor.c.o.provides

src/CMakeFiles/er-map-editor.dir/map_editor.c.o.provides.build: src/CMakeFiles/er-map-editor.dir/map_editor.c.o

src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o: src/CMakeFiles/er-map-editor.dir/flags.make
src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o: ../src/map_editor_drawing.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o   -c /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_drawing.c

src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/er-map-editor.dir/map_editor_drawing.c.i"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_drawing.c > CMakeFiles/er-map-editor.dir/map_editor_drawing.c.i

src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/er-map-editor.dir/map_editor_drawing.c.s"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_drawing.c -o CMakeFiles/er-map-editor.dir/map_editor_drawing.c.s

src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o.requires:
.PHONY : src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o.requires

src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o.provides: src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o.requires
	$(MAKE) -f src/CMakeFiles/er-map-editor.dir/build.make src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o.provides.build
.PHONY : src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o.provides

src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o.provides.build: src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o

src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o: src/CMakeFiles/er-map-editor.dir/flags.make
src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o: ../src/map_editor_graphics.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o   -c /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_graphics.c

src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/er-map-editor.dir/map_editor_graphics.c.i"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_graphics.c > CMakeFiles/er-map-editor.dir/map_editor_graphics.c.i

src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/er-map-editor.dir/map_editor_graphics.c.s"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_graphics.c -o CMakeFiles/er-map-editor.dir/map_editor_graphics.c.s

src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o.requires:
.PHONY : src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o.requires

src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o.provides: src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o.requires
	$(MAKE) -f src/CMakeFiles/er-map-editor.dir/build.make src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o.provides.build
.PHONY : src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o.provides

src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o.provides.build: src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o

src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o: src/CMakeFiles/er-map-editor.dir/flags.make
src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o: ../src/map_editor_menus.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/er-map-editor.dir/map_editor_menus.c.o   -c /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_menus.c

src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/er-map-editor.dir/map_editor_menus.c.i"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_menus.c > CMakeFiles/er-map-editor.dir/map_editor_menus.c.i

src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/er-map-editor.dir/map_editor_menus.c.s"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/map_editor_menus.c -o CMakeFiles/er-map-editor.dir/map_editor_menus.c.s

src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o.requires:
.PHONY : src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o.requires

src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o.provides: src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o.requires
	$(MAKE) -f src/CMakeFiles/er-map-editor.dir/build.make src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o.provides.build
.PHONY : src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o.provides

src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o.provides.build: src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o

src/CMakeFiles/er-map-editor.dir/global_graphics.c.o: src/CMakeFiles/er-map-editor.dir/flags.make
src/CMakeFiles/er-map-editor.dir/global_graphics.c.o: ../src/global_graphics.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/CMakeFiles $(CMAKE_PROGRESS_5)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/CMakeFiles/er-map-editor.dir/global_graphics.c.o"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/er-map-editor.dir/global_graphics.c.o   -c /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/global_graphics.c

src/CMakeFiles/er-map-editor.dir/global_graphics.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/er-map-editor.dir/global_graphics.c.i"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/global_graphics.c > CMakeFiles/er-map-editor.dir/global_graphics.c.i

src/CMakeFiles/er-map-editor.dir/global_graphics.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/er-map-editor.dir/global_graphics.c.s"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src/global_graphics.c -o CMakeFiles/er-map-editor.dir/global_graphics.c.s

src/CMakeFiles/er-map-editor.dir/global_graphics.c.o.requires:
.PHONY : src/CMakeFiles/er-map-editor.dir/global_graphics.c.o.requires

src/CMakeFiles/er-map-editor.dir/global_graphics.c.o.provides: src/CMakeFiles/er-map-editor.dir/global_graphics.c.o.requires
	$(MAKE) -f src/CMakeFiles/er-map-editor.dir/build.make src/CMakeFiles/er-map-editor.dir/global_graphics.c.o.provides.build
.PHONY : src/CMakeFiles/er-map-editor.dir/global_graphics.c.o.provides

src/CMakeFiles/er-map-editor.dir/global_graphics.c.o.provides.build: src/CMakeFiles/er-map-editor.dir/global_graphics.c.o

# Object files for target er-map-editor
er__map__editor_OBJECTS = \
"CMakeFiles/er-map-editor.dir/map_editor.c.o" \
"CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o" \
"CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o" \
"CMakeFiles/er-map-editor.dir/map_editor_menus.c.o" \
"CMakeFiles/er-map-editor.dir/global_graphics.c.o"

# External object files for target er-map-editor
er__map__editor_EXTERNAL_OBJECTS =

bin/er-map-editor: src/CMakeFiles/er-map-editor.dir/map_editor.c.o
bin/er-map-editor: src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o
bin/er-map-editor: src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o
bin/er-map-editor: src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o
bin/er-map-editor: src/CMakeFiles/er-map-editor.dir/global_graphics.c.o
bin/er-map-editor: src/CMakeFiles/er-map-editor.dir/build.make
bin/er-map-editor: src/CMakeFiles/er-map-editor.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking C executable ../bin/er-map-editor"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/er-map-editor.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/CMakeFiles/er-map-editor.dir/build: bin/er-map-editor
.PHONY : src/CMakeFiles/er-map-editor.dir/build

src/CMakeFiles/er-map-editor.dir/requires: src/CMakeFiles/er-map-editor.dir/map_editor.c.o.requires
src/CMakeFiles/er-map-editor.dir/requires: src/CMakeFiles/er-map-editor.dir/map_editor_drawing.c.o.requires
src/CMakeFiles/er-map-editor.dir/requires: src/CMakeFiles/er-map-editor.dir/map_editor_graphics.c.o.requires
src/CMakeFiles/er-map-editor.dir/requires: src/CMakeFiles/er-map-editor.dir/map_editor_menus.c.o.requires
src/CMakeFiles/er-map-editor.dir/requires: src/CMakeFiles/er-map-editor.dir/global_graphics.c.o.requires
.PHONY : src/CMakeFiles/er-map-editor.dir/requires

src/CMakeFiles/er-map-editor.dir/clean:
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src && $(CMAKE_COMMAND) -P CMakeFiles/er-map-editor.dir/cmake_clean.cmake
.PHONY : src/CMakeFiles/er-map-editor.dir/clean

src/CMakeFiles/er-map-editor.dir/depend:
	cd /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/src /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src /home/harry/Documents/Robotics/semantic-understanding/software/world_model/map_editor/pod-build/src/CMakeFiles/er-map-editor.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/CMakeFiles/er-map-editor.dir/depend

