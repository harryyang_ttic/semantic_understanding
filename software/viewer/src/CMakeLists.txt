add_definitions(
    -std=gnu99)

#build the viewer
add_executable(er-viewer 
    udp_util.c
    main.cpp
    )

include_directories(
    ${GTK2_INCLUDE_DIRS}
    ${GLUT_INCLUDE_DIR})

target_link_libraries(er-viewer
    ${GTK2_LDFLAGS}
    ${GLUT_LIBRARIES})

pods_use_pkg_config_packages(er-viewer
    bot2-vis 
    bot2-lcmgl-renderer 
    bot2-frames-renderers 
    bot2-frames
    opencv
    er-renderers
    laser-util-renderer
    image-util-renderer
    lcmtypes_er-lcmtypes
    map3d_interface
    er-path-util
    image-util-renderer
    occ-map-renderers
    )

pods_install_executables(er-viewer)