/*
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "dp_planner.hpp"
#include <vector>
#include <list>
#include <queue>

using namespace std;
using namespace occ_map;

namespace dp_planner {

/*
 * Utility class for maintaining the score of candidate contour merges
 */
class FrontierElement2d {
public:
  float value;
  int ixy[2];
  FrontierElement2d(float v, int _ixy[2]) :
      value(v)
  {
    ixy[0] = _ixy[0];
    ixy[1] = _ixy[1];
  }
};

/*
 * comparator class that allows us to put the joins into a priority_queue
 */
class FrontierCompare2d {
public:
  bool operator()(const FrontierElement2d & f1, const FrontierElement2d & f2)
  {
    return f1.value > f2.value;
  }
};

// sets utility in meters
FloatPixelMap * createUtilityMap(const FloatPixelMap * pixmap, const double start[2])
{
  if (pixmap->readValue(start) > 0) {
    fprintf(stderr, "ERROR: start point is in an obstacle!\n");
    return NULL;
  }

  // create a blank map with cells initialized to -1
  FloatPixelMap * utility = new FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);

  int ixy[2] = { 0 };
  utility->worldToTable(start, ixy);
  utility->writeValue(ixy, 0);
  priority_queue<FrontierElement2d, std::vector<FrontierElement2d>, FrontierCompare2d> frontier;
  frontier.push(FrontierElement2d(0, ixy));

  int count = 0;
  while (!frontier.empty()) {
    count++;

    const FrontierElement2d f = frontier.top();

    assert(pixmap->readValue(f.ixy) < DPP_UNK_THRESH);

    // add all the pixel neighbors
    int ixyn[2] = { 0 };
    for (int i = -1; i <= 1; i++) {
      ixyn[0] = f.ixy[0] + i;
      for (int j = -1; j <= 1; j++) {
        ixyn[1] = f.ixy[1] + j;
        // skip myself
        if (i == 0 && j == 0)
          continue;
        // skip if out of map
        if (!utility->isInMap(ixyn))
          continue;
        // if we've already set the utility, it's optimal
        if (utility->readValue(ixyn) >= 0)
          continue;
        // check that cell is in free space
        if (pixmap->readValue(ixyn) < DPP_UNK_THRESH) {
          float dist = sqrt(i * i + j * j) * pixmap->metersPerPixel; // distance in m
          frontier.push(FrontierElement2d(f.value + dist, ixyn));

          // set a high value for the neighbor so that it doesn't get pushed
          // on to the frontier again
          // when the neighbor is expanded on the queue, it's utility will be
          // corrected
          utility->writeValue(ixyn, 1e9);
        }
        else if (pixmap->readValue(ixyn) > DPP_OCC_THRESH) {
          utility->writeValue(ixyn, INFINITY);
        }
        else {
          utility->writeValue(ixyn, INFINITY);
        }
      }
    }
    utility->writeValue(f.ixy, f.value);

    frontier.pop();
  }

  return utility;
}

/**
 * Dilate pixel map.  Dilates obstacle cells outward in x and y directions, with
 * (+/- dilation[i] / 2) pixels
 */
FloatPixelMap * dilateMap(const FloatPixelMap *pixmap, double dilation_size)
{
  FloatPixelMap* pixmap_dilated = new FloatPixelMap(pixmap, false);
  FloatPixelMap* tmp_map = new FloatPixelMap(pixmap, false);
  FloatPixelMap* dst_map;
  FloatPixelMap* src_map;

  int size_x = pixmap->dimensions[0];
  int size_y = pixmap->dimensions[1];
  int half_wsize[2];
  for (int i = 0; i < 2; i++) {
    half_wsize[i] = (int) ceil(0.5 * dilation_size / pixmap->metersPerPixel);
  }

  int ixy[2];

  //src_map = pixmap;
  dst_map = tmp_map;

  // X pass
  for (int iy = 0; iy < size_y; iy++) {
    ixy[1] = iy;
    int half_ws = half_wsize[0];
    for (int ix = 0; ix < size_x; ix++) {
      float worst_val = -INFINITY;
      for (int off = -half_ws; off < half_ws; off++) {
        ixy[0] = ix + off;
        if (ixy[0] < 0 || ixy[0] >= size_x)
          continue;
        float val = pixmap->readValue(ixy);
        if (val > worst_val)
          worst_val = val;
      }
      ixy[0] = ix;
      dst_map->writeValue(ixy, worst_val);
    }
  }

  src_map = tmp_map;
  dst_map = pixmap_dilated;

  // Y pass
  for (int ix = 0; ix < size_x; ix++) {
    ixy[0] = ix;
    int half_ws = half_wsize[1];
    for (int iy = 0; iy < size_y; iy++) {
      float worst_val = -INFINITY;
      for (int off = -half_ws; off < half_ws; off++) {
        ixy[1] = iy + off;
        if (ixy[1] < 0 || ixy[1] >= size_y)
          continue;
        float val = src_map->readValue(ixy);
        if (val > worst_val)
          worst_val = val;
      }
      ixy[1] = iy;
      dst_map->writeValue(ixy, worst_val);
    }
  }

  delete tmp_map;

  return pixmap_dilated;
}

/**
 * Update a dilated map. Allows you to specify an "active region" that has changed since the last dilation
 * Dilate pixel map.  Dilates obstacle cells outward in x and y directions, with
 * (+/- dilation[i] / 2) pixels
 */
void dilateMap(const FloatPixelMap *pixmap, double dilation_size, FloatPixelMap * pixmap_dilated_to_update,
    const int active_region_ixy0[2], const int active_region_ixy1[2])
{
  FloatPixelMap* tmp_map = new FloatPixelMap(pixmap, false);
  FloatPixelMap* dst_map;
  FloatPixelMap* src_map;

  int size_x = pixmap->dimensions[0];
  int size_y = pixmap->dimensions[1];

  int start_x = 0;
  int start_y = 0;
  if (active_region_ixy0 != NULL) {
    if (active_region_ixy0[0] < 0 || active_region_ixy0[1] < 0) {
      fprintf(stderr, "ERROR: active dilation start point (%d,%d) is outside map (0,0)\n",
          active_region_ixy0[0], active_region_ixy0[1]);
      return;
    }
    start_x = active_region_ixy0[0];
    start_y = active_region_ixy0[1];
  }
  int end_x = 0;
  int end_y = 0;
  if (active_region_ixy1 != NULL) {
    if (active_region_ixy1[0] > size_x || active_region_ixy1[1] > size_y) {
      fprintf(stderr, "ERROR: active dilation end point (%d,%d) is outside map (%d,%d)\n", active_region_ixy1[0],
          active_region_ixy1[1], size_x, size_y);
      return;
    }
    end_x = active_region_ixy1[0];
    end_y = active_region_ixy1[1];
  }

  int half_wsize[2];
  for (int i = 0; i < 2; i++) {
    half_wsize[i] = (int) ceil(0.5 * dilation_size / pixmap->metersPerPixel);
  }

  int ixy[2];

  //src_map = pixmap;
  dst_map = tmp_map;

  // X pass
  //need to expand y-region by the half_wsize so that the y-pass doesn't pull in garbage
  for (int iy = max(0, start_y - half_wsize[1]); iy < min(size_y, end_y + half_wsize[1]); iy++) {
    ixy[1] = iy;
    int half_ws = half_wsize[0];
    for (int ix = start_x; ix < end_x; ix++) {
      float worst_val = -INFINITY;
      for (int off = -half_ws; off < half_ws; off++) {
        ixy[0] = ix + off;
        if (ixy[0] < 0 || ixy[0] >= size_x)
          continue;
        float val = pixmap->readValue(ixy);
        if (val > worst_val)
          worst_val = val;
      }
      ixy[0] = ix;
      dst_map->writeValue(ixy, worst_val);
    }
  }

  src_map = tmp_map;
  dst_map = pixmap_dilated_to_update;

  // Y pass
  for (int ix = start_x; ix < end_x; ix++) {
    ixy[0] = ix;
    int half_ws = half_wsize[1];
    for (int iy = start_y; iy < end_y; iy++) {
      float worst_val = -INFINITY;
      for (int off = -half_ws; off < half_ws; off++) {
        ixy[1] = iy + off;
        if (ixy[1] < 0 || ixy[1] >= size_y)
          continue;
        float val = src_map->readValue(ixy);
        if (val > worst_val)
          worst_val = val;
      }
      ixy[1] = iy;
      dst_map->writeValue(ixy, worst_val);
    }
  }

  delete tmp_map;
}

static bool waypoints_are_collinear(const Waypoint2d& a, const Waypoint2d& b, const Waypoint2d& c)
{
  double m[2] = { c.x - a.x, c.y - a.y };
  double n[2] = { b.x - a.x, b.y - a.y };
  double t = (m[0] * n[0] + m[1] * n[1]) / (m[0] * m[0] + m[1] * m[1]);
  double cp[2] = { a.x + t * m[0], a.y + t * m[1] };
  double d[2] = { b.x - cp[0], b.y - cp[1] };
  double dist = sqrt(d[0] * d[0] + d[1] * d[1]);
  return dist < 1e-3;
}

void getPath(const occ_map::FloatPixelMap * pixmap, const std::vector<int> &path_indices,
    const Waypoint2d start_wypt, const Waypoint2d & goal_wypt, std::vector<Waypoint2d> &waypoints)
{
  waypoints.clear();
  int nwp = 0;
  fprintf(stderr, "Getting Path, World is:\n");
  for (int i = 0; i < path_indices.size(); i++) {
    int ixy[2];
    pixmap->indToLoc(path_indices[i], ixy);
    double xyt[3] = { 0 };
    pixmap->tableToWorld(ixy, xyt);

    fprintf(stderr, "\tx:%f\ty%f\n", xyt[0], xyt[1]);

    Waypoint2d wp(xyt);

    // sparsify path
    if (nwp > 1 && waypoints_are_collinear(waypoints[nwp - 2], waypoints[nwp - 1], wp)) {
      waypoints[nwp - 1] = wp; //update prev
      continue;
    }
    waypoints.push_back(wp);
    nwp++;
  }
  waypoints[0] = start_wypt; //Go from exactly where we started, not discretized version...

  //set the yaw for the waypoints
  for (int i = 1; i < waypoints.size(); i++) {
    const Waypoint2d& prev_wp = waypoints[i - 1];
    Waypoint2d& wp = waypoints[i];
    double dx = wp.x - prev_wp.x;
    double dy = wp.y - prev_wp.y;
    wp.theta = atan2(dy, dx);
  }
  waypoints[0].theta = waypoints[1].theta; //rotate in place first?
  waypoints.push_back(goal_wypt); //add the goal waypt for final yaw...
}

bool dpPathToGoal(const occ_map::FloatPixelMap * pixmap, const double start_xyt[3], const double goal_xyt[3],
    std::vector<Waypoint2d>& waypoints)
{
  if (pixmap->readValue(start_xyt) > 0) {
    fprintf(stderr, "ERROR: start point is in an obstacle!\n");
    return false;
  }
  if (pixmap->readValue(goal_xyt) > 0) {
    fprintf(stderr, "ERROR: goal point is in an obstacle!\n");
    return false;
  }

  FloatPixelMap* cost_to_go = new FloatPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);
  IntPixelMap* parents = new IntPixelMap(pixmap->xy0, pixmap->xy1, pixmap->metersPerPixel, -1);
  vector<int> path_indices;

  int ixy[2] = { 0 };
  cost_to_go->worldToTable(goal_xyt, ixy);
  int goal_ind = cost_to_go->getInd(ixy);
  cost_to_go->writeValue(ixy, 0);
  priority_queue<FrontierElement2d, std::vector<FrontierElement2d>, FrontierCompare2d> frontier;
  frontier.push(FrontierElement2d(0, ixy));

  int ixy_start[2];
  cost_to_go->worldToTable(start_xyt, ixy_start);

  //fprintf(stderr, "DP-Path-To_goal: Start x:%.2f y:%.2f t:%.2f\t Goal x:%.2f y:%.2f t:%.2f\n", start_xyt[0], start_xyt[1], start_xyt[2], goal_xyt[0], goal_xyt[1], goal_xyt[2]);
  //fprintf(stderr, "START: x:%d y:%d -- GOAL: x:%d y:%d\n", ixy_start[0], ixy_start[1], ixy[0], ixy[1]);

  int count = 0;
  while (!frontier.empty()) {
    count++;

    const FrontierElement2d f = frontier.top();

    assert(pixmap->readValue(f.ixy) < DPP_UNK_THRESH);
    //add all the neighbors
    int ixyn[2] = { 0 };
    for (int i = -1; i <= 1; i++) {
      ixyn[0] = f.ixy[0] + i;
      for (int j = -1; j <= 1; j++) {
        ixyn[1] = f.ixy[1] + j;
        if (i == 0 && j == 0)
          continue;
        if (!cost_to_go->isInMap(ixyn) || cost_to_go->readValue(ixyn) >= 0)
          continue;
        if (pixmap->readValue(ixyn) < DPP_UNK_THRESH) {
          float dist = sqrt(i * i + j * j);
          frontier.push(FrontierElement2d(f.value + dist, ixyn));

          parents->writeValue(ixyn, cost_to_go->getInd(f.ixy));

          if (ixyn[0] == ixy_start[0] && ixyn[1] == ixy_start[1]) {
            // found a path.  Traverse from start to goal to get the path
            fprintf(stderr, "FOUND PATH:\n");
            //int pxy[2];
            //fprintf(stderr, "ixy:[0] %d\t[1] %d\n", ixyn[0], ixyn[1]);

            int cur_ind = cost_to_go->getInd(ixyn);
            //fprintf(stderr, "cur_ind = %d\n", cur_ind);
            //cost_to_go->indToLoc(cur_ind, pxy);
            //fprintf(stderr, "pxy:[0] %d\t[1]%d\n", pxy[0], pxy[1]);
            for (; cur_ind != goal_ind; cur_ind = parents->data[cur_ind]) {
              path_indices.push_back(cur_ind);
              //cost_to_go->indToLoc(cur_ind, pxy);
              //fprintf(stderr, "\t- x:%d\ty:%d\n", pxy[0], pxy[1]);
            }
            path_indices.push_back(cur_ind);
            getPath(cost_to_go, path_indices, Waypoint2d(start_xyt), Waypoint2d(goal_xyt), waypoints);
            delete cost_to_go;
            delete parents;
            return true;
          }

          // set a high value for the neighbor so that it doesn't get pushed
          // on to the frontier again
          cost_to_go->writeValue(ixyn, 1e9);
        }
        else if (pixmap->readValue(ixyn) > DPP_OCC_THRESH) {
          cost_to_go->writeValue(ixyn, INFINITY);
        }
        else {
          cost_to_go->writeValue(ixyn, INFINITY);
        }
      }
    }
    cost_to_go->writeValue(f.ixy, f.value);

    frontier.pop();
  }

  delete cost_to_go;
  delete parents;

  return false;
}

}          //namespace
