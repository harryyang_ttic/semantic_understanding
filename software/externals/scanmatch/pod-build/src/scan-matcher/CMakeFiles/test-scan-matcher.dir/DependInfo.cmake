# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/harry/Documents/Robotics/semantic-understanding/software/externals/scanmatch/src/scan-matcher/rotations.c" "/home/harry/Documents/Robotics/semantic-understanding/software/externals/scanmatch/pod-build/src/scan-matcher/CMakeFiles/test-scan-matcher.dir/rotations.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/harry/Documents/Robotics/semantic-understanding/software/externals/scanmatch/src/scan-matcher/test_scan_matcher.cpp" "/home/harry/Documents/Robotics/semantic-understanding/software/externals/scanmatch/pod-build/src/scan-matcher/CMakeFiles/test-scan-matcher.dir/test_scan_matcher.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../lcmtypes/cpp"
  "../lcmtypes/c"
  "include"
  "/home/harry/Documents/Robotics/semantic-understanding/software/build/include"
  "/home/harry/Documents/Robotics/semantic-understanding/software/externals/../build/include"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "../src"
  "/usr/include/opencv"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
