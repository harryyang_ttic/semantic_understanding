cmake_minimum_required(VERSION 2.6.0)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(POD_NAME python_scripts)
include(cmake/pods.cmake)

# require python
find_package(PythonInterp REQUIRED)

# install all python files in the python/ subdirectory
pods_install_python_packages(${CMAKE_CURRENT_SOURCE_DIR}/python)

# install a script "hello-python" that runs the hello.main python module
# This script gets installed to ${CMAKE_INSTALL_PREFIX}/bin/hello-python
# and automatically sets the correct python path.

pods_install_python_script(const-vel pnc.command_velocity)

pods_install_python_script(topo-navigator pnc.topo_navigator)

pods_install_python_script(er-floor-detector pnc.floor_detector)

pods_install_python_script(log-vel-to-file log_analysis.log_velocity_to_file)

pods_install_python_script(er-host-status utils.host_status)

pods_install_python_script(er-save-pose-val log_analysis.save_pose_values)

pods_install_python_script(er-plot-control log_analysis.plot_control)

pods_install_python_script(er-get-reacquisition-command-list log_analysis.get_reacquisition_command_list)

pods_install_python_script(er-ps3-arm utils.er-ps3-arm-joy)

pods_install_python_script(er-voice-recorder audio_speech.voice_recorder)

pods_install_python_script(er-fetch-responses audio_speech.fetch_speech_files)

pods_install_python_script(er-speech-synthesizer audio_speech.speech_sythesizer)

pods_install_python_script(er-audio2summit audio_speech.er_audio_lcm)

pods_install_python_script(er-audio2lcm audio_speech.audio2lcm)

pods_install_python_script(er-lcm2audio audio_speech.lcm2audio)

pods_install_python_script(er-annotation2lcm utils.annotation2lcm)

pods_install_python_script(er-compareutimes log_analysis.compare_timestamps)