from time import time
from lcm import EventLog
from erlcm.reacquisition_segmentation_list_t import reacquisition_segmentation_list_t
import sys

fname_in = sys.argv[1]
fname_out = sys.argv[2]
log = EventLog(fname_in, "r")
new_log = EventLog(fname_out, "w", overwrite=True)

print >> sys.stderr, "opened %s" % fname_in


for e in log:

    print e.timestamp
    
    if e.channel == "TABLET_SEGMENTATION":
        seg_list = reacquisition_segmentation_list_t.decode (e.data)

        print "%d %d %d" % (e.timestamp, seg_list.utime, seg_list.num_segs)

        new_log.write_event(seg_list.utime, e.channel, e.data)

new_log.close()
