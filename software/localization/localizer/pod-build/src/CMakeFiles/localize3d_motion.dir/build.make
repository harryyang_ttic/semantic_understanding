# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/cmake-gui

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build

# Include any dependencies generated for this target.
include src/CMakeFiles/localize3d_motion.dir/depend.make

# Include the progress variables for this target.
include src/CMakeFiles/localize3d_motion.dir/progress.make

# Include the compile flags for this target's objects.
include src/CMakeFiles/localize3d_motion.dir/flags.make

src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o: src/CMakeFiles/localize3d_motion.dir/flags.make
src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o: ../src/localize3d_motion.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o   -c /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/src/localize3d_motion.c

src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/localize3d_motion.dir/localize3d_motion.c.i"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/src/localize3d_motion.c > CMakeFiles/localize3d_motion.dir/localize3d_motion.c.i

src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/localize3d_motion.dir/localize3d_motion.c.s"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/src && /usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/src/localize3d_motion.c -o CMakeFiles/localize3d_motion.dir/localize3d_motion.c.s

src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o.requires:
.PHONY : src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o.requires

src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o.provides: src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o.requires
	$(MAKE) -f src/CMakeFiles/localize3d_motion.dir/build.make src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o.provides.build
.PHONY : src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o.provides

src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o.provides.build: src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o

# Object files for target localize3d_motion
localize3d_motion_OBJECTS = \
"CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o"

# External object files for target localize3d_motion
localize3d_motion_EXTERNAL_OBJECTS =

lib/liblocalize3d_motion.a: src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o
lib/liblocalize3d_motion.a: src/CMakeFiles/localize3d_motion.dir/build.make
lib/liblocalize3d_motion.a: src/CMakeFiles/localize3d_motion.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking C static library ../lib/liblocalize3d_motion.a"
	cd /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/src && $(CMAKE_COMMAND) -P CMakeFiles/localize3d_motion.dir/cmake_clean_target.cmake
	cd /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/src && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/localize3d_motion.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/CMakeFiles/localize3d_motion.dir/build: lib/liblocalize3d_motion.a
.PHONY : src/CMakeFiles/localize3d_motion.dir/build

src/CMakeFiles/localize3d_motion.dir/requires: src/CMakeFiles/localize3d_motion.dir/localize3d_motion.c.o.requires
.PHONY : src/CMakeFiles/localize3d_motion.dir/requires

src/CMakeFiles/localize3d_motion.dir/clean:
	cd /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/src && $(CMAKE_COMMAND) -P CMakeFiles/localize3d_motion.dir/cmake_clean.cmake
.PHONY : src/CMakeFiles/localize3d_motion.dir/clean

src/CMakeFiles/localize3d_motion.dir/depend:
	cd /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/src /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/src /home/harry/Documents/Robotics/semantic-understanding/software/localization/localizer/pod-build/src/CMakeFiles/localize3d_motion.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/CMakeFiles/localize3d_motion.dir/depend

