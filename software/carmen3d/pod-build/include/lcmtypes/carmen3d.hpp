#ifndef __lcmtypes_carmen3d_hpp__
#define __lcmtypes_carmen3d_hpp__

#include "carmen3d/seg_point_list_t.hpp"
#include "carmen3d/cluster_list_t.hpp"
#include "carmen3d/place_t.hpp"
#include "carmen3d/cluster_t.hpp"
#include "carmen3d/seg_sem_vec_list_t.hpp"
#include "carmen3d/place_list_t.hpp"
#include "carmen3d/seg_sem_vec_t.hpp"
#include "carmen3d/seg_point_t.hpp"
#include "carmen3d/seg_point_list_array_t.hpp"

#endif
