/*********************************************************
 *
 * This source code is part of the Carnegie Mellon Robot
 * Navigation Toolkit (CARMEN)
 *
 * CARMEN Copyright (c) 2002 Michael Montemerlo, Nicholas
 * Roy, Sebastian Thrun, Dirk Haehnel, Cyrill Stachniss,
 * and Jared Glover
 *
 * CARMEN is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option)
 * any later version.
 *
 * CARMEN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU General 
 * Public License along with CARMEN; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307 USA
 *
 ********************************************************/

#include <er_carmen/carmen.h>
#include <bot_frames/bot_frames.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


#include <bot_core/bot_core.h>
#include <lcm/lcm.h>
#include <lcmtypes/er_lcmtypes.h>
#include <interfaces/map3d_interface.h>
#include <er_lcmtypes/lcm_channel_names.h>
#include <er_common/carmen3d_global.h>
#include <er_common/lcm_utils.h>

#include <bot_param/param_util.h>

#include "simulator.h"
#include "simulator_simulation.h"
#include "simulator_messages.h"

#include "objects3d.h"

typedef struct _sim_state_t{
    carmen_simulator_config_t *simulator_config;
    erlcm_gridmap_t* global_map_msg; // = NULL;
    erlcm_map_p global_carmen3d_map;// = NULL;
    carmen_point_t g_pos; // = {.0,.0,.0};
    int simulator_state; // = 1;
    int use_robot;// = 1;
    int publish_front_laser; 
    int publish_rear_laser;
    BotParam * conf;
    lcm_t * lcm;
    BotFrames *frames;
} sim_state_t; 

//publish the simulated odometry
    void lcm_odometry_publish(sim_state_t *s, carmen_base_odometry_message * odometry)
{
    erlcm_raw_odometry_msg_t msg;
    msg.x = odometry->x;
    msg.y = odometry->y;
    msg.theta = odometry->theta;
    msg.tv = odometry->tv;
    msg.rv = odometry->rv;
    msg.acceleration = odometry->acceleration;
    msg.utime = 1e6 * odometry->timestamp;
    erlcm_raw_odometry_msg_t_publish(s->lcm,"ODOMETRY", &msg);
}

//publish the simulated laser messages 
void planar_laser_pub(sim_state_t *s, char * CHAN, carmen_laser_laser_message * laser)
{  
    bot_core_planar_lidar_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.rad0 = laser->config.start_angle;
    msg.radstep = laser->config.angular_resolution;
    msg.nranges = laser->num_readings;
    msg.ranges = laser->range;
    msg.utime = laser->timestamp * 1e6;
    bot_core_planar_lidar_t_publish(s->lcm,CHAN,&msg);
}

void lcm_simulator_cmd_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), 
                               const char *channel __attribute__((unused)), 
                               const erlcm_simulator_cmd_t *msg, //change state msg
                               void *user)
{
    sim_state_t *s = (sim_state_t *) user;
    s->simulator_state = msg->command;   
}

//initilization handler to set the simulator true position
static void init_handler(sim_state_t *s , carmen_localize_initialize_message *init_msg)
{
    if(init_msg->distribution == CARMEN_INITIALIZE_GAUSSIAN) {
        s->simulator_config->true_pose.x = init_msg->mean[0].x;
        s->simulator_config->true_pose.y = init_msg->mean[0].y;
        s->simulator_config->true_pose.theta = init_msg->mean[0].theta;
        s->simulator_config->tv = 0;
        s->simulator_config->rv = 0;
        s->simulator_config->target_tv = 0;
        s->simulator_config->target_rv = 0;
    }
    else{
        fprintf(stderr, "Error : Non Gaussian reinit\n");
    }
}

void lcm_localize_reinitialize_handler(const lcm_recv_buf_t *rbuf __attribute__((unused)), const char *channel __attribute__((unused)), const erlcm_localize_reinitialize_cmd_t *msg,
				       void *user)
{
    sim_state_t *s = (sim_state_t *) user;
    if(s->global_map_msg ==NULL){
        return;
    }
    printf("\ngot new reinitialize msg - Setting the Simulator Position (%f, %f, %f) (%f, %f, %f)\n\n",
           msg->mean[0], msg->mean[1], msg->mean[2],
           sqrt(msg->variance[0]), sqrt(msg->variance[1]), sqrt(msg->variance[2]));

    carmen_localize_initialize_message initialize_msg;
    memset(&initialize_msg, 0, sizeof(initialize_msg));
    initialize_msg.host = "self";
    initialize_msg.timestamp = carmen_get_time();

    initialize_msg.distribution = CARMEN_INITIALIZE_GAUSSIAN;
    initialize_msg.num_modes = 1;
    carmen_point_t mean = { msg->mean[0], msg->mean[1], msg->mean[2] };
    carmen_point_t mean_map = carmen3d_map_global_to_map_coordinates(mean, s->global_carmen3d_map);

    carmen_point_t std = {
        sqrt(msg->variance[0]),
        sqrt(msg->variance[1]),
        sqrt(msg->variance[2])
    };
    initialize_msg.mean = &mean_map;
    initialize_msg.std = &std;

    init_handler(s, &initialize_msg);
}

/* handles base velocity messages */ 
static void lcm_velocity_handler(const lcm_recv_buf_t *rbuf, const char *channel, const erlcm_velocity_msg_t *msg, void *user)
{
    sim_state_t *s = (sim_state_t *) user;
    s->simulator_config->target_tv = msg->tv;
    s->simulator_config->target_rv = msg->rv;
    s->simulator_config->time_of_last_command = carmen_get_time();
}

//handles the gridmap updates
static void gridmap_handler(const lcm_recv_buf_t *rbuf, const char *channel, const erlcm_gridmap_t *msg, void *user)
{
    sim_state_t *s = (sim_state_t *) user;

    fprintf(stderr,"Gridmap Received\n");
    if (s->global_map_msg != NULL) {
        erlcm_gridmap_t_destroy(s->global_map_msg);
    }
    s->global_map_msg = erlcm_gridmap_t_copy(msg);

    fprintf(stderr,"New map recieved\n");
    carmen3d_map_uncompress_lcm_map(s->global_carmen3d_map, s->global_map_msg);

    //not sure if this is correct
    carmen_map_p map_ptr = carmen3d_map_map3d_map_copy(s->global_carmen3d_map);
    s->simulator_config->map = *map_ptr;

    carmen_point_t local_pose = carmen3d_map_global_to_map_coordinates(s->g_pos, s->global_carmen3d_map);
    s->simulator_config->true_pose.x = local_pose.x;
    s->simulator_config->true_pose.y = local_pose.y;
    s->simulator_config->true_pose.theta = local_pose.theta;
    s->simulator_config->tv = 0;
    s->simulator_config->rv = 0;
    s->simulator_config->target_tv = 0;
    s->simulator_config->target_rv = 0;

    free(map_ptr);
  
    fprintf(stderr,"Done");
}

void create_simulator_map(sim_state_t *s)
{
    carmen_map_t *map = &s->simulator_config->map;

    /* subscripe to map, and wait for it to come in... */
    erlcm_map_request_msg_t msg;
    msg.utime =  carmen_get_time()*1e6;
    msg.requesting_prog = "SIMULATOR";

    int sent_map_req = 0;
    erlcm_map_request_msg_t_publish(s->lcm,"MAP_REQUEST_CHANNEL",&msg);

    erlcm_gridmap_t_subscription_t * sub = erlcm_gridmap_t_subscribe(s->lcm, "MAP_SERVER", gridmap_handler,
                                                                     s);

    erlcm_gridmap_t_subscription_t * sub_1 = erlcm_gridmap_t_subscribe(s->lcm, "FINAL_SLAM", gridmap_handler,
                                                                       s);
    carmen_warn("Waiting for a map");
    //issue is that this listening starts way after this request is sent 
    while (s->global_map_msg == NULL) {
        if(!sent_map_req){
            sent_map_req = 1;
            erlcm_map_request_msg_t_publish(s->lcm,"MAP_REQUEST_CHANNEL",&msg);
        }
        lcm_sleep(s->lcm, .25);
        //usleep(25000);
        fprintf(stderr, ".");
    }
    fprintf(stderr,"Done with Map\n");
}

/* handles C^c */
static void shutdown_module(int x)
{
    if(x == SIGINT) {
        //if (use_robot) 
        //  carmen_robot_shutdown(x);

        carmen_warn("\nDisconnected.\n");
        exit(1);
    }
}

/* updates the robot's position,
   the people's position, and the laser reading 
   and then publishes the laser reading and the odometry */
static int
publish_readings(sim_state_t *s)
{
    carmen_simulator_config_t *simulator_config = s->simulator_config;
    static int first = 1;

    static carmen_laser_laser_message flaser;
    static carmen_laser_laser_message rlaser;
    static carmen_base_odometry_message odometry;

    static carmen_simulator_truepos_message position;
    static carmen_simulator_objects_message objects;

    double delta_time;
    int64_t timestamp;

    if (first) {
        odometry.host = carmen_get_host();
        position.host = carmen_get_host();
    
        objects.host = carmen_get_host();
    
        odometry.x = position.truepose.x;
        odometry.y = position.truepose.y;
        odometry.theta = position.truepose.theta;
    
        odometry.tv = odometry.rv = 0;
    
        if (simulator_config->use_front_laser) {
            flaser.host = carmen_get_host();
            flaser.num_readings = 
                simulator_config->front_laser_config.num_lasers;
            flaser.range = (float *)calloc
                (simulator_config->front_laser_config.num_lasers, sizeof(float));
            //carmen_test_alloc(flaser.range);

            flaser.num_remissions = 0;
            flaser.remission = 0;      

        }
    
        if (simulator_config->use_rear_laser) {
            rlaser.host = carmen_get_host();
      
            rlaser.num_readings = simulator_config->rear_laser_config.num_lasers;
            rlaser.range = (float *)calloc
                (simulator_config->rear_laser_config.num_lasers, sizeof(float));
            //carmen_test_alloc(rlaser.range);

            rlaser.num_remissions = 0;
            rlaser.remission = 0;      

        }
    
        first = 0;
    }

    timestamp = bot_timestamp_now();// / (double)1e6; //carmen_get_time();

    //    fprintf(stderr, " %f \n", timestamp/1.0e6);

    if (simulator_config->real_time && !simulator_config->sync_mode) {
        delta_time = timestamp/1.0e6 - simulator_config->time_of_last_command;
        if ((simulator_config->tv > 0 || simulator_config->rv > 0) && 
            delta_time > simulator_config->motion_timeout) {
            simulator_config->target_tv = 0;
            simulator_config->target_rv = 0;
        }
    }


    carmen_simulator_recalc_pos(simulator_config);
  
    odometry.x = simulator_config->odom_pose.x;
    odometry.y = simulator_config->odom_pose.y;
    odometry.theta = simulator_config->odom_pose.theta;
    odometry.tv = simulator_config->tv;
    odometry.rv = simulator_config->rv;
    odometry.acceleration = simulator_config->acceleration;
    odometry.timestamp = timestamp/1.0e6;
    lcm_odometry_publish(s, &odometry);

    static bot_core_pose_t slam_pose;
    memset(&slam_pose, 0, sizeof(slam_pose));

    carmen_point_t map_pos = {simulator_config->true_pose.x, simulator_config->true_pose.y,
                              simulator_config->true_pose.theta};
			    

    s->g_pos = carmen3d_map_map_to_global_coordinates(map_pos, s->global_carmen3d_map);

    slam_pose.pos[0] = s->g_pos.x;//simulator_config->true_pose.x;
    slam_pose.pos[1] = s->g_pos.y;//simulator_config->true_pose.y;
    double rpy[3] = { 0, 0, s->g_pos.theta};//simulator_config->true_pose.theta };
    bot_roll_pitch_yaw_to_quat(rpy, slam_pose.orientation);
    slam_pose.utime = timestamp;
    bot_core_pose_t_publish(s->lcm, "POSE", &slam_pose);

    position.truepose = simulator_config->true_pose;
    position.odometrypose = simulator_config->odom_pose;
    position.timestamp = timestamp;

    carmen_simulator_update_objects(simulator_config);
    carmen_simulator_get_object_poses
        (&(objects.num_objects), &(objects.objects_list));
    objects.timestamp = timestamp/1.0e6;

    if (simulator_config->use_front_laser) {
        carmen_simulator_calc_laser_msg(&flaser, s->simulator_config, 0);
  
        flaser.timestamp = timestamp/1.0e6;

        planar_laser_pub(s, "SKIRT_FRONT", &flaser);

        //carmen_simulator_calc_object_free_laser_msg(&flaser, simulator_config, 0);
        //flaser.timestamp = timestamp/1.0e6;
        //planar_laser_pub("HOKUYO_FORWARD", &flaser, lcm);
    }
  
    if (simulator_config->use_rear_laser) {
        carmen_simulator_calc_laser_msg(&rlaser, s->simulator_config, 1);
    
        rlaser.timestamp = timestamp/1.0e6;
        planar_laser_pub(s, "HOKUYO_REVERSE", &rlaser);
    }

    //carmen_publish_heartbeat("simulator");

    return 1;

}

static void install_params(carmen3d_localize_motion_model_t *model,BotParam * b_param)
{
    model->mean_c_d = bot_param_get_double_or_fail(b_param, "localizer.mean_c_d");
    model->mean_c_t = bot_param_get_double_or_fail(b_param, "localizer.mean_c_t");
    model->std_dev_c_d = bot_param_get_double_or_fail(b_param, "localizer.std_dev_c_d");
    model->std_dev_c_t = bot_param_get_double_or_fail(b_param, "localizer.std_dev_c_t");
    model->mean_d_d = bot_param_get_double_or_fail(b_param, "localizer.mean_d_d");
    model->mean_d_t = bot_param_get_double_or_fail(b_param, "localizer.mean_d_t");
    model->std_dev_d_d = bot_param_get_double_or_fail(b_param, "localizer.std_dev_d_d");
    model->std_dev_d_t = bot_param_get_double_or_fail(b_param, "localizer.std_dev_d_t");
    model->mean_t_d =  bot_param_get_double_or_fail(b_param, "localizer.mean_t_d");
    model->mean_t_t =  bot_param_get_double_or_fail(b_param, "localizer.mean_t_t");
    model->std_dev_t_d = bot_param_get_double_or_fail(b_param, "localizer.std_dev_t_d");
    model->std_dev_t_t = bot_param_get_double_or_fail(b_param, "localizer.std_dev_t_t");

    fprintf(stderr,"Loaded motion Model\n");
}

carmen3d_localize_motion_model_t *localize_motion_initialize_from_conf(BotParam * b_param)
{
    carmen3d_localize_motion_model_t *model;

    model = (carmen3d_localize_motion_model_t *)
        calloc(1, sizeof(carmen3d_localize_motion_model_t));
    //carmen_test_alloc(model);

    install_params(model, b_param);

    return model;
}

void fill_laser_config_data(carmen_simulator_laser_config_t *lasercfg) 
{
    lasercfg->num_lasers = 1 + carmen_round(lasercfg->fov / lasercfg->angular_resolution);
    lasercfg->start_angle = -0.5*lasercfg->fov;

    /* give a warning if it is not a standard configuration */

    if ( fabs(lasercfg->fov - M_PI) > 1e-6 && 
         fabs(lasercfg->fov - 100.0/180.0 * M_PI) > 1e-6 && 
         fabs(lasercfg->fov -  90.0/180.0 * M_PI) > 1e-6)
        carmen_warn("Warning: You are not using a standard SICK configuration (fov=%.4f deg)\n",
                    carmen_radians_to_degrees(lasercfg->fov) );

    if ( fabs(lasercfg->angular_resolution - carmen_degrees_to_radians(1.0)) > 1e-6 && 
         fabs(lasercfg->angular_resolution - carmen_degrees_to_radians(0.5)) > 1e-6 && 
         fabs(lasercfg->angular_resolution - carmen_degrees_to_radians(0.25)) > 1e-6)
        carmen_warn("Warning: You are not using a standard SICK configuration (res=%.4f deg)\n",
                    carmen_radians_to_degrees(lasercfg->angular_resolution) );

}

gboolean heartbeat_cb (gpointer data)
{
    sim_state_t *s = (sim_state_t *) data;
    //the handling stuff needs to be added here 
    static int i = 0;
    i++;
    if(i == 20){
        fprintf(stderr,".");
        i=0;
    }
    //carmen_ipc_sleep(simulator_config->real_time);
    //simulator_config->target_tv = 0.4;
    if (!s->simulator_config->sync_mode) {
        //if (use_robot)
        //carmen_robot_run();

        //here is where you should pause it
    
        if(s->simulator_state ==1){
            publish_readings(s);
        }
    }
    return TRUE;
}


int read_parameters_to_state(sim_state_t *state){
   
    BotFrames *frames = state->frames;
    BotParam *conf = state->conf;

    carmen_simulator_config_t *config = state->simulator_config;

    config->delta_t = bot_param_get_double_or_fail(conf,"simulator.dt");
    fprintf(stderr,"Delta t : %f\n", config->delta_t);
    config->real_time = bot_param_get_double_or_fail(conf,"simulator.time");

    bot_param_get_int(conf,"simulator.sync_mode", &config->sync_mode);
    bot_param_get_int(conf,"simulator.use_robot", &state->use_robot);
#ifndef OLD_MOTION_MODEL
    config->motion_model = localize_motion_initialize_from_conf(conf);
    fprintf(stderr, " Dev : %f\n", config->motion_model->mean_d_t);
    fprintf(stderr,"New motion Model\n");
#else
    bot_param_get_double(conf,"localizer.odom_a1", &(config->odom_a1));
    bot_param_get_double(conf,"localizer.odom_a2", &(config->odom_a2));
    bot_param_get_double(conf,"localizer.odom_a3", &(config->odom_a3));
    bot_param_get_double(conf,"localizer.odom_a4", &(config->odom_a4));
#endif
    bot_param_get_double(conf,"base.motion_timeout", &(config->motion_timeout));
  
    bot_param_get_int(conf,"simulator.simulate_frontlaser",&(config->use_front_laser));
    if(state->publish_front_laser){
        config->use_front_laser = 1;
    }
    bot_param_get_int(conf,"robot.frontlaser_id",&(config->front_laser_config.id));
    bot_param_get_int(conf,"simulator.simulate_rearlaser",&(config->use_rear_laser));
    bot_param_get_int(conf,"robot.rearlaser_id",&(config->rear_laser_config.id));
    bot_param_get_double(conf,"robot.width",&(config->width));
    bot_param_get_double(conf,"simulator.acceleration", &(config->acceleration));
    bot_param_get_double(conf,"simulator.rot_acceleration", &(config->rot_acceleration));

    fprintf(stderr,"Acceleration : %f\n", config->acceleration);

    fprintf(stderr,"Delta t : %f\n", config->delta_t);

    //-------- paremeters for the front laser --------//
  
    bot_param_get_double(conf,"planar_lidars.SKIRT_FRONT.max_range", &(config->front_laser_config.max_range));
    fprintf(stderr,"F - Max Range : %f\n", config->front_laser_config.max_range);


    bot_param_get_double(conf,"planar_lidars.SKIRT_FRONT.fov", &(config->front_laser_config.fov));

    bot_param_get_double(conf,"planar_lidars.SKIRT_FRONT.resolution", &(config->front_laser_config.angular_resolution));

    bot_param_get_double(conf,"simulator.laser_probability_of_random_max",&(config->front_laser_config.prob_of_random_max));
    bot_param_get_double(conf,"simulator.laser_probability_of_random_reading", &(config->front_laser_config.prob_of_random_reading));
    bot_param_get_double(conf,"simulator.laser_sensor_variance", &(config->front_laser_config.variance));

    char * coord_frame_front = bot_param_get_planar_lidar_coord_frame (conf, "SKIRT_FRONT");

    if (!coord_frame_front)
        fprintf (stderr, "\tError determining front laser coordinate frame\n");

    char * coord_frame_rear = bot_param_get_planar_lidar_coord_frame (conf, "SKIRT_REAR");

    if (!coord_frame_rear)
        fprintf (stderr, "\tError determining front laser coordinate frame\n");

    BotTrans front_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame_front, "body", &front_laser_to_body))
        fprintf (stderr, "\tError determining front laser coordinate frame\n");
    else
        fprintf(stderr,"\tFront Laser Pos : (%f,%f,%f)\n",
                front_laser_to_body.trans_vec[0], front_laser_to_body.trans_vec[1], front_laser_to_body.trans_vec[2]);

    config->front_laser_config.offset = front_laser_to_body.trans_vec[0];
    config->front_laser_config.side_offset = front_laser_to_body.trans_vec[1];

    double front_rpy[3];
    bot_quat_to_roll_pitch_yaw (front_laser_to_body.rot_quat, front_rpy);

    config->front_laser_config.angular_offset  = front_rpy[2];

    //-------- paremeters for the front laser --------//

    bot_param_get_double(conf,"planar_lidars.SKIRT_REAR.max_range", &(config->rear_laser_config.max_range));
    fprintf(stderr,"R - Max Range : %f\n", config->rear_laser_config.max_range);


    bot_param_get_double(conf,"planar_lidars.SKIRT_REAR.fov", &(config->rear_laser_config.fov));

    bot_param_get_double(conf,"planar_lidars.SKIRT_REAR.resolution", &(config->rear_laser_config.angular_resolution));

    bot_param_get_double(conf,"simulator.laser_probability_of_random_max",&(config->rear_laser_config.prob_of_random_max));
    bot_param_get_double(conf,"simulator.laser_probability_of_random_reading", &(config->rear_laser_config.prob_of_random_reading));
    bot_param_get_double(conf,"simulator.laser_sensor_variance", &(config->rear_laser_config.variance));

    BotTrans rear_laser_to_body;
    if (!bot_frames_get_trans (frames, coord_frame_rear, "body", &rear_laser_to_body))
        fprintf (stderr, "\tError determining front laser coordinate frame\n");
    else
        fprintf(stderr,"\tRear Laser Pos : (%f,%f,%f)\n",
                rear_laser_to_body.trans_vec[0], rear_laser_to_body.trans_vec[1], rear_laser_to_body.trans_vec[2]);

    config->rear_laser_config.offset = rear_laser_to_body.trans_vec[0];
    config->rear_laser_config.side_offset = rear_laser_to_body.trans_vec[1];

    double rear_rpy[3];
    bot_quat_to_roll_pitch_yaw (rear_laser_to_body.rot_quat, rear_rpy);

    config->rear_laser_config.angular_offset  = rear_rpy[2];
        
    if(config->use_front_laser) {
        fprintf(stderr,"Using the Front Laser\n");
    
        config->front_laser_config.angular_resolution =
            carmen_degrees_to_radians(config->front_laser_config.angular_resolution);

        config->front_laser_config.fov =
            carmen_degrees_to_radians(config->front_laser_config.fov);
    }

    if(state->publish_rear_laser){
        config->use_rear_laser = 1;
    }

    if(config->use_rear_laser) {
        fprintf(stderr,"Using the Rear Laser\n");
        config->rear_laser_config.angular_resolution =
            carmen_degrees_to_radians(config->rear_laser_config.angular_resolution);

        config->rear_laser_config.fov =
            carmen_degrees_to_radians(config->rear_laser_config.fov);
    }

    fill_laser_config_data( &(config->front_laser_config) );

    if(config->use_rear_laser)
        fill_laser_config_data( &(config->rear_laser_config));

    return 0;
 }


int main(int argc, char** argv)
{

    sim_state_t *state = calloc(1, sizeof(sim_state_t));
    
    state->simulator_config = calloc(1, sizeof(carmen_simulator_config_t));
    state->global_map_msg = NULL;
    state->global_carmen3d_map = (erlcm_map_p) calloc(1, sizeof(erlcm_map_t));
    state->lcm  = bot_lcm_get_global(NULL);
    state->conf =  bot_param_new_from_server(state->lcm, 1);  
    state->frames = bot_frames_get_global (state->lcm, state->conf);
    state->g_pos.x = 0;
    state->g_pos.y = 0;
    state->g_pos.theta = 0;
    state->use_robot = 1;
    state->simulator_state =1;
    int c = 0;
    int force_laser_pub = 0;
    while ((c = getopt (argc, argv, "vlfr")) >= 0) {
        switch (c) {
            case 'f':
                state->publish_front_laser = 1;
                break;
            case 'r':
                state->publish_rear_laser = 1;
                break;
            case 'l':
                force_laser_pub = 1;
                break;
            case 'v':
                fprintf(stderr,"Should be verbose\n");
                break;
            case 'h':
            case '?':
                fprintf (stderr, "Usage: %s [-v] [-c] [-d] [-p]\n\
                        Options:\n                               \
                        -v     Be verbose\n                      \
                        -l     Force Laser Publish\n             \
                        -h     This help message\n", argv[0]);
        return 1;
        }
    }

    read_parameters_to_state(state);
    carmen_simulator_initialize_object_model(state->conf);
    signal(SIGINT, shutdown_module);
    carmen_randomize(&argc, &argv);

    erlcm_velocity_msg_t_subscribe(state->lcm,"ROBOT_VELOCITY_CMD", lcm_velocity_handler, state);
    erlcm_velocity_msg_t_subscribe(state->lcm,"ROBOT_VELOCITY_CMD_JS", lcm_velocity_handler, state);

    erlcm_localize_reinitialize_cmd_t_subscribe(state->lcm, LOCALIZE_REINITIALIZE_CHANNEL,
                                                lcm_localize_reinitialize_handler, state);

    erlcm_simulator_cmd_t_subscribe(state->lcm, "SIMULATOR_CMD", lcm_simulator_cmd_handler, state);

    create_simulator_map(state);
  
    bot_glib_mainloop_attach_lcm (state->lcm);
    
    g_timeout_add ( state->simulator_config->real_time * 1000, heartbeat_cb, state);

    /* Main Loop */
    GMainLoop *mainloop = g_main_loop_new(NULL, FALSE);

    if (!mainloop) {
        fprintf(stderr,"Error: Failed to create the main loop\n");
        return -1;
    }

    bot_signal_pipe_glib_quit_on_kill (mainloop);
    /* sit and wait for messages */
    g_main_loop_run(mainloop);

    carmen_simulator_clear_objects();

    exit(0);
}
