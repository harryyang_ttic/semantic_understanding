 #!/usr/bin/python
import os
import sys
import signal
import glib
import gobject
import glob
import argparse
import time
import shutil
import math
import fnmatch

from numpy import *
from prettytable import PrettyTable

from lcm import EventLog

from common.quaternion import Quaternion

from bot_core import pose_t
from bot_param import update_t
from policy_status import status_t
from sp import waypoint_list_t
from slam import object_list_t
from slam import object_t
from slam import world_model_t
from sp import behavior_set_t
from sp.instruction_t import instruction_t


success_distance = 2.0 # Distance from goal considered success

                
fname = sys.argv[1]
log = EventLog(fname, "r")

print >> sys.stderr, "opened %s" % fname

        
# For now, assume goal is to be NEAR particular object
pos_last = None
quat_last = None
time_start = None
time_last = 0
distance = 0
policy_status_last = None
    

for event in log:

    if event.channel == "POSE":
        pose = pose_t.decode (event.data)

        if pos_last is not None:
            pos = pose.pos
            distance = distance + math.sqrt( (pos[0] - pos_last[0])**2 +
                                            (pos[1] - pos_last[1])**2 +
                                            (pos[2] - pos_last[2])**2)
        pos_last = pose.pos
        quat_last = Quaternion(pose.orientation)
        
        time_last = pose.utime

    if event.channel == "POLICY_STATUS":
        policy_status_last = status_t.decode (event.data)

        if time_start is None:
            if policy_status_last.current_policy_status == status_t.STATUS_ACT:
                time_start = time_last

            

 
print "        Distance = %.2f, Time = %.2f seconds" % (distance, (time_last-time_start)*1E-6)
 
