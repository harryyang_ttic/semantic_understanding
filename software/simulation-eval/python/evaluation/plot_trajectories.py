 #!/usr/bin/python
import os
import glob
import argparse
import time
import math

from numpy import *
import matplotlib.pyplot as pyplot

from lcm import EventLog

from common.quaternion import Quaternion

from bot_core import pose_t
from bot_param import update_t
from policy_status import status_t
from sp import waypoint_list_t
from slam import object_list_t
from slam import object_t
from slam import world_model_t
from sp import behavior_set_t


class Object(object):
    def __init__(self, _id, _pos, _rpy, _type):
        self.id = _id
        self.pos = _pos
        self.rpy = _rpy
        self.type = _type


class Trajectory(object):
    def __init__(self, _lcm_logfile):
        self.lcm_logfile = _lcm_logfile
        self.x = []
        self.y = []


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Analyze LCM results log(s)',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument ('input', metavar='filename', nargs='+', help='Directories containing LCM log files to analyze')
    parser.add_argument ('--verbose', help='Verbose output', action='store_true', default=False)

    args = parser.parse_args()


    ground_truth_object_id = None
    ground_truth_goal_pos = None
    ground_truth_goal_rpy = None
    Objects = {}
    Trajectories = []

        
    for input in args.input:

        if os.path.isdir(input) is not True:
            continue

        # Process the LCM logs contained in this folder
        LCM_logfiles = glob.glob(input + '/*.lcm_log.*')

        if (len(LCM_logfiles) <= 0):
            continue

        if args.verbose == True:
            print "Processing LCM logfiles in directory " + input
        
        for lcm_logfile in LCM_logfiles:
            if args.verbose == True:
                print "    Processing LCM log: " + lcm_logfile
                
            log = EventLog (lcm_logfile, 'r')

            trajectory = Trajectory(lcm_logfile)
            
            
            for event in log:

                if event.channel == "POSE":
                    pose = pose_t.decode (event.data)

                    pos_last = pose.pos
                    quat_last = pose.orientation

                    trajectory.x = append(trajectory.x, pose.pos[0])
                    trajectory.y = append(trajectory.y, pose.pos[1])
                    
                    quat_last = Quaternion(pose.orientation)

                    
                if event.channel == "GROUND_TRUTH_GOAL":
                    if ground_truth_object_id is None:
                        object = object_t.decode (event.data)
                        ground_truth_object_id = object.id

                    
                if event.channel == "SIMULATOR_WORLD_MODEL":
       
                    wm = world_model_t.decode (event.data)

                    for obj in wm.objects:
                        if obj.id not in Objects:
                            quat = Quaternion (obj.quat)
                            object = Object(obj.id, obj.pos, quat.to_roll_pitch_yaw(), None)

                            Objects[obj.id] = object

                                
                if event.channel == "APRIL_OBJECT_DETECTIONS":
                    object_list = object_list_t.decode (event.data)

                    for obj in object_list.objects:
                        
                        # For April Tags, we will grab the most recent
                        v1 = quat_last.rotate(obj.pos)
                        pos = add(pos_last,v1) 
                        Objects[obj.id] = Object(obj.id, pos, obj.quat, None)
                        
        
                #if event.channel == "POLICY_STATUS":
                    #policy_status_last = status_t.decode (event.data)

                    #if time_start is None:
                    #    if policy_status_last.current_policy_status == status_t.STATUS_ACT:


            Trajectories.append(trajectory)
            print len(trajectory.x)
            print len(Trajectories)


            fig = pyplot.figure()
            ax = fig.add_subplot(111)


            for t in Trajectories:
                print "Plotting trajectory"
                print (t.x,t.y)
                ax.plot(t.x,t.y,'g.-')

            pyplot.show()
