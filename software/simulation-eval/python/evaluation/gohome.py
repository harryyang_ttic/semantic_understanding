import os
import sys
import time

import lcm

from erlcm.goal_list_t import goal_list_t
from erlcm.goal_t import goal_t

if __name__ == '__main__':

    _lcm = lcm.LCM()

    msg = goal_list_t()
    msg.utime = int(time.time() * 1000000)
    msg.sender_id = 1
    msg.num_goals = 1
    msg.goals = []
    goal = goal_t()
    goal.id = 10
    goal.pos[0] = 1.0
    goal.pos[1] = 0.0
    goal.theta = 0
    goal.size[0] = 0.15
    goal.size[1] = 0.15
    goal.heading_tol = 0.08
    goal.use_theta = True
    goal.speed = 0.5
    goal.speed_tol = 0.2
    goal.max_speed = 1.0
    goal.min_speed = 0.0
    goal.wait_until_reachable = False
    goal.do_turn_only = False
    msg.goals.append(goal)

    _lcm.publish("RRTSTAR_GOALS", msg.encode())
