import os
import sys

import lcm


from slam.object_t import object_t
import sp.object_t


if __name__ == '__main__':

    _lcm = lcm.LCM()

    try:
        while True:
            ground_truth_id = input('Enter the object id for the correct destination: ')

            msg = object_t()
            msg.id = ground_truth_id
            msg.pos = [0, 0, 0] # leave these blank for now
            msg.quat = [0, 0, 0, 1]
            #sp_object = sp.object_t()
            msg.properties = sp.object_t()
            msg.properties.name = "unknown"
            msg.properties.type = sp.object_t.UNKNOWN

            _lcm.publish ("GROUND_TRUTH_GOAL", msg.encode())
            
    except KeyboardInterrupt:
        print 'Goodbye'
