 #!/usr/bin/python
import os
import sys
import signal
import glib
import gobject
import glob
import argparse
import time
import shutil
import math
import fnmatch

from numpy import *
from prettytable import PrettyTable

from lcm import EventLog

from common.quaternion import Quaternion

from bot_core import pose_t
from bot_param import update_t
from policy_status import status_t
from sp import waypoint_list_t
from slam import object_list_t
from slam import object_t
from slam import world_model_t
from sp import behavior_set_t
from sp.instruction_t import instruction_t


success_distance = 2.0 # Distance from goal considered success

class Results(object):

    def __init__(self):
        self.Distance = array ([])
        self.Time = array([])
        self.Success = array([])
        self.DistanceFromGoal = array([])
        self.PlannerDone = array([])
        self.LcmLogfiles = array([])

class Scenario(object):

    def __init__(self,dirname):
        self.results = dict()
        self.dirname = dirname
        self.instruction = None

    def add_version(self, version):
        self.results[version] = Results()

    def evaluate_results (self):

        for version in self.results:
            Success = self.results[version].Success
            Distance = self.results[version].Distance
            Time = self.results[version].Time

            self.results[version].num_success = sum(Success==True)
            self.results[version].num_failure = sum(Success==False)
            
            success_ind = where(Success == True)
            if (len(success_ind[0]) > 0):
                self.results[version].success_distance_mean = mean(Distance[success_ind])
                self.results[version].success_distance_stddev = std(Distance[success_ind])
                self.results[version].success_time_mean = mean(Time[success_ind])
                self.results[version].success_time_stddev = std(Time[success_ind])
            else:
                self.results[version].success_distance_mean = NaN
                self.results[version].success_distance_stddev = NaN
                self.results[version].success_time_mean = NaN
                self.results[version].success_time_stddev = NaN

            failure_ind = where(Success == False)
            if (len(failure_ind[0]) > 0):
                self.results[version].failure_distance_mean = mean(Distance[failure_ind])
                self.results[version].failure_distance_stddev = std(Distance[failure_ind])
                self.results[version].failure_time_mean = mean(Time[failure_ind])
                self.results[version].failure_time_stddev = std(Time[failure_ind])
            else:
                self.results[version].failure_distance_mean = NaN
                self.results[version].failure_distance_stddev = NaN
                self.results[version].failure_time_mean = NaN
                self.results[version].failure_time_stddev = NaN

            
        if 'standard' in self.results:
            Success = self.results['standard'].Success
            Distance = self.results['standard'].Distance
            Time = self.results['standard'].Time

            success_ind = where(Success == True)
            
            self.results['standard'].success_vs_knownmap_distance_mean = NaN
            self.results['standard'].success_vs_knownmap_distance_stddev = NaN
            self.results['standard'].success_vs_knownmap_time_mean = NaN
            self.results['standard'].success_vs_knownmap_time_stddev = NaN
            if ('known-map' in self.results) & (sum(Success==True) > 0):
                self.results['standard'].success_vs_knownmap_distance_mean = mean(Distance[success_ind] - self.results['known-map'].success_distance_mean)
                self.results['standard'].success_vs_knownmap_distance_stddev = std(Distance[success_ind] - self.results['known-map'].success_distance_mean)
                
                self.results['standard'].success_vs_knownmap_time_mean = mean(Time[success_ind] - self.results['known-map'].success_time_mean)
                self.results['standard'].success_vs_knownmap_time_stddev = std(Time[success_ind] - self.results['known-map'].success_time_mean)



                


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Analyze LCM results log(s)',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument ('input', metavar='filename', nargs='+', help='Directories containing LCM log files to analyze')
    parser.add_argument ('--verbose', help='Verbose output', action='store_true', default=False)
    parser.add_argument ('-g', '--goal-id', help='ID of the object that is the goal', default=None, type=int, dest='goalid')

    args = parser.parse_args()

    _ground_truth_object_id = args.goalid
    if args.goalid is not None:
        print "Assuming that the goal object ID = %d" % (_ground_truth_object_id)

    num_logs = 0
    num_valid_logs = 0
    Scenarios = []
    for input in args.input:

        if os.path.isdir(input) is not True:
            continue

        # Process the LCM logs contained in this folder
        LCM_logfiles = glob.glob(input + '/*.lcm_log.*')

        if (len(LCM_logfiles) <= 0):
            continue

        scenario = Scenario(input)

        if args.verbose == True:
            print "\nProcessing LCM logfiles in directory " + input
        
        for lcm_logfile in LCM_logfiles:
            if args.verbose == True:
                print "\n    Processing LCM log: " + lcm_logfile
                
            log = EventLog (lcm_logfile, 'r')
            num_logs += 1
        
            # For now, assume goal is to be NEAR particular object
            ground_truth_object_id = _ground_truth_object_id
            ground_truth_goal_pos = None
            ground_truth_goal_rpy = None
            simulation_mode = None

            pos_last = None
            quat_last = None
            time_start = None
            time_last = None
            distance = 0
            policy_status_last = None


            for event in log:

                if event.channel == "POSE":
                    pose = pose_t.decode (event.data)

                    if pos_last is not None:
                        pos = pose.pos
                        distance = distance + math.sqrt( (pos[0] - pos_last[0])**2 +
                                                     (pos[1] - pos_last[1])**2 +
                                                     (pos[2] - pos_last[2])**2)
                    pos_last = pose.pos
                    quat_last = Quaternion(pose.orientation)

                    time_last = pose.utime

                    
                if event.channel == "GROUND_TRUTH_GOAL":
                    if (_ground_truth_object_id is None) and (ground_truth_object_id is not None):
                        print "Found more than one GROUND_TRUTH_GOAL message!"
                    object = object_t.decode (event.data)

                    if _ground_truth_object_id is not None:
                        if _ground_truth_object_id != object.id:
                            print "Ignoring GROUND_TRUTH_GOAL that specifies ID as %d rather than %d" % (object.id, _ground_truth_object_id)
                    else:
                        ground_truth_object_id = object.id

                    
                if event.channel == "SIMULATOR_WORLD_MODEL":
                    if simulation_mode is None:
                        simulation_mode = True
                        #if args.verbose == True:
                        #    print "    This log appears to correspond to a simulation"
                            
                    wm = world_model_t.decode (event.data)

                    if ((ground_truth_goal_pos is None) & (ground_truth_object_id is not None)):
                        for object in wm.objects:
                            if object.id == ground_truth_object_id:
                                ground_truth_goal_pos = object.pos
                                quat = Quaternion (object.quat)
                                ground_truth_goal_rpy = quat.to_roll_pitch_yaw()

                                
                if (event.channel == "APRIL_OBJECT_DETECTIONS") and (ground_truth_object_id is not None) and (pos_last is not None):
                    object_list = object_list_t.decode (event.data)

                    for object in object_list.objects:
                        if object.id == ground_truth_object_id:

                            # Transform pose from body to local frame
                            v1 = quat_last.rotate(object.pos)

                            ground_truth_goal_pos = add(pos_last,v1)
        
                if event.channel == "POLICY_STATUS":
                    policy_status_last = status_t.decode (event.data)

                    if time_start is None:
                        if policy_status_last.current_policy_status == status_t.STATUS_ACT:
                            time_start = time_last

                if event.channel == "INSTRUCTION":
                    instruction_last = instruction_t.decode (event.data)
                    if scenario.instruction is None:
                        scenario.instruction = instruction_last.text



            # Is this a good log?
            
            # Consider this to be a valid run if
            #    (1) policy_status_last = STATUS_DONE
            #    (2) pos_last is not None
            #    (3) time_last is not None
            #    (4) ground_truth_goal_pos is None and we are not in simulation mode (i.e., robot never saw goal object)
            
         
            valid_log = True
            if time_last is None:
                print "Unable to determine the time that the planner stopped for log " + lcm_logfile
                valid_log = False

            if policy_status_last is None:
                print "No POLICY_STATUS found in log " + lcm_logfile
                valid_log = False
            else:
                if policy_status_last.current_policy_status != status_t.STATUS_DONE:
                    print "Last policy status is %d, not STATUS_DONE. Ignoring log" % (policy_status_last.current_policy_status)
                    valid_log = False

            if pos_last is None:
                print "No POSE found in log " + lcm_logfile
                valid_log = False

            if ground_truth_object_id is None:
                print "No GROUND_TRUTH_GOAL message in log " + lcm_logfile
                valid_log = False
                
            if (ground_truth_goal_pos is None) and (simulation_mode is True):
                print "Unable to determine ground truth object's pose for log with real robot " + lcm_logfile + " Treating it as a failure"
                valid_log = False

 
            if valid_log is True: #& (ground_truth_goal_pos is not None)):
                num_valid_logs += 1
                version = 'standard'
                if fnmatch.fnmatch(lcm_logfile, '*known-map*'):
                    version = 'known-map'
                if version not in scenario.results:
                    scenario.add_version(version)

                
                if policy_status_last.current_policy_status == status_t.STATUS_DONE:
                    scenario.results[version].PlannerDone = append (scenario.results[version].PlannerDone, [True])
                else:
                    scenario.results[version].PlannerDone = append (scenario.results[version].PlannerDone, [False])

                if ground_truth_goal_pos is None:
                    distance_from_goal = NaN
                else:
                    distance_from_goal = math.sqrt ( (pos_last[0] - ground_truth_goal_pos[0])**2 +
                                                    (pos_last[1] - ground_truth_goal_pos[1])**2 +
                                                    (pos_last[2] - ground_truth_goal_pos[2])**2)

                if (distance_from_goal < success_distance):
                    success = True
                else:
                    success = False

                scenario.results[version].Success = append (scenario.results[version].Success, success)
                scenario.results[version].Distance = append (scenario.results[version].Distance, distance)
                scenario.results[version].Time = append (scenario.results[version].Time, (time_last-time_start)*1E-6)
                scenario.results[version].DistanceFromGoal = append (scenario.results[version].DistanceFromGoal, [distance_from_goal])
                scenario.results[version].LcmLogfiles = append (scenario.results[version].LcmLogfiles, [lcm_logfile])


                if args.verbose == True:
                    print "        Distance = %.2f, Time = %.2f seconds, Distance from goal = %.2f, Success = %d" % (distance, (time_last-time_start)*1E-6, distance_from_goal, success)
            else:
                print "There was a problem processing LCM log " + lcm_logfile


        if scenario.instruction is None:
            scenario.instruction = "unknown"
        scenario.evaluate_results()
        Scenarios.append(scenario)


    print "%d out of %d logs were deemed valid" % (num_valid_logs, num_logs)

        

    # Compute overall results
    overall = Scenario('overall')
    overall.add_version('known-map')
    overall.add_version('standard')
    for scenario in Scenarios:
        Versions = {'known-map', 'standard'}
        for version in Versions:
            overall.results[version].Distance = append (overall.results[version].Distance, scenario.results[version].Distance)
            overall.results[version].Time = append (overall.results[version].Time, scenario.results[version].Time)
            overall.results[version].Success = append (overall.results[version].Success, scenario.results[version].Success)
            overall.results[version].DistanceFromGoal = append (overall.results[version].DistanceFromGoal, scenario.results[version].DistanceFromGoal)
            overall.results[version].PlannerDone = append (overall.results[version].PlannerDone, scenario.results[version].PlannerDone)
            overall.results[version].LcmLogfiles = append (overall.results[version].LcmLogfiles, scenario.results[version].LcmLogfiles)

    # Compute the average statistics. Note that this will incorrectly compute the mean and standard deviation for the distance and time relative to the known-map case
    overall.evaluate_results()

    RelativeDistance = []
    RelativeTime = []
    for scenario in Scenarios:
        success_ind = where(scenario.results['standard'].Success == True)
        RelativeDistance = append(RelativeDistance, scenario.results['standard'].Distance[success_ind] - scenario.results['known-map'].success_distance_mean)
        RelativeTime = append(RelativeTime, scenario.results['standard'].Time[success_ind] - scenario.results['known-map'].success_time_mean)

    overall.results['standard'].success_vs_knownmap_distance_mean = mean(RelativeDistance)
    overall.results['standard'].success_vs_knownmap_distance_stddev = std(RelativeDistance)
    overall.results['standard'].success_vs_knownmap_time_mean = mean(RelativeTime)
    overall.results['standard'].success_vs_knownmap_time_stddev = std(RelativeTime)
      
    # Parse the results
    T1 = PrettyTable(['Scenario', 'KnownMap: # Success', 'KnownMap: # Fail', 'Standard: # Success', 'Standard: # Fail',  'KnownMap: Dist (mean)', 'KnownMap: Dist (stddev)', 'Standard: Dist (mean)', 'Standard: Dist (stddev)', 'Relative: Dist (mean)', 'Relative: Dist (stddev)', 'Scenario Directory', 'Command'])
    T1.align['Scenario'] = 'r'
    T1.align['KnownMap: # Success'] = 'r'
    T1.align['KnownMap: # Fail'] = 'r'
    T1.align['Standard: # Success'] = 'r'
    T1.align['Standard: # Fail'] = 'r'
    T1.align['Standard: Dist (mean)'] = 'r'
    T1.align['Standard: Dist (stddev)'] = 'r'
    T1.align['KnownMap: Dist (mean)'] = 'r'
    T1.align['KnownMap: Dist (stddev)'] = 'r'
    T1.align['Relative: Dist (mean)'] = 'r'
    T1.align['Relative: Dist (stddev)'] = 'r'
    T1.float_format['Standard: Dist (mean)'] = '.2'
    T1.float_format['Standard: Dist (stddev)'] = '.2'
    T1.float_format['KnownMap: Dist (mean)'] = '.2'
    T1.float_format['KnownMap: Dist (stddev)'] = '.2'
    T1.float_format['Relative: Dist (mean)'] = '.2'
    T1.float_format['Relative: Dist (stddev)'] = '.2'

    count = 1
    for scenario in Scenarios:
        T1.add_row([count, scenario.results['known-map'].num_success, scenario.results['known-map'].num_failure, scenario.results['standard'].num_success, scenario.results['standard'].num_failure, scenario.results['known-map'].success_distance_mean, scenario.results['known-map'].success_distance_stddev, scenario.results['standard'].success_distance_mean, scenario.results['standard'].success_distance_stddev,
                    scenario.results['standard'].success_vs_knownmap_distance_mean, scenario.results['standard'].success_vs_knownmap_distance_stddev, os.path.basename(scenario.dirname), scenario.instruction])
        count+=1

    # Append the overall results
    T1.add_row(['OVERALL', overall.results['known-map'].num_success, overall.results['known-map'].num_failure, overall.results['standard'].num_success, overall.results['standard'].num_failure, overall.results['known-map'].success_distance_mean, overall.results['known-map'].success_distance_stddev, overall.results['standard'].success_distance_mean, overall.results['standard'].success_distance_stddev,
                    overall.results['standard'].success_vs_knownmap_distance_mean, overall.results['standard'].success_vs_knownmap_distance_stddev, os.path.basename(overall.dirname), "---"])
        
    print T1



    T2 = PrettyTable(['Scenario', 'KnownMap: # Success', 'KnownMap: # Fail', 'Standard: # Success', 'Standard: # Fail',  'KnownMap: Time (mean)', 'KnownMap: Time (stddev)', 'Standard: Time (mean)', 'Standard: Time (stddev)', 'Relative: Time (mean)', 'Relative: Time (stddev)', 'Scenario Directory', 'Command'])
    T2.align['Scenario'] = 'r'
    T2.align['KnownMap: # Success'] = 'r'
    T2.align['KnownMap: # Fail'] = 'r'
    T2.align['Standard: # Success'] = 'r'
    T2.align['Standard: # Fail'] = 'r'
    T2.align['Standard: Time (mean)'] = 'r'
    T2.align['Standard: Time (stddev)'] = 'r'
    T2.align['KnownMap: Time (mean)'] = 'r'
    T2.align['KnownMap: Time (stddev)'] = 'r'
    T2.align['Relative: Time (mean)'] = 'r'
    T2.align['Relative: Time (stddev)'] = 'r'
    T2.float_format['Standard: Time (mean)'] = '.2'
    T2.float_format['Standard: Time (stddev)'] = '.2'
    T2.float_format['KnownMap: Time (mean)'] = '.2'
    T2.float_format['KnownMap: Time (stddev)'] = '.2'
    T2.float_format['Relative: Time (mean)'] = '.2'
    T2.float_format['Relative: Time (stddev)'] = '.2'

    count = 1
    for scenario in Scenarios:
        T2.add_row([count, scenario.results['known-map'].num_success, scenario.results['known-map'].num_failure, scenario.results['standard'].num_success, scenario.results['standard'].num_failure, scenario.results['known-map'].success_time_mean, scenario.results['known-map'].success_time_stddev, scenario.results['standard'].success_time_mean, scenario.results['standard'].success_time_stddev,
                    scenario.results['standard'].success_vs_knownmap_time_mean, scenario.results['standard'].success_vs_knownmap_time_stddev, os.path.basename(scenario.dirname), scenario.instruction])
        count+=1



    # Append the overall results
    T2.add_row(['OVERALL', overall.results['known-map'].num_success, overall.results['known-map'].num_failure, overall.results['standard'].num_success, overall.results['standard'].num_failure, overall.results['known-map'].success_time_mean, overall.results['known-map'].success_time_stddev, overall.results['standard'].success_time_mean, overall.results['standard'].success_time_stddev,
                    overall.results['standard'].success_vs_knownmap_time_mean, overall.results['standard'].success_vs_knownmap_time_stddev, os.path.basename(overall.dirname), "---"])
        
    #print T2
