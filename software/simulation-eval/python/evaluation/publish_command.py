import os
import sys
import time

import lcm

from sp.instruction_t import instruction_t


if __name__ == '__main__':

    _lcm = lcm.LCM()

    try:
        command_string_prev = None
        while True:
            if command_string_prev is not None:
                command_string = raw_input('Enter the command to be sent (\"' + command_string_prev + '\"): ')
                if command_string == '':
                    command_string = command_string_prev
            else:
                command_string = raw_input('Enter the command to be sent: ')

            command_string_prev = command_string
            
            msg = instruction_t()
            msg.utime = int(time.time() * 1000000)
            msg.text = command_string
            _lcm.publish("INSTRUCTION", msg.encode())
            
    except KeyboardInterrupt:
        print 'Goodbye'
