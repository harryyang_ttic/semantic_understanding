#!/usr/bin/python
import xml.etree.ElementTree as ET
import argparse
import os
import random

import sys
import math

import matplotlib.pyplot as pyplot
import matplotlib.patches as patches
from matplotlib.path import Path

def run(arguments):

    num_world_models = 10
    output_dir = os.getcwd()


    parser = argparse.ArgumentParser(description='Generate random world models')
    parser.add_argument('--number', help='number of world models to generate')
    parser.add_argument('--destination', help='directory in which to place the world models (default is current directory)')
    parser.add_argument('xmlFile', help='XML file describing world model composition')
    parser.add_argument('--savefig', action='store_true', help='save figure to file')

    args = parser.parse_args(arguments)

    if args.number:
        num_world_models = int(args.number)

    if args.destination:
        output_dir = args.destination

    save_figure = args.savefig

    if os.path.isdir(output_dir) == False:
        print 'Directory ' + output_dir + ' does not exist. Exiting'
        exit(0)

    wmc_file = os.path.basename(args.xmlFile)
    temp = os.path.splitext(wmc_file)
    wm_file_prefix = temp[0];
    wm_file_full_prefix = output_dir + '/' + wm_file_prefix + '_'

    # Load the XML file specifying the world model composition
    print "Loading world model template %s" % (args.xmlFile)
    wmc_tree = ET.parse(args.xmlFile)
    wmc_root = wmc_tree.getroot()


    colors = ['red', 'green', 'orange', 'blue', 'yellow']
    color_idx = 1
    fig = pyplot.figure()
    ax = fig.add_subplot(111)

    min_x = 1E6
    max_x = -1E6
    min_y = 1E6
    max_y = -1E6

    # First draw the world to make it easy to specify command and ground-truth goal
    for bb in wmc_root.findall('bb'):
        object_type = bb.get('object_type')
        id = int(bb.get('id'))
        lr_pos = bb.get('lr_pos').split(',')
        lr_x = float(lr_pos[0])
        lr_y = float(lr_pos[1])
        lr_z = float(lr_pos[2])
        size = bb.get('size').split(',')
        size_x = float(size[0])
        size_y = float(size[1])
        size_z = float(size[2])
        rpy = bb.get('rpy').split(',')
        heading = float(rpy[2])

        # XML uses X-up, Y-left reference frame
        vertices = [(-lr_y, lr_x),
                    (-lr_y, lr_x+size_x),
                    (-(lr_y+size_y), lr_x+size_x),
                    (-(lr_y+size_y), lr_x),
                    (-lr_y, lr_x),]

        codes = [Path.MOVETO,
                 Path.LINETO,
                 Path.LINETO,
                 Path.LINETO,
                 Path.CLOSEPOLY,]

        if (-lr_y > max_x):
            max_x = -lr_y

        if (-(lr_y+size_y) < min_x):
            min_x = -(lr_y+size_y)

        if (lr_x+size_x > max_y):
            max_y = lr_x+size_x

        if (lr_x < min_y):
            min_y = lr_x


        path = Path(vertices, codes)
        patch = patches.PathPatch(path, facecolor=colors[color_idx], lw=2)
        ax.add_patch(patch)

        center_x = -(lr_y + size_y/2)
        center_y = lr_x + size_x/2
        label = '%s (id = %d)' % (object_type, id)
        if save_figure:  # Different label for save-figure mode.
            label = '%s' % (object_type, )
        ax.text(center_x, center_y, label,
                horizontalalignment='center',
                verticalalignment='center')

        dx = math.cos((heading+90)*math.pi/180)
        dy = math.sin((heading+90)*math.pi/180)

        # Don't draw an arrow if we are saving the figure.
        if not save_figure:
            ax.arrow(center_x, center_y, dx, dy,head_width=0.5, head_length=0.8, fc='k', ec='k')

        color_idx = color_idx + 1
        if (color_idx > len(colors)):
            color_idx = 1

    # Render the environment
    ax.set_xlim(min_x-5, max_x+5)
    ax.set_ylim(min_y-5, max_y+5)
    pyplot.ion()
    pyplot.show()

    if save_figure:
        fpath = wm_file_prefix + '.png'
        pyplot.savefig(fpath)
        print 'Saved figure to {}'.format(fpath)

    # Enter the command & reference object here. Do not do this if we are saving
    # the figure.
    if not save_figure:
        command = raw_input('Enter the command: ')
        ground_truth_id = input('Enter the id of correct destination: ')
        print ground_truth_id

        random.seed()

        for n in range(0, num_world_models):
            wm_root = ET.Element('root')

            wm_command = ET.SubElement (wm_root, "scenario")
            wm_command.set('command', command)
            ground_truth_id_str = "%d" % (ground_truth_id)
            wm_command.set('ground_truth_id', ground_truth_id_str)

            for bb in wmc_root.findall('bb'):
                object_type = bb.get('object_type')
                id = bb.get('id')
                lr_pos = bb.get('lr_pos').split(',')
                lr_x = float(lr_pos[0])
                lr_y = float(lr_pos[1])
                lr_z = float(lr_pos[2])
                size = bb.get('size').split(',')
                size_x = float(size[0])
                size_y = float(size[1])
                size_z = float(size[2])
                rpy = bb.get('rpy').split(',')

                pos_x = lr_x + size_x * random.random()
                pos_y = lr_y + size_y * random.random()
                pos_z = lr_z + size_z * random.random()
                pos_str = "%.3f,%.3f,%.3f" % (pos_x, pos_y, pos_z)
                rpy_str = "%.3f,%.3f,%.3f" % (float(rpy[0]), float(rpy[1]), float(rpy[2]))

                wm_object = ET.SubElement (wm_root, "object")

                wm_object.set('rpy', rpy_str)
                wm_object.set('pos', pos_str)
                wm_object.set("type", object_type)
                wm_object.set("id", id)

            wm_tree = ET.ElementTree(wm_root)
            output_filename = "%s%05d.xml" % (wm_file_full_prefix, n)
            wm_tree.write(output_filename)
            pass
        pass
    pass




if __name__ == '__main__':
    run(sys.argv[1:])
