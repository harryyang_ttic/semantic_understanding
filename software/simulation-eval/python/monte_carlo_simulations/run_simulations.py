#!/usr/bin/python
import os
import sys
import subprocess
import signal
import glib
import gobject
import glob
import argparse
import time
import shutil
import random

import xml.etree.ElementTree as ET


import lcm
import bot_procman.sheriff as sheriff
from bot_procman.sheriff import SheriffCommandSpec, DEFAULT_STOP_SIGNAL, DEFAULT_STOP_TIME_ALLOWED, RUNNING

from bot_procman.orders_t import orders_t
from bot_param.set_t import set_t
from bot_param.entry_t import entry_t
from policy_status.status_t import status_t
from sp.instruction_t import instruction_t
from slam.object_t import object_t
from sp.screenshot_t import screenshot_t
import sp.object_t

try:
    from bot_procman.build_prefix import BUILD_PREFIX
except ImportError:
    BUILD_PREFIX = None


import math

# Time to wait before publishing instruction
WAIT_TO_PUBLISH_INSTRUCTION_SECONDS = 4

# Abort current simulation if it takes longer than this threshold
MAX_DURATION_SECONDS = 240


SP_HOME = BUILD_PREFIX + "/../../"


def find_bot_procman_deputy_cmd():
    search_path = []
    if BUILD_PREFIX is not None:
        search_path.append("%s/bin" % BUILD_PREFIX)
    search_path.extend(os.getenv("PATH").split(":"))
    for dirname in search_path:
        fname = "%s/bot-procman-deputy" % dirname
        if os.path.exists(fname) and os.path.isfile(fname):
            return fname
    return None

def find_bot_procman_sheriff_cmd():
    search_path = []
    if BUILD_PREFIX is not None:
        search_path.append("%s/bin" % BUILD_PREFIX)
    search_path.extend(os.getenv("PATH").split(":"))
    for dirname in search_path:
        fname = "%s/bot-procman-sheriff" % dirname
        if os.path.exists(fname) and os.path.isfile(fname):
            return fname
    return None

def find_bot_param_tool_cmd():
    search_path = []
    if BUILD_PREFIX is not None:
        search_path.append("%s/bin" % BUILD_PREFIX)
    search_path.extend(os.getenv("PATH").split(":"))
    for dirname in search_path:
        fname = "%s/bot-param-tool" % dirname
        if os.path.exists(fname) and os.path.isfile(fname):
            return fname
    return None


class MonteCarloSimulator(object):

    def __init__(self, src_dir, dest_dir, cmd_string,
                 options={'viewer': False, 'verbose': False, 'publish-value-map': False, 'known-map': False, 'coverage': False, 'show-h2sl-gui': False}):
        self.verbose = options['verbose']
        self.src_dir = os.path.abspath(src_dir)
        self.dest_dir = os.path.abspath(dest_dir)
        self.simulation_dir = None # Directory where results of current simulation will be placed
        self.cmd_string = cmd_string
        self.mainloop = None

        self.lcm = lcm.LCM()
        self.sheriff = sheriff.Sheriff (self.lcm)


        # Create a random temporary directory
        random.seed()
        tmp_dir = '/tmp/%d' % (random.random()*1000)
        while (os.path.isdir(tmp_dir)):
            tmp_dir = '/tmp.%d' % (random.random()*1000)

        self.tmp_dir = tmp_dir
        if self.verbose:
            print "Creating directory %s where we will temporarily store files" % (self.tmp_dir)
        os.mkdir(self.tmp_dir)

        # Start a deputy
        self.deputy_stdout_fd = open(self.dest_dir + '/deputy.stdout', 'w')
        self.deputy_stderr_fd = open(self.dest_dir + '/deputy.stderr', 'w')
        if self.verbose:
            print "Starting local deputy with name \"localhost\""
        bot_procman_deputy_cmd = find_bot_procman_deputy_cmd()
        args = [ bot_procman_deputy_cmd, "-n", "localhost"]
        if not bot_procman_deputy_cmd:
            sys.stderr.write("Can't find bot-procman-deputy binary")
            self.deputy_stdout_fd.close()
            self.deputy_stderr_fd.close()
            sys.exit(1)
        self.deputy = subprocess.Popen(args, stdout=self.deputy_stdout_fd, stderr=self.deputy_stderr_fd)

        self.sheriff_observer = None
        if options['sheriff'] is True:
            self.sheriff_observer_stdout_fd = open(self.dest_dir + '/sheriff.stdout', 'w')
            self.sheriff_observer_stderr_fd = open(self.dest_dir + '/sheriff.stderr', 'w')
            bot_procman_sheriff_cmd = find_bot_procman_sheriff_cmd()
            args = [ bot_procman_sheriff_cmd, "-o"]
            if not bot_procman_sheriff_cmd:
                sys.stderr.write("Can't find bot-procman-sheriff binary")
                self.sheriff_observer_stdout_fd.close()
                self.sheriff_observer_stderr_fd.close()
                sys.exit(1)
            self.sheriff_observer = subprocess.Popen(args, stdout=self.sheriff_observer_stdout_fd, stderr=self.sheriff_observer_stderr_fd)

        # Subscribe to policy status message
        self.lcm.subscribe("POLICY_STATUS", self.on_policy_status)

        self.worldmodel_files = glob.glob(self.src_dir + '/*.xml')
        self.worldmodel_file_index = -1
        self.current_cmd_string = None
        self.current_ground_truth_id = -1
        self.running = False
        self.overall_start_time = time.time()
        self.current_simulation_start_time = None
        self.command_sent = False

        self.semantic_mapping_logfile_last = None
        self.planner_logfile_last = None
        self.lcm_logfile_last = None
        
        # Create a readme
        self.readme = self.dest_dir + '/README.txt'
        readme_exists = os.path.exists(self.readme)
        f = open(self.readme, 'a')

        # Append a header
        s = "# " + time.asctime(time.localtime()) + '\n'
        f.write(s)
        s = "# World Model Directory = " + self.src_dir + '\n'
        f.write(s)
        s = '# Output Directory = ' + self.dest_dir + '\n'
        f.write(s)
        if options['fov-range']:
            s = '# FOV: Range = %s\n' % (options['fov-range'])
        else:
            s = '# FOV: Range = default\n'
        f.write(s)
        if options['fov-bearing']:
            s = '# FOV: Bearing = %s\n' % (options['fov-bearing'])
        else:
            s = '# FOV: Bearing = default\n'
        f.write(s)
        f.write('# World Model File, Command, Known-Map/Standard/Coverage, Ground Truth ID, Time Required, Error Code, LCM Log, Semantic Mapping Log, Planner Log\n')
        f.close()


        # Commands to be run:
        #    1) bot-param-server (persistent, but run separately)
        #    2) sp-simulator (re-run with each simulation)
        #    3) sp-semantic-map (re-run with each simulation)
        #    4) planner (re-run with each simulation)
        #    5) h2sl (re-run with each simulation)
        #    6) lcm-logger (re-run with each simulation)
        #    7) viewer (optional)
        #    8) sp-wheelchair-bridge
        #    9) sp-collision-check
        self.param_server_command = []
        self.persistent_commands = []
        self.intermittent_commands = []
        self.lcm_logger_command = None

        # param server
        spec = SheriffCommandSpec()
        spec.deputy_name = "localhost"
        spec.exec_str = BUILD_PREFIX + "/bin/bot-param-server " + SP_HOME + "config/wheelchair.cfg"
        spec.command_id = "param-server"
        spec.auto_respawn = 0
        spec.stop_signal = DEFAULT_STOP_SIGNAL
        spec.stop_time_allowed = DEFAULT_STOP_TIME_ALLOWED

        cmd = self.sheriff.add_command (spec)
        self.param_server_command.append(cmd)

        # If we are using the default values from the config file, we don't have to wait
        # on the param server to get the new values
        if options['fov-range'] is None and options['fov-bearing'] is None:
            self.param_server_ready = True
        else:
            self.param_server_ready = False
        
        if options['viewer']:
            spec.exec_str = BUILD_PREFIX + "/bin/sp-viewer"
            spec.command_id = "viewer"

            cmd = self.sheriff.add_command (spec)
            self.persistent_commands.append(cmd)


        # sp-wheelchair-bridge (aka goal translator)
        spec.exec_str = BUILD_PREFIX + "/bin/sp-wheelchair-bridge"
        spec.command_id = "wheelchair-bridge"

        cmd = self.sheriff.add_command (spec)
        self.persistent_commands.append(cmd)


        # sp-collision-check
        spec.exec_str = BUILD_PREFIX + "/bin/sp-collision-check"
        spec.command_id = "collision-check"

        cmd = self.sheriff.add_command (spec)
        self.persistent_commands.append(cmd)

        
        # sp-semantic-map
        # Are we running with a known map
        if options['known-map']:
           oracle_args = "-u -p"
        elif options['coverage']:
            oracle_args = "-i"
        else:
            oracle_args = ""
            
        if options['disable-resampling']:
            spec.exec_str = BUILD_PREFIX + "/bin/sp-semantic-map " + oracle_args + " --xmin -1 --xmax 14 --ymin -6 --ymax 6 -n %d --disable-resampling -o %s/semantic_mapping.log" % (options['num-particles'], self.tmp_dir)
        else:
            spec.exec_str = BUILD_PREFIX + "/bin/sp-semantic-map " + oracle_args + " --xmin -1 --xmax 14 --ymin -6 --ymax 6 -n %d -o %s/semantic_mapping.log" % (options['num-particles'], self.tmp_dir)

        spec.command_id = "semantic-mapping"

        cmd = self.sheriff.add_command (spec)
        self.intermittent_commands.append(cmd)

        
        # planner
        if options['publish-value-map']:
            value_map_arg = ''
        else:
            value_map_arg = '--no-value_map '

        if options['coverage']:
            planner_coverage_arg = '--exploration '
        else:
            planner_coverage_arg = ''
            
        spec.exec_str = BUILD_PREFIX + "/bin/policy_executor " + value_map_arg + planner_coverage_arg + " --simulation --graph_ranges -1 14 -6 6 --policy_cycles 150 --log %s/planner.log" % (self.tmp_dir)
        spec.command_id = "planner"

        cmd = self.sheriff.add_command (spec)
        self.intermittent_commands.append(cmd)

        
        # nlu
        if options['show-h2sl-gui']:
            h2sl_cmd = '/bin/h2sl-sp-gui '
        else:
            h2sl_cmd = '/bin/h2sl-sp-commandline '
            
        spec.exec_str = BUILD_PREFIX + h2sl_cmd + "--grammar " + SP_HOME + "data/h2sl_sp/grammar.xml --llm_annotation " + SP_HOME + "data/h2sl_sp/llm_annotation.xml --llm_behavior " + SP_HOME + "data/h2sl_sp/llm_behavior.xml"
        spec.command_id = "nlu"

        cmd = self.sheriff.add_command (spec)
        self.intermittent_commands.append(cmd)


        # simulator
        #    we will copy each world model xml file to world.xml and then restart the simulator
        if options['known-map']:
            spec.exec_str = BUILD_PREFIX + "/bin/sp-simulator -u -x %s/world.xml" % (self.tmp_dir)
        else:
            spec.exec_str = BUILD_PREFIX + "/bin/sp-simulator -x %s/world.xml" % (self.tmp_dir)
        spec.command_id = "simulator"

        cmd = self.sheriff.add_command (spec)
        self.intermittent_commands.append(cmd)



    def stop_current_simulation(self,errorcode):
        self.stop_processes (self.intermittent_commands)

        # stop and remove the logger
        self.sheriff.stop_command (self.lcm_logger_command)
        self.sheriff.schedule_command_for_removal(self.lcm_logger_command)
        self.lcm_logger_command = None
        self.running = False

       # Save the logs from the semantic mapping and planner processes
        self.cycle_logs()
                
        # Update the README
        dt = time.time() - self.current_simulation_start_time
        f = open(self.readme, 'a')
        semantic_mapping_logfile = "unknown"
        if self.semantic_mapping_logfile_last:
            semantic_mapping_logfile = self.semantic_mapping_logfile_last
            
        planner_logfile = "unknown"
        if self.planner_logfile_last:
            planner_logfile = self.planner_logfile_last

        lcm_logfile = "unknown"
        if self.lcm_logfile_last:
            lcm_logfile = self.lcm_logfile_last

        if options['known-map']:
            scenario = "Known Map"
        elif options['coverage']:
            scenario = "Coverage"
        else:
            scenario = "Standard"
            
        s = os.path.basename(self.worldmodel_files[self.worldmodel_file_index]) + ", command=\"" + self.current_cmd_string + "\", " + scenario + ", %d, %.4f, %s, %s, %s, %s\n" % (self.current_ground_truth_id, dt, errorcode,
                                                                                                                                                                 lcm_logfile, semantic_mapping_logfile, planner_logfile)
        f.write(s)
        f.close()

 

    def start_next_simulation(self):
        if self.running:
            print "Error: It seems that a simulation is running. Can't start a new one"
            return

        # Are we done?
        if len(self.worldmodel_files) == (self.worldmodel_file_index+1):
            print "Finished"
            self.cleanup()
            sys.exit(0)

        next_worldmodel = self.worldmodel_files[self.worldmodel_file_index+1]
        dest = '%s/world.xml' % (self.tmp_dir)
        shutil.copyfile (next_worldmodel, dest)

        temp = os.path.splitext(os.path.basename(next_worldmodel))
        next_worldmodel_noext = temp[0]

        self.simulation_dir = self.dest_dir + "/" + next_worldmodel_noext
        if os.path.isdir(self.simulation_dir) == False:
            if self.verbose:
                print "creating " + self.simulation_dir
            os.mkdir(self.simulation_dir)
        #else:
        #    if self.verbose:
        #        print "Output directory " + self.simulation_dir + " already exists"

        # Copy the world model to the output directory
        if (os.path.exists(self.simulation_dir + "/" + os.path.basename(os.path.basename(next_worldmodel)))) == False:
            shutil.copyfile(next_worldmodel, self.simulation_dir + "/" + os.path.basename(os.path.basename(next_worldmodel)))
            
        # Are we using the command string specified in the XML?
        if self.cmd_string == None:
            wm_tree = ET.parse(next_worldmodel)
            wm_root = wm_tree.getroot()
            for scenario in wm_root.findall('scenario'):
                self.current_cmd_string = scenario.get('command')
                self.current_ground_truth_id = int(scenario.get('ground_truth_id'))
                break
        else:
            self.current_cmd_string = self.cmd_string
            
        self.worldmodel_file_index = self.worldmodel_file_index + 1

        # Instantiate the logger
        #if options['known-map']:
        #    lcm_logfile = os.path.splitext(os.path.basename(next_worldmodel))[0] + ".known-map.lcm_log"
        #else:
        #    lcm_logfile = os.path.splitext(os.path.basename(next_worldmodel))[0] + ".standard.lcm_log"

        lcm_logfile = self.tmp_dir + "/lcm.log"
        spec = SheriffCommandSpec()
        spec.deputy_name = "localhost"
        if options['minimal-logging']:
            minimal_args = '-v --channel=\"BEHAVIOR|REGION_PARTICLE_ISAM_RESULT\" '
        else:
            minimal_args = ' '
            
        #spec.exec_str = BUILD_PREFIX + "/bin/lcm-logger -f " + self.simulation_dir + "/" + lcm_logfile
        spec.exec_str = BUILD_PREFIX + "/bin/lcm-logger -f " + minimal_args + lcm_logfile
        spec.command_id = "logger"
        spec.auto_respawn = 0
        spec.stop_signal = DEFAULT_STOP_SIGNAL
        spec.stop_time_allowed = DEFAULT_STOP_TIME_ALLOWED

        self.lcm_logger_command = self.sheriff.add_command(spec)
        self.sheriff.start_command(self.lcm_logger_command)

        self.running = True


        print "\n"
        print "Run %d of %d: Starting with world model %s" % (self.worldmodel_file_index+1,  len(self.worldmodel_files), next_worldmodel)

        # Start the intermittent processes
        self.start_processes (self.intermittent_commands)
        self.sheriff.send_orders()

        self.command_sent = False

        self.current_simulation_start_time = time.time()

    # Moves the semantic mapping and planner logs from self.tmp_dir to dest_dir,
    # appending an incremented suffix so as to not overwrite existing logs
    def cycle_logs (self):
        worldmodel_file = self.worldmodel_files[self.worldmodel_file_index]
        semantic_mapping_logfile = None
        self.semantic_mapping_logfile_last = None
        for i in range(100):
            if options['known-map']:
                semantic_mapping_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".known-map.semantic_mapping_log.%02d" % (i)
            elif options['coverage']:
                semantic_mapping_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".coverage.semantic_mapping_log.%02d" % (i)
            else:
                semantic_mapping_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".standard.semantic_mapping_log.%02d" % (i)

            semantic_mapping_logfile = self.simulation_dir + "/" + semantic_mapping_logfile_basename
            if (os.path.exists(semantic_mapping_logfile) == False):
                self.semantic_mapping_logfile_last = semantic_mapping_logfile_basename
                break

        if semantic_mapping_logfile:
            if self.verbose:
                print "copying %s/semantic_mapping.log to %s" % (self.tmp_dir, semantic_mapping_logfile)
            if os.path.exists(self.tmp_dir + '/semantic_mapping.log'):
                shutil.copyfile (self.tmp_dir + '/semantic_mapping.log', semantic_mapping_logfile)
            else:
                print "Error copying semantic mapping logfile to %s: Temporary logfile %s/semantic_mapping.log doesn't exist " % (semantic_mapping_logfile, self.tmp_dir) 
        else:
            print "Error: Unable to save semantic mapping process log file"

        planner_logfile = None
        self.planner_logfile_last = None
        for j in range(100):
            if options['known-map']:
                planner_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".known-map.planner_log.%02d" % (j)
            elif options['coverage']:
                planner_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".coverage.planner_log.%02d" % (j)
            else:
                planner_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".standard.planner_log.%02d" % (j)

            planner_logfile = self.simulation_dir + "/" + planner_logfile_basename
            if (os.path.exists(planner_logfile) == False):
                self.planner_logfile_last = planner_logfile_basename
                break

        if (i != j):
            print "Error: Suffix for semantic mapping log file (%02d) and planner log file (%02d) differ" % (i,j)

        if planner_logfile:
            if self.verbose:
                print "copying %s/planner.log to %s" % (self.tmp_dir, planner_logfile)
            if os.path.exists(self.tmp_dir + '/planner.log'):
                shutil.copyfile (self.tmp_dir + '/planner.log', planner_logfile)
            else:
                print "Error copying planner logfile to %s: Temporary logfile %s/planner.log doesn't exist " % (self.tmp_dir, planner_logfile)    
        else:
            print "Error: Unable to save planner process log file"



        lcm_logfile = None
        self.lcm_logfile_last = None
        for k in range(100):
            if options['known-map']:
                lcm_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".known-map.lcm_log.%02d" % (k)
            elif options['coverage']:
                lcm_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".coverage.lcm_log.%02d" % (k)
            else:
                lcm_logfile_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".standard.lcm_log.%02d" % (k)

            lcm_logfile = self.simulation_dir + "/" + lcm_logfile_basename
            if (os.path.exists(lcm_logfile) == False):
                self.lcm_logfile_last = lcm_logfile_basename
                break

        if (i != k):
            print "Error: Suffix for semantic mapping log file (%02d) and LCM log file (%02d) differ" % (i,k)

        if (j != k):
            print "Error: Suffix for planner mapping log file (%02d) and LCM log file (%02d) differ" % (j,k)

        if lcm_logfile:
            if self.verbose:
                print "copying %s/lcm.log to %s" % (self.tmp_dir, lcm_logfile)
            if os.path.exists(self.tmp_dir + '/lcm.log'):
                shutil.copyfile (self.tmp_dir + '/lcm.log', lcm_logfile)
            else:
                print "Error copying LCM logfile to %s: Temporary LCM logfile %s/lcm.log doesn't exist " % (self.tmp_dir, lcm_logfile)
        else:
            print "Error: Unable to save LCM log file"
            

            

        if os.path.exists(self.tmp_dir + '/screenshot.ppm'):
            screenshot = None
            self.screenshot_last = None
            for l in range(100):
                if options['known-map']:
                    screenshot_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".known-map.%02d.ppm" % (l)
                elif options['coverage']:
                    screenshot_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".coverage.%02d.ppm" % (l)
                else:
                    screenshot_basename = os.path.splitext(os.path.basename(worldmodel_file))[0] + ".standard.%02d.ppm" % (l)

                screenshot = self.simulation_dir + "/" + screenshot_basename
                if (os.path.exists(screenshot) == False):
                    self.screenshot_last = screenshot_basename
                    break
            
            if screenshot:
                if self.verbose:
                    print "copying %s/screenshot.ppm to %s" % (self.tmp_dir, screenshot)
                shutil.copyfile (self.tmp_dir + '/screenshot.ppm', screenshot)
            else:
                print "Error: Unable to save screenshot"



    def stop_processes (self, cmds):
        for cmd in cmds:
            self.sheriff.stop_command(cmd)
        return


    def start_processes (self, cmds):
        for cmd in cmds:
            self.sheriff.start_command(cmd)
        return



    def terminate_deputy(self):
        if not self.deputy:
            print "No deputy exists to terminate"
            return

        if self.verbose:
            print "Terminating deputy"

        try:
            self.deputy.terminate()
        except AttributeError: # python 2.4, 2.5 don't have Popen.terminate()
            os.kill(self.deputy.pid, signal.SIGTERM)
            self.deputy.wait()
        self.deputy = None

        # Close the stdout/stderr files
        self.deputy_stdout_fd.close()
        self.deputy_stderr_fd.close()


    def terminate_sheriff_observer(self):
        if not self.sheriff_observer:
            print "No sheriff exists to terminate"
            return

        if self.verbose:
            print "Terminating sheriff"

        try:
            self.sheriff_observer.terminate()
        except AttributeError: # python 2.4, 2.5 don't have Popen.terminate()
            os.kill(self.sheriff_observer.pid, signal.SIGTERM)
            self.sheriff_observer.wait()
        self.sheriff_observer = None

        # Close the stdout/stderr files
        self.sheriff_observer_stdout_fd.close()
        self.sheriff_observer_stderr_fd.close()


    def on_policy_status (self, channel, data):
        msg = status_t.decode (data)

        if msg.current_policy_status == status_t.STATUS_ERROR:
            print "Error: policy_status_t reported STATUS_ERROR"

        if msg.current_policy_status == status_t.STATUS_DONE:
            if self.running:
                print "Planner status indicates that destination has been reached"

                # Publish a screenshot command for the viewer
                msg = screenshot_t()
                msg.utime = int(time.time() * 1000000)
                msg.filename = self.tmp_dir + '/screenshot.ppm'
                self.lcm.publish ("SCREENSHOT", msg.encode())
                
                # Stop the current simulation
                # wait for executor to stop cleanly
                time.sleep(1)
                self.stop_current_simulation("NONE")


    # Status timer
    def on_timer(self):

        # The param server gets started first
        for pcmd in self.param_server_command:
            if pcmd.pid <= 0:
                self.sheriff.start_command (pcmd)
            else:
                if self.param_server_ready == False:
                    if options['fov-range'] is not None:
                        args = [ find_bot_param_tool_cmd(),'simulator.observation_model.range', str(options['fov-range'])]
                        subprocess.call(args)
                    if options['fov-bearing'] is not None:
                        args = [ find_bot_param_tool_cmd(),'simulator.observation_model.fov', str(options['fov-bearing'])]
                        subprocess.call(args)

                    time.sleep(0.5)
                    self.param_server_ready = True

        for pcmd in self.persistent_commands:
            if pcmd.pid <= 0 and self.param_server_ready == True:
                if self.verbose:
                    print "Command %s not running: Starting" % (pcmd.command_id)
                self.sheriff.start_command (pcmd)



        if self.running and (self.command_sent == False):
            dt = time.time() - self.current_simulation_start_time
            if (dt >= WAIT_TO_PUBLISH_INSTRUCTION_SECONDS):

                # Publish the instruction
                msg = instruction_t()
                msg.utime = int(time.time() * 1000000)
                msg.text = self.current_cmd_string
                self.lcm.publish("INSTRUCTION", msg.encode())
                self.command_sent = True
    
                # Publish the id of the ground truth object goal, but sleep to make sure logger has started
                time.sleep(0.5)
                msg = object_t()
                msg.id = self.current_ground_truth_id
                msg.pos = [0, 0, 0] # leave these blank for now
                msg.quat = [0, 0, 0, 1]
                sp_object = sp.object_t()
                sp_object.name = "unknown"
                sp_object.type = sp_object.UNKNOWN
                msg.properties = sp_object
                self.lcm.publish ("GROUND_TRUTH_GOAL", msg.encode())


        # This is where simulations get started. After the previous simulation finishes,
        # the self.running flag is set to False. When the timer fires, we check to see that the
        # relevant processes have exited, at which point we start the next simulation
        if self.running == False and self.param_server_ready == True:

            # If the processes have exited, we can start a new simulation
            processes_exited = True
            for icmd in self.intermittent_commands:
                if icmd.pid > 0:
                    if self.verbose:
                        print "Waiting for %s to exit" % (icmd.command_id)
                    self.sheriff.stop_command (icmd)
                    processes_exited = False


            if processes_exited:
                if self.verbose:
                    print "Status indicates that we are done with this run (running=False) and all processes have exited. Starting next run"
                self.start_next_simulation()

            return True

        if self.running == False:
            return True

        dt = time.time() - self.current_simulation_start_time
        if (dt > MAX_DURATION_SECONDS):
            print "It has been %d seconds since the current simulation started. Aborting" % (dt)
            self.stop_current_simulation("TIMEOUT")


        return True


    def cleanup(self):
        self.stop_processes (self.persistent_commands)
        self.stop_processes (self.intermittent_commands)
        if self.lcm_logger_command:
            if self.lcm_logger_command.status() == RUNNING:
                self.stop_processes ([self.lcm_logger_command])

        self.terminate_deputy()

        if self.sheriff_observer:
            self.terminate_sheriff_observer()

        # Remove the temporary directory
        shutil.rmtree(self.tmp_dir)


    def run(self):
        self.mainloop = glib.MainLoop()

        # Start a status timer to catch scenarios when current simulation takes too long
        gobject.timeout_add(1000, self.on_timer)

        # LCM handling
        gobject.io_add_watch(self.lcm, gobject.IO_IN, lambda *s: self.lcm.handle() or True)
        gobject.timeout_add(1000, lambda *s: self.sheriff.send_orders() or True)

        signal.signal(signal.SIGINT, lambda *s: self.mainloop.quit())
        signal.signal(signal.SIGTERM, lambda *s: self.mainloop.quit())
        signal.signal(signal.SIGHUP, lambda *s: self.mainloop.quit())

        try:
            self.mainloop.run()
        except:
            pass

        print "Terminating"
        self.cleanup()
        return 0




def run_simulations(src_dir, dest_dir, command, options):
    simulator = MonteCarloSimulator(src_dir, dest_dir, command, options)

    simulator.run()
    #simulator.terminate_deputy()


if __name__ == '__main__':

    dest_dir = os.getcwd()
    parser = argparse.ArgumentParser(description='Perform monte carlo simulations',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--command', help='Command to be issued (default is to use the one specified in the world XML)', required=False)
    parser.add_argument('src_dir', help='Directory containing the world models')
    parser.add_argument('-d', '--destination', help='Directory in which to place the results (default is current directory)')
    parser.add_argument('-n', '--num-particles', help='Number of particles', default=20, type=int, dest='num_particles')
    parser.add_argument('-r', '--fov-range', help='FOV range to use for simulations', default=None, type=float, dest='fov_range')
    parser.add_argument('-b', '--bearing', help='FOV angle to use for simulations', default=None, type=float, dest='fov_bearing')
    parser.add_argument('--disable-resampling',help='Disaable resampling',action='store_true',default=False,dest='disableresampling')
    parser.add_argument('--verbose',help='Verbose output',action='store_true',default=False)
    parser.add_argument('--viewer',help='Spawn a viewer instance',action='store_true',default=False)
    parser.add_argument('--publish-value-map',help='Have planner publish value map',action='store_true',default=False,dest='valuemap')
    parser.add_argument('--known-map',help='Run scenario in which the map is known',action='store_true',default=False,dest='knownmap')
    parser.add_argument('--coverage',help='Run coverage-based scenario (CURRENTLY NOT SUPPORTED)',action='store_true',default=False,dest='coverage')
    parser.add_argument('--minimal-logging',help='Log minimal messages (no BEHAVIOR or REGION_PARTICLE_ISAM_RESULT)',action='store_true',default=False,dest='minimallogging')
    parser.add_argument('--show-h2sl-gui',help='Run H2SL in non-headless mode',action='store_true',default=False,dest='h2slgui')
    parser.add_argument('--sheriff',help='Spawn a sheriff in observer mode',action='store_true',default=False)

    args = parser.parse_args()

    if args.destination:
        dest_dir = args.destination

    if os.path.isdir(dest_dir) == False:
        print 'Directory ' + dest_dir + ' does not exist. Exiting'
        exit(0)

    num_particles = args.num_particles
    if args.knownmap==True:
        num_particles = 1
        print "Running with known map. Setting number of particles to %d" % (num_particles)

    if args.coverage==True:
        num_particles = 1
        print "Running coverage mode. Setting number of particles to %d" % (num_particles)
        
    if args.knownmap==True and args.coverage==True:
        print "Must select either coverage mode OR known-map mode"
        exit(0)
        
    options = dict()
    options['num-particles'] = num_particles
    options['fov-range'] = args.fov_range
    options['fov-bearing'] = args.fov_bearing
    options['disable-resampling'] = args.disableresampling
    options['verbose'] = args.verbose
    options['viewer'] = args.viewer
    options['sheriff'] = args.sheriff
    options['publish-value-map'] = args.valuemap
    options['known-map'] = args.knownmap
    options['coverage'] = args.coverage
    options['show-h2sl-gui'] = args.h2slgui
    options['minimal-logging'] = args.minimallogging

    run_simulations(args.src_dir, dest_dir, args.command, options)
