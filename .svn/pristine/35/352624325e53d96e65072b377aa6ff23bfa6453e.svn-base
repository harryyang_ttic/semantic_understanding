from pyTklib.pyTklib import tklib_normalize_theta, tklib_normalize_theta_array, tklib_init_rng, tklib_log_gridmap
from pyTklib.pyTklib import kNN_index, tklib_euclidean_distance
from scipy import arctan2, transpose, cos, sin, array, zeros, argmin, mean, arange, ones, isnan, nan
from tklib.spectral_clustering import spectral_clustering_W
from tklib.spectral_clustering_utils import dists2weights_perona

class rrg_graph:
    def __init__(self, init_loc, max_nn=100):
        tklib_init_rng(0)
        self.root_I = 0
        self.nodes_pts = [init_loc]
        self.graph = {}
        self.max_nn = max_nn
        #self.expansion_dist = 1.0
    
    #this adds a node to the RRT
    def expand_graph(self):
        map = self.get_map()
        
        sloc = map.get_random_open_location(0.0)
        
        #get nearest neighbors (from_loc)
        I_nn = kNN_index(sloc, transpose(self.nodes_pts), 
                         min(self.max_nn, len(self.nodes_pts)));

        is_visible = False; from_loc = None; theta = None;
        for j in range(len(I_nn)):
            i = I_nn[j]
            from_loc = self.nodes_pts[int(i)]

            #get the orientation from the nearest neighbor and 
            #    ray trace to see if the path is free
            theta = arctan2(sloc[1]-from_loc[1], sloc[0]-from_loc[0])
            d, = map.ray_trace(from_loc[0], from_loc[1], [theta])
            d_true = tklib_euclidean_distance(from_loc, sloc);

            #if the path is free then extend by that length
            if(d > d_true):
                d=d_true
                is_visible = True
                break;
        
        if(not is_visible):
            return
        
        #get the new node and add it to the set of nodes
        new_node_xy = [from_loc[0]+d*cos(theta), from_loc[1]+d*sin(theta)]
        self.nodes_pts.append(new_node_xy)
        
        #now add the relevant connections to the neighbors
        for i in I_nn:
            theta = arctan2(self.nodes_pts[int(i)][1]-new_node_xy[1], 
                            self.nodes_pts[int(i)][0]-new_node_xy[0])
            
            d, = map.ray_trace(new_node_xy[0], new_node_xy[1], [theta])
            d_true = tklib_euclidean_distance(new_node_xy, self.nodes_pts[int(i)]);
            
            if(d > d_true):
                if(self.graph.has_key(int(i))):
                    self.graph[int(i)].add((len(self.nodes_pts)-1))
                else:
                    self.graph[int(i)] = set([len(self.nodes_pts)-1])
                
                if(self.graph.has_key(len(self.nodes_pts)-1)):
                    self.graph[len(self.nodes_pts)-1].add(int(i))
                else:
                    self.graph[len(self.nodes_pts)-1] = set([int(i)])


    def get_path_xyth_interpolate(self, pose1, poses_XYTh, use_interpolated_dest_orientation=False):
        
        D, Paths = self.get_path_xyth(pose1, poses_XYTh)
        
        Paths_Int = []
        for j in range(len(D)):
            path=Paths[j]
            #X_r = [0,0]; Y_r = [0,0.1]; Th_r = [0,0.1];
            X_r = [pose1[0]]; Y_r = [pose1[1]]; Th_r = [pose1[2]];

            for i in range(1,len(path[0])):
                x1, y1, th1 = path[:,i-1]
                x2, y2, th2 = path[:,i]

                if(x1 == x2 and y1 == y2):
                    X, Y, Th =self.interpolate_theta(x1, y1, th1, th2, 0.0174)
                elif(th1 == th2):
                    X, Y, Th =self.interpolate_path(x1, y1, x2, y2, th1, 0.1)
                else:
                    print "path should not exist"
                    raise 
                
                X_r.extend(X); Y_r.extend(Y); Th_r.extend(Th);
            Paths_Int.append([X_r, Y_r, Th_r])
        
        return D, Paths_Int

    def interpolate_theta(self, x, y, th1, th2, angle):
        if(tklib_normalize_theta(th2-th1) >= 0):
            print "turn left", th1, "to", th2
            Th = arange(0, tklib_normalize_theta(th2-th1), angle)+th1
            Th = tklib_normalize_theta_array(Th)
        else:
            print "turn right from ", th1, "to", th2
            Th = arange(0, tklib_normalize_theta(th2-th1), -1.0*angle)+th1
            Th = tklib_normalize_theta_array(Th)
        
        X = zeros(len(Th))*1.0 + x; 
        Y = zeros(len(Th))*1.0 + y;

        return [X, Y, Th]
    
    def interpolate_path(self, x1, y1, x2, y2, th1, dist):
        d = tklib_euclidean_distance([x1,y1], [x2,y2])
        th = arctan2(y2-y1, x2-x1)
        
        D = arange(0, d, dist)
        Th = ones(len(D))*1.0*th
        
        X = D*cos(Th)+x1; Y=D*sin(th)+y1;
        
        return [X, Y, Th]

    def get_path_xyth(self, pose1, poses_XYTh):
        D, paths_XY = self.get_path(pose1[0:2], poses_XYTh[0:2,:])
        
        paths_XYTh = []
        for j in range(len(D)):
            path = paths_XY[j]

            th1 = arctan2(path[1,0]-pose1[1], path[0,0]-pose1[0])
            X = [pose1[0], pose1[0]]; Y = [pose1[1], pose1[1]]; Th = [pose1[2], th1];
        
            for i in range(len(path[0])):
                o_th = Th[-1]
                X.append(path[0,i]); Y.append(path[1,i]); Th.append(o_th);
            
                if(i < len(path[0])-1):
                    n_th = arctan2(path[1,i+1]-path[1,i], path[0,i+1]-path[0,i]);
                    X.append(path[0,i]); Y.append(path[1,i]); Th.append(n_th);
            
            pose2 = poses_XYTh[:,j]
            th_end = arctan2(pose2[1]-Y[-1], pose2[0]-X[-1])
            X.extend([X[-1], pose2[0], pose2[0]]); 
            Y.extend([Y[-1], pose2[1], pose2[1]]); 
            Th.extend([th_end, th_end, pose2[2]]);
            paths_XYTh.append(array([X,Y,Th]))
            
        return D, paths_XYTh            

    #need to fill out
    def get_path(self, xy1, dests_XY):
        #get the relevant indices
        i1, = kNN_index(xy1, transpose(self.nodes_pts), 1)
        
            
        #initialize the various datastructures
        dist = zeros(len(self.nodes_pts))+10.0e100
        dist[i1] = 0
        prev = zeros(len(self.nodes_pts))-1
        
        Q = range(len(self.nodes_pts))
        
        while(len(Q) > 0):        
            i = argmin(dist.take(Q));
            u = Q[i]
            
            if(dist[u] > 10.0e99):
                break

            Q.remove(u)
            
            for v in self.graph[u]:
                d_alt = dist[u]+tklib_euclidean_distance(self.nodes_pts[u], 
                                                         self.nodes_pts[v])
                if(d_alt < dist[v]):
                    dist[v] = d_alt
                    prev[v] = u


        I = []
        for j in range(len(dests_XY[0])):
            I.append(kNN_index(dests_XY[:,j], transpose(self.nodes_pts), 1)[0])

        D = dist.take(I)
        
        paths_XY = []
        for i_dst in I:
            path = [int(i_dst)]
            while(prev[path[-1]]!=-1):
                path.append(int(prev[path[-1]]))

            #print path
            #paths_XY.append(transpose(array(self.nodes_pts).take(list(reversed(path)), axis=0)))
            paths_XY.append(transpose([self.nodes_pts[i] for i in reversed(path)]))
        
        return D, paths_XY



                    
class rrg_gridmap(rrg_graph):
    def __init__(self, map_filename, init_loc=None, lcm=False):
        tklib_init_rng(0);
        
        self.map_filename = map_filename

        self.lcm = lcm
        self.map = None
        self.map = self.get_map()
        
        if(init_loc == None):
            self.init_loc = self.map.get_random_open_location(0.5)
        else:
            self.init_loc = init_loc

        rrg_graph.__init__(self, self.init_loc)
        
    def get_map(self):
        if(self.map == None):
            self.map = tklib_log_gridmap()
            if (self.lcm):
                self.map.load_carmen_map_from_lcm_log (self.map_filename)
            else:
                self.map.load_carmen_map(self.map_filename)

        return self.map

    def create(self, num_expansions=100):
        map = self.get_map()
        
        for i in range(num_expansions):
            self.expand_graph()        


    def get_distance_matrix(self):
        dists = zeros([len(self.nodes_pts), len(self.nodes_pts)])-1.0
        for i, u in enumerate(self.graph.keys()):
            for j, v in enumerate(self.graph[u]):
                #print self.graph.keys()
                #print u, "to", v, " total:", len(self.nodes_pts)
                #print self.nodes_pts[u]
                #print self.nodes_pts[v]
                
                dists[u,v] = tklib_euclidean_distance(self.nodes_pts[u], self.nodes_pts[v])
                dists[v,u] = dists[u,v]

        return dists

    #cluster the regions and return the centroid and labels
    def _cluster_regions_spectral(self, alpha=0.5, t=0.185, max_dist=0.2):
        print "get distances"
        dists = self.get_distance_matrix()
        
        print "do spectral clustering"
        W = dists2weights_perona(dists, alpha)
        labels, k = spectral_clustering_W(W,None,numkmeans=1,seed_number=0,max_dist=max_dist,t=t)
        labels = labels[0]
        
        print "postprocess data and return"
        class_to_I = {}
        for i, c in enumerate(labels):
            if(isnan(c)):
                #print "was nan"
                #raw_input()
                continue
            if(class_to_I.has_key(int(c))):
                class_to_I[int(c)].append(i)
            else:
                class_to_I[int(c)] = [i]

        #get the centroid points and return these as a representative of the 
        # region
        means = []; Pts = [];
        for c in class_to_I:
            pts = array(self.nodes_pts).take(class_to_I[c], axis=0)
            Pts.append(pts)
            mpt = mean(pts, axis=0)
            
            #get the nearest points to each of the mean points
            i, = kNN_index(mpt, transpose(pts), 1)
            means.append(pts[int(i)])
        means = transpose(means)
        
        return means, Pts

    def cluster_regions_spectral(self, alpha=0.5, t=0.185, max_dist=0.2):
        return self._cluster_regions_spectral(alpha, t, max_dist)[0]
