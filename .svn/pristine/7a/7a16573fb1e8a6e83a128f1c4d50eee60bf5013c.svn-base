#This block should only store calibration data.
coordinate_frames {
    root_frame = "local";                 # a root_frame must be defined

    body {
        relative_to = "local";
        history = 1000;                    # number of past transforms to keep around,
        pose_update_channel = "POSE";      # use pose updates on POSE for body_to_local
        initial_transform{
            translation = [ 0, 0, 0 ];         # (x,y,z) translation vector
            quat = [ 1, 0, 0, 0 ];           # may be specified as a quaternion, rpy, rodrigues, or axis-angle
        }
    }

    
    global {
        relative_to = "local";
        history = 1000;
        update_channel = "GLOBAL_TO_LOCAL";
        initial_transform {
            translation = [0, 0, 0 ];
            quat = [1, 0, 0, 0];
        }
    }

    body_g {
        relative_to = "local";
        history = 1000;                    # number of past transforms to keep around,
        update_channel = "BODY_G_TO_GLOBAL";      # use pose updates on POSE for body_to_local
        initial_transform{
            translation = [ 0, 0, 0 ];         # (x,y,z) translation vector
            quat = [ 1, 0, 0, 0 ];           # may be specified as a quaternion, rpy, rodrigues, or axis-angle
        }
    }


    # These transformations correspond to going from RELATIVE_TO frame to the specified COORDINATE_FRAME
    # via the ordering: yaw(z) ---> pitch(y) ---> roll(x) where the angles are in DEGREES
    #
    # For example: The following specifies the transformation for the BROOM_REAR_LEFT from agile
    #              in which the broom is pitched down
    # BROOM_REAR_LEFT {
    #    relative_to = "body";
    #    history = 0;                       
    #    initial_transform{
    #        translation = [-1.345181, 0.382595, 2.640000];;
    #        rpy = [-0.000000, 33.122265, 133.750634];
    #    }
    #}

    SKIRT_FRONT {
        relative_to = "body";
        history = 0;                       
        initial_transform{
          #translation = [.44, .0, .1]; # Original (manual)
          #rpy      = [0, 0, 0];        # Original (manual)
            translation = [0.4460, -0.0101, 0.1];     # Based on 06-08-12 calibration
          #rpy         = [-1.6425,7.8375,0.3715];    # Based on 07-29-11 calibration 
            rpy         =  [-0.84, 1.6, 1.9218]; # 06-08-12 
        }
    }

    SKIRT_FULL {
        relative_to = "body";
        history = 0;                       
        initial_transform{
            translation = [0, 0, 0];     # Based on 06-08-12 calibration
            rpy         =  [0, 0, 0 ]; # 06-08-12 
        }
    }

    SKIRT_REAR {
        relative_to = "body";
        history = 0;
        initial_transform{
            #translation =[-0.45, -.02, .1]; #[.44, .0, .1]; # Original (manual)            
            translation =[-0.4561, -.0520, .1]; # 04-15-12 calibration (using scan match pose)                                                           
            #rpy      = [ 0, 0, 178.6];#[0, 0, 0];        # Original (manual)
            rpy      = [ 0, 0, -178.6]; #10-04-2014- manual # 04-15-12 calibration (using scan match pose)
        }
    }

    TOP_LASER {
        relative_to = "body";
        history = 0;                       
        initial_transform{
            translation = [.12, .276, .1];   
            rpy      = [0, 0, 0];
        }
    }

    HEAD {
        relative_to = "body";
        history = 1000;
        update_channel = "BODY_TO_HEAD";
        initial_transform {
            translation = [ 0.15, 0.2, 1.48 ];
            rpy         = [ 0, 9, -3 ];
        }
    } 

    HEAD_BASE {
        relative_to = "body";
        history = 0;
        initial_transform {
            translation = [ 0.15, 0.2, 1.48 ];
            rpy         = [ 0, 9, -3 ];
        }
    } 

    dragonfly {
        relative_to = "body";
        history = 0;
        initial_transform {
            #translation = [ 0.05, 0.05, 0.75 ];
            #translation = [ 0.05, 0.05, 0.547 ];
	    translation =  [ 0.235, 0.153, 0.547 ]; #[ 0.26, 0.16, 0.547 ];
            rpy          = [-90, 0, -90];
        }
    }

    ##Maybe its easier to define these with respect to the center camera
    dragonfly_left {
        relative_to = "dragonfly";
        history = 0;
        initial_transform {
            translation = [ -0.0875, 0.00, -0.035 ];
            rpy          = [ 0, -45, 0];
        }
    }

    dragonfly_right {
        relative_to = "dragonfly";
        history = 0;
        initial_transform {
            translation = [ 0.0875, 0.00, -0.035 ];
            rpy          = [ 0, 45, 0];
        }
    }

    projector {
        relative_to = "body";
        history = 0;
        initial_transform {
            translation = [ 0.05, 0.05, 0.75 ];
            rpy          = [-90, 0, -90];
        }
    }
    
    imu {
        relative_to = "body";
        history = 0;
        initial_transform {
            translation = [ 0, 0, 0.3 ];
            rpy         = [ 180, 0, 180 ]; # Upside down, facing backwards
        }     
    }

    VELODYNE {
        relative_to = "body";
        history = 0;
        initial_transform {
            #translation = [ 0.25, 0.19, 1.23 ]; # [ 0.25, 0.26, 1.21 ]; # Guess
            translation = [ 0.2159, 0.1697, 1.2243 ]; # 06-08-12 calibration
            rpy         = [ 6.1496, 0.3802, -90.4015]; # 06-08-12 calibration
          #rpy         = [ 0, 0, -90];
            #rpy         = [ 6.2, 0, -91.0]; #7.5
        }
    }
    
    base {
        relative_to = "body";
        history = 0;
        initial_transform{
           #translation = [ 0.127, -0.293, 0.8636 ]; # Guess
           translation =  [ 0.127, -0.278, 0.8636 ]; #Temporary
            rpy = [0, 0, 0];
        }
    }

    base_yaw {
        relative_to = "base";
        history = 1000;
        update_channel = "BASE_TO_BASE_YAW";
        initial_transform{
            translation = [ 0, 0, 0 ]; # Guess
            rpy = [0, 0, 0];
        }
    }
    shoulder {
        relative_to = "base_yaw";    
        history = 1000;    
        update_channel = "SHOULDER_TO_BASE";
        initial_transform{
            translation =  [ .04445, 0, 0 ]; #[ .047625, 0, 0 ];
            rpy = [90, 0, 0];
        }
    }

    elbow {
        relative_to = "shoulder";
        history = 1000;
        update_channel = "ELBOW_TO_SHOULDER";
        initial_transform {
            translation = [0.3895, 0, 0];
            rpy = [0, 0, 0];
        }
    }
    lateral_wrist {
        relative_to = "elbow";
        history = 1000;
        update_channel = "LWRIST_TO_ELBOW";
        initial_transform {
            translation = [0.3826, 0, 0];
            rpy = [0, 0, 0];    
        }
    }
    #these values are wrong now
    twist_wrist {
        relative_to = "lateral_wrist";
        history = 1000;
        update_channel = "TWIST_TO_LWRIST";
        initial_transform {
            translation = [0.06985, 0, 0];
            rpy = [0, -90, 0];    
        }
    }
    gripper {
        relative_to = "twist_wrist";
        history = 1000;
        update_channel = "GRIPPER_TO_TWIST";
        initial_transform {
            translation =[0, 0, -0.04445];
            rpy = [90, 0, 0];
        }
    }
    gripper_base{
        relative_to = "twist_wrist";
        history = 0;
        initial_transform {
            translation =[0.04445, 0, 0];
            rpy = [90, 0, 0];
        }
    }

    gripper_static_endpoint {
        relative_to = "gripper_base";
        history = 0;
        initial_transform {
            translation =[0.111125, 0, 0];
            rpy = [0, 0, 0];
        }

    }
    gripper_dynamic_endpoint {
        relative_to = "gripper";
        history = 0;
        initial_transform {
            translation =[0.111125, 0, 0];
            rpy = [0, 0, 0];
        }

    }
}

models {
    wheelchair {
        wavefront_model = "wheelchair/wavefront/ElectricWheelchairl.obj";
        translate = [-0.4774696935, -0.288993438, 0];
        rotate_xyz = [90, 0, 0];
        scale = 0.001;
    }
    person {
        wavefront_model = "person/walking_person.obj";
        translate = [-2, -0.7, 0];
        rotate_xyz = [90, 0, 0];
        scale = 0.02;
    }
    #haven't been calibrated
    table {
        wavefront_model = "table/table.obj";
        #scale 0.7911, 1.35, 1.28
        #translate 0,0.63, 0
        #rotate 90, 0, 0
        translate = [0,0.63, 0];#-0.63188, 0, 0.390958];
        rotate_xyz = [90, 0, 0];
        scale = 0.02;
        scale_xyz = [0.7911, 1.35, 1.28];
    }
    chair {
        wavefront_model = "chair/Chair.obj";
        #scale 0.0227, 0.0186, 0.0137
        #translate -3.56, 0, -0.86 - this is prob wrong
        #rotate 90, 0, 0
        translate = [-3.56, 0, -0.86];#0, 0, 0];
        rotate_xyz = [90, 0, 0];
        scale = 0.02;
        scale_xyz = [0.0227, 0.0186, 0.0137];
    }
    trashcan {
        wavefront_model = "trashcan/trashcan.obj";
        translate = [-2, -0.7, 0];
        rotate_xyz = [90, 0, 0];
        scale = 0.02;
    }
    tv {
        wavefront_model = "tv/LCD_Widescreen.obj";
        translate = [-2, -0.7, 0];
        rotate_xyz = [90, 0, 0];
        scale = 0.02;
    }
    bed {
        wavefront_model = "bed/bed.obj";
        translate = [-2, -0.7, 0];
        rotate_xyz = [90, 0, 0];
        scale = 0.02;
    }
    lamp {
        wavefront_model = "lamp/lamp.obj";
        translate = [-2, -0.7, 0];      
        rotate_xyz = [90, 0, 0];
        scale = 0.02;
    }
    laptop {
        wavefront_model = "laptop/laptop.obj";
        #scale 0.081, 0.11479, 0.0942
        #translate 0.58, 0.0, 0.86
        #rotate 90, 0, 0
        translate = [0.58, 0.0, 0.86];#-2, -0.7, 0];      
        rotate_xyz = [90, 0, 90];
        scale = 0.02;
        scale_xyz = [0.081, 0.11479, 0.0942];
    }
    pc {
        wavefront_model = "pc/pc.obj";
        translate = [-2, -0.7, 0];      
        rotate_xyz = [90, 0, 0];
        scale = 0.02;
    }
}

calibration{
    velodyne {
        model = "HDL_32E";
        channel = "VELODYNE";
        #    x_vs = [-0.11,0,-1.35,180,-30,0];
        x_vs = [-0.11,0,-1.35,180,0,0];
        x_lr = [0,0,0,0,0,0];
        intrinsic_calib_file = "velodyne/HDL32E_segway_db.xml";
    }

    # Ad-hoc methods used; assumed laser is level, and at body center... we should probably fix the center part
    # Used some manually-measured quantities

    # All in meters
    vehicle_bounds { # Changed width from +/- 0.3 to +/- 0.4
        front_left  = [.5,.4];
        front_right = [.5,-.4];
        rear_left   = [-.5,.4];
        rear_right  = [-.5,-.4];
    }
    
    calibration_to_body {
        position    = [0,0,0];
        orientation = [1,0,0,0];
    }

    imu{
        gyro_biases = [0.019356, 0.021331, 0.012442];
        accel_biases = [0.231766, -0.258528, -9.847225]; 
    }   
}

#coppied over from agile - need to check 
obstacles {
    global_mask_forward = 40.0;
    global_mask_backward = 1.5;
    global_mask_width = 2.3;
    
    # Obstacle "chuncks" are expired if they haven't been seen some number of seconds.
    # The tracker differentiates between chunks that lie within a rectangle around the
    # vehicle (the "persistent mask") and those outside this persistent mask.
    chunk_persist_mask_forward = 3.0; #5.5;     #[m] fwd of body-fixed frame
    chunk_persist_mask_backward = 2.0; #5.5;    #[m] bwd of body-fixed frame
    chunk_persist_mask_width = 5.0; #6.0;       #[m] total width centered along vehicle x-axis
    chunk_persist_mask_expire_sec = 0.6; #0.8;  #[s] time to persist objects within mask if unseen
    chunk_outside_persist_mask_expire_sec = 0.2; #0.4; #[s] time after which chunks outside the mask are expired if unseen
    too_close = 1.7; # [m] Remnant from agile that specifies distance to closest obstacle before bot stops.
}


three_dimensional_lidars {
    VELODYNE {
        coord_frame = "VELODYNE";
        laser_type = "VELODYNE_HDL_32E";
        lcm_channel = "VELODYNE_LIST";
    }
}

planar_lidars {
    SKIRT_FRONT {
        #serial = "H1109797"; #H0902978;
        #device = "192.168.237.90";
	serial = "H1007960"; #902978;	
        viewer_color = [ 0.0, 0.0, 1.0 ]; # blue
        max_range = 30.0;
        min_range = 0.25;
        angle_range = [135, -135];
        #mask = [-2.0, 2.0];
        down_region = [-1, -1];
        up_region = [-1,-1];
        surround_region = [0, 1080];
        verifiers=[];
        frequency = 40;
        flipped = 0;  
        laser_type = "HOKUYO_UTM";
        #added additional parameters 
        fov        = 270;
        resolution = 0.25;
        relative_to = "body";
        coord_frame = "SKIRT_FRONT";
        lcm_channel = "SKIRT_FRONT";
    }

    SKIRT_FULL {
        serial = "H0902000";
        viewer_color = [ 0.0, 1.0, 0.0 ]; # red
        max_range = 30;
        min_range = .25;
        angle_range = [180, -180];
        down_region = [-1, -1];
        up_region = [-1,-1];
        surround_region = [0, 1440];
        verifiers=[];
        frequency = 40;
        flipped = 0;
        laser_type = "HOKUYO_UTM";
        
        fov        = 360;
        resolution = 0.25;
        relative_to = "body";
        coord_frame = "SKIRT_FULL";    
        lcm_channel = "SKIRT_FULL";
    }


    # SKIRT_FRONT_1 {
    #     serial = "";
    #     viewer_color = [ 0.0, 1.0, 1.0 ]; # blue
    #     max_range = 30.0;
    #     min_range = 0.25;
    #     angle_range = [135, -135];
    #     frequency = 40;
    #     flipped = 0;  
    #     laser_type = "HOKUYO_UTM";
    #     #added additional parameters 
    #     fov        = 270;
    #     resolution = 0.25;
    #     relative_to = "body";
    #     coord_frame = "SKIRT_FRONT";
    #     lcm_channel = "SKIRT_FRONT_1";
    # }
    # SKIRT_FRONT_2 {
    #     serial = "";
    #     viewer_color = [ 0.0, 0.5, 1.0 ]; # blue
    #     max_range = 30.0;
    #     min_range = 0.25;
    #     angle_range = [135, -135];
    #     frequency = 40;
    #     flipped = 0;  
    #     laser_type = "HOKUYO_UTM";
    #     #added additional parameters 
    #     fov        = 270;
    #     resolution = 0.25;
    #     relative_to = "body";
    #     coord_frame = "SKIRT_FRONT";
    #     lcm_channel = "SKIRT_FRONT_2";
    # }
    SKIRT_REAR {
        serial = "H0902065";
        viewer_color = [ 1.0, 0.0, 0.0 ]; # red
        max_range = 30;
        min_range = .25;
        angle_range = [135, -135];
        #mask = [-2.0, 2.0];
        down_region = [-1, -1];
        up_region = [-1,-1];
        surround_region = [0, 1080];
        verifiers=[];
        frequency = 40;
        flipped = 0;
        laser_type = "HOKUYO_UTM";
        
    #added additional parameters 
        fov        = 270;
        resolution = 0.25;
        relative_to = "body";
        coord_frame = "SKIRT_REAR";    
        lcm_channel = "SKIRT_REAR";
    }
    TOP_LASER {        
    	serial = "H1109797"; #H0902978;
        device = "192.168.237.90";
        #serial = "H805723"; #902978; #00805723        
        viewer_color = [ 0.0, 0.0, 1.0 ]; # blue
        max_range = 30;
        min_range = .25;
        angle_range = [135, -135];
        #mask = [-2.0, 2.0];
        down_region = [-1, -1];
        up_region = [-1,-1];
        surround_region = [0, 1080];
        verifiers=[];
        frequency = 40;
        flipped = 0;
        laser_type = "HOKUYO_UTM";
        
    #added additional parameters 
        fov        = 270;
        resolution = 0.25;
        relative_to = "body";
        coord_frame = "TOP_LASER";    
        lcm_channel = "TOP_LASER";
    }
    NODDING_LASER {
        serial = "00803545";#"H0902062";#"H1007960"; #902978;
        viewer_color = [ 0.0, 1.0, 0.0 ]; # green
        max_range = 30.0;
        min_range = .25;
        angle_range = [135, -135];
        #mask = [-2.0, 2.0];
        down_region = [-1, -1];
        up_region = [-1,-1];
        surround_region = [0, 1080];
        #surround_region = [100, 1000];
        verifiers=[];
        frequency = 40;
        flipped = 0;
        laser_type = "HOKUYO_UTM";
        #added additional parameters
        fov        = 270;
        resolution = 0.25;
        relative_to = "nodder";
        coord_frame = "NODDING_LASER";
        lcm_channel = "NODDING_LASER";
    }
}

semantic_classification{
        image{
        #relative to the data path 
                model_path = "/model_trainer_model/image_models/all_images_model";
                lcm_channel = "DRAGONFLY_IMAGE";
        }       
        laser{
                model_path = "/model_trainer_model/laser_models/all_model";
        }
}


cameras {
    dragonfly {
        cam_units_xml = "camera/dragonfly_color_jpeg.xml";
        lcm_channel = "DRAGONFLY_IMAGE";
        coord_frame = "dragonfly";
        use_for_detection = 1; # Use for object reacquisition
        
        intrinsic_cal {
            width = 1296;
            height = 964;

            # Updated from 07-02-12 calibration
            # pinhole = [fx, fy, 0, cx, cy];
            # pinhole = [603.53925622, 605.34379842, 0.000, 631.2826309, 506.37125114];
            # distortion_model = "plumb-bob";
            # distortion_k = [ -0.30335195,  0.141782, 0.00057303];
            # distortion_p = [ -0.0005192, -0.04105515];

            # Updated from 11-19-12 calibration
            # pinhole = [fx, fy, 0, cx, cy];
            #pinhole = [708.7098834, 703.2220111, 0.000, 605.98, 502.73];
            #distortion_model = "plumb-bob";
            #distortion_k = [ -0.3139991,  0.1182782, -0.02094001];
            #distortion_p = [ -0.0007667, 0.00137993];

            # Updated from 10-23-13 calibration
            pinhole = [612.486, 614.032, 0.000, 637.539, 426.127];
            distortion_model = "plumb-bob";
            distortion_k = [-0.2462, 0.0634, 0.0011];
            distortion_p = [0.0013, -0.0073];
        }

        streams {
            logging {
                width = 1296;
                height = 964;
                channel = "DRAGONFLY_IMAGE";
                hz = 15;
                jpeg_quality = 90;
            }
        }
    }

    dragonfly_right {
        cam_units_xml = "camera/dragonfly_color_jpeg_right.xml";
        lcm_channel = "DRAGONFLY_IMAGE_RIGHT";
        coord_frame = "dragonfly_right";
        use_for_detection = 1; # Use for object reacquisition
        
        intrinsic_cal {
            width = 1296;
            height = 964;
           
            # Updated from 10-23-13 calibration
            pinhole = [612.486, 614.032, 0.000, 637.539, 426.127];
            distortion_model = "plumb-bob";
            distortion_k = [-0.2462, 0.0634, 0.0011];
            distortion_p = [0.0013, -0.0073];
        }

        streams {
            logging {
                width = 1296;
                height = 964;
                channel = "DRAGONFLY_IMAGE_RIGHT";
                hz = 15;
                jpeg_quality = 90;
            }
        }
    }

    dragonfly_left {
        cam_units_xml = "camera/dragonfly_color_jpeg_left.xml";
        lcm_channel = "DRAGONFLY_IMAGE_LEFT";
        coord_frame = "dragonfly_left";
        use_for_detection = 1; # Use for object reacquisition
        
        intrinsic_cal {
            width = 1296;
            height = 964;
           
            # Updated from 10-23-13 calibration
            pinhole = [612.486, 614.032, 0.000, 637.539, 426.127];
            distortion_model = "plumb-bob";
            distortion_k = [-0.2462, 0.0634, 0.0011];
            distortion_p = [0.0013, -0.0073];
        }

        streams {
            logging {
                width = 1296;
                height = 964;
                channel = "DRAGONFLY_IMAGE_LEFT";
                hz = 15;
                jpeg_quality = 90;
            }
        }
    }

    dragonfly_log {
        cam_units_xml = "camera/dragonfly_gray_jpeg.xml";
        lcm_channel = "DRAGONFLY_IMAGE_LOG";
        coord_frame = "dragonfly";
        use_for_detection = 1; # Use for object reacquisition
        
        intrinsic_cal {
            width = 1296;
            height = 964;
            # Updated from 07-02-12 calibration
            # pinhole = [fx, fy, 0, cx, cy];
            pinhole = [603.53925622, 605.34379842, 0.000, 631.2826309, 506.37125114];
            distortion_model = "plumb-bob";
            distortion_k = [ -0.30335195,  0.141782, 0.00057303];
            distortion_p = [ -0.0005192, -0.04105515];
        }

        logging {
            width = 640;
            height = 480;
            channel = "DRAGONFLY_IMAGE_LOG";
            hz = 5;
            jpeg_quality = 90;
        }
    }

    dragonfly_log {
        cam_units_xml = "camera/dragonfly_gray_jpeg.xml";
        lcm_channel = "DRAGONFLY_IMAGE_SMALL";
        coord_frame = "dragonfly";
        use_for_detection = 1; # Use for object reacquisition
        
        intrinsic_cal {
            width = 1296;
            height = 964;
            # Updated from 07-02-12 calibration
            # pinhole = [fx, fy, 0, cx, cy];
            pinhole = [603.53925622, 605.34379842, 0.000, 631.2826309, 506.37125114];
            distortion_model = "plumb-bob";
            distortion_k = [ -0.30335195,  0.141782, 0.00057303];
            distortion_p = [ -0.0005192, -0.04105515];
        }

        logging {
            width = 640;
            height = 480;
            channel = "DRAGONFLY_IMAGE_SMALL";
            hz = 5;
            jpeg_quality = 90;
        }
    }
}


base{
    wheelbase        = 0.58;
    use_hardware_integrator = 1;
    motion_timeout         = 1.0;
}


localizer{
    mean_c_d    = -0.0182;
    mean_c_t    = -0.105;
    std_dev_c_d = 0.0;
    std_dev_c_t = 0.0612;

    mean_d_d    = 1.0065;
    mean_d_t    = -0.0072;
    std_dev_d_d = 0.0932;
    std_dev_d_t = 0.0000;

    mean_t_d    = 0.0144;
    mean_t_t    = 0.8996;
    std_dev_t_d = 0.00;
    std_dev_t_t = 0.3699;

    num_particles   = 1000;#5000
    laser_max_range = 30.0;
    use_rear_laser  = false; #off
    tracking_beam_minlikelihood = 0.45;
    global_beam_minlikelihood   = 0.9;

    odom_a1 = 0.2;    # a1 = error in rotation
                      # as a function of rotation
    odom_a2 = 0.01;    # a2 = error in rotation
                    # as a function of translation
    odom_a3 = 0.2;    # a3 = error in translation
                                        # as a function of translation
    odom_a4 = 0.01;

    robot_particles  = 1000;#5000            # number of samples
    use_front_laser  = false; #off
    use_laser1       = true;#on
    use_laser2       = false;#  off
    use_laser3       = false;
    use_laser4       = false;
    use_laser5       = false;
    min_wall_prob    = 0.5; #0.5;   #was 0.25 Sachi changed ZMay 20
    outlier_fraction = 0.85;
    #might need to decrease these values
    update_distance  = 0.20; #was 0.2
    update_heading   = 0.20;
    ## integrate a beam each n rads (new version of laser_skip)
    integrate_angle  = 0.15; ## 0.052359   ## 3 degrees
    integrate_angle_deg  = 3.0;
    do_scanmatching  = false; #off    #was off
    constrain_to_map = false; #off    #was off
    occupied_prob    = 0.5;
    lmap_std         = 0.3;
    global_lmap_std  = 0.6;
    global_evidence_weight = 0.01;
    global_distance_threshold = 2.0;
    global_test_samples = 1000000; #was 100000
    use_sensor          = true; 
    tracking_beam_minlikelihood = 0.45;
    global_beam_minlikelihood = 0.9;    
}


simulator{
    simulate_frontlaser = 0;
    simulate_rearlaser  = 0;
    dt                  = 0.025;
    time                = 0.025;
    sync_mode           = 0; #0/1 
    use_robot           = 0; #was called off - make sure compatible
    acceleration        = 1.0;#0.5;
    rot_acceleration    = 5.0;#2.0;

    person_leg_width    = 0.1;
    person_dist_from_robot = 0.4;
    person_speed        = 0.3;

    laser_probability_of_random_max     = .0001;
    laser_probability_of_random_reading = .0001;
    laser_sensor_variance               = .0001;
}

robot{
    length     = 1.09;
    width      = 0.65;        # 0.58 was used ***consider changin this 
    wheel_base = 1.486;         # front to back distance, value taken from agile, needs to be changed
    odometry_inverted  = 0; 
    laser1_offset      = 0.44; #these things are repeated - probably can use the laser as the source
    frontlaser_offset  = 0.44; 
    frontlaser_flipped = 0;    

    frontlaser_use            = 1;
    frontlaser_side_offset    = 0.0;
    frontlaser_angular_offset = 0.0;  #-0.785398163#45#0.0  #sachi added to new carmen
    frontlaser_id             = 1;    #sachi added

    rearlaser_offset      = -0.42;
    laser2_offset         = -0.42;      
    rearlaser_side_offset = 0.0;
    rearlaser_flipped     = 0;
    rearlaser_angular_offset = -3.1415926535897931;
    rearlaser_use            = 1;
    rearlaser_id             = 2;    #sachi added

    frontlaser_ignore_first_n_beams = 0;  # avoid self-occlusion
    frontlaser_ignore_last_n_beams  = 0; #not sure if these are used

    rearlaser_ignore_first_n_beams = 10;   # avoid self-occlusion
    rearlaser_ignore_last_n_beams  = 10;    # avoid self-occlusion
    
    min_approach_dist = 0.4; #was 0.1 sachi changed to 0.2
    min_side_dist     = 0.1;  #sachi changed
    acceleration      = 0.3; #was 0.2 - increased for person following         # m/s^2    sachi 0.3
    deceleration      = 0.3; #was 0.2 - increased for person following         # m/s^2    sac 0.3
    reaction_time     = 0.1;
    ##The following gain values were tested and found to work adquetly for 40% power on the wheelchair 
    sensor_timeout     = 3.0;

    max_t_vel          = 1.5; #  - this is the value for best person following#1.0#1.5 #m/s #increased for person following - not sure if this will screw with the dynamics
    max_r_vel          = 5.0;# - the value used for best person following#rad/s 
    theta_gain         = 4.0;#2.0#not used for velocity commands
    theta_d_gain       = 2.0;#not used for velocity commands
    displacement_gain  = 4.0;#2.0#5.0
    allow_rear_motion  = 0; #off#on
    rectangular        = 0;
    use_sonar          = 0;
    use_laser          = 1;        
    use_laser1         = 1;
    use_laser2         = 0;
    use_laser3         = 0;
    use_laser4         = 0;
    use_laser5         = 0;
    use_front_laser    = 0;
    use_back_laser     = 0;
    collision_avoidance = 0;
}

navigator{
    goal_size            = 0.2;
    goal_theta_tolerance = 1.5;
    ignore_new_goals_until_completed = 0;
    waypoint_tolerance   = 0.3;
    
    min_approach_dist = 0.3;
    min_side_dist     = 0.1;

    robot_length      = .75;
    robot_width       = 0.5;#.75;
    

    replan_frequency  = 5;
    smooth_path       = true;#false;#true;
    plan_to_nearest_free_point = true;
}


motion_planner {

    gridmap {
        convolve_line_searches = 10; #was 4 - should retry 4        # (an integer)                                          
        failsafe_fudge = 0.0;              # extra dilation = (2-failsafe_level) x failsafe_fudge
    }

    rrtstar {
        iteration_limit = 50000000;    
        time_limit = 0;
        tree_pub_iteration_limit = 100; #how often the tree is published 
        tree_pub_time_limit = 0;
        tree_pub_final = true;
        traj_pub_iteration_limit = 500;
        traj_pub_time_limit = 0;
        traj_pub_final = true;
    }

    speed_design {
        default_tv = 0.5;
        max_tv = 0.5;
        max_rv = 0.7; # 40 deg/s
    }
}

# WARNING: planar lidar configuration not valid (copied from LR3 to test
#          sim_planar_lidar)
# this block stores all planar lidar config params except for calibration




state_estimator {
    #parameters for when the filter is used in absolute measurement mode
    absolute_filter{
#measurement noise 
        xy_sm =  1177.968638903253350;         
        z_sm  =  0.000152362059262;         
        yaw_sm=  0.000001074166515;         
        pr_sm =  100000.000000000000000;         
        
        xy_vo =  1.000000000000000;         
        z_vo  =  1.000000000000000;         
        yaw_vo=  1.000000000000000;         
        pr_vo =  1.000000000000000;         
        
        pr_imu =  0.000000010000000;        
        xydd_imu =  1.393918961009140;        
        zdd_imu =  0.000069671517062;        
        
        
#process noise 
        xy =  0.031834588576815;            
        z =  0.000717813481170;             
        yaw =  83.134088763758839;           
        pr =  0.000500000000000;            
        
        xyd =  41.438859707784871;           
        zd =  0.006335179080745;            
        yawd =  0.386131813529540;            
        
        xydd =  1.339597152090030;           
        zdd =  2.004871488556871;            
        
        imubias =  0.000000201108050;   
        
        
    }
    
    
    #parameters for when the filter is used in relative measurement mode
    relative_filter{
#measurement noise 
        xy_sm =  5021.551995469418216;         
        z_sm  =  0.000058245081814;         
        yaw_sm=  0.000000516583161;         
        pr_sm =  100000.000000000000000;         
        
        xy_vo =  1.000000000000000;         
        z_vo  =  1.000000000000000;         
        yaw_vo=  1.000000000000000;         
        pr_vo =  1.000000000000000;         
        
        pr_imu =  0.000000010000000;        
        xydd_imu =  1.882103815321574;        
        zdd_imu =  0.000183566428530;        
        
        
#process noise 
        xy =  1.684954921932901;            
        z =  0.001081906770113;             
        yaw =  2095.702310036921517;           
        pr =  0.000500000000000;            
        
        xyd =  37.927617082981584;           
        zd =  0.016343833392782;            
        yawd =  0.227919799318317;            
        
        xydd =  0.817997360583666;           
        zdd =  2.741147170501390;            
        
        imubias =  0.000000010000000;      
        
    }
}

#obstacle avoidance:
obstacle_avoidance{
    distToWallAvoidThresh =      1;
    distToWallStopThresh =        .7;
}



scanmatcher{
    beam_skip          = 3;
    spatial_decimation = .05;
    metersPerPixel     = .03;
    thetaResolution    = .015;

    addScanHitThresh   = .80;
    maxNumScans        = 10;
    maxAttitudeForAdd  = .12;

    initialSearchRangeXY    = .12;
    initialSearchRangeTheta = .06;
    maxSearchRangeXY        = .8;
    maxSearchRangeTheta     = .2;
    useGradientAscentPolish = 1;
    useMultiRes             = 3;
}

person_tracking{
    no_particles = 400;    
}

arm_renderer {
    base{
        servo_index = 0;
        independent_variable = "yaw";
        min_range = -2.61;
        max_range = 2.61;
        speed = 100;
        torque_enable = 1;
        torque = 200;
    }
    shoulder{
        servo_index = 1;
        servo_coupled = 1;
        servo_coupled_invert = 1;
        servo_coupled_index = 2;
        independent_variable = "yaw";
        min_range = -1.0;
        max_range = 1.5; #this value can go higher, but was set to be perpendicular to the ground for testing
    }
    elbow{
        servo_index = 3;
        independent_variable = "yaw";
        min_range = -1.9;
        max_range = 2.6;
    }
    lateral_wrist {
        servo_index = 4;
        independent_variable = "yaw";
        min_range = -1.8;
        max_range = 1.8;
    }
    gripper{
        servo_index = 5;
        independent_variable = "yaw";
        min_range = 0.3;
        max_range = 1.2;
    }
}

arm_config{
    num_joints = 6; 
    num_servos = 7;                
    joints{
        base{    
            joint_index = 0;
            servos{
                servo_1{
                    servo_index = 0;
                    min_range = 64;
                    max_range = 736;
                    offset  = 541;
                    sign = 1;
                    use_for_frame = 1;
                }
            }
            variable_ind = 2; #which ind of rpy should come from the servo
            
            deg_to_ticks = 3.5;
        #ticks = degree*deg_to_ticks + offset
            joint_name = "base_yaw";
            joint_frame = "base_yaw";
            relative_to = "base";
            seg_dim = [0.06, 0.025, 0.025]; #{x, y, z} in meters
        }
        shoulder{    
            joint_index = 1;
            servos{
                servo_1{
                    servo_index = 1;
                    min_range = 55;
                    max_range = 672;
                    offset = 198;
                    sign = 1;
                    use_for_frame = 1;
                }
                servo_2{
                    servo_index = 2;
                                #for servo 2
                    min_range = 339;
                    max_range = 958;
                    offset = 813;
                    sign = -1; 
                    use_for_frame = 0;
                }
            }                     
            
            deg_to_ticks = 3.5;        
            joint_frame = "shoulder";   
            relative_to = "base_yaw";
            variable_ind = 2;
            joint_name = "shoulder";
            seg_dim = [0.4, 0.025, 0.025]; #x, y, z} in meters
        }
        elbow{    
            joint_index = 2;
            servos{
                servo_1{
                    servo_index = 3;
                    min_range = 34;
                    max_range = 954;
                    sign = 1;
                    offset  = 510;
                    use_for_frame = 1;
                }
            }
            variable_ind = 2;
            deg_to_ticks = 3.5; #negative direction of servo movement is positive yaw
        #ticks = degree*deg_to_ticks + offset
            joint_frame = "elbow";
            joint_name = "elbow";
            relative_to = "shoulder";
            seg_dim = [0.4, 0.017, 0.017]; #{x, y, z} in meters
        }
        lateral_wrist{
            joint_index = 3;
            servos{     
                servo_1{
                    servo_index = 4;
                    min_range = 160;
                    max_range = 872;
                    offset  = 513;
                    sign = 1;
                    use_for_frame = 1;
                }
            }
            deg_to_ticks = 3.5; 
            variable_ind = 2;
        #ticks = degree*deg_to_ticks + offset
            joint_frame = "lateral_wrist";
            joint_name = "lateral_wrist";
            relative_to = "elbow";
            seg_dim = [0.08, 0.017, 0.017]; #{x, y, z} in meters
        }
        twist_wrist{
            joint_index = 4;
            servos{
                servo_1{
                    servo_index = 6;
                    min_range = 5;
                    max_range = 1020;
                    offset  = 515;
                    sign = -1;
                    use_for_frame = 1;
                }
            }
                #this should change 
            variable_ind = 2;

            deg_to_ticks = 3.5; 
        #ticks = degree*deg_to_ticks + offset
            joint_name = "twist_wrist";
            joint_frame = "twist_wrist";   
            relative_to = "lateral_wrist";  
            #seg_dim = [0.08, 0.017, 0.017]; #{x, y, z} in meters
            seg_dim = [0.017, 0.017, -0.08]; #{x, y, z} in meters in joint frame
        }
        gripper{
            joint_index = 5;
            servos{
                servos_1{
                    servo_index = 5;
                    min_range = 444;
                    max_range = 730;
                    offset  = 572;
                    sign = 1;
                    use_for_frame = 1;
                }
            }
            variable_ind = 2;
            deg_to_ticks = 3.5; 
        #ticks = degree*deg_to_ticks + offset
            joint_name = "gripper";
            joint_frame = "gripper";
            relative_to = "twist_wrist";  
            #seg_dim = [0.09, 0.002, 0.015]; #{x, y, z} in meters
            seg_dim = [0.002, -0.09, 0.015]; #{x, y, z} in meters in joint frame
        }
    }        
}

rndf = "/Users/mwalter/research/code/agile/software/config/rndf/Lee/Lee_RNDF_demo.txt";
